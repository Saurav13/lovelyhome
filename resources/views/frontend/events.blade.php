@extends('layouts.app')
@section('title','| Events')
@section('body')
<style>
    #index_timer .time span{
        bottom: 0px;
    }
    #index_timer .time{
        margin: 20px 10px;
    }
</style>

@if(count($events)==null)
<section id="events_header" class="events_header" data-parallax="scroll" data-bleed="200" data-z-index="-100" data-speed="0.1" data-image-src="/frontend-assets/img/events/events_bg.jpg" data-over-scroll-fix="true">
    <div class="overlay_opacity" ></div>
    <div class="container front_index">
        <div class="row">
            <div class="col-md-12 t_white">
                <ol class="breadcrumb">
                    <li><a href="{{route('home')}}">Home</a></li>
                    <li class="active"><a href="route('events')">Events</a></li>
                </ol>
            </div>
        </div>
       
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <h2 class="section-title">Currently there are no events to display!</h2>
                <br>
                <br>
                <div class="events_button">
                    <ul>
                        <li id="past"><a href="{{route('pastevents')}}">Past Events</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
       
    </div>
</section>
<br>
<br>
<br>
<br>
@else
<section id="events_header" class="events_header" data-parallax="scroll" data-bleed="200" data-z-index="-100" data-speed="0.1" data-image-src="img/events/events_bg.jpg" data-over-scroll-fix="true">
    <div class="overlay_opacity" @foreach($mainevent as $event)
    style="background:url({{asset('events-images'.'/'.$event->featured_image)}}) no-repeat center"
    @endforeach @if(count($mainevent)==null)  style="background:url({{asset('frontend-assets/img/footer/rainbowbg.png')}})" @endif></div>
    <div class="events_wave"></div>
    <div class="container front_index">
        <div class="row">
            <div class="col-md-12 t_white">
                <ol class="breadcrumb">
                    <li><a href="{{route('home')}}">Home</a></li>
                    <li class="active"><a href="route('events')">Events</a></li>
                </ol>
            </div>
        </div>
        @foreach($mainevent as $event)
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <h2 class="section-title">{{$event->title}}</h2>
                <input type="hidden" id="timerdate" value="{{ $event->event_date. ' '. $event->start_time }}">
                <div id="index_timer" class="index_timer section-title"></div>
                <br>
                <br>
                <div class="events_button">
                    <ul>
                        <li class="active"><a href="{{route('events')}}">Upcoming Events</a></li>
                        <li id="past"><a href="{{route('pastevents')}}">Past Events</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
        @endforeach
    </div>
</section>
<section id="events_grids" class="events_grids" style="padding-top: 30px;">
    <div class="container">
        <div class="triple_grids">
            <div class="row">
                @foreach($events as $event)
                <div class="col-md-4 wow fadeInUp">
                    <div class="grids_block">
                        <div class="grids_block_top">
                            <img src="{{asset('events-images'.'/'.$event->featured_image)}}" alt="{{$event->title}}">
                        </div>
                        <div class="grids_block_center">
                            <div class="date_events_left b_green_light_light">
                                <div class="date_middle">
                                    <p class="pb_10"><span>{{date('j',strtotime($event->event_date))}}</span></p>
                                    <p>{{date('M, Y',strtotime($event->event_date))}}</p>
                                </div>
                            </div>
                            <div class="date_events_right b_green_green_light">
                                <p class="block_time pb_10"><i class="fa fa-clock-o"><span></span></i>{{date('h:i A', strtotime($event->start_time))}} - {{date('h:i A', strtotime($event->end_time))}}</p>
                                <p class="block_time normal"><i class="fa fa-map-marker"><span></span></i>{{$event->venue}}</p>
                            </div>
                        </div>
                        <div class="grids_brief">
                            <h4 class="grids_title t_green_green_light"><a href="{{route('single.event',$event->slug)}}">{{$event->title}}</a></h4>
                            <p class="grids_description t_grey">{{strip_tags(str_limit($event->description, $limit = 300, $end = '...'))}}</p>
                        </div>
                    </div>
                </div>
                @endforeach                                      
            </div>
            <br>
            <div class="text-center">
                    {!!$events->links()!!}
                </div>	
        </div>
    </div>
</section>
@endif
<br>
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<script>
    $(document).ready(function(){
        $('#past').mouseenter(function(){
            $('#past').addClass('active');
        })
        $('#past').mouseleave(function(){
            $('#past').removeClass('active');
        });
    });
</script>
<script>
$(document).ready(function(){
    var timerdate=$('#timerdate').val(); 

    $("#index_timer").countdown(timerdate, function(event) { /* change date */
      $(this).html(
          event.strftime(' <div class="card" style="color: #fffefe;background: #bbbbbbad;" ><div class="time">%D<span class="label" >days</span></div>' 
          + '<div class="time">%H<span class="label" >hours</span></div>'
          + '<div class="time dots">%M<span class="label" >minutes</span></div>'
          + '<div class="time dots">%S<span class="label" >seconds</span></div></div>')
          );
      });

  });
  
  </script>
@stop