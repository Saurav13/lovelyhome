@if($ano == 0)
    <p class="notification-text font-small-3 text-muted text-xs-center" style="margin:20px">No new requests</p>
@endif
@foreach($alreqs as $f)
    <?php 
        $interval = \Carbon\Carbon::createFromTimeStamp(strtotime($f->created_at))->diffForHumans();
    ?>
    <a href="{{ route('alumni_requests.show',$f->id) }}" class="list-group-item">
    <div class="media">
        <div class="media-left"><span class="avatar avatar-sm avatar-online rounded-circle"><img src="{{ asset('alumni_pp/'.$f->photo) }}" alt="avatar"><i></i></span></div>
        <div class="media-body">
        <h6 class="media-heading">{{ $f->name }}</h6>
        <p class="notification-text font-small-3 text-muted" style="line-height:1.2em">requested an alumni account.</p><small>
            <time class="media-meta text-muted">{{ $interval }}</time></small>
        </div>
    </div>
    </a>
@endforeach