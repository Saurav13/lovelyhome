<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('test',function(){
	factory(App\AlumniPhoto::class, 20)->create();
});


Route::get('/gallery','Frontend\GalleryController@gallery');
Route::get('/gallery/getmorealbums','Frontend\GalleryController@getmorealbums')->name('albums.getmore');

Route::get('/gallery/{slug}/getmoreimages','Frontend\GalleryController@getmoreimages')->name('images.getmore');
Route::get('/gallery/{category}/{slug}','Frontend\GalleryController@images')->name('frontend.album');

Route::get('calendar','Frontend\HomeController@calender')->name('calender');

Route::post('contact_us', 'Frontend\HomeController@contact_us')->name('contact_us');
Route::post('feedback', 'Frontend\HomeController@feedback');
Route::post('subscribe', 'Frontend\HomeController@subscribe');
Route::get('unsubscribe/{token}', 'Frontend\HomeController@unsubscribe')->name('unsubscribe');
Route::post('unsubscribe/{token}', 'Frontend\HomeController@unsubscribed')->name('unsubscribed');

Route::prefix('alumni')->group(function(){
	Route::post('/login','Frontend\Alumni\Auth\LoginController@login')->name('alumni_login');
	Route::post('/logout','Frontend\Alumni\Auth\LoginController@logout')->name('alumni_logout');
	Route::post('/register','Frontend\Alumni\Auth\RegisterController@register')->name('alumni_register');
	Route::post('/password/email','Frontend\Alumni\Auth\ForgotPasswordController@sendResetLinkEmail')->name('alumni_password.email');
	Route::post('/password/reset','Frontend\Alumni\Auth\ResetPasswordController@reset');
	Route::get('/password/reset','Frontend\Alumni\Auth\ForgotPasswordController@showLinkRequestForm')->name('alumni_password.request');
	Route::get('/password/reset/{token}','Frontend\Alumni\Auth\ResetPasswordController@showResetForm')->name('alumni_password.reset');
	
	Route::get('gallery','Frontend\Alumni\AlumniController@gallery')->name('alumni.gallery');
	Route::get('/gallery/filterBatch','Frontend\Alumni\AlumniController@filterBatch')->name('alumni_photos.filterBatch');

	Route::get('/post/{slug}','Frontend\Alumni\AlumniController@singlePost')->name('alumni.singlepost');
	Route::post('post/{id}/comment','Frontend\Alumni\AlumniController@comment')->name('post.comment');
	Route::post('post/{post_id}/getmorecomments','Frontend\Alumni\AlumniController@getMoreComments')->name('post.getmorecomments');
	
	Route::get('blog','Frontend\Alumni\AlumniController@index')->name('alumni.blog');
	Route::get('blog/getmoreposts','Frontend\Alumni\AlumniController@getMorePosts')->name('alumni.blog.getmoreposts');

	Route::get('myposts','Frontend\Alumni\AlumniController@myposts')->name('alumni.myposts');
	Route::get('myposts/getmoreposts','Frontend\Alumni\AlumniController@getMoreMyPosts')->name('alumni.myposts.getmoreposts');
	Route::post('myposts/add','Frontend\Alumni\AlumniController@addPost')->name('alumni.myposts.add');
	Route::post('myposts/{id}/edit','Frontend\Alumni\AlumniController@editPost')->name('alumni.myposts.edit');
	Route::post('myposts/{id}/delete','Frontend\Alumni\AlumniController@deletePost')->name('alumni.myposts.delete');

	Route::get('/','Frontend\Alumni\AlumniController@index')->name('alumni');
});

Route::get('changepassword','AdminUserController@changepassword')->name('admin.changepassword');
Route::post('changedpassword','AdminUserController@changedpassword')->name('admin.changedpassword');

Route::prefix('admin')->group(function(){
	Auth::routes();
	Route::get('dashboard','Admin\AdminController@index')->name('dashboard');

	Route::get('staffs','Admin\StaffController@index')->name('staffs.index');
    Route::post('staffs/update','Admin\StaffController@update')->name('staffs.update');    
    Route::post('staffs/delete/{id}','Admin\StaffController@destroy')->name('staffs.destroy');
    Route::post('verifyPassword','Admin\StaffController@verifyPassword')->name('verifyPassword');
    
    Route::get('profile','Admin\ProfileController@index')->name('profile');
    Route::post('profile/changePassword','Admin\ProfileController@changePassword')->name('profile.changePassword');
    Route::post('profile/changeName','Admin\ProfileController@updateName')->name('profile.updateName');

	Route::get('notice','NoticeController@index');
	Route::get('notice/search','NoticeController@search');
	Route::get('notice/create','NoticeController@create');
	Route::post('notice/create','NoticeController@store');
	Route::post('notice/changeStatus','NoticeController@changeStatus');
	Route::get('/notice/{notice}/edit','NoticeController@edit');
	Route::post('/notice/{notice}/update','NoticeController@update');
	Route::post('/notice/delete','NoticeController@destroy');

	Route::resource('/resources', 'ResourcesController');
	Route::post('/resources/update','ResourcesController@update');
	Route::post('/resources/delete','ResourcesController@destroy');

	//Route::resource('/materials', 'MaterialsController');
	Route::get('materials/create','ResourcesController@matcreate');
	Route::get('materials/{material}/edit','ResourcesController@matedit');
	Route::get('materials/{material}/matshow','ResourcesController@matshow');
	Route::post('materials/create','ResourcesController@matstore');
	Route::post('/materials/update','ResourcesController@matupdate');
	Route::post('/materials/delete','ResourcesController@matdestroy');
	Route::post('materials/matchangeStatus','ResourcesController@matchangeStatus');

	Route::post('/gallery/{album_id}/imageStore','GalleryController@imageStore')->name('gallery.addImages');
	Route::delete('/gallery/{album_id}/imageDelete/{image_id}','GalleryController@imageDelete')->name('gallery.deleteImage');
	Route::resource('gallery',"GalleryController",['except' => ['create','edit']]);	

	//downloads
	Route::get('/downloads','DownloadsController@index')->name('downloads.index');
	Route::post('/downloads','DownloadsController@store')->name('downloads.store');
	Route::post('/downloads/delete','DownloadsController@destroy')->name('downloads.destroy');
	Route::get('/downloads/search','DownloadsController@search');
	Route::post('/downloads/update','DownloadsController@update')->name('downloads.update');

	///news
	// Route::get('/news','NewsController@index')->name('admin.news');
	// Route::post('news','NewsController@store')->name('news.store');
	// Route::post('news/delete','NewsController@destroy')->name('news.delete');
	// Route::get('news/{id}/edit','NewsController@edit')->name('news.edit');
	// Route::post('news/update','NewsController@update')->name('news.update');
	// Route::get('news/{id}/view','NewsController@show')->name('news.show');

	//testimonials
	Route::get('/testimonials','TestimonialsController@index')->name('admin.testimonials');
	Route::post('/testimonials','TestimonialsController@store')->name('testimonials.store');
	Route::post('testimonials/delete','TestimonialsController@destroy')->name('testimonials.destroy');
	Route::get('/testimonials/{id}/edit','TestimonialsController@edit')->name('testimonials.edit');
	Route::post('testimonials/update','TestimonialsController@update')->name('testimonials.update');
	
	//sliderimage backend
	// Route::get('slider','SliderImageController@index')->name('admin.slider');
	// Route::post('slider','SliderImageController@store')->name('slider.store');
	// Route::post('slider/delete','SliderImageController@destroy')->name('slider.destroy');
	// Route::post('slider/update','SliderImageController@update')->name('slider.update');

	//Teachers configuration 
	Route::get('teachers','TeacherController@index')->name('admin.teachers');
	Route::post('teachers','TeacherController@store')->name('teachers.store');
	Route::get('teachers/{id}/edit','TeacherController@edit')->name('teachers.edit');
	Route::post('teachers/update','TeacherController@update')->name('teachers.update');
	Route::post('teachers/delete','TeacherController@destroy')->name('teachers.destroy');

	//downloadscategory routes
	Route::post('downloadscategory','DownloadsController@storeCat')->name('downloadscategory.store');
	Route::post('downloadscategory/update','DownloadsController@updatecat')->name('downloadscategory.update');
	Route::post('downloadscategory/delete','DownloadsController@deletecat')->name('downloadscategory.delete');


	//events routes
	Route::get('events','EventController@index')->name('admin.events');
	Route::post('events','EventController@store')->name('events.store');
	Route::post('events/delete','EventController@destroy')->name('events.destroy');
	Route::get('events/{id}/edit','EventController@edit')->name('events.edit');
	Route::post('events/update','EventController@update')->name('events.update');
	Route::get('events/{id}/view','EventController@view')->name('events.view');
	Route::get('events/{id}/images','EventController@images')->name('events.images.index');
	Route::post('events/{id}/images/upload','EventController@UploadImages')->name('events.images.upload');
	Route::delete('events/{event_id}/images/{image_id}','EventController@imageDelete')->name('events.images.delete');

	Route::resource('calender', 'Admin\EventCalenderController');

	Route::get('contact-us-messages/unseen','Admin\ContactController@unseenMsg')->name('contact-us-messages.unseen');    
    Route::resource('contact-us-messages', 'Admin\ContactController',['only' => ['show','destroy','index']]);
    Route::get('getUnseenMsgCount','Admin\ContactController@getUnseenMsgCount')->name('getUnseenMsgCount');
    Route::get('getUnseenMsg','Admin\ContactController@getUnseenMsg')->name('getUnseenMsg');

    Route::get('feedbacks/unseen','Admin\FeedbackController@unseenMsg')->name('feedbacks.unseen');  
    Route::resource('feedbacks', 'Admin\FeedbackController',['only' => ['show','destroy','index']]);
    Route::get('getUnseenFeedbackCount','Admin\FeedbackController@getUnseenFeedbackCount')->name('getUnseenFeedbackCount');
    Route::get('getUnseenFeedback','Admin\FeedbackController@getUnseenFeedbackMsg')->name('getUnseenFeedbackMsg');

	//faqs
	Route::get('faqs','FaqController@index')->name('admin.faqs');
	Route::post('faqs','FaqController@store')->name('faqs.store');
	Route::post('faqs/delete','FaqController@destroy')->name('faqs.delete');
	Route::post('faqs/update','FaqController@update')->name('faqs.update');

	//activities
	Route::get('activities','ActivitiesController@index')->name('admin.activities');
	Route::post('activities','ActivitiesController@store')->name('activities.store');
	Route::post('activities/update','ActivitiesController@update')->name('activities.update');
	Route::post('activities/delete','ActivitiesController@destroy')->name('activities.destroy');


	Route::get('advertises','AdvertiseController@index')->name('admin.advertises');
	Route::post('advertises','AdvertiseController@store')->name('advertises.store');
	Route::post('advertises/update','AdvertiseController@update')->name('advertises.update');
	Route::post('advertises/delete','AdvertiseController@destroy')->name('advertises.destroy');

	Route::get('newsletter','Admin\NewsletterController@newsletter')->name('newsletter');
	Route::post('newsletter/send','Admin\NewsletterController@newsletterSend')->name('newsletterSend');
	
	Route::post('alumni_requests/{id}/verify','Admin\AlumniRequestController@verify')->name('alumni_requests.verify');    
    Route::resource('alumni_requests', 'Admin\AlumniRequestController',['only' => ['show','destroy','index']]);
    Route::get('getUnseenAlReqCount','Admin\AlumniRequestController@getUnseenAlReqCount')->name('getUnseenAlReqCount');
	Route::get('getUnseenAlReq','Admin\AlumniRequestController@getUnseenAlReq')->name('getUnseenAlReq');
	
	Route::resource('alumni_photos', 'Admin\AlumniPhotoController',['except' => ['show','create']]);

});

//frontend routes
Route::get('/', 'Frontend\HomeController@home')->name('home');
Route::get('/jawalakhel', 'Frontend\HomeController@home')->name('home');
//events
Route::get('/events','Frontend\HomeController@events')->name('events');
Route::get('/pastevents','Frontend\HomeController@pastevents')->name('pastevents');
Route::get('events/{slug}','Frontend\HomeController@SingleEvent')->name('single.event');

//news
// Route::get('/news','Frontend\HomeController@news')->name('news');
// Route::get('news/{slug}','Frontend\HomeController@SingleNews')->name('single.news');

//download
Route::get('/download','Frontend\HomeController@downloads')->name('download');

//resources
Route::get('/resources','Frontend\HomeController@resources')->name('resources');
Route::get('resources/{slug}/{id}','Frontend\HomeController@MaterialsSort')->name('sort.materials');


Route::get('/assets/{source}/{img}/{h}/{w}',function($source,$img, $h, $w){
    return \Image::make(public_path("/".$source."/".$img))->resize($h, $w)->response('jpg');
})->name('asset');
//notice
Route::get('/notices','Frontend\HomeController@notice')->name('notices');
Route::get('/faqs','Frontend\HomeController@faqs');
