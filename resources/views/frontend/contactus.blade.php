@extends('layouts.app')

@section('body')
@section('title','| Contact')
<section id="contact_page" class="contact_page">
    <div class="container">
        <div class="row pb_50">
            <div class="col-md-1"></div>
            <div class="col-md-10 dot">
                <h2 class="section-title">Contact Us</h2>
            </div>
            <div class="col-md-1"></div>
        </div>
        <div class="row">
            <div class="col-md-12 contact_link">
                <p class="contact-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                <div class="row pb_50">
                    <div class="col-md-6 col-sm-6 contact_block">
                        <span class="contact_icon"><i class="fa fa-phone"></i></span>
                        <ul>
                            <li class="title">Phone</li>
                            <li><span>tel: </span>888-258-1669</li>
                            <li><span>tel: </span>888-258-1669</li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-sm-6 contact_block">
                        <span class="contact_icon"><i class="fa fa-envelope"></i></span>
                        <ul>
                            <li class="title">Email</li>
                            <li><a href="#">event@yeahkids.com</a></li>
                            <li><a href="#">office@yeahkids.com</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 contact_block">
                        <span class="contact_icon"><i class="fa fa-share-alt"></i></span>
                        <ul class="social_icon">
                            <li class="title">Follow Us</li>
                            <li class="b_blue_dark_social"><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li class="b_blue_social"><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li class="b_red_social"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li class="b_linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li class="b_red_social"><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-sm-6 contact_block">
                        <span class="contact_icon"><i class="fa fa-map-marker"></i></span>
                        <ul>
                            <li class="title">Location</li>
                            <li><p>Anderson Blvd PT Box 4355444, Tampa, FR 954816</p></li>
                        </ul>
                    </div>
                </div>
            </div>
          
        </div>
        <div class="contact_map">
            <div class="row">
                <div class="col-md-12">
                    <div id="map"></div>
                </div>
            </div>
        </div>
        <div class="send_us">
            <div class="row">
                <div class="col-md-12">
                    <div class="leave_reply">
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                                <h3 class="title">Send Us a Message</h3>
                                <p class="description">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo. Please provide your name, email and cell phone number.</p>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                        <div class="contact_form_blue">
                        <form method="post" id="contact_form">
                            <div id="form-messages"></div>
                            <div class="form-group">
                            <input id="name" name="name" type="text" placeholder="Name" required="required">
                            </div>
                            <div class="form-group m_form-group">
                            <input class="m_input" id="email" name="email" type="text" placeholder="mymail@mail.co" required="required">
                            </div>
                            <div class="form-group">
                            <input class="mr_0" id="phone" name="phone" type="text" placeholder="Phone Number" required="required">
                            </div>
                            <textarea id="message" name="message" rows="4" placeholder="Text" required="required"></textarea>
                            <div id="messegeResult" class="messegeResult">
                                <p>The message was successfully sent</p>
                                <button class="button" type="submit">Send</button>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@stop