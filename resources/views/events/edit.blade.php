@extends('admin.main')
@section('title','| Events')
@section('body')
@include('tinymcebox')

<style>
        .btn-file {
            position: relative;
            overflow: hidden;
        }
        .btn-file input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            min-width: 100%;
            min-height: 100%;
            font-size: 100px;
            text-align: right;
            filter: alpha(opacity=0);
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
        }
        
        #img-upload{
            height:30%;
            width: 30%;
            text-align: center;
            padding-top:10px;
        }
</style>

<div class="app-content content container-fluid">
        <div class="content-wrapper">
          <div class="content-body">
<section>  
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" >Update Event Details | {{$events->title}}</h4>
                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    
                    @if (Session::has('message'))
                    <div class="alert alert-info alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ Session::get('message') }}
                        </div>
                        @endif
                        
                        <div class="card-body">                    
                            <div class="card-block ">
                                <form class="form" method="POST" action="{{route('events.update')}}" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="row">
                                        <div class="form-body">
                                            <div class="form-group col-sm-12">
                                                <label for="title">Title for the event*</label>
                                                <input type="text" id="title" class="form-control" placeholder="example - Zoo Visiting" name="title" value="{{$events->title}}" required>
                                            </div>
                                            
                                            <div class="form-group col-sm-12">
                                                <label for="description">Events Description*</label>
                                                <textarea class="form-control" id="detail" name="description" placeholder="Description of the event">{{$events->description}}
                                                </textarea>
                                            </div>

                                            <div class="form-group col-sm-12">
                                                    <label for="venue">Venue for the event*</label>
                                                    <input type="text" id="venue" class="form-control" placeholder="example - Lovely Home premises" name="venue" value="{{$events->venue}}" required>
                                                </div>
                                                <div class="row col-sm-12">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="timesheetinput5">Start Time</label>
                                                                <div class="position-relative has-icon-left">
                                                                    <input type="time" id="timesheetinput5" class="form-control" name="start_time" value="{{$events->start_time}}">
                                                                    <div class="form-control-position">
                                                                        <i class="icon-clock5"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="timesheetinput6">End Time</label>
                                                                <div class="position-relative has-icon-left">
                                                                    <input type="time" id="timesheetinput6" class="form-control" value="{{$events->end_time}}" name="end_time">
                                                                    <div class="form-control-position">
                                                                        <i class="icon-clock5"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            <div class="form-group col-sm-12">
                                                <label for="timesheetinput3">Date</label>
                                                    <div class="position-relative has-icon-left">
                                                            <input type="date" id="timesheetinput3" class="form-control" value="{{$events->event_date}}" name="event_date">
                                                            <div class="form-control-position">
                                                                <i class="icon-calendar5"></i>
                                                            </div>
                                                        </div>
                                            </div>  
                                            <div class="form-group col-sm-12">
                                                    <label for="donationinput3">Contact Number of Event Co-ordinator</label>
                                                    <input type="tel" id="donationinput3" class="form-control square" value="{{$events->coordinator_number}}" name="coordinator_number">
                                                </div>                                  
                                        </div>
                                        <div class="row col-sm-12">
                                        <div class="form-group col-sm-6" style="margin-bottom:0px;">
                                                <label>Upload Featured Image</label>
                                                <div class="input-group">
                                                    <span class="input-group-btn">
                                                        <span class="btn btn-primary btn-file">
                                                            Browse images… <input type="file" name="featured_image"  id="imgInp">
                                                        </span>
                                                    </span>
                                                    <input type="text" value="{{$events->featured_image}}" class="form-control"  readonly>
                                                </div>
                                                <img src="{{asset('events-images'.'/'.$events->featured_image)}}" style="padding-bottom: 15px;"  id='img-upload'/>
                                            </div>
                                        </div>
                            
                                    <div class="row col-sm-12">
                                      <div class="form-group col-sm-6">
                                          <label for="image">Select Documents for event</label>
                                          <div class="input-group">
                                          <span class="input-group-btn">
                                                <span class="btn btn-primary btn-file">
                                                    Browse document files… <input type="file" name="document" id="imgInp"  multiple>
                                                </span>
                                            </span>
                                            <input type="text" value="{{$events->document}}" class="form-control"  readonly>
                                        </div>
                                    </div>
                                </div>
      
                                      <div class="form-group col-sm-12">
                                          <button type="submit" class="btn btn-md btn-success" id="add">
                                          <i class="icon-check2">Update Event</i> 
                                        </button>
                                      </div>  
                                    </div>
                                  </div>
                                  <input type="hidden" value="{{$events->id}}" name="id"/>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
</div>
@section('js')
<script>
        $(document).ready( function() {
            $(document).on('change', '.btn-file :file', function() {
            var input = $(this),
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
            });
    
            $('.btn-file :file').on('fileselect', function(event, label) {
                
                var input = $(this).parents('.input-group').find(':text'),
                    log = label;
                
                if( input.length ) {
                    input.val(log);
                } else {
                    if( log ) alert(log);
                }
            
            });
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    
                    reader.onload = function (e) {
                        $('#img-upload').attr('src', e.target.result);
                    }
                    
                    reader.readAsDataURL(input.files[0]);
                }
            }
    
            $("#imgInp").change(function(){
                readURL(this);
            }); 	
        });
</script>
@endsection
@endsection