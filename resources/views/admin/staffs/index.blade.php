@extends('admin.main')
@section('title','| Staffs')
@section('body')
    <link href="{{ asset('backend/multiselect/jquery.multiselect.css') }}" rel="stylesheet" />
    
    <div class="app-content content container-fluid">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"><a data-action="collapse">Add New Staff</a></h4>
                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="{{ count($errors)>0 ? 'icon-minus4':'icon-plus4' }}"></i></a></li>
                                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body collapse {{ count($errors)>0 ? 'in':'out' }}">
                        <div class="card-block card-dashboard">
                            <form class="form" method="POST" id="AddStaffForm" action="{{ route('register') }}">
                                {{ csrf_field() }}
                                <div class="form-body">
                                    <h4 class="form-section"><i class="icon-eye6"></i> About Staff</h4>

                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input class="form-control{{ $errors->has('name') ? ' border-danger' : '' }}" id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                        @if ($errors->has('name'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </div>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label for="userinput5">Email</label>
                                        <input class="form-control{{ $errors->has('email') ? ' border-danger' : '' }}" type="email" placeholder="Email"  name="email" value="{{ old('email') }}" required>

                                        @if ($errors->has('email'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </div>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group">   
                                            <label for="password">Password</label>
                                            <input type="text" class="form-control" id="password" name="password" rel="gp" data-size="32" data-character-set="a-z,A-Z,0-9,#" required>
                                            <span class="input-group-btn"><button type="button" class="btn btn-primary getNewPass" style="margin-top:27px"><span class="icon-reload"></span></button></span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="role">Module Access</label>
                                        <select name="roles[]" id ='roleadd' multiple="multiple" class="3col active">
                                            @foreach($roles as $role)
                                                <option value="{{ $role->id }}">{{ $role->title }}</option>  
                                            @endforeach
                                        </select>
                                        
                                    </div>

                                </div>

                                <div class="form-actions right">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="icon-check2"></i> Save
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Staffs</h4>
                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body collapse in">
                        <div class="card-block card-dashboard">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Access</th>
                                            <th width="20%">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($staffs as $staff)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $staff->name }}</td>
                                            <td>
                                                @foreach($staff->roles as $role)
                                                    <div class="tag tag-info" style="margin:2px">{{ $role->title }}</div>
                                                @endforeach
                                            </td>
                                            <td>
                                                @if($staff->id != Auth::user()->id)
                                                    <button type="button" class="btn btn-outline-warning" onclick="edit({{ $staff->id }},{{ $staff->roles }})"><i class="icon-edit"></i></button>
                                                    <form action="{{ route('staffs.destroy',$staff->id) }}" method="POST" style="display:inline">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="page" value="{{$staffs->currentPage()}}"/>
                                                        <button id='removeStaff' type="button" class="btn btn-outline-danger"><i class="icon-trash-o"></i></button>
                                                    </form>
                                                @else
                                                    <a href="{{ route('profile') }}" class="btn btn-success">Profile</a>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                @if(count($staffs) == 0)
                                    <p class="font-medium-3 text-muted text-xs-center" style="margin:100px">No Staffs Yet</p>
                                @endif
                                <div class="text-xs-center mb-3">
									<nav aria-label="Page navigation">
										{{ $staffs->links() }}
									</nav>
								</div>
                            </div>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade text-xs-left" id="edit" role="dialog" aria-labelledby="myModalLabel5" aria-hidden="true" >
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="form" id="staffEditForm" method="POST" action="{{ route('staffs.update') }}">
                                        <div class="modal-body">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="page" value="{{$staffs->currentPage()}}"/>
                                            <div class="form-body">
                                                <h4 class="form-section">Module Access Update</h4>
                                                <input type="text" name="staffId" value='' id="staffId" hidden/>
                                                <div class="form-control" id="roleedit">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-outline-primary">Save changes</button>
                                        </div>
                                    </form>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
 @endsection

 @section('js')
    
	<script src="{{ asset('backend/multiselect/jquery.multiselect.js') }}"></script>
    <script>
        // Generate a password string
        function randString(id){
            var dataSet = $(id).attr('data-character-set').split(',');  
            var possible = '';
            if($.inArray('a-z', dataSet) >= 0){
                possible += 'abcdefghijklmnopqrstuvwxyz';
            }
            if($.inArray('A-Z', dataSet) >= 0){
                possible += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            }
            if($.inArray('0-9', dataSet) >= 0){
                possible += '0123456789';
            }
            if($.inArray('#', dataSet) >= 0){
                possible += '![]{}()%&*$#^<>~@|';
            }
            var text = '';
            for(var i=0; i < $(id).attr('data-size'); i++) {
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            }
            return text;
        }

        // Create a new password
        $(".getNewPass").click(function(){
            var field = $(this).closest('div').find('input[rel="gp"]');
            field.val(randString(field));
        });

        // Auto Select Pass On Focus
        $('input[rel="gp"]').on("click", function () {
            $(this).select();
        });

        $('#roleadd').multiselect({
            columns: 3,
            placeholder: 'Select Access',
            search: false,
            selectAll: true
        });

        $('#AddStaffForm').submit(function(e){
            e.preventDefault();
            var ele = this;
            if($('#AddStaffForm select option:selected').length > 0){
                verify(ele); 
            }
            else{
                swal('Please select at least one Module Access','','error');
                return;
            }
        });

        function edit(id, old){
            var roles = JSON.parse('<?php echo $roles ?>');
            $('#edit').modal('show');
            $('#roleedit').empty();
            $('#staffId').val(id);
            roles.forEach(function(r){
                var b=0
                old.forEach(function(o){
                    if(o.id==r.id)
                        b=1
                });
                if(b==1)
                    $('#roleedit').append("<input type='checkbox' name='"+r.title+"' value='"+r.id+"' checked/>&nbsp;&nbsp;"+r.title+"<br><br>");
                else
                    $('#roleedit').append("<input type='checkbox' name='"+r.title+"' value='"+r.id+"'/>&nbsp;&nbsp;"+r.title+"<br><br>");
            });

        }

        $('#staffEditForm').submit(function(e){
            e.preventDefault();
            var ele = this;
            if($('#staffEditForm input[type=checkbox]:checked').length > 0){
                verify(ele);
            }
            else{
                swal('Please check at least one','','error');
                return;
            }
        });

        $("[id*='removeStaff']").click(function(){
            var ele = this;
        
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this record!",
                type: "warning",

                showCancelButton: true,

            }).then((result) => {
                if (result.value) {
                    verify(ele.form);
                }
            });
        });

        function verify(ele){
            swal({
                title: 'Verify Password',
                input: 'password',
                showCancelButton: true,
                confirmButtonText: 'Verify',
                showLoaderOnConfirm: true,
                preConfirm: (password) => {
                    return new Promise((resolve) => {
                        setTimeout(() => {
                            if (password === '') {
                                swal.showValidationError(
                                    'Enter the Password.'
                                )
                            }
                            resolve()
                        }, 1000)
                    })
                },
                allowOutsideClick: () => !swal.isLoading()
            }).then((result) => {
                if (result.value) {
                    $.ajax(
                    {
                        type: "post",
                        url: "{{ route('verifyPassword') }}",
                        data: { password: result.value,_token:'{{ csrf_token() }}' },
                        success: function(response){
                            if(response == 'correct'){
                                ele.submit();
                            }
                            else{
                                swal('Incorrect Password','','error');
                            }
                        }
                    });
                }
            });
        }

    </script>
@endsection