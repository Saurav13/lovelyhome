@extends('admin.main')
@section('title','| News')

@section('body')
@include('tinymcebox')



<div class="app-content content container-fluid">
      <div class="content-wrapper">
          <div class="content-body">

	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					
					<h3 class="card-title content-header-center" id="updatenotice">Update News</h3>
					
					
					<div class="heading-elements">
						<ul class="list-inline mb-0">
							
								<a href="{{ url()->previous() }}" class="btn btn-labeled btn-default">
										<span class="btn-label"><i class="fa fa-arrow-left" aria-hidden="true"></i></span> Back</a>
							<li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
							<li><a data-action="expand"><i class="icon-expand2"></i></a></li>
						</ul>
					</div>

				</div>

				<div class="card-body collapse in">
					<div class="card-block">

						<form class="form" method="POST" action="{{route('news.update')}}" enctype="multipart/form-data">
						    {{csrf_field()}}
						    <div class="row">
						        <div class="form-body">
						          <div class="form-group col-sm-12">
						            <label for="title">Title</label>
						            <input type="text" id="title" class="form-control" value="{{$news->title}}" placeholder="Title" name="title" required>
						          </div>

						          <div class="form-group col-sm-12">
						            <label for="detail">News Description</label>
						             <textarea id="detail" class="form-control" name="description">{{$news->description}}
						             </textarea>
						          </div>

						          <div  class="form-group col-sm-12">
						            <input type="file" name="image" class="form-control-file">
						          </div>

						          <div class="form-group col-sm-12">
						             <button type="submit" class="btn btn-primary" id="add">
						              <i class="icon-check2">Update News</i> 
						          </button>
						          </div>  
						        </div>
						      </div>
						      <input type="hidden" name="id" value="{{$news->id}}">
						    </form>

					</div>
				</div>
			</div>
			@if(count($errors))
			    <div class="alert alert-danger">
			      <ul>
			        @foreach ($errors->all() as $error)
			          <li>{{$error}}</li>
			          @endforeach
			      </ul>
			  </div>
			  @endif
		</div>
	</div>
	
</div>
</div>
</div>


@endsection

@section('js')
@endsection