@forelse($myposts as $p)
    <div class="blog_block wow fadeIn" id="mypost{{ $p->id }}">
        <h3 class="blog_title" id="postTitle{{ $p->id }}">{{ $p->title }}</h3>
        <div class="row blog_info">
            <div class="col-md-2 col-sm-2 col-xs-12 pr_0">
                <div class="date" style="background:#49b450">
                    <ul>
                        <li class="number">{{ $p->created_at->day }}</li>
                        <li class="date_month">{{ date('M',strtotime($p->created_at)) }}<br>{{ $p->created_at->year }}</li>
                    </ul>
                </div>
            </div>
            
            <div class="col-md-7 col-sm-7 col-xs-12 pl_0">
                
            </div>
        </div>
        <div class="blog_block_img">
            <img {{ $p->image ? '':'hidden' }} src="{{ asset('alumni_blogs/'.$p->image) }}" alt="{{ $p->title }}" id="postImg{{ $p->id }}">
        </div>
        
        <div class="blog_brief">
            <p id="postContent{{ $p->id }}">{!! $p->content !!}</p>
        </div>
        <div class="blog_link">
            <div class="blog_share social_icon t_white">
                <ul>
                    <li class="plr_10" style="margin-top: 2rem;"><a href="#"><i class="plr_10 pr_10 b_comment fa fa-comment">{{ $p->comments->count() }}</i></a></li>
                </ul>
            </div>
            <div class="button buttongreen">
                <a href="{{ route('alumni.singlepost',$p->slug) }}" id="postURL{{ $p->id }}">View</a>
            </div>
            <div class="button buttonyellow">
                <a style="cursor:pointer" class="editMyPost" id="editPost{{ $p->id }}" post-id="{{ $p->id }}">Edit</a>
            </div>
            <div class="button buttonred">
                <a style="cursor:pointer" class="deleteMyPost" id="deletePost{{ $p->id }}" post-id="{{ $p->id }}">Delete</a>
            </div>
        </div>
    </div>
@empty
    <p style="font-size:2rem;margin-top:50px;text-align: center;">No Posts Found</p>
@endforelse