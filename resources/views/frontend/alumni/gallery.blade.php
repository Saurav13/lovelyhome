@extends('layouts.app')

@section('body')
@section('title','| Alumini Gallery')

<style>
    alumini_tabs .nav-tabs {
        border: none;
    }

    @media(min-width: 300px)
    {
        .alumini_tabs ul li a {
            padding: 8px 0;
            font-size: 1rem;
            border-radius: 50px;
            font-family: 'OpenSansSemiBold', sans-serif;
        }
    }
    @media(min-width: 600px)
    {
        .alumini_tabs ul li a {
            padding: 8px 0;
            font-size: 1.5rem;
            border-radius: 50px;
            font-family: 'OpenSansSemiBold', sans-serif;
        }
    }
    @media (min-width: 1200px)
    {
        .alumini_tabs ul li a {
            padding: 8px 0;
            font-size: 2rem;
            border-radius: 50px;
            font-family: 'OpenSansSemiBold', sans-serif;
        }
    }
    .alumini_tabs ul {
        position: relative;
        list-style-type: none;
        margin: 0;
        text-align: center;
        padding: 0;
    }
    .alumini_tabs ul li {
        display: inline-block;
        float: none;
        text-transform: uppercase;
        width: 23%;
        margin: 30px 7px;
    }
    .alumini_tabs ul li a {
    
    }

    .alumini_tabs ul li .tab_red {
        border: 2px solid #b70404;
        color: #b70404;
        -webkit-transition: all .2s linear;
        transition: all .2s linear;
    }

    .alumini_tabs ul li .tab_yel {
        border: 2px solid #fff400;
        color: #fff400;
        -webkit-transition: all .2s linear;
        transition: all .2s linear;
    }

    .alumini_tabs ul li .tab_green {
        border: 2px solid #49b450;
        color: #49b450;
        -webkit-transition: all .2s linear;
        transition: all .2s linear;
    }


    .nav-tabs>li>.tab_yel:hover, .ui_hover .nav-tabs>li>.tab_green {
        background: #fff400;
        color: #fff;
        border: 2px solid #fff400;
        -webkit-background-clip: padding-box;
        background-clip: padding-box;
    }
    .nav-tabs>li.active>.tab_yel, .nav-tabs>li.active>.tab_yel:focus, .nav-tabs>li.active>.tab_yel:hover {
        background: #fff400;
        color: #fff;
        border: 2px solid #fff400;
    }

    .year_div{
        float:right;
    }
    .year_div select{
        font-size: 20px;
        border-radius: 10px;
        padding: 7px;
        margin: 10px;
        float: right;
        margin-right: 5rem;
        margin-left: 0px;
        z-index:9999;
    }
    .year_div h4{
        font-size: 20px;
        margin-top: 2rem;
    }
</style>
<section id="blog_page" class="blog_page">
    <div class="container">
        <div class="row pb_20">
            <div class="col-md-1"></div>
            <div class="col-md-10 dot">
                <h3 class="section-title">Welcome Back!!!</h3>
                <div class="alumini_tabs">
                    <ul class="nav nav-tabs">
                        <li class="active"><a class="tab_red acttabfont" href="#gallery">Gallery</a></li>
                        <li><a class="tab_green acttabfont" href="{{ route('alumni.blog') }}">Blog</a></li>
                        @if(Auth::guard('alumni')->check())
                            <li><a class="tab_yel acttabfont" href="{{ route('alumni.myposts') }}">My posts</a></li>
                        @endif
                    </ul>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
        <div class="tab-content about_tabs_content">
            <div class="tab-pane card-container active" id="gallery">
                <section id="teachers_portfolio" class="teachers_portfolio text-center section" style="border-top:none; padding:0px;">
                    <div class="container">
                        <div class="year_div">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4>year:</h4>
                                </div>
                                <div class="col-md-6">
                                    <select id="selectyear">
                                        <option value="All" selected>All</option>
                                        @foreach($batch as $b)
                                            <option value="{{ $b }}">{{ $b }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        
                        <div id="alumniPhotos">
                            @include('frontend.alumni.partials.photos')
                        </div>
                        <img id="loader" hidden src="{{ asset('ajax-loader.gif') }}"/>
                    </div>
                </section>
            </div>

            <div class="tab-pane card-container" id="blog">
                
            </div>
            <div class="tab-pane card-container" id="myposts">
               
            </div>
        </div>
    </div>
</section>
@stop

@section('js')

    <script>
        var nextBatch = "{{ $nextBatch }}";
        var filterBatch = 'All';
        var ajaxCall = false;
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
    
                reader.onload = function (e) {
                   
                    $('#uploadimg')
                    .removeAttr('hidden')
                        .attr('src', e.target.result);
                };
    
                reader.readAsDataURL(input.files[0]);
            }
        }
    
        $(document).ready(function(){
            $('#selectyear').change(function(){

                filterBatch = $(this).val();
                
                $.ajax({ 
                    type: 'GET', 
                    url: "{{ route('alumni_photos.filterBatch') }}",
                    dataType: 'json', 
                    data: {filter_batch : filterBatch}, 
                    beforeSend: function(data) {
                        $('#alumniPhotos').attr('hidden','hidden');
                        $('#loader').removeAttr('hidden');
                    },
                    success: function(data){
                        nextBatch = data.nextBatch
                        $('#alumniPhotos').empty();
                        $('#alumniPhotos').append(data.html);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        location.reload();
                    },
                    complete: function(data) {
                        $('#loader').attr('hidden','hidden');
                        $('#alumniPhotos').removeAttr('hidden');
                    }
                            
                });
            });

            $(window).scroll(function() {
                if($(window).scrollTop() > $('#alumniPhotos').height() && nextBatch != '' && !ajaxCall) {
                    ajaxCall = true;
                    loadMoreData();
                }
            });

            function loadMoreData(){

                $.ajax({ 
                    type: 'GET', 
                    url: "{{ route('alumni_photos.filterBatch') }}",
                    dataType: 'json', 
                    data: {filter_batch : 'All',next_batch: nextBatch}, 
                    beforeSend: function(data) {
                        $('#loader').removeAttr('hidden');
                    },
                    success: function(data){
                        nextBatch = data.nextBatch;
                        $('#alumniPhotos').append(data.html);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        // location.reload();
                    },
                    complete: function(data) {
                        ajaxCall = false;
                        $('#loader').attr('hidden','hidden');
                    }
                            
                });
            }
        });
    </script>
@endsection