<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Feedback;
use App\ContactUsMessage;
use Auth;
use App\Advertise;
class FrontendComposer
{
    /**
     * Create a movie composer.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $advertises=Advertise::all()->shuffle()->take(6);
        $view->with('advertises',$advertises);
    }
}