@extends('layouts.app')

@section('body')
@section('title','| Gallery')


<section id="gallery_page" class="gallery_page">
    <div class="container">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10 dot">
                <h2 class="section-title"><a href="{{ URL::to('gallery') }}" style="color:#333333" id="albumname">Gallery</a></h2>
              </div>
            <div class="col-md-1"></div>
        </div>
    </div>
    <div class="container_fluent gallery_portfolio">
        <ul class="filter" hidden>
            <li class="active"><a href="#" id="albumna" ></a></li>
        </ul>
        <div class="row_fluent">
            <div class="portfolio_grid">
                <div id="facebook" class="portfolio_grid_item wow fadeInUp">

                </div>
            </div>
            <div class="load_more">
                <a style="cursor:pointer" id="more">load more</a>
            </div>
        </div>
    </div>
</section>

@stop

@section('js')
    <script>
        $(document).ready(function() {
            $.ajaxSetup({ cache: true });
            var next = '';
            var id ="/{{$id}}";
            $.getScript('https://connect.facebook.net/en_US/sdk.js', function(){
                FB.init({
                    appId: '1736045103105696',
                    version: 'v2.12' // or v2.1, v2.2, v2.3, ...
                });     
                /* make the API call */
                FB.api(
                    id,
                    'GET',
                    {"fields":"photos.limit(12){images},from,name",'access_token': '1736045103105696|eMXM_137WEw3s4NMEeUf8_-F0DE'},
                    function(response) {
                        if(response.from.id != "454158518008498")
                            return;

                        $('#albumname').text(response.name);

                        var html = '';
                        for(var i=0;i<response.photos.data.length;i++){
                            html += '<div class="col-portfolio col-xs-12 col-sm-6 col-md-4 col-lg-3 portfolio_grid_item wow fadeInUp"><figure class="hover-line"><div class="image-holder"><img alt="" style="height:217px;width:325px" src="'+response.photos.data[i].images[0].source+'"></div><figcaption><div class="portfolio-lead"><div class="gallery_full_screen"><a class="link" href="'+response.photos.data[i].images[0].source+'"><img src="/frontend-assets/img/icons/search_gallery.png" alt=""></a></div><div class="gallery_description"><p><span></span></p></div></div></figcaption></figure></div>';
                        }
                        if(response.photos.paging.next)
                            next = response.photos.paging.next;
                        else{
                            $('#more').text('No more Photos');
                            $('#more').attr('id','');
                        }   
                        $('#facebook').append(html);
                        $('#albumna').trigger('click');

                        $('.link').magnificPopup({
                            type:'image',
                            titleSrc: 'title',
                            mainClass: 'gallery_popup',
                            gallery:{enabled:true}
                        });
                    }
                );
    
                $('#more').click(function(){
                    if(!next) return;
                    $('#more').text('LOADING...');
                    FB.api(
                        next,
                        'GET',
                        
                        function(response) {
                            var html = '';
                            for(var i=0;i<response.data.length;i++){
                                
                                if(!response.data[i].images) continue;
                                html += '<div class="col-portfolio col-xs-12 col-sm-6 col-md-4 col-lg-3 portfolio_grid_item wow fadeInUp"><figure class="hover-line"><div class="image-holder"><img style="height:217px;width:325px" alt="" src="'+response.data[i].images[0].source+'"></div><figcaption><div class="portfolio-lead"><div class="gallery_full_screen"><a class="link" href="'+response.data[i].images[0].source+'"><img src="/frontend-assets/img/icons/search_gallery.png" alt=""></a></div></div></figcaption></figure></div>';
                            
                            }

                            $('#facebook').append(html);
                            $('#more').text('LOAD MORE');

                            if(response.paging.next)
                                next = response.paging.next;
                            else{
                                $('#more').text('No more Photos');
                                $('#more').attr('id','');
                                next = null;
                            }
                            $('#albumna').trigger('click');
                            $('.link').magnificPopup({
                                type:'image',
                                titleSrc: 'title',
                                mainClass: 'gallery_popup',
                                gallery:{enabled:true}
                            });
                        }
                    );
                });
            });
    
            
        });
    </script>
@stop