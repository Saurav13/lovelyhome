@extends('admin.main')
@section('body')
@section('title','| Downloads')

<div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
       

          <div class="content-header-right col-sm-6" >
            <form class="form " method="get" action="{{URL::to('/admin/downloads/search')}}" >
            
            <input type="text" class="form-control"  id="searchDownloads" name="searchDownloads" placeholder="Search Downloads">
            
            <div class="form-group" >
              <button type="submit" hidden class="btn btn-primary" id="add"> Search
              </button>
            </div>             
            </form>

          </div>
          
      
        </div>
        
        <div class="content-body">
          <!-- Basic example section start -->
              <section id="addresources">  
                <div class="row">
                  <div class="col-md-12">
                    <div class="card">
                      <div class="card-header">
                          <div class="card-title" ><a><h3>Category for Downloads</h3></a></div>
                          <a class="heading-elements-toggle">
                            <i class="icon-ellipsis font-medium-3"></i>
                          </a>
                          <div class="heading-elements">
                              <ul class="list-inline mb-0">
                                  <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                              </ul>
                          </div>
                      </div>
                    
                      <div class="card-body"> <!-- // collapse -->
                        <div class="card-block">
                          <div class="row">
                            <div class="col-md-12">
                              @if(count($cat)==null)
                              <h6>No downloads Category added yet procced by adding categories first and then you will be able to add downloable resources</h6>
                              @endif
                                @if(count($cat)!=null)
                                  <div class="table-responsive " style= "
                                    height: 200px;
                                    overflow: auto; " >
                                      
                                    <table class="table table-bordered">
                                      <thead>
                                      <tr>
                                        <th style="width: 40%">Category</th>
                                        <th style="width: 20%">Edit</th>
                                        <th style="width: 20%">Delete</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                        @foreach($cat as $c)
                                          <tr>
                                            <td class="text-middle">
                                              <h4>{{$c->category_name}} </h4>
                                              <div class="row">
                                              <div id="ResupdateCancel{{$c->id}}" hidden>
                                                  <form class="form" method="POST" action="{{URL::to('/admin/downloadscategory/update')}}" >
                                                    <div class="form-body">
                                                      <div class="form-group col-sm-6">
                                                        {{csrf_field()}}
                                                        <input type="text" hidden name="id" value="{{$c->id}}">
                      
                                                        <input type="text" id="Restitle" class="form-control" placeholder="Edit Category name" name="category_title" value={{$c->category_name}} required> 
                      
                                                      <button type="submit" class="btn btn-blue" id="Resupdate{{$c->id}}"> 
                                                        <i class="icon-check2">Update</i> </button>
                      
                                                        <button type="button" class="btn btn-red" id="Rescancel{{$c->id}}">
                                                        <i class="icon-check2">Cancel</i>
                                                        </button>          
                                                    </div>
                                                    </div> 
                                                </form>
                                              </div>
                                              </div>
                                            </td>
                                          
                                            <td class="text-middle" id="Resedit{{$c->id}}">
                                              <a href="javascript:void(0)"  class="btn btn-outline-blue " style=""><i class="icon-edit"></i></a>
                                              </td>
                                            <td class="text-middle">
                                              <a href="javascript:void(0)" id="Resdelete{{$c->id}}" class="btn btn-outline-red"> <i class="icon-remove"></i></a>
                                              </td>
                                          </tr>
                    
                                        @endforeach
                    
                                      </tbody>
                                    </table>
                                  </div>
                                @endif
                              <div>
                                
                                <div class="row" style="padding-top:20px;">
                                  <form class="form" method="post" action="{{route('downloadscategory.store')}}">
                                    {{csrf_field()}}
                                    <div class="form-body">
                                      <div class="form-group col-sm-3">
                                        <input type="text" id="Restitle" class="form-control" placeholder="Add New Category" name="category_title" required>
                                      </div>
              
                                      <div class="form-group  col-sm-3">
                                        <button type="submit" class="btn btn-success" id="Resadd">
                                          <i class="icon-check2">Add Downloads Category</i> 
                                      </button>
                                      </div>  
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>
        
                          </div>
        
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              @if(count($cat)!=null)
              <section id="addnotice">  
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title" ><a data-action="collapse"><button  class="btn btn-md btn-primary"><i class="icon-plus4" aria-hidden="true"></i> Add Downloadable resources </button></a></h4>
                                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="icon-plus4"></i></a></li>
                                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>

                                    </ul>
                                </div>
                            </div>
                
                              <div class="card-body collapse">
                                
                                <div class="card-block ">
                                    <form class="form" method="POST" action="{{route('downloads.store')}}" enctype='multipart/form-data'>
                                        {{csrf_field()}}
                                        <div class="row">
                                            <div class="form-body">
                                              <div class="form-group col-sm-12">
                                                <label for="title">Title</label>
                                                <input type="text" id="title" class="form-control" placeholder="title" name="title" required>
                                              </div>
                                              <div class="form-group col-sm-12">
                                                <select name="category" class="form-control">
                                                    <option disabled selected value>Choose Category</option>
                                                    @foreach($cat as $c)
                                                    <option value="{{$c->id}}">{{$c->category_name}}</option>
                                                    @endforeach
                                                </select>
                                              </div>
                                              <div class="form-group col-sm-12">
                                                <input type="file" name="file" class="form-control-file">
                                              </div>

                                              <div class="form-group col-sm-12">
                                                <button type="submit" class="btn btn-success" id="add">
                                                  <i class="icon-check2">Add to Downloads</i> 
                                              </button>
                                              </div>  
                                            </div>
                                          </div>
                                        </form>
                                        </div>
                                    
                                </div>
                        </div>
                    </div>
                </div>
                    
                    @if (Session::has('message'))
                      <div class="alert alert-info alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          {{ Session::get('message') }}
                        </div>
                    @endif

              @if(count($downloads)==null)
              <h1>No downloadable resources found!</h1>
              @endif

              @if(count($downloads)!=null)
              <div class="row match-height">
                @foreach($downloads as $d)
                <div class="col-xl-3 ">
                    <div class="card">
                      <div class="card-body ">
                        <div class="card-block w3-margin" id="noticeShow">
                          <div class="card-title">
                          <h4 id="title{{$d->id}}">{{$d->title}} </h4> 
                          <p id="cat{{$d->id}}">{{$d->category_name}}</p>
                          <h6>Added {{$d->created_at->diffForHumans()}}</h6>
                          </div>

                          <a href="{{ asset('downloads/'.$d->file)}}" download="{{$d->file}}" class="btn btn-sm btn-outline-green">Download</a>
                          <button  class="btn btn-sm btn-outline-blue" data-toggle="modal" data-target="#myModal" cid={{$d->id}} id="update{{$d->id}}">Edit</button>
                          <button  id="delete{{$d->id}}" cid={{$d->id}} class="btn btn-sm btn-outline-red">Delete</button>
                        </div>
                      </div>
                    </div>
                </div>
                @endforeach
                
              </div>
              @endif
              </section>
              @endif
        </div>
      </div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Downloads</h4>
      </div>
      <div class="modal-body">
        <form class="form" method="POST" action="{{route('downloads.update')}}" enctype='multipart/form-data'>
          {{csrf_field()}}
          <div class="row">
            <div class="form-body">
              <div class="form-group col-sm-12">
                <label for="title">Title</label>
                <input type="text" name="edittitle" id="edittitle" class="form-control" placeholder="title" required>
              </div>
              <div class="form-group col-sm-12">
                  <select name="category" class="form-control" id="editcat">
                      @foreach($cat as $c)
                      <option value="{{$c->id}}">{{$c->category_name}}</option>
                      @endforeach
                  </select>
                </div>

              <div class="form-group col-sm-12">
                <input type="file" name="file" class="form-control-file">
              </div>
              
              <div class="form-group col-sm-12">
                <button type="submit" class="btn btn-success" id="add">
                  <i class="icon-check2">Save Changes</i> 
                </button>
              </div>  
            </div>
          </div>
          <input type="hidden" name="id" id="editid">
        </form>
      </div>
      
    </div>
    
  </div>
</div>
@section('js')

<script type="text/javascript">
  $(document).ready(function(){
    $("[id*='delete']").click(function(){
      var id = $(this).attr("id").slice(6);
      swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          $.post("{{route('downloads.destroy')}}",{id:id,_token:"{{csrf_token()}}"},function(data){ 
            swal({
              title:"Deleted Successfully",
              type:"success"
            }).then(function(){
              window.location.reload();
            })
          })
        }
      })
      
    });
    $('[id *= "update"]').click(function(){
      var id= $(this).attr('cid');
      var title = $('#title'+id).html();
      $('#edittitle').val(title);
      $("#editcat option:contains('"+$('#cat'+id).html()+"')").attr('selected', 'selected');
      $('#editid').val(id);
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $("[id*='Resedit']").click(function(){
           //alert('baam');
            var id = $(this).attr("id").slice(7);
            
            //$("#updateCancel"+id).attr("hidden","true");
            $("#ResupdateCancel"+id).removeAttr("hidden");
          });

          $("[id*='Rescancel']").click(function(){
           //alert('baam');
            var id = $(this).attr("id").slice(9);
            $("#ResupdateCancel"+id).attr("hidden","true");
          });
          
        $("[id*='Resdelete']").click(function(){
          var id = $(this).attr("id").slice(9);
          swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
            if (result.value) {
              $.post("{{route('downloadscategory.delete')}}",{id:id,_token:"{{csrf_token()}}"},function(data){  
                  swal({
                    title:"Deleted Successfully",
                    type:"success"
                  }).then(function(){
                    window.location.reload();
                  })
        })
            }
            })

        });  

    });
</script>

@endsection
@endsection