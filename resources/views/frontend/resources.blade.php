@extends('layouts.app')

@section('body')
@section('title','| Resources')

    
    <section id="shop_page" class="shop_page">
        <div class="container">
            <div class="row pb_20">
                <div class="col-md-1"></div>
                <div class="col-md-10 dot">
                    <h2 class="section-title">Kid's Resource</h2>
                    
                </div>
                <div class="col-md-1"></div>
            </div>
            <div class="row">
                <div class="col-md-9">
                        {{--  <div class="shop_nav">
								<div class="shop_view">
									<ul>
										<li class="active"><a href="#"><i class="fa fa-th-list"></i></a></li>
									</ul>
								</div>
								<div class="shop_sorting">
									<div class="dropdown">
										<button class="btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">Default Sorting
											<span class="btn_caret"><i class="fa fa-angle-down"></i></span></button>
											<ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
												<li role="presentation"><a role="menuitem" href="#">School</a></li>
												<li role="presentation"><a role="menuitem" href="#">Event</a></li>
												<li role="presentation"><a role="menuitem" href="#">Music lesson</a></li>
												<li role="presentation"><a role="menuitem" href="#">Healthy food</a></li>
											</ul>
										</div>
									</div>
                                </div>  --}}
                                @if(count($materials)==null)
                                <div class="text-center">
                                    <h2>No Materials Available</h2>
                                </div>
                                @endif
                                @if(count($materials)!=null)
                            @foreach($materials as $m)    
                        <div class="shop_block_list">
                            <div class="shop_item">
                                <div class="shop_block_img">
                                    <a href="#"><img src="{{asset('materials-images'.'/'.$m->image)}}" alt="{{$m->title}}"></a>
                                </div>
                                <div class="item_describe" style="min-height: 220px;">
                                    <h5><a href="#">{{$m->title}}</a></h5>
                                  
                                    <div class="item_content">
                                        <p>{{$m->detail}}</p>
                                    </div>
                                   
                                    <a href="{{asset('materials-files'.'/'.$m->file)}}" download class="button b_green_green_light">Download</a>
                                    <br>
                                    <br>
                                    <br>
                                </div>
                            </div>
                        </div>
                        <br>
                        <br>
                        <br>
                        @endforeach
                        <div class="text-center">
                                {{$materials->links()}}
                              </div>	
                        @endif      
                    </div>
                    <div class="col-md-3">
                        <div class="sidebar">
                            <div class="categories b_green_green_light">
                                <h4 class="title">Categories</h4>
                                <ul>
                                    <li><a href="{{route('resources')}}" @if(Request::is('resources'))style="background-color: rgba(0,0,0,0.1);" @endif>All <span>{{count($allmaterials)}}</span></a></li>
                                    @foreach($categories as $c)
                                    <li class="active"><a @if(Request::is('resources'.'/'.$c->slug.'/'.$c->id)) style="background-color: rgba(0,0,0,0.1);" @endif href="{{ route('sort.materials', ['slug' => $c->slug , 'id' => $c->id]) }}">{{$c->title}}<span>{{count($allmaterials->where('id','=',$c->id))}}</span></a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
@stop