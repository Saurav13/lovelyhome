<div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
  <!-- main menu header-->
  
  <!-- / main menu header-->
  <!-- main menu content-->
  <div class="main-menu-content">
    <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
        <li class=" nav-item @if(Request::is('admin/dashboard*')) active @endif">
          <a href="{{URL::to('/admin/dashboard')}}"><i class="icon-home3"></i><span data-i18n="nav.dash.main" class="menu-title">Home</span></a>
        </li>
          
        <li class="nav-item {{ Request::is('admin/staffs*')?'active':'' }}" {{Auth::user()->roles()->where('title', '=', 'Staffs')->exists() ? '':'hidden'}}><a href="{{ route('staffs.index') }}"><i class="icon-android-contacts"></i><span data-i18n="nav.staffs.main" class="menu-title">Staffs</span></a>
        </li>

        <li class="nav-item @if(Request::is('admin/notice*'))
                active
                @endif" {{Auth::user()->roles()->where('title', '=', 'Notices')->exists() ? '':'hidden'}}><a href="{{URL::to('/admin/notice')}}"><i class="fa fa-bell" aria-hidden="true"></i><span data-i18n="nav.dash.main" class="menu-title">Notices</span>
          </a></li>      
              
        <li class=" nav-item @if(Request::is('admin/downloads*'))
                  active
                  @endif" {{Auth::user()->roles()->where('title', '=', 'Downloads')->exists() ? '':'hidden'}}><a href={{URL::to('/admin/downloads')}}><i class="icon-download"></i><span data-i18n="nav.dash.main" class="menu-title">Downloads</span>
      
          </a></li>
          <li class=" nav-item @if(Request::is('admin/resources*') || Request::is('admin/materials*') )
                    active
                    @endif" {{Auth::user()->roles()->where('title', '=', 'Resources')->exists() ? '':'hidden'}}><a href={{URL::to('/admin/resources')}}><i class="fa fa-sticky-note-o" aria-hidden="true"></i><span data-i18n="nav.dash.main" class="menu-title">Resources</span>
        
          </a></li>
          <li class=" nav-item @if(Request::is('admin/gallery*'))
                    active
                    @endif" {{Auth::user()->roles()->where('title', '=', 'Gallery')->exists() ? '':'hidden'}}><a href={{URL::to('/admin/gallery')}}><i class="fa fa-picture-o" aria-hidden="true"></i><span data-i18n="nav.dash.main" class="menu-title">Gallery</span> </a></li>
          <li class=" nav-item @if(Request::is('admin/testimonials*'))
                    active
                    @endif" {{Auth::user()->roles()->where('title', '=', 'Testimonials')->exists() ? '':'hidden'}}><a href={{URL::to('/admin/testimonials')}}><i class="fa fa-quote-left" aria-hidden="true"></i><span data-i18n="nav.dash.main" class="menu-title">Testimonials</span> </a></li>
          
          <li class=" nav-item @if(Request::is('admin/teachers*'))
                      active
                      @endif" {{Auth::user()->roles()->where('title', '=', 'Teachers')->exists() ? '':'hidden'}}><a href={{URL::to('/admin/teachers')}}><i class="fa fa-user" aria-hidden="true"></i><span data-i18n="nav.dash.main" class="menu-title">Teachers</span> </a></li>

          <li class=" nav-item @if(Request::is('admin/events*'))
                        active
                        @endif" {{Auth::user()->roles()->where('title', '=', 'Event Settings')->exists() ? '':'hidden'}}><a href="{{route('admin.events')}}"><i class="fa fa-calendar" aria-hidden="true"></i><span data-i18n="nav.dash.main" class="menu-title">Event Settings</span></a></li>
          <li class="nav-item {{ Request::is('admin/calender*') ? 'active' : '' }}" {{Auth::user()->roles()->where('title', '=', 'Calendar')->exists() ? '':'hidden'}}><a href="{{route('calender.index')}}"><i class="fa fa-calendar" aria-hidden="true"></i><span data-i18n="nav.dash.main" class="menu-title">Calendar</span></a></li>

          <li class="nav-item {{ Request::is('admin/faqs*') ? 'active' : '' }}" {{Auth::user()->roles()->where('title', '=', 'FAQs')->exists() ? '':'hidden'}}><a href="{{route('admin.faqs')}}"><i class="fa fa-question" aria-hidden="true"></i><span data-i18n="nav.dash.main" class="menu-title">FAQs</span></a></li>

          <li class="nav-item {{ Request::is('admin/activities*') ? 'active' : '' }}" {{Auth::user()->roles()->where('title', '=', 'Activities')->exists() ? '':'hidden'}}><a href="{{route('admin.activities')}}"><i class="fa fa-tasks" aria-hidden="true"></i><span data-i18n="nav.dash.main" class="menu-title">Activities</span></a></li>

          <li class="nav-item {{ Request::is('admin/alumni_requests*') ? 'active' : '' }}" {{Auth::user()->roles()->where('title', '=', 'Alumni Requests')->exists() ? '':'hidden'}}><a href="{{route('alumni_requests.index')}}"><i class="fa fa-user"><i class="fa fa-clock-o" style="font-size: 11px;vertical-align: top;"></i></i><span data-i18n="nav.dash.main" class="menu-title">Alumni Requests</span></a></li>

          <li class="nav-item {{ Request::is('admin/alumni_photos*') ? 'active' : '' }}" {{Auth::user()->roles()->where('title', '=', 'Alumni Photos')->exists() ? '':'hidden'}}><a href="{{route('alumni_photos.index')}}"><i class="fa fa-user"><i class="fa fa-picture-o" style="font-size: 11px;vertical-align: top;"></i></i><span data-i18n="nav.dash.main" class="menu-title">Alumni Photos</span></a></li>
          
          <li class="nav-item {{ Request::is('admin/advertises*') ? 'active' : '' }}" {{Auth::user()->roles()->where('title', '=', 'Advertisements')->exists() ? '':'hidden'}}><a href="{{route('admin.advertises')}}"><i class="fa fa-tasks" aria-hidden="true"></i><span data-i18n="nav.dash.main" class="menu-title">Advertisements</span></a></li>

          <li class="nav-item {{ Request::is('admin/contact-us-messages*') ? 'active' : '' }}" {{Auth::user()->roles()->where('title', '=', 'Contact Us Messages')->exists() ? '':'hidden'}}><a href="{{ route('contact-us-messages.index') }}"><i class="icon-at2"></i><span data-i18n="nav.contact-us-messages.main" class="menu-title">Contact Us Messages</span></a>
          </li>

          <li class="nav-item {{ Request::is('admin/feedbacks*') ? 'active' : '' }}" {{Auth::user()->roles()->where('title', '=', 'Feedback Messages')->exists() ? '':'hidden'}}><a href="{{ route('feedbacks.index') }}"><i class="fa fa-comments"></i><span data-i18n="nav.feedbacks.main" class="menu-title">Feedbacks Messages</span></a>
          </li>
          
          <li class="nav-item {{ Request::is('admin/newsletter*') ? 'active' : '' }}" {{Auth::user()->roles()->where('title', '=', 'Newsletter')->exists() ? '':'hidden'}}><a href="{{ route('newsletter') }}"><i class="icon-mail6"></i><span data-i18n="nav.newsletter.main" class="menu-title">Newsletter</span></a>
          </li>
          
        </ul>
  </div>
  <!-- /main menu content-->
  <!-- main menu footer-->
  <!-- include includes/menu-footer-->
  <!-- main menu footer-->
</div>
