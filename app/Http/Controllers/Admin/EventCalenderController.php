<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\EventCalender;
use Carbon\Carbon;
use Auth;

class EventCalenderController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            
            if (Auth::user()->roles()->where('title', '=', 'Calendar')->exists()){
                return $next($request);
            }
            else
                abort(403);
        });
    }
    
    public function index()
    {
        $events = [];
        $calender_events = EventCalender::all();
        foreach($calender_events as $c){
            $events[] = \Calendar::event(
                $c->event_name,
                true,
                new Carbon($c->event_date),
                new Carbon($c->event_date),
                $c->id,
                [
                    'description' => $c->description,
                    'url' => route('calender.edit',$c->id),
                    'type' => $c->type
                ]
            );
        }
        
        $calendar = \Calendar::addEvents($events)
            ->setOptions([
                'firstDay' => 0,
            ])->setCallbacks([
                //'eventMouseover' => 'function() {alert("Callbacks!");}'
                'eventLimit' => 3,
                'eventRender' => "function(event, element) { element.qtip({content: {text:event.description,title:event.title+' (' +event.type+')' }})}"
            ]); 

        return view('admin.calender.index', compact('calendar'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.calender.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required|max:191',
            'description'=>'required',
            'date' =>'required|date_format:m/d/Y',
            'type' => 'required|in:Holiday,Half-Holiday,Celebration Day'
        ]);
        
        $event = new EventCalender;
        $event->event_name = $request->name;
        $event->description = $request->description;
        $event->type = $request->type;
        $event->event_date = new Carbon($request->date);
        $event->save();

        $request->session()->flash('success', 'Event successfully added to calender.');
        return redirect()->route('calender.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = EventCalender::find($id);

        if(!$event) abort(404);

        return view('admin.calender.edit')->with('event',$event);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required|max:191',
            'description'=>'required',
            'date'=>'required|date_format:m/d/Y',
            'type' => 'required|in:Holiday,Half-Holiday,Celebration Day'
        ]);

        $event = EventCalender::find($id);

        if(!$event) abort(404);

        $event->event_name = $request->name;
        $event->description = $request->description;
        $event->event_date = new Carbon($request->date);
        $event->type = $request->type;
        $event->save();

        $request->session()->flash('success', 'Event successfully edited.');
        return redirect()->route('calender.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $event = EventCalender::find($id);

        if(!$event) abort(404);

        $event->delete();

        $request->session()->flash('success', 'Event successfully deleted.');
        return redirect()->route('calender.index');
    }
}
