<?php

use Faker\Generator as Faker;

$factory->define(App\AlumniPhoto::class, function (Faker $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'batch' => rand(2060,2074),
        'photo' => 'photo.jpg'
    ];
});
