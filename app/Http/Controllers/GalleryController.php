<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UploadRequest;
use App\Gallery;
use App\Image;
use Validator;
use Auth;
use Image as Img;

class GalleryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            
            if (Auth::user()->roles()->where('title', '=', 'Gallery')->exists()){
                return $next($request);
            }
            else
                abort(403);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galleries = Gallery::latest()->get();

        return view('gallery.index', compact('galleries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'category'=>'required'
        ]);
        
        $i = 1;
        $name = $request->name;
        while(Gallery::where('name', $name)->where('category', $request->category)->exists()){
            $name = $request->name. '('.$i.')';
            $i++;
        }

        $gallery = new Gallery;
        $gallery->name = $name;
        $gallery->category = $request->category;
        $gallery->slug = str_replace(' ','-',strtolower($name.' '.$request->category));
        $gallery->save();
        
        $request->session()->flash('success', 'Album successfully created.');
        return redirect('/admin/gallery#'.$request->category);
    }

    public function show($id)
    {
        $album = Gallery::find($id);

        if(!$album) abort(404);

        return view('gallery.showImages',compact('album'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'editname'=>'required'
        ]);
        
        $i = 1;
        $name = $request->editname;
        $gallery = Gallery::find($request->get('id'));
        if(!$gallery) abort(404);

        if($name != $gallery->name){
            while(Gallery::where('name', $name)->where('category', $gallery->category)->exists()){
                $name = $request->editname. '('.$i.')';
                $i++;
            }

            $gallery->name = $name;
            $gallery->slug = str_replace(' ','-',strtolower($name.' '.$gallery->category));
            $gallery->save();
        }
        $request->session()->flash('success', 'Album successfully edited.');        
        return redirect('/admin/gallery#'.$gallery->category);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {  
        $images = Image::where('gallery_id',$id)->get();
        foreach ($images as $image){
            unlink(public_path('gallery_images'.'/'.$image->name));
            $image->delete();
        }

        $cat=Gallery::where('id',$id)->first()->category;
        Gallery::where('id',$id)->delete();
        
        $request->session()->flash('success', 'Album successfully deleted.');
        return redirect('/admin/gallery#'.$cat);
    }
   
    public function imageStore(Request $request,$id)
    {
        $cat = Gallery::where('id',$id)->first()->category;
        if($request->hasFile('files')){
            if($cat == 'Videos'){

                $validator = Validator::make($request->all(), [
                    'files.*'=>'mimes:avi,flv,wmv,mp4,mov'
                ]);
        
                if ($validator->fails()) {
                    $request->session()->flash('error', 'File must be a video of type: avi, flv, wmv, mp4, mov');
                    return response()->json('error', 422);
                }
            }
            else{
                $validator = Validator::make($request->all(), [
                    'files.*'=>'image',
                ]);
        
                if ($validator->fails()) {
                    $request->session()->flash('error', 'File must be an image.');
                    return response()->json('error', 422);
                }
            }
            $images = $request->file('files');  
        }
        else
            return;

        $count = 0;
        foreach($images as $image){
            $gallery = new Image;
            $filename = time().'G'.$count++ . '.' . $image->getClientOriginalExtension();
            $location = public_path('gallery_images/');
            $image->move($location,$filename);
            $img = Img::make(public_path('gallery_images/'.$filename));
            $img->insert(public_path('backend/systemimages/logo.png'), 'bottom-right', 15, 15);
            $img->save(public_path('gallery_images/'.$filename));
            $gallery->name = $filename;
            $gallery->gallery_id = $id;
            $gallery->save();

        }
        if($cat == 'Videos')
            $request->session()->flash('success', 'Videoss Uploaded');
        else
            $request->session()->flash('success', 'Images Uploaded');
        return;
    }
    
    public function imageDelete($album_id, $image_id, Request $request){

        $image = Image::find($request->image_id);
        unlink(public_path('gallery_images'.'/'.$image->name));
        $image->delete();

        $request->session()->flash('success','Image Succesfully Deleted');
        return back(); 
    }
}
