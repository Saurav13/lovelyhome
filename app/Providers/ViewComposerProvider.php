<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewComposerProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {   
        view()->composer(
            'layouts.app',
            'App\Http\ViewComposers\FrontendComposer'
        );
        view()->composer(
            'admin.main',
            'App\Http\ViewComposers\AdminMenuComposer'
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
