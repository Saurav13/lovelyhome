<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Download;
use App\Downloadscategory;
use Auth;

class DownloadsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            
            if (Auth::user()->roles()->where('title', '=', 'Downloads')->exists()){
                return $next($request);
            }
            else
                abort(403);
        });
    }
    
    public function index()
    {
        $downloads = Download::join('downloadscategories','downloads.category_id','downloadscategories.id')->select('downloads.*','downloadscategories.category_name')->orderBy('id','desc')->get();
        $cat=Downloadscategory::all();
    	return view ('downloads.index')->with('downloads',$downloads)->with('cat',$cat);
    }

    public function store(Request $request)
    {

    	$this->validate($request, array(
            'title'=>'required|max:255',
            'category'=>'required|exists:downloadscategories,id',
            'file'=>'required|max:10000', 
        ));
    	$downloads=new Download;
        $downloads->title=$request->title;
        $downloads->category_id=$request->category;
    	if($request->hasFile('file')){
     	    $file = $request->file('file');
            $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
            $path = public_path('downloads/');
            $file->move($path,$filename);
            $downloads->file = $filename;
     	}
     	$downloads->save();
     	session()->flash('message','Downloadable resources were successfully added');
     	return redirect()->route('downloads.index'); 

    }

  public function search()
    {
        $search = \Request::get('searchDownloads');
        $cat=Downloadscategory::all();
        $downloads = Download::join('downloadscategories','downloads.category_id','downloadscategories.id')->select('downloads.*','downloadscategories.category_name')->orderBy('id','desc')->where('title','like','%'.$search.'%')->get();
        return view('downloads.search')->with('cat',$cat)->with('downloads',$downloads);

    }
    public function update(Request $request)
    {
    		$this->validate($request, array(
                    'edittitle'=>'required|max:255',
                    'category'=>'required|exists:downloadscategories,id',
     	           ));
        $downloads=Download::find($request->id);
        $downloads->title=$request->edittitle;
        $downloads->category_id=$request->category;
        if($request->hasFile('file')){
            unlink('downloads/'.$downloads->file);
             $file = $request->file('file');
            $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
            $path = public_path('downloads/'.$filename);
            $file->move($path,$filename);
            $downloads->file = $filename;
        }
        $downloads->save();
        session()->flash('success','Downloadble resource was Updated successfully!');

        return redirect()->route('downloads.index'); 

    }

    public function destroy(Request $request)
    {
    	$downloads = Download::findorFail($request->id);
         if($downloads->file!=null){
         unlink('downloads/'.$downloads->file);
         }
         $downloads->delete();
         return redirect()->route('downloads.index');
    }

    public function storeCat(Request $request)
    {
        $this->validate($request, array(
            'category_title'=>'required|max:255',
        ));
        $cat=new Downloadscategory;
        $cat->category_name=$request->category_title;
        $cat->save();
        session()->flash('success','Category for downloads was added successfully');
        return redirect()->route('downloads.index');
    }

    public function deletecat(Request $request)
    {
        $cat=DownloadsCategory::findorFail($request->id);
        $cat->delete();
        return redirect()->route('downloads.index');
    }

    public function updatecat(Request $request)
    {
        $this->validate(
            request(),
            [
                'category_title'=>'required|max:255',
            ]);
        
        $cat = Downloadscategory::find($request->get('id'));
        $cat->category_name = $request->get('category_title');
        $cat->save();
        session()->flash('message','Category Succesfully Edited');
        return redirect()->route('downloads.index');
    }
}
