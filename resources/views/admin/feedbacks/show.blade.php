@extends('admin.main')
@section('title','| Feedback')
@section('body')
<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-xs-12 mb-1">
                <h2 class="content-header-title">Feedback #{{ $feedback->id }}</h2>
            </div>
            <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
                <div class="breadcrumb-wrapper col-xs-12">
                    <ol class="breadcrumb">
                        <a class="btn btn-primary btn-min-width mr-1 mb-1 " href="{{ route('feedbacks.index') }}">See all feedbacks</a>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section class="card">
                <div id="invoice-template" class="card-block">
                    <!-- Invoice Company Details -->
                    <div id="invoice-company-details" class="row">
                        <div class="col-md-6 col-sm-12 text-xs-center text-md-left">
                            <ul class="px-0 list-unstyled">
                                <li><strong>Name      : </strong>{{ $feedback->name }}</li>
                                <li><strong>Email     : </strong>{{ $feedback->email }}</li> 
                                <li><strong>Phone No. : </strong>{{ $feedback->phone }}</li> <br>
                                <li><span class="text-muted">Received Date :</span> {{ date('M j, Y', strtotime($feedback->created_at)) }}</li>
                            </ul>
                        </div>
                    </div>
                    <!--/ Invoice Company Details -->

                    <!-- Invoice Footer -->
                    <div id="invoice-items-details" class="pt-2">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="text-xs-center">
                                    <strong>Subject : </strong>{{ $feedback->subject }}
                                </div><br>
                                <p class="lead">Message:</p>
                                <p>{{ $feedback->message }}</p>
                            </div>
                        </div>
                    </div>
                    <!--/ Invoice Footer -->

                </div>
            </section>
        </div>
    </div>
</div>
@endsection
