@extends('admin.main')
@section('title','| News')

@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

@section('body')
@include('tinymcebox')
<style type="text/css">
  .index-content a:hover{
      color:black;
      text-decoration:none;
  }
   .index-content button:hover{
      color:black;
      text-decoration:none;
  }
  .index-content{
      margin-bottom:20px;
      padding:50px 0px;
      
  }
  .index-content .row{
      margin-top:20px;
  }
  .index-content a{
      color: black;
  }
  .index-content button{
      color: black;
  }
  .index-content .card{
      background-color: #FFFFFF;
      padding:0;
      -webkit-border-radius: 4px;
      -moz-border-radius: 4px;
      border-radius:4px;
      box-shadow: 0 4px 5px 0 rgba(0,0,0,0.14), 0 1px 10px 0 rgba(0,0,0,0.12), 0 2px 4px -1px rgba(0,0,0,0.3);

  }
  .index-content .card:hover{
      box-shadow: 0 16px 24px 2px rgba(0,0,0,0.14), 0 6px 30px 5px rgba(0,0,0,0.12), 0 8px 10px -5px rgba(0,0,0,0.3);
      color:black;
  }
  .index-content .card img{
      width:100%;
      border-top-left-radius: 4px;
      border-top-right-radius: 4px;
  }
  .index-content .card h4{
      margin:20px;
  }
  .index-content .card p{
      margin:20px;
      opacity: 0.65;
  }
  .index-content .blue-button{
      width: 100px;
      -webkit-transition: background-color 1s , color 1s; /* For Safari 3.1 to 6.0 */
      transition: background-color 1s , color 1s;
      min-height: 20px;
      background-color: #002E5B;
      color: #ffffff;
      border-radius: 4px;
      text-align: center;
      font-weight: lighter;
      margin: 0px 20px 15px 20px;
      padding: 5px 0px;
      display: inline-block;
  }
  .index-content .blue-button:hover{
      background-color: #dadada;
      color: #002E5B;
  }

    .index-content .red-button{
      width: 100px;
      -webkit-transition: background-color 1s , color 1s; /* For Safari 3.1 to 6.0 */
      transition: background-color 1s , color 1s;
      min-height: 20px;
      background-color: #FF0000;
      color: #ffffff;
      border-radius: 4px;
      text-align: center;
      font-weight: lighter;
      margin: 0px 20px 15px 20px;
      padding: 5px 0px;
      display: inline-block;
  }

   .index-content .red-button:hover{
      background-color: #dadada;
      color: #002E5B;
  }

  .card-down {
      min-height: 200px;
  }
  @media (max-width: 768px) {

      .index-content .col-lg-4 {
          margin-top: 20px;
      }
  }
</style>

<!--  -->
<div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-body">
          <!-- Basic example section start -->
<section id="addnotice">  
  <div class="row">
      <div class="col-md-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title" ><a data-action="collapse"><button  class="btn btn-md btn-primary"><i class="icon-plus4" aria-hidden="true"></i> Add news </button></a></h4>
                  <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                  <div class="heading-elements">
                      <ul class="list-inline mb-0">
                        
                          <li><a data-action="collapse"><i class="icon-plus4"></i></a></li>
                          <li><a data-action="expand"><i class="icon-expand2"></i></a></li>

                      </ul>
                  </div>
              </div>
   
                <div class="card-body collapse">
                  
                  <div class="card-block ">
                      <form class="form" method="POST" action="{{route('news.store')}}" enctype="multipart/form-data">
                          {{csrf_field()}}
                          <div class="row">
                              <div class="form-body">
                                <div class="form-group col-sm-12">
                                  <label for="title">Title*</label>
                                  <input type="text" id="title" class="form-control" placeholder="Title for the news..." name="title" value="{{old('title')}}" required>
                                </div>

                                <div class="form-group col-sm-12">
                                  <label for="detail">News Description*</label>
                                   <textarea id="detail" class="form-control" name="description">{{old('description')}}
                                   </textarea>
                                </div>

                                <div  class="form-group col-sm-12">
                                    <label for="image">Select Featured Image for News</label>
                                  <input type="file" name="image" class="form-control-file">
                                </div>

                                <div class="form-group col-sm-12">
                                   <button type="submit" class="btn btn-primary" id="add">
                                    <i class="icon-check2">Add News</i> 
                                </button>
                                </div>  
                              </div>
                            </div>
                          </form>
                          </div>
                      
                  </div>
              </div>
          </div>
      </div>
      

      
<div class="index-content" style="padding-top: 0px;">
    <div class="container">
     @foreach($news as $s) 
            <a href="{{route('news.show',$s->id)}}">
                <div class="col-lg-4" style="padding-bottom: 20px; padding-top: 20px;">
                    <div class="card" style="margin-bottom: 0px;">
                        <img src="{{asset('news-images'.'/'.$s->image)}}">
                        <h4>{!! str_limit($s->title, $limit = 25, $end = '..') !!}</h4>
                          <p>Lovely Home | <i class="fa fa-clock-o" aria-hidden="true"></i>{{$s->created_at->diffForHumans()}}</p>
                       <a href="{{route('news.edit',$s->id)}}" class="blue-button">Edit</a>
                       <a href="#" id="delete{{$s->id}}" class="red-button">Delete</a>
                    </div>
                </div>
            </a>
    @endforeach        
    </div>
    
</div>
</section>
</div>
</div>
</div>





@endsection
@section('js')
    
    <script type="text/javascript">
  $(document).ready(function() {       
        $("[id*='delete']").click(function(){
          var id = $(this).attr("id").slice(6);
          console.log(id);
          swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
            if (result.value) {
             // console.log(result.value)
              $.post("{{route('news.delete')}}",{id:id,_token:"{{csrf_token()}}"},function(data){ 
          swal({
            title:"Deleted Successfully",
            type:"success"
          }).then(function(){
            window.location.reload();
          })
        })
            }
            })

        });
    });
</script>
@endsection