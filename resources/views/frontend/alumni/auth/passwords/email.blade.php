@extends('layouts.app')

@section('body')
@section('title','| Alumni Password Reset')
    <style>
        
        #alumni_email_form .has-error{
            border: 3px solid #b70404c7;
        }
    </style>
    <section class="authorization_page">
                            
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10 dot">
                <h2 class="section-title">Alumni Password Reset</h2>
                <p class="section-description pb_20">This will send you a link to reset your password.</p>
            </div>
            <div class="col-md-1"></div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if (session('status'))
                    <div class="help-block" style="text-align:  center;font-size:  15px;color: green;">
                        {{ session('status') }}
                    </div>
                @endif
                <form id="alumni_email_form" method="POST" action="{{ route('alumni_password.email') }}">
                    {{ csrf_field() }}
                    <div class="reg_box">
                        <input class="{{ $errors->has('email') ? 'has-error' : '' }}" id="new_email" name="email" type="email" placeholder="Enter Your email" value="{{ old('email') }}" required="required">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong style="color:  #b70404c7;">{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        
                    </div>
                    <button class="submit" type="submit">Send Reset Link</button>
                </form>
            </div>
        </div>
    </section>
@endsection