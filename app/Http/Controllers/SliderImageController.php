<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sliderimage;
use Image;
use Session;
use Auth;

class SliderImageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            
            if (Auth::user()->roles()->where('title', '=', 'Slider Configuration')->exists()){
                return $next($request);
            }
            else
                abort(403);
        });
    }

    public function index()
    {
        $img=Sliderimage::all();
        return view('slider.index')->with('img',$img);
    }

    public function store(Request $request)
    {
       $this->validate($request, array(
    	    	           'image'=>'mimes:jpeg,jpg,png,gif|required|max:10000', 
    	    	           ));
    	$img=new Sliderimage;
    	$img->title=$request->title;
        $img->description=$request->description;
    	if($request->hasFile('image')){
            $file = $request->file('image');
            $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
            $path = public_path('slider-images/'.$filename);
            Image::make($file)->resize(1100,380)->save($path);
            $image= Image::make(public_path('slider-images/'.$filename));
            $image->insert(public_path('backend/systemimages/logo.png'), 'bottom-right', 20, 30);
            $image->save(public_path('slider-images/'.$filename));
            $img->image = $filename;
    	}
    	$img->save();
    	Session::flash('success','Slider Image was added successfully!');

    	return redirect()->route('admin.slider'); 
    }

    public function update(Request $request)
    {
        $this->validate($request, array(
                           'title'=>'required|max:255',
                           'image'=>'mimes:jpeg,jpg,png,gif|max:10000', 
        ));
        $img=Sliderimage::find($request->id);
        $img->title=$request->title;
        $img->description=$request->description;
        if($request->hasFile('image')){
            unlink('slider-images/'.$img->image);
             $file = $request->file('image');
            $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
            $path = public_path('slider-images/'.$filename);
            Image::make($file)->resize(1100, 380)->save($path);
            $image = Image::make(public_path('slider-images/'.$filename));
            $image->insert(public_path('backend/systemimages/logo.png'), 'bottom-right', 20, 30);
            $image->save(public_path('slider-images/'.$filename));
            $img->image = $filename;
        }
        $img->save();
        Session::flash('success','Slide Image was Updated successfully!');

        return redirect()->route('admin.slider'); 
    }

    public function destroy(Request $request)
    {
        $img=Sliderimage::findorFail($request->id);
        unlink('slider-images/'.$img->image);
        $img->delete();
        return redirect()->route('admin.slider');
    }
}
