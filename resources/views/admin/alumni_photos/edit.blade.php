@extends('admin.main')
@section('title','| Edit Alumni Photo')
@section('body')
<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" ><a data-action="collapse">Edit Alumni Photo</a></h4>
                            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a href="{{ route('alumni_photos.index') }}" title="Back"><i class="fa fa-arrow-left"></i></a></li>
                                    <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="card-body collapse in">                    
                            <div class="card-block ">
                                <form class="form" method="POST" action="{{route('alumni_photos.update',$alumni_photo->id)}}" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="PUT" >
                                    <div class="row">
                                        <div class="form-body">
                                            <div class="form-group col-sm-12">
                                                <label for="name">Name</label>
                                                <input type="text" id="name" class="form-control" placeholder="example - John Doe" name="name" value="{{ $alumni_photo->name }}" required>
                                            </div>

                                            <div class="form-group col-sm-12">
                                                <label for="designation">Batch</label>
                                                <input type="text" id="designation" class="form-control" placeholder="2018" name="batch" value="{{ $alumni_photo->batch }}" required>
                                            </div> 

                                            <div  class="form-group col-sm-12">
                                                <label for="image">Select Image (Optional)</label>
                                                <input type="file" name="photo" class="form-control-file">
                                            </div>

                                            <div class="form-group col-sm-12">
                                                <button type="submit" class="btn btn-md btn-success" id="add">
                                                    <i class="icon-check2">Save Photo</i> 
                                                </button>
                                            </div>  
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>