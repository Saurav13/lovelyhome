@extends('admin.main')
@section('title','| Notice')
@section('body')


<!--  -->
<div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
       

          <div class="content-header-right col-sm-6" >
            <form class="form " method="get" action="{{URL::to('/admin/notice/search')}}" >
            
            <input type="text" class="form-control"  id="searchNotice" name="searchNotice" placeholder="Search Notices">
            
            <div class="form-group" >
              <button type="submit" hidden class="btn btn-primary" id="add"> Search
              </button>
            </div>             
            </form>

          </div>
          
      
        </div>
        
        <div class="content-body">
          <!-- Basic example section start -->
<section id="addnotice">  
  <div class="row">
      <div class="col-md-12">
          <div class="card">
              <div class="card-header">
                 <h4 class="card-title" ><a data-action="collapse"><button  class="btn btn-md btn-primary"><i class="icon-plus4" aria-hidden="true"></i> Add Notice </button></a></h4>
                  <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                  <div class="heading-elements">
                      <ul class="list-inline mb-0">
                        @if(Request::is('admin/notice/search*'))
                    
                        <a href="{{ URL::to('admin/notice') }}"  class="btn btn-success" id="newback">
                       Back 
                    @endif
                  </a>
                          <li><a data-action="collapse"><i class="icon-plus4"></i></a></li>
                          <li><a data-action="expand"><i class="icon-expand2"></i></a></li>

                      </ul>
                  </div>
              </div>
  
                <div class="card-body collapse">
                  
                  <div class="card-block ">
                      <form class="form" method="POST" action="{{URL::to('/admin/notice/create')}}">
                          {{csrf_field()}}
                          <div class="row">
                              <div class="form-body">
                                <div class="form-group col-sm-12">
                                  <label for="title">Title</label>
                                  <input type="text" id="title" class="form-control" placeholder="title" name="title" required>
                                </div>

                                <div class="form-group col-sm-12">
                                  <label for="detail">Details</label>
                                   <textarea id="detail" class="form-control" id="detail" name="detail"></textarea>
                                </div>

                                <div class="form-group col-sm-12">
                                   <button type="submit" class="btn btn-success" id="add">
                                    <i class="icon-check2">Add Notice</i> 
                                </button>
                                </div>  
                              </div>
                            </div>
                          </form>
                          </div>
                      
                  </div>
              </div>
          </div>
      </div>
      
@if (Session::has('message'))
    <div class="alert alert-info alert-dismissable">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      {{ Session::get('message') }}
    </div>
@endif
      
<!--  -->
@if(count($notices)==null)

<h1>No notice available</h1>

@endif



@if(count($notices)!=null)
<div class="row match-height">
  @foreach($notices as $notice)
  <div class="col-xl-6 ">
      <div class="card">
        <div class="card-body ">
           <div class="card-block w3-margin" id="noticeShow">
            <div class="card-title">
            <h4  style="color: inherit; font-family: Atma,cursive">{{$notice->title}} </h4> <h6>on {{$notice->updated_at->toFormattedDateString()}} </h6>
            </div>
            <div class="{{$notice->status=='published'?'tag tag-success btn':'tag tag-warning btn'}}" id="NoticeStatus{{$notice->id}}"> {{$notice->status}} <br>
            </div>
            <div id="detail{{$notice->id}}">
            <p class="card-text" > {{str_limit(strip_tags($notice->detail),150)}}</p>
            </div>


            <br>
            <a href="javascript:void(0)"  class="btn btn-outline-purple btn-sm" data-toggle="modal" data-target="#myModal{{$notice->id}}">View </a>

            @include('notice.show')




            <a href="/admin/notice/{{$notice->id}}/edit"  class="btn btn-outline-blue btn-sm" style="">Edit</a>

            <a href="javascript:void(0)" id="delete{{$notice->id}}" class="btn btn-outline-red btn-sm">Delete</a>

          </div>
        </div>
      </div>
  </div>
  @include('notice.show')
  @endforeach
</div>
@endif






</section>
</div>
</div>
</div>





@endsection
@section('js')
    
    <script type="text/javascript">
  $(document).ready(function() {
    
        $("[id*='NoticeStatus'").click(function(){
          var id = $(this).attr("id").slice(12);
          //jquery bata ajax 
          $.post("{{URL::to('admin/notice/changeStatus')}}",{id:id,_token:"{{csrf_token()}}"},function(data){ 
              console.log(data); 
          swal({
            title:"Notice Status Changed Successfully",
            type:"success"
          }).then(function(){
            window.location.reload();
          })
        })
      
          });
        

        
        $("[id*='delete']").click(function(){
          var id = $(this).attr("id").slice(6);
          swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
            if (result.value) {
             // console.log(result.value)
              $.post("{{URL::to('admin/notice/delete')}}",{id:id,_token:"{{csrf_token()}}"},function(data){ 
          swal({
            title:"Deleted Successfully",
            type:"success"
          }).then(function(){
            window.location.reload();
          })
        })
            }
            })

        });
    });
</script>
@endsection