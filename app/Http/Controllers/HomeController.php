<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function news()
    {
        
        $news=News::latest()->paginate('8');
        // foreach($news->split(4) as $n)
        // {
            // dd($n);
        // }
        
        return view('frontend.news',compact('news'));
    }
    
}
