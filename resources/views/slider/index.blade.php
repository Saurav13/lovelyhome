@extends('admin.main')
@section('title','| Slider Image')
@section('body')
<style type="text/css">

    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: auto; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
    }
  
     .modal-img {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
    }
  
    /* Modal Content (Image) */
    .modal-content-image {
        margin: auto;
        display: block;
        width: 80%;
        height: 420px;
        max-width: 720px;
    }
  
    /* Caption of Modal Image (Image Text) - Same Width as the Image */
    #caption {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        text-align: center;
        color: #ccc;
        padding: 0px;
        height: 100px;
    }
  
    /* Add Animation - Zoom in the Modal */
    .modal-content-image, #caption { 
        -webkit-animation-name: zoom;
        -webkit-animation-duration: 0.6s;
        animation-name: zoom;
        animation-duration: 0.6s;
    }
  
    @-webkit-keyframes zoom {
        from {-webkit-transform:scale(0)} 
        to {-webkit-transform:scale(1)}
    }
  
    @keyframes zoom {
        from {transform:scale(0)} 
        to {transform:scale(1)}
    }
  
    /* The Close Button */
    .close {
        position: absolute;
        top: 15px;
        right: 35px;
        color: #f1f1f1;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
    }
  
    .close:hover,
    .close:focus {
        color: #bbb;
        text-decoration: none;
        cursor: pointer;
    }
  
    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px){
        .modal-content {
            width: 100%;
        }
    }
    
  </style>
  
  @section('body')
  
  <style type="text/css">
    
  
    h1 {
      color: black;
      padding-left: 10px;
    }
  
     h2 {
      color: black;
      padding-left: 10px;
    }
  
    #hover {
      color: rgba(188, 175, 204, 0.9);
    }
  
    h2#testimonials {
      color: #fffae3;
    }
  
    div#all {
      width: 100%;
      height: 100%;
    }
  
  
    /* generic css */
  
    .view {
      margin: 10px;
      float: left;
      border: 10px solid #fff;
      overflow: hidden;
      position: relative;
      text-align: center;
      box-shadow: 1px 1px 2px #e6e6e6;
      cursor: default;
      background: #fff url(../images/bgimg.jpg) no-repeat center center
    }
  
    .view .mask,
    .view .content {
      width: 300px;
      height: 200px;
      position: absolute;
      overflow: hidden;
      top: 0;
      left: 0
    }
  
    .view img {
      display: block;
      position: relative
    }
  
    .view h2 {
      text-transform: uppercase;
      color: #fff;
      text-align: center;
      position: relative;
      font-size: 17px;
      font-family: Raleway, serif;
      padding: 10px;
      /*background: rgba(0, 0, 0, 0.8);*/
      margin: 20px 0 0 0
    }
  
    .view p {
      font-family: Merriweather, serif;
      font-style: italic;
      font-size: 14px;
      position: relative;
      color: #fff;
      padding: 0px 20px 0px;
      text-align: center
    }
  
    .view a.info {
      display: inline-block;
      text-decoration: none;
      padding: 7px 14px;
      background: #000;
      color: #fff;
      font-family: Raleway, serif;
      text-transform: uppercase;
      box-shadow: 0 0 1px #000
    }
  
    .view a.info:hover {
      box-shadow: 0 0 5px #000
    }
  
  
    /*1*/
  
    .view-first img {
      /*1*/
      transition: all 0.2s linear;
      width: 300px;
      height: 200px;
    }
  
    .view-first .mask {
      opacity: 0;
      background-color: rgba(58, 1, 132, 0.44);
      transition: all 0.4s ease-in-out;
    }
  
    .view-first h2 {
      transform: translateY(-100px);
      opacity: 0;
      font-family: Raleway, serif;
      transition: all 0.2s ease-in-out;
    }
  
    .view-first p {
      transform: translateY(100px);
      opacity: 0;
      transition: all 0.2s linear;
    }
  
    .view-first a.info {
      opacity: 0;
      transition: all 0.2s ease-in-out;
    }
  
  
    /* */
  
    .view-first:hover img {
      transform: scale(1.1);
    }
  
    .view-first:hover .mask {
      opacity: 1;
    }
  
    .view-first:hover h2,
    .view-first:hover p,
    .view-first:hover a.info {
      opacity: 1;
      transform: translateY(0px);
    }
  
    .view-first:hover p {
      transition-delay: 0.1s;
    }
  
    .view-first:hover a.info {
      transition-delay: 0.2s;
    }
  
    </style>
  
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,700|Merriweather' rel='stylesheet' type='text/css'>
<div class="app-content content container-fluid">
    <div class="content-wrapper">
      <div class="content-body">
            <h3 style="font-family: 'Raleway', sans-serif;"> Manage <span id="hover">Slider Images</span> <span><button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#addImage"><i class="fa fa-plus" aria-hidden="true"></i> Add Images</button></span></h3>
            <div id="slider" class="row">
            @foreach($img as $s)
              <div class="view view-first">
                <img src="{{asset('slider-images'.'/'.$s->image)}}" id="img{{$s->id}}" />
                <div class="mask">
                  <h2 id="cattitle{{$s->id}}">{{$s->title}}</h2>
                  <input type="hidden" name="description" value="{{$s->description}}" id="descriptionimg{{$s->id}}">
                  <a href="#" class="info"  data-toggle="modal" data-target="#myModal" cid={{$s->id}} id="view{{$s->id}}">View</a>
                  <a href="#" class="info"  data-toggle="modal" data-target="#imageeditModal" cid={{$s->id}} id="editt{{$s->id}}">Edit</a>
                  <a cid={{$s->id}} id="delete{{$s->id}}" class="info">Delete</a>
                </div>
              </div>
            @endforeach  
            </div>
      </div>
    </div>
</div>
@section('js')
<script type="text/javascript">
    $(document).ready(function() {       
        $("[id*='delete']").click(function(){
          var id = $(this).attr("id").slice(6);
          swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
            if (result.value) {
              $.post("{{route('slider.destroy')}}",{id:id,_token:"{{csrf_token()}}"},function(data){ 
          swal({
            title:"Deleted Successfully",
            type:"success"
          }).then(function(){
            window.location.reload();
          })
        })
            }
        })
    });
});
</script>
      
      <script>
        $(document).ready(function(){
          $('[id *= "view"]').click(function(){
      
            var id= $(this).attr('cid');
            var title = $('#cattitle'+id).html();
            var picture = $('#img'+id).attr('src');
            console.log(picture);
            var description = $('#descriptionimg'+id).val();
            console.log(description);
            $('#caption').empty();
            $('#caption').append('<h4>'+title+'</h4><p>'+description);
            $('#img01').attr('src',picture);

          });
        });
      
       
      </script>
      
      {{-- edit --}}
      <script>
        $(document).ready(function(){
          $('[id *= "editt"]').click(function(){
      
            var id= $(this).attr('cid');
            var title = $('#cattitle'+id).html();
            var description = $('#descriptionimg'+id).val();
            $('#titleedit').val(title);
            $('#descriptionedit').val(description);
            $('#hiddenid').val(id);
          });
        });
      
       
      </script>
      
      
      
      
      
      @endsection
      
      {{-- Add Image --}}
      <div id="addImage" class="modal fade" role="dialog">
        <div class="modal-dialog">
      
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Add Image</h4>
            </div>
            <div class="modal-body">
            <form action="{{route('slider.store')}}" enctype="multipart/form-data" method="POST">
              {{csrf_field()}}
              <div class="form-group">
                <label for="image">Image*:</label>
                <input type="file" name="image" class="form-control-file">
              </div>
              <div class="form-group">
                <label for="title">Image Title(Optional):</label>
                <input type="text" name="title" placeholder="Title of the slider Image" class="form-control" value="{{old('title')}}">
              </div>
              <div class="form-group">
                <label for="description">Description(Optional):</label>
                <textarea class="form-control" rows="5" id="description" placeholder="Small description of the slider image. Maximum 250 characters is appropriate" name="description">{{old('description')}}</textarea>
              </div>
              
              <div class="form-group">
                <input type="submit" name="submit" value="Add Image" class="btn btn-block btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
      </div>
      {{-- End of add image --}}
      
      {{-- Edit Image --}}
      <div id="imageeditModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
      
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Edit Image</h4>
            </div>
            <div class="modal-body">
            <form action="{{route('slider.update')}}" enctype="multipart/form-data" method="POST">
              {{csrf_field()}}
              <div class="form-group">
                <label for="title">Image Title:</label>
                <input type="text" name="title" class="form-control" id="titleedit">
              </div>
              <div class="form-group">
                <label for="description">Description:</label>
                <textarea class="form-control" rows="5" id="descriptionedit" name="description"></textarea>
              </div>
              <div class="form-group">
                <label for="image">Image:</label>
                <input type="file" name="image" class="form-control">
              </div>
              <div class="form-group">
                <input type="submit" name="submit" value="Add Image" class="btn btn-block btn-success">
                <input type="hidden" id="hiddenid" name="id" value="">
              </div>
            </form>
          </div>
        </div>
      </div>
      </div>
      {{-- End edit image --}}
      
      
      <div id="myModal" class="modal" style="padding-top: 100px;">
      
        <!-- The Close Button -->
        <button type="button" class="close" data-dismiss="modal" style="color: white;">&times;</button>
      
      
        <!-- Modal Content (The Image) -->
        <img class="modal-content-image" id="img01">
      
        <!-- Modal Caption (Image Text) -->
        <div id="caption"></div>
        
      </div>
      



@endsection