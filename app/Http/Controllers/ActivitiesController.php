<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Activity;
use Session;
use Image;
use Auth;

class ActivitiesController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            
            if (Auth::user()->roles()->where('title', '=', 'Activities')->exists()){
                return $next($request);
            }
            else
                abort(403);
        });
    }

    public function index()
    {
        $activities=Activity::all();
        return view('admin.activities.index')->with('activities',$activities);
    }

    public function store(Request $request)
    {
       $this->validate($request, array(
                           'image'=>'mimes:jpeg,jpg,png,gif|required|max:10000', 
                           'title'=>'required',
                           'category'=>'required',
    	    	           ));
    	$activities=new Activity;
    	$activities->title=$request->title;
        $activities->category=$request->category;
    	if($request->hasFile('image')){
            $file = $request->file('image');
            $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
            $path = public_path('activities-images/'.$filename);
            Image::make($file)->resize(480,360)->save($path);
            $image= Image::make(public_path('activities-images/'.$filename));
            $image->insert(public_path('backend/systemimages/logo.png'), 'bottom-right', 20, 30);
            $image->save(public_path('activities-images/'.$filename));
            $activities->image = $filename;
    	}
    	$activities->save();
    	Session::flash('success','Activity was added successfully!');

    	return redirect()->route('admin.activities'); 
    }
    public function update(Request $request)
    {
        $this->validate($request, array(
            'image'=>'mimes:jpeg,jpg,png,gif|max:10000', 
            'title'=>'required',
            'category'=>'required',
        ));
        $activities=Activity::find($request->id);
        $activities->title=$request->title;
        $activities->category=$request->category;
        if($request->hasFile('image')){
            unlink('activities-images/'.$img->image);
             $file = $request->file('image');
            $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
            $path = public_path('activities-images/'.$filename);
            Image::make($file)->resize(480, 360)->save($path);
            $image = Image::make(public_path('activities-images/'.$filename));
            $image->insert(public_path('backend/systemimages/logo.png'), 'bottom-right', 20, 30);
            $image->save(public_path('activities-images/'.$filename));
            $activities->image = $filename;
        }
        $activities->save();
        Session::flash('success','Activity was Updated successfully!');

        return redirect()->route('admin.activities'); 
    }

    public function destroy(Request $request)
    {
        $activities=Activity::findorFail($request->id);
        unlink('activities-images/'.$activities->image);
        $activities->delete();
        return redirect()->route('admin.activities');
    }
}
