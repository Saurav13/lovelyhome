@extends('admin.main')
@section('body')
@section('title','| Search Downloads')

<div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
       

          <div class="content-header-right col-sm-6" >
            <form class="form " method="get" action="{{URL::to('/admin/downloads/search')}}" >
            
            <input type="text" class="form-control"  id="searchDownloads" name="searchDownloads" placeholder="Search Downloads">
            
            <div class="form-group" >
              <button type="submit" hidden class="btn btn-primary" id="add"> Search
              </button>
            </div>             
            </form>

          </div>
          
      
        </div>
        
        <div class="content-body">
          <!-- Basic example section start -->
        

<section id="addnotice">  
  <div class="row">
      <div class="col-md-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title" ><a data-action="collapse"><button  class="btn btn-md btn-primary"><i class="icon-plus4" aria-hidden="true"></i> Add Downloadable resources </button></a></h4>
                  <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                  <div class="heading-elements">
                      <ul class="list-inline mb-0">
                            <a href="{{ url()->previous() }}" class="btn btn-labeled btn-default">
                                    <span class="btn-label"><i class="fa fa-arrow-left" aria-hidden="true"></i></span> Back</a>
                          <li><a data-action="collapse"><i class="icon-plus4"></i></a></li>
                          <li><a data-action="expand"><i class="icon-expand2"></i></a></li>

                      </ul>
                  </div>
              </div>
   
                <div class="card-body collapse">
                  
                  <div class="card-block ">
                      <form class="form" method="POST" action="{{route('downloads.store')}}" enctype='multipart/form-data'>
                          {{csrf_field()}}
                          <div class="row">
                              <div class="form-body">
                                <div class="form-group col-sm-12">
                                  <label for="title">Title</label>
                                  <input type="text" id="title" class="form-control" placeholder="title" name="title" required>
                                </div>

                                <div class="form-group col-sm-12">
                                    <select name="category" class="form-control">
                                        <option disabled selected value>Choose Category</option>
                                        @foreach($cat as $c)
                                        <option value="{{$c->id}}">{{$c->category_name}}</option>
                                        @endforeach
                                    </select>
                                  </div>

                                <div class="form-group col-sm-12">
                                  <input type="file" name="file" class="form-control-file">
                                </div>

                                <div class="form-group col-sm-12">
                                   <button type="submit" class="btn btn-success" id="add">
                                    <i class="icon-check2">Add to Downloads</i> 
                                </button>
                                </div>  
                              </div>
                            </div>
                          </form>
                          </div>
                      
                  </div>
              </div>
          </div>
      </div>
      
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

@if(count($downloads)==null)
<h1>No downloadable resources found!</h1>
@endif

@if(count($downloads)!=null)
<div class="row match-height">
	@foreach($downloads as $d)
	<div class="col-xl-3 ">
      <div class="card">
        <div class="card-body ">
           <div class="card-block w3-margin" id="noticeShow">
            <div class="card-title">
            <h4  style="color: inherit; font-family: Atma,cursive" id="title{{$d->id}}">{{$d->title}}</h4> 
            <p id="cat{{$d->id}}">{{$d->category_name}}</p>
            <h6>Added {{$d->created_at->diffForHumans()}}  </h6>
            </div>

            <a href="{{ asset('downloads/'.$d->file)}}" download="{{$d->file}}" class="btn btn-sm btn-outline-green">Download</a>
            <button  class="btn btn-sm btn-outline-blue" data-toggle="modal" data-target="#myModal" cid={{$d->id}} id="update{{$d->id}}">Edit</button>
            <button  id="delete{{$d->id}}" cid={{$d->id}} class="btn btn-sm btn-outline-red">Delete</button>
          </div>
        </div>
      </div>
  </div>
  @endforeach
  
</div>
@endif
</section>
</div>
</div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Downloads</h4>
      </div>
      <div class="modal-body">
        <form class="form" method="POST" action="{{route('downloads.update')}}" enctype='multipart/form-data'>
          {{csrf_field()}}
          <div class="row">
            <div class="form-body">
              <div class="form-group col-sm-12">
                <label for="title">Title</label>
                <input type="text" name="edittitle" id="edittitle" class="form-control" placeholder="title" required>
              </div>

              <div class="form-group col-sm-12">
                  <select name="category" class="form-control" id="editcat">
                      @foreach($cat as $c)
                      <option value="{{$c->id}}">{{$c->category_name}}</option>
                      @endforeach
                  </select>
                </div>

              <div class="form-group col-sm-12">
                <input type="file" name="file" class="form-control-file">
              </div>
              
              <div class="form-group col-sm-12">
                <button type="submit" class="btn btn-success" id="add">
                  <i class="icon-check2">Save Changes</i> 
                </button>
              </div>  
            </div>
          </div>
          <input type="hidden" name="id" id="editid">
        </form>
      </div>
      
    </div>
    
  </div>
</div>
@section('js')

<script type="text/javascript">
  $(document).ready(function(){
    $("[id*='delete']").click(function(){
      var id = $(this).attr("id").slice(6);
      swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          // console.log(result.value)
          $.post("{{route('downloads.destroy')}}",{id:id,_token:"{{csrf_token()}}"},function(data){ 
            swal({
              title:"Deleted Successfully",
              type:"success"
            }).then(function(){
              window.location.reload();
            })
          })
        }
      })
      
    });
    $('[id *= "update"]').click(function(){
      var id= $(this).attr('cid');
      var title = $('#title'+id).html();
      $('#edittitle').val(title);
      $("#editcat option:contains('"+$('#cat'+id).html()+"')").attr('selected', 'selected');
      $('#editid').val(id);
    });
  });
</script>
@endsection
@endsection