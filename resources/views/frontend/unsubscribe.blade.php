@extends('layouts.app')
@section('title','| Unsubscribe')
@section('body')
    <section id="error_page" class="error_page">
        <div class="container">
            <div class="row">
                <form method="POST" id="unsubscribe_form" action="{{ route('unsubscribed',$subscriber->token) }}" class="col-md-12 error_block">
                    {{ csrf_field() }}
                    <h2>Unsubscribe?</h2>
                    <p id="unsubscribedResult">{{ $subscriber->email }} will no longer be subscribed to us.</p>

                    <button id="unsub_b_1" type="button" class="button"><a style="color:#fff" href="{{ route('home') }}">Cancel</a></button>
                    <button id="unsub_b_2" class="button" type="submit" style="background-color: #e65874;">Unsubscribe</button>
                                     
                </form>
            </div>
        </div>
    </section>
@endsection

@section('js')

    <script>
        $("#unsubscribe_form").submit(function(){ 
            var form = $(this); 
            
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({ 
                type: 'POST', 
                url: "{{ route('unsubscribed',$subscriber->token) }}",
                dataType: 'json',
                success: function(data){
                    $('#unsubscribedResult').html('You are no longer be subscribed to us.'); 
                    $('#unsub_b_2').remove(); 
                    $('#unsub_b_1 a').html('Go to Home'); 
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    location.reload();
                }
                            
            });
            return false; 
        });
    </script>
@endsection