<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\Testimonial;
use App\Teacher;
use App\News;
use App\Event;
use App\Download;
use Carbon\Carbon;
use App\Downloadscategory;
use App\Materials;
use App\Resources;
use App\Notices;
use App\EventCalender;
use App\Activity;
use App\Faq;
use App\ContactUsMessage;
use App\Feedback;
use App\Subscriber;

class HomeController extends Controller
{
    public function home()
    {
        $today=Carbon::now();
        $testimonials=Testimonial::all();
        $teachers=Teacher::all();
        $activities=Activity::all();
        $events=Event::where('event_date','>',$today->format('Y-m-d'))->orderBy('event_date','asc')->take(3)->get();
        $c=0;
        $notice=Notices::latest()->first();
        // dd($teachers->chunk(4));
        foreach ($teachers->chunk(4) as $t) {
            // if($c>0)
          
            // $c++;
            
        
           # code...
        }
        $news=News::orderBy('created_at','asc')->take(4)->get();
        return view('frontend.landing')->with('teachers',$teachers)->with('testimonials',$testimonials)->with('news',$news)->with('events',$events)->with('notice',$notice)->with('activities',$activities);
    }
    //Events
    public function events()
    {
        $today=Carbon::now();
        
        $mainevent=Event::where('event_date',$today->format('Y-m-d'))->where('end_time','>=',$today->format('H:i'))->orderBy('end_time','asc')->get();
        if(count($mainevent)==0){
            $mainevent=Event::where('event_date','>',$today->format('Y-m-d'))->orderBy('event_date','asc')->take(1)->get();
        }
        $events=Event::where('event_date','>=',$today->format('Y-m-d'))->orderBy('event_date','asc')->Paginate(3);
        // dd($events);
        return view('frontend.events')->with('mainevent',$mainevent)->with('events',$events);
    }

    public function SingleEvent($slug)
    {
        $event=Event::where('slug','=',$slug)->first();
        if(!$event) abort(404);
        return view('frontend.innerevent')->with('event',$event);
    }

    public function pastevents()
    {
        $today=Carbon::now();
        $mainevent=Event::where('event_date',$today->format('Y-m-d'))->where('end_time','>=',$today->format('H:i'))->orderBy('end_time','asc')->get();
        if(count($mainevent)==0){
            $mainevent=Event::where('event_date','>',$today->format('Y-m-d'))->orderBy('event_date','asc')->take(1)->get();
        }
        $events=Event::where('event_date','<',$today->format('Y-m-d'))->orderBy('event_date','asc')->paginate(6);
        return view('frontend.pastevents')->with('events',$events)->with('mainevent',$mainevent);
    }
    //Events End
    //News
    public function news()
    {
        $news=News::paginate(4);
        return view('frontend.news')->with('news',$news);
    }

    public function SingleNews($slug)
    {
        $news=News::where('slug','=',$slug)->first();
        $latestnews=News::orderBy('id','desc')->take(3)->get();
        return view('frontend.innernews')->with('news',$news)->with('latestnews',$latestnews);
    }
    //End News

    //Downloads
    public function downloads()
    {
        $categories=Downloadscategory::all();
        $downloads=Download::all();
        
        return view('frontend.downloads')->with('downloads',$downloads)->with('categories',$categories);
    }
    //Downloads Ends

    //Resources
    public function resources()
    {
        $categories=Resources::all();
        $materials=Materials::paginate(5);
        $allmaterials=Materials::all();
        return view('frontend.resources')->with('categories',$categories)->with('materials',$materials)->with('allmaterials',$allmaterials);
    }
    
    public function MaterialsSort($slug,$id)
    {
        $categories=Resources::all();
        $materials=Materials::where('id','=',$id)->paginate(10);   
        $allmaterials=Materials::all();
        return view('frontend.resources')->with('categories',$categories)->with('allmaterials',$allmaterials)->with('materials',$materials);
    }
    //End Resources
    
    //notice
    public function notice()
    {
        $notices=Notices::where('status','published')->latest()->paginate(6);
        return view('frontend.notices')->with('notices',$notices);
    }

    public function calender()
    {
        $events = [];
        $calender_events = EventCalender::all();
        foreach($calender_events as $c){
            $events[] = \Calendar::event(
                $c->event_name,
                true,
                new Carbon($c->event_date),
                new Carbon($c->event_date),
                $c->id,
                [
                    'description' => $c->description,
                    'type' => $c->type,
                    'textColor' => '#fff'
                ]
            );
        }
        
        $calendar = \Calendar::addEvents($events)
            ->setOptions([
                'firstDay' => 0,
            ])->setCallbacks([
                'header'=>"{'left': 'prev,next','center': 'title','right': ''}",                
                'eventLimit' => 1,
                'viewRender' => "function( view, element ){var today = moment().format('YYYY-MM-DD');$(view.el[0]).find('.fc-day[data-date=' + today + ']').attr('style','box-shadow: inset 0px 0px 0px 2px #e5322d; box-sizing: border-box;background: #fff;');}",
                'eventRender' => "function(event, element, view) { var dateString = event.start.format('YYYY-MM-DD');$(view.el[0]).find('.fc-day[data-date=' + dateString + ']').css('background', (event.type=='Holiday' ? '#0aa910':(event.type=='Half-Holiday'?'linear-gradient(to bottom, rgb(255, 221, 0) 0%, rgb(255, 235, 0) 50%, #e1ff3b 50%, rgb(10, 169, 16) 100%)':'rgb(255, 221, 0)')));}",
                'eventClick' => "function( event, jsEvent, view ) { $('#modalTitle').text(event.title);$('#modalBody').text(event.description);$.magnificPopup.open({items: {src: '#calevent'},type: 'inline',focus: '#name',mainClass: 'calendar_popup',closeOnBgClick:true,alignTop:true});}",
                
            ]);

        return view('frontend.calender', compact('calendar'));
    }
    public function faqs()
    {
        $faqs=Faq::all();
        return view('frontend.faqs',compact('faqs'));
    }

    public function contact_us(Request $request){
        
        $request->validate([
            'name'=>'required|max:255',
            'email'=> 'required|email',
            'phone'=> 'required',
            'subject'=> 'required',
            'message'=> 'required',
        ]);

        $contact_message = new ContactUsMessage;
        $contact_message->name = $request->name;
        $contact_message->email = $request->email;
        $contact_message->phone = $request->phone;
        $contact_message->subject = $request->subject;
        $contact_message->message = $request->message;
        $contact_message->save();
        
        return response()->json('success', 200);
        
    }

    public function feedback(Request $request){
        $request->validate([
            'name'=>'required|max:255',
            'email'=> 'required|email',
            'phone'=> 'required',
            'subject'=> 'required',
            'message'=> 'required',
        ]);

        $feedback = new Feedback;
        $feedback->name = $request->name;
        $feedback->email = $request->email;
        $feedback->phone = $request->phone;
        $feedback->subject = $request->subject;
        $feedback->message = $request->message;
        $feedback->save();
        
        return response()->json('success', 200);
    }

    public function subscribe(Request $request){
        $request->validate([
            'email'=> 'required|email|unique:subscribers,email'
        ],[
            'email.required'=> 'Please provide a valid email address.',
            'email.email'=> 'Please provide a valid email address.',
            'email.unique'=> 'Looks like you are already in our subscription list.'
        ]);

        $subscriber = new Subscriber();
        $subscriber->email = $request->email;
        $subscriber->token = Str::random(60);
        $subscriber->save();
        
        return response()->json('success', 200);
    }

    public function unsubscribe($token){
        $subscriber = Subscriber::where('token','=',$token)->first();
        
        if(!$subscriber) abort(404);
        return view('frontend.unsubscribe')->with('subscriber',$subscriber);
    }

    public function unsubscribed($token)
    {
        $subscriber = Subscriber::where('token','=',$token)->first();
        
        if(!$subscriber) abort(404);
        $subscriber->delete();
        return response()->json('success', 200);
    }
}
