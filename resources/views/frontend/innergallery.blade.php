@extends('layouts.app')

@section('body')
@section('title','| Gallery')


<section id="gallery_page" class="gallery_page">
    <div class="container">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10 dot">
                <h2 class="section-title"><a href="{{ URL::to('gallery') }}" style="color:#333333">{{ $album->name }}</a></h2>
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>
    <div class="container_fluent gallery_portfolio">
        <ul class="filter" hidden>
            <li class="active"><a href="#" id="album" data-filter=".album" >{{ $album->name }}</a></li>
        </ul>
        <div class="row_fluent">
            <div class="portfolio_grid">
                @include('frontend.partials.innergallery')                    
            </div>
            <div class="load_more">
                @if(count($images)==10)
                    <a style="cursor:pointer" id="more">Load More</a>
                @endif
            </div>
        </div>
    </div>
</section>

@stop

@section('js')
    <script>
        var last = '{{ $images->last()->id }}';
        $('#more').click(function(){
            if(last == 0) {
                $('#more').text('No more media');
                return;
            }

            $('#more').text('LOADING...');
            $.ajax({
                url: "{{ route('images.getmore',$album->slug) }}" ,
                type: "get",
                data : { itemId: last},
            })
            .done(function(response)
            {
                
                if(response.data == ''){
                    $('#more').text('No more media');
                    last = 0;
                }
                else{
                    last = response.last;
                    var $container = $('.portfolio_grid');
                    var $content = $(response.data);
                    $container.append( $content ).isotope( 'appended', $content )
                    $('#album').trigger('click');

                    $('.link').magnificPopup({
                        type:'image',
                        titleSrc: 'title',
                        mainClass: 'gallery_popup',
                        gallery:{enabled:true}
                    });

                    $('.popup-youtube').magnificPopup({
                        type: 'iframe',
                        mainClass: 'popup_close',
                        removalDelay: 160,
                        preloader: false,
                        fixedContentPos: false
                    });

                    $('#more').text('LOAD MORE'); 
                }

            })
            .fail(function(){
                location.reload();
            });
        });
    </script>
@stop