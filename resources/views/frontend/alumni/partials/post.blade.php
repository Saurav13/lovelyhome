@forelse($posts as $post)
<div class="blog_block wow fadeIn">
        <h3 class="blog_title">{{ $post->title }}</h3>
        <div class="row blog_info">
            <div class="col-md-2 col-sm-2 col-xs-12 pr_0">
                <div class="date" style="background:#49b450">
                    <ul>
                        <li class="number">{{ $post->created_at->day }}</li>
                        <li class="date_month">{{ date('M',strtotime($post->created_at)) }}<br>{{ $post->created_at->year }}</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12 prpl_0">
                <div class="author" style="padding: 8px 10px">
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <div class="post_img">
                                <img src="{{ asset('alumni_pp/'. $post->poster->photo) }}" alt="{{ $post->poster->name }}">
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <ul>
                                <li class="title">Posted by</li>
                                <li class="description"><a href="#">{{ $post->poster->name }}</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-7 col-sm-7 col-xs-12 pl_0">
                
            </div>
        </div>
        @if($post->image)
            <div class="blog_block_img">
                <img src="{{ asset('alumni_blogs/'.$post->image) }}" alt="{{ $post->title }}">
            </div>
        @endif

        <div class="blog_brief">
            <p>{{ strip_tags(str_limit($post->content, $limit = 300, $end = '...')) }}</p>
        </div>
        <div class="blog_link">
            <div class="blog_share social_icon t_white">
                <ul>
                    <li class="plr_10" style="margin-top: 2rem;"><a href="#"><i class="plr_10 pr_10 b_comment fa fa-comment">{{ $post->comments->count() }}</i></a></li>
                </ul>
            </div>
            <div class="button">
                <a href="{{ route('alumni.singlepost',$post->slug) }}">read more</a>
            </div>
        </div>
    </div>
@empty
    <p style="font-size:2rem;margin-top:50px;text-align: center;">No Posts Found</p>
@endforelse