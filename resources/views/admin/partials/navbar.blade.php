
 <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">

    <!-- navbar-fixed-top-->
    <nav class="header-navbar navbar navbar-with-menu navbar-fixed-top navbar-semi-dark navbar-shadow">
      <div id="load"></div>
      <div class="navbar-wrapper">
        <div class="navbar-header">
          <ul class="nav navbar-nav">
            <li class="nav-item mobile-menu hidden-md-up float-xs-left"><a class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="icon-menu5 font-large-1"></i></a></li>
            <li class="nav-item"><a href="index.html" class="navbar-brand nav-link"><img alt="branding logo" src="{{asset('backend/systemimages/logo.png')}}" data-expand="{{asset('backend/systemimages/logo.png')}}" data-collapse="{{asset('backend/systemimages/small-logo.png')}}" class="brand-logo" ></a></li>
            <li class="nav-item hidden-md-up float-xs-right"><a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container"><i class="icon-ellipsis pe-2x icon-icon-rotate-right-right"></i></a></li>
          </ul>
        </div>
        <div class="navbar-container content container-fluid">
          <div id="navbar-mobile" class="collapse navbar-toggleable-sm">
            <ul class="nav navbar-nav">
              <li class="nav-item hidden-sm-down"><a class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="icon-menu5">         </i></a></li>
              <li class="nav-item hidden-sm-down"><a href="#" class="nav-link nav-link-expand"><i class="ficon icon-expand2"></i></a></li>
              
            </ul>
            <ul class="nav navbar-nav float-xs-right">
                @if(Auth::user()->roles()->where('title', '=', 'Feedback Messages')->exists())
                  <li class="dropdown dropdown-notification nav-item"><a title="Alumni Requests" href="#" data-toggle="dropdown" class="nav-link nav-link-label" onclick="getUnseenAlReq()"><i class="ficon icon-bell4"></i><span class="tag tag-pill tag-default tag-danger tag-default tag-up" id="ano1">{{ $ano }}</span></a>
                    <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                      <li class="dropdown-menu-header">
                        <h6 class="dropdown-header m-0"><span class="grey darken-2">Alumni Requests</span><span class="notification-tag tag tag-default tag-danger float-xs-right m-0" id="ano2">{{ $ano }} New</span></h6>
                      </li>
                      <li class="list-group scrollable-container" id="Anotis">
                        
                      </li>
                      <li class="dropdown-menu-footer"><a href="{{ route('alumni_requests.index') }}" class="dropdown-item text-muted text-xs-center">See all unverified requests</a></li>
                    </ul>
                  </li>
                @endif

                @if(Auth::user()->roles()->where('title', '=', 'Feedback Messages')->exists())
                  <li class="dropdown dropdown-notification nav-item"><a title="Feedbacks" href="#" data-toggle="dropdown" class="nav-link nav-link-label" onclick="getUnseenFeedback()"><i class="ficon icon-mail3"></i><span class="tag tag-pill tag-default tag-danger tag-default tag-up" id="fno1">{{ $fno }}</span></a>
                    <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                      <li class="dropdown-menu-header">
                        <h6 class="dropdown-header m-0"><span class="grey darken-2">Feedbacks</span><span class="notification-tag tag tag-default tag-danger float-xs-right m-0" id="fno2">{{ $fno }} New</span></h6>
                      </li>
                      <li class="list-group scrollable-container" id="Fnotis">
                        
                      </li>
                      <li class="dropdown-menu-footer"><a href="{{ route('feedbacks.unseen') }}" class="dropdown-item text-muted text-xs-center">Read all new feedbacks</a></li>
                    </ul>
                  </li>
                @endif
            
                @if(Auth::user()->roles()->where('title', '=', 'Contact Us Messages')->exists())
                  <li class="dropdown dropdown-notification nav-item"><a href="#" title="Contact Us Messages" onclick="getUnseenMsg()" data-toggle="dropdown" class="nav-link nav-link-label"><i class="ficon icon-mail6"></i><span class="tag tag-pill tag-default tag-info tag-default tag-up" id="mno1">{{ $mno }}</span></a>
                    <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                      <li class="dropdown-menu-header">
                        <h6 class="dropdown-header m-0"><span class="grey darken-2">Contact Us Messages</span><span class="notification-tag tag tag-default tag-info float-xs-right m-0" id="mno2">{{ $mno }} New</span></h6>
                      </li>
                      <li class="list-group scrollable-container" id="Mnotis">
                        
                      </li>
                      <li class="dropdown-menu-footer"><a href="{{ route('contact-us-messages.unseen') }}" class="dropdown-item text-muted text-xs-center">Read all new messages</a></li>
                    </ul>
                  </li>
                @endif  
              <li class="dropdown dropdown-user nav-item"><a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link"><span class="avatar avatar-online"><img src="{{asset('backend/app-assets/images/portrait/small/avatar-s-1.png')}}" alt="avatar"><i></i></span><span class="user-name" id="adminName">{{ Auth::user()->name }}</span></a>
                <div class="dropdown-menu dropdown-menu-right">
                  <a href="{{ route('profile') }}" class="dropdown-item">
                    <i class="icon-head"></i> Edit Profile
                  </a>
                  <div class="dropdown-divider"></div>
                  <a href="{{ url('/admin/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="dropdown-item">
                    <i class="icon-power3"></i> Logout</a>
                    <form id="logout-form" action="{{ url('/admin/logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                    </form>
                  </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>