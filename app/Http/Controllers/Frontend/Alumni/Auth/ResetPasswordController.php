<?php

namespace App\Http\Controllers\Frontend\Alumni\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Auth;
use Illuminate\Support\Facades\Password;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Validator;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/alumni';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:alumni');
    }

    public function showResetForm(Request $request, $token = null)
    {
        
        return view('frontend.alumni.auth.passwords.reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }

    protected function resetPassword($user, $password)
    {
        $user->forceFill([
            'password' => bcrypt($password),
            'remember_token' => Str::random(60),
        ])->save();
        
        $validator = Validator::make($user->toArray(), [
            'verified' => 'accepted'
        ]);
            
        if ($validator->fails()) {
            $validator->errors()->add('notverified', 'Your account has not been verified by the admin yet.');
            return redirect()->route('alumni.blog')->withErrors($validator,'loginErrors')->withInput();
        }
        else{
            $this->guard()->login($user);
        }
    }

    public function broker()
    {
        return Password::broker('alumnis');
    }

    /**
     * Get the guard to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('alumni');
    }
}
