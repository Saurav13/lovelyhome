@extends('layouts.app')

@section('body')
@section('title','| Calendar')

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
<link rel="stylesheet" href="/backend/tooltip/jquery.qtip.css">
<style>
    
    .about_page_text a {
        text-decoration: none;
        color: #fff;
        -webkit-transition: all .2s linear;
        transition: all .2s linear;
        cursor: pointer;
    }

    .about_page_text a :hover {
        text-decoration: none;
    }

    a.fc-more{
        text-decoration: none;
        color: #fff;
        -webkit-transition: all .2s linear;
        transition: all .2s linear;
        cursor: pointer;
    }

    a.fc-more:hover{
        text-decoration: none;
        color: #fff;
    }

    .fc button {
        background: #e5322d;
        color: #fff;
        text-shadow: 0 1px 1px rgb(26, 0, 31);
    }

    .fc-sat{
        color:red;
    }
    
    .fc-more-cell, .fc-event {
        text-align:center !important;
        position: relative;
        display: block;
        font-size: 1.8em;
        line-height: 1.6;
        border-radius: 3px;
        border: none;
        background-color: #ffffff00;
        font-weight: 400;
        margin-top: 12px;
        font-family: 'Boogaloo', cursive;
    }

    .fc-content{
        text-overflow: ellipsis;
    }

    .fc-more-popover{
        background-color: #0aa910!important;
        font-family: 'Boogaloo', cursive;
    }

    .fc-unthemed .fc-popover .fc-header, .fc-unthemed hr {
        background: #ffdd00;
    }

    .fc-more-popover a.fc-event{
        font-size: 1.5em;
        margin-top: 5px;
    }

    @media only screen and (max-width: 640px){
        table {
            overflow-x: auto;
            display: table;
        }
    }

</style>

<section id="about_page" class="about_page">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="about_page_text">
                    {!! $calendar->calendar() !!}
                </div>
            </div>
        </div>
    </div>
</section>

<section style="
padding-top: 0px;
padding-bottom: 150px;
">
</section>

{{-- <div id="calevent" class="modal fade in"  aria-hidden="false">
    <div class="modal-dialog">
        <div class="register_form_top">
            <div class="register_wave"></div>
            <div>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span> <span class="sr-only">close</span>
                </button>
                <h2 id="modalTitle" class="section-title"></h2>
            </div>
            <div id="modalBody" class="section-description">
                <h4  class="modal-title"></h4>
            </div>
            <div  class="modal-body" style="font-size:  15px;">
                
            </div>
            <hr>
            <h4 class="section-title">Lovely Homes</h4>          
        </div>
    </div>
</div> --}}
<div id="calevent" class="white-popup-block mfp-hide">
    <div class="calendar_block">
        <button title="Close (Esc)" type="button" id="calenderclose" class="mfp-close">×</button>
        <div class="calendar_top">
            <h2 class="section-title" id="modalTitle"></h2>
            <p class="section-description" id="modalBody"></p>
        </div>
    </div>			
</div>
@stop

@section('js')
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="/backend/tooltip/jquery-ui.min.js"></script>
    <script src="{{ asset('backend/tooltip/jquery.qtip.js') }}"></script>    
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
    {!! $calendar->script() !!}
    {{--  <script>
        $(document).ready(function(){
            
            var rows=$('.fc-day-grid').first().children();
            var setWorkingEventStyle=function(rowl,columnl){
                for(var i=0;i<rows.length;i++)
                {
                    var columns=rows.eq(i).children().eq(1).children().first().children().eq(1).children().first().children();
                    for(var j=0;j<columns.length;j++)
                    {
                        var clss=columns.eq(j).first().attr('class');
                        console.log(clss);
                        if(i==rowl && j==columnl && clss=='fc-more-cell')
                        {   
                            console.log(clss);
                            columns.eq(j).first().children().first().children().first().attr('style','color:black;');
                        }
                    }

                }
                return "asd"
            }
            for(var i=0;i<rows.length;i++)
            {
                var columns=rows.eq(i).children().first().children().first().children().first().children().first().children();

                for(var j=0;j<columns.length;j++)
                {
                    var stl=columns.eq(j).attr('style');
                    if(stl=='background: rgb(255, 255, 255);')
                    {
                        var k=setWorkingEventStyle(i,j);
                    }
                }

            }
            
        })
    </script>  --}}

    <script>
        $(document).ready(function(){
            $('.fc-more-cell').each(function(i, obj){
                var color = $(this).prev().find('a').css('color');
                $(this).find('.fc-more').attr('style','color:'+color);
            });

            $('#calenderclose').click(function(){
                $.magnificPopup.close();
            });
        });
    </script>
@endsection