<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Post extends Model
{
    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function comments(){
        return $this->hasMany('App\Comment');
    }

    public function poster(){
        return $this->belongsTo('App\Alumni','alumni_id');
    }
}
