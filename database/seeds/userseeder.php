<?php

use Illuminate\Database\Seeder;

class userseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'name' => 'Sharad',
        	'email' => 'lalnepal@gmail.com',
        	'password' => bcrypt('lovelyhomesharad123'),
        ]);
    }
}
