<?php

namespace App\Http\Controllers\Frontend\Alumni;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AlumniPhoto;
use App\Post;
use App\Comment;
use Auth;

class AlumniController extends Controller
{
    public function index(){
        $posts = Post::orderBy('id','desc')->limit(5)->get();
        return view('frontend.alumni.blog')->with('posts',$posts);
    }

    public function getMorePosts(Request $request){
        if ($request->ajax()) {
            $last = '';
            try{
                $posts = Post::where('id','<',$request->lastId)->orderBy('id','desc')->limit(5)->get();
                
                $view = view('frontend.alumni.partials.post')->with('posts',$posts)->render();
                $last = count($posts) > 0 ? $posts->last()->id : '';
                
                return response()->json(['html'=>$view,'last'=>$last]);
            }
            catch (\Exception $e) {
                return response()->json('error',422);
            }
        }
    }

    public function gallery(){
        $batch = AlumniPhoto::orderBy('batch','desc')->groupBy('batch')->pluck('batch');
        
        $batches = [];
        if(count($batch) > 0)
            $batches[$batch[0]] = AlumniPhoto::where('batch',$batch[0])->orderBy('name','asc')->get();
    
        return view('frontend.alumni.gallery')->with('batch',$batch)->with('batches',$batches)->with('nextBatch',(count($batch) > 1 ? $batch[1] : ''));
    }

    public function filterBatch(Request $request){
        if ($request->ajax()) {

            $filter_batch = $request->filter_batch;
            $nextBatch = '';

            if($filter_batch == 'All'){
                
                if($request->next_batch){
                    $filter_batch = $request->next_batch;
                    $next = AlumniPhoto::where('batch','<',$filter_batch)->orderBy('batch','desc')->first();
                    $nextBatch = $next ? $next->batch : '';
                }
                else{
                    $batch = AlumniPhoto::orderBy('batch','desc')->groupBy('batch')->pluck('batch');
                    $filter_batch = (count($batch) > 0 ? $batch[0] : '');
                    $nextBatch = (count($batch) > 1 ? $batch[1] : '');
                }
            }
            
            $batches = [];
        
            $batches[$filter_batch] = AlumniPhoto::where('batch',$filter_batch)->orderBy('name','asc')->get();
        
            $view = view('frontend.alumni.partials.photos')->with('batches',$batches)->render();
            return response()->json(['html'=>$view,'nextBatch' => $nextBatch],200);
        }
        else
            abort(404);
    }

    public function myposts(){
        if(Auth::guard('alumni')->check()){
            $myposts = Auth::guard('alumni')->user()->posts()->orderBy('id','desc')->limit(5)->get();
            return view('frontend.alumni.myposts')->with('myposts',$myposts);
        }
        else{
            return redirect()->route('alumni.blog');
        }
    }

    public function getMoreMyPosts(Request $request){
        if ($request->ajax() && Auth::guard('alumni')->check()) {
            $last = '';
            try{
                $myposts = Auth::guard('alumni')->user()->posts()->where('id','<',$request->lastId)->orderBy('id','desc')->limit(5)->get();
                
                $view = view('frontend.alumni.partials.mypost')->with('myposts',$myposts)->render();
                $last = count($myposts) > 0 ? $myposts->last()->id : '';
                
                return response()->json(['html'=>$view,'last'=>$last]);
            }
            catch (\Exception $e) {
                return response()->json('error',422);
            }
        }
        else
            return response()->json('error',403);
    }

    public function addPost(Request $request){
        if ($request->ajax() && Auth::guard('alumni')->check()) {

            $request->validate([
                'content' => 'required',
                'title' => 'required|max:255',
                'image' => 'image'
            ],[
                'image.image' => 'Uploaded file must be an image'
            ]);

            $mypost = new Post;
            $mypost->content = $request->content;
            $mypost->title = $request->title;
            $mypost->alumni_id = Auth::guard('alumni')->user()->id;
            if($request->file('image')){
                $image = $request->file('image');
                $filename = time().rand(111,999).'.'.$image->getClientOriginalExtension();
                $location = public_path('alumni_blogs/');
                $image->move($location,$filename);
                $mypost->image = $filename;
            }
            $mypost->save();
            
            $view = view('frontend.alumni.partials.mypost')->with('myposts',array($mypost))->render();
            
            return response()->json(['html'=>$view],200);
        }
        else
            return response()->json('error',403);
    }

    public function editPost($id,Request $request){
        if ($request->ajax() && Auth::guard('alumni')->check()) {

            $request->validate([
                'content' => 'required',
                'title' => 'required|max:255',
                'image' => 'image'
            ],[
                'image.image' => 'Uploaded file must be an image'
            ]);

            $mypost = Auth::guard('alumni')->user()->posts()->findOrFail($id);
            $mypost->content = $request->content;
            $mypost->title = $request->title;

            if($request->file('image')){
                if($mypost->image)
                    unlink(public_path('alumni_blogs/'.$mypost->image));
                $image = $request->file('image');
                $filename = time().rand(111,999).'.'.$image->getClientOriginalExtension();
                $location = public_path('alumni_blogs/');
                $image->move($location,$filename);
                $mypost->image = $filename;
            }
            $mypost->save();
            
            return response()->json(['mypost'=>$mypost],200);
        }
        else
            return response()->json('error',403);
    }

    public function deletePost($id,Request $request){
        
        if ($request->ajax() && Auth::guard('alumni')->check()) {

            $mypost = Auth::guard('alumni')->user()->posts()->findOrFail($id);
            
            if($mypost->image)
                unlink(public_path('alumni_blogs/'.$mypost->image));
               
            $mypost->delete();
            
            return response()->json(['post_id'=>$id],200);
        }
        else
            return response()->json('error',403);
    }

    public function singlePost($slug){
        $post = Post::where('slug',$slug)->first();
        $latest_posts = Post::where('id','!=',$post->id)->orderBy('created_at','desc')->limit(3)->get();
        $comments = $post->comments()->orderBy('id','desc')->limit(5)->get();
        return view('frontend.alumni.singlepost')->with('post',$post)->with('comments',$comments)->with('latest_posts',$latest_posts);
    }

    public function getMoreComments($post_id,Request $request){
        if ($request->ajax()) {
            $last = '';
            try{
                $post = Post::findOrFail($post_id);
                $comments = $post->comments()->where('id','<',$request->lastId)->orderBy('id','desc')->limit(5)->get();
                
                $view = view('frontend.alumni.partials.comment')->with('comments',$comments)->render();
                $last = count($comments) > 0 ? $comments->last()->id : '';
                
                return response()->json(['html'=>$view,'last'=>$last]);
            }
            catch (\Exception $e) {
                return response()->json('error',422);
            }
        }
    }

    public function comment($id,Request $request){
        if(Auth::guard('alumni')->check()){
            $request->validate([
                'content' => 'required'
            ]);

            $comment = new Comment;
            $comment->content = $request->content;
            $comment->alumni_id = Auth::guard('alumni')->user()->id;
            $comment->post_id = $id;
            $comment->save();
            
            $view = view('frontend.alumni.partials.comment')->with('comments',array($comment))->render();

            return response()->json(['html'=>$view], 200);
        }
        else
            return response()->json(['errors'=>['content'=>[0 => 'Please Sign in to your alumni account first']]], 422);
    }
}
