  @extends('admin.main')
  @section('title','| Gallery')
  @section('body')

    <div class="app-content content container-fluid">
      <div class="content-wrapper">        
        <div class="content-body">
          <div class="row">
            <div class="col-xs-12 mt-1">
              <h4>Gallery</h4>
              
              <hr>
            </div>
          </div>

          <div class="row match-height">
            <div class="col-xs-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">Add Albums</h4>
                </div>
                <div class="card-body">
                  <div class="card-block">
                    <ul class="nav nav-tabs nav-justified">
                      
                      <li class="nav-item">
                        <a class="nav-link" id="photos-tab" data-toggle="tab" href="#photos" aria-controls="photos" aria-expanded="false">Photos</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="arts-tab" data-toggle="tab" href="#arts" aria-controls="arts">Arts</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="videos-tab" data-toggle="tab" href="#videos" aria-controls="videos">Videos</a>
                      </li>
                      
                    </ul>
                    <div class="tab-content px-1 pt-1">
                        
                      <div class="tab-pane fade" id="photos" role="tabpanel" aria-labelledby="photos-tab" aria-expanded="true">
                        <form class="form-group" method="POST" action="{{ route('gallery.store') }}">
                          {{csrf_field()}}
                          <div class="row">
                            <div class="col-lg-4">
                              <input type="hidden" name="category" value="Photos"/>
                              <input class="form-control" name="name" type="text" placeholder="Enter album name" required/>
                            </div>
                            <div class="col-lg-2">
                              <button type="submit" class="btn btn-primary">Add Album</a>
                            </div>
                          </div>
                        </form>
                        <br>
                        <div class="row">
                          @forelse($galleries->where('category','Photos') as $album)
                            <div class="col-xl-3 col-md-6 col-sm-12 " style="margin-bottom:10px">
                                <?php $images = $album->images()->limit(3)->get(); ?>
                                <a  href="{{URL::to('/admin/gallery', [$album->id])}}">
                                    @if(count($images)>0)
                                        <div id="carousel-wrap{{$album->id}}" class="carousel slide" data-ride="carousel" data-wrap="false" style="height:200px;width:200px">
                                            <ol class="carousel-indicators">
                                                @foreach($images as $i)
                                                    <li data-target="#carousel-wrap{{$album->id}}" data-slide-to="{{$loop->iteration-1}}" class="{{$loop->iteration==1?'active':''}}"></li>
                                                @endforeach
                                            </ol>
                                            <div class="carousel-inner" role="listbox">
                                                @foreach($images as $i)
                                                    <div class="carousel-item {{$loop->iteration==1?'active':''}}">
                                                        <img src="{{asset('gallery_images'.'/'.$i->name)}}" alt="{{$loop->iteration}} slide" style="height:200px;width:200px">
                                                    </div>
                                                @endforeach
                                            </div>
                                            <a class="left carousel-control" href="#carousel-wrap{{$album->id}}" role="button" data-slide="prev">
                                                <span class="icon-prev" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control" href="#carousel-wrap{{$album->id}}" role="button" data-slide="next">
                                                <span class="icon-next" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    @else
                                        <img src="{{asset('backend/systemimages/gallery.jpg')}}" style="height:200px;width:200px"/><br>
                                    @endif
                                </a><br>
                                <a href="{{URL::to('/admin/gallery', [$album->id])}}"><h4 class="card-title ">{{ $album->name }}</h4></a>

                                <a class="btn btn-outline-warning" data-toggle="modal" data-target="#editAlbum" onclick="editAlbum({{ $album }})"><i class="icon-edit"></i></a>
                                <form action="{{ route('gallery.destroy',$album->id) }}" method="POST" style="display:inline">
                                  {{ csrf_field() }}
                                  <input type="hidden" name="_method" value="DELETE" >
                                  <button id='deleteAlbum{{$album->id}}' type="button" class="btn btn-outline-danger" ><a><i class="icon-trash-o"></i></a></button>
                                </form>
                            </div>
                          @empty
                            <p class="font-medium-3 text-muted text-xs-center" style="margin:100px">No Albums Yet</p>
                          @endforelse
                        </div>
                      </div>

                      <div class="tab-pane fade" id="arts" role="tabpanel" aria-labelledby="arts-tab" aria-expanded="false">
                        <form class="form-group" method="POST" action="{{ route('gallery.store') }}">
                          {{csrf_field()}}
                          <input type="hidden" name="category" value="Arts"/>  
                          <div class="row">
                            <div class="col-lg-4">
                              <input class="form-control" name="name" type="text" placeholder="Enter album name" required/>
                            </div>
                            <div class="col-lg-2">

                              <button type="submit" class="btn btn-primary">Add Album</a>
                            </div>
                          </div>
                        </form>  
                        <br>
                        <div class="row">
                          @forelse($galleries->where('category','Arts') as $album)
                            <div class="col-xl-3 col-md-6 col-sm-12 " style="margin-bottom:10px">
                              <?php $images = $album->images()->limit(3)->get(); ?>
                              <a  href="{{URL::to('/admin/gallery', [$album->id])}}">
                                  @if(count($images)>0)
                                      <div id="carousel-wrap{{$album->id}}" class="carousel slide" data-ride="carousel" data-wrap="false" style="height:200px;width:200px">
                                          <ol class="carousel-indicators">
                                              @foreach($images as $i)
                                                  <li data-target="#carousel-wrap{{$album->id}}" data-slide-to="{{$loop->iteration-1}}" class="{{$loop->iteration==1?'active':''}}"></li>
                                              @endforeach
                                          </ol>
                                          <div class="carousel-inner" role="listbox">
                                              @foreach($images as $i)
                                                  <div class="carousel-item {{$loop->iteration==1?'active':''}}">
                                                      <img src="{{asset('gallery_images'.'/'.$i->name)}}" alt="{{$loop->iteration}} slide" style="height:200px;width:200px">
                                                  </div>
                                              @endforeach
                                          </div>
                                          <a class="left carousel-control" href="#carousel-wrap{{$album->id}}" role="button" data-slide="prev">
                                              <span class="icon-prev" aria-hidden="true"></span>
                                              <span class="sr-only">Previous</span>
                                          </a>
                                          <a class="right carousel-control" href="#carousel-wrap{{$album->id}}" role="button" data-slide="next">
                                              <span class="icon-next" aria-hidden="true"></span>
                                              <span class="sr-only">Next</span>
                                          </a>
                                      </div>
                                  @else
                                      <img src="{{asset('backend/systemimages/gallery.jpg')}}" style="height:200px;width:200px"/><br>
                                  @endif
                              </a><br>
                              <a href="{{URL::to('/admin/gallery', [$album->id])}}"><h4 class="card-title ">{{ $album->name }}</h4></a>
  
                              <a class="btn btn-outline-warning" data-toggle="modal" data-target="#editAlbum" onclick="editAlbum({{ $album }})"><i class="icon-edit"></i></a>
                              <form action="{{ route('gallery.destroy',$album->id) }}" method="POST" style="display:inline">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="DELETE" >
                                <button id='deleteAlbum{{$album->id}}' type="button" class="btn btn-outline-danger" ><a><i class="icon-trash-o"></i></a></button>
                              </form>
                            </div>
                          @empty
                            <p class="font-medium-3 text-muted text-xs-center" style="margin:100px">No Albums Yet</p>
                          @endforelse
                        </div>
                      </div>

                      <div class="tab-pane fade" id="videos" role="tabpanel" aria-labelledby="videos-tab" aria-expanded="false">
                          <form class="form-group" method="POST" action="{{ route('gallery.store') }}">
                            <input type="hidden" name="category" value="Videos"/>
                            {{csrf_field()}}
                            <div class="row">
                              <div class="col-lg-4">
                                <input class="form-control" name="name" type="text" placeholder="Enter album name" required/>
                              </div>
                              <div class="col-lg-2">
                                <button type="submit" class="btn btn-primary">Add Album</a>
                              </div>
                            </div>
                          </form>
                          <br>  
                        <div class="row">
                          @forelse($galleries->where('category','Videos') as $album)
                            <div class="col-xl-3 col-md-6 col-sm-12 " style="margin-bottom:10px">
                                <?php $image = $album->images()->limit(1)->get(); ?>
                                <a  href="{{URL::to('/admin/gallery', [$album->id])}}">
                                    @if(count($image)>0)
                                        <div id="carousel-wrap{{$album->id}}" class="carousel slide" data-ride="carousel" data-wrap="false" style="height:200px;width:200px">
                                            <div class="carousel-inner" role="listbox">
                                              <div class="carousel-item active">
                                                <video width="200" height="200">
                                                    <source src="/gallery_images/{{ $image[0]->name }}" >
                                                </video>
                                              </div>
                                            </div>
                                        </div>
                                    @else
                                        <img src="{{asset('backend/systemimages/gallery.jpg')}}" style="height:200px;width:200px"/><br>
                                    @endif
                                </a><br>
                                <a href="{{URL::to('/admin/gallery', [$album->id])}}"><h4 class="card-title ">{{ $album->name }}</h4></a>
    
                                <a class="btn btn-outline-warning" data-toggle="modal" data-target="#editAlbum" onclick="editAlbum({{ $album }})"><i class="icon-edit"></i></a>
                          
                                <form action="{{ route('gallery.destroy',$album->id) }}" method="POST" style="display:inline">
                                  {{ csrf_field() }}
                                  <input type="hidden" name="_method" value="DELETE" >
                                  <button id='deleteAlbum{{$album->id}}' type="button" class="btn btn-outline-danger" ><a><i class="icon-trash-o"></i></a></button>
                                </form>
                            </div>
                          @empty
                            <p class="font-medium-3 text-muted text-xs-center" style="margin:100px">No Albums Yet</p>
                          @endforelse
                        </div>
                          
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        <!-- add materials section -->
        </div>
      </div>
    </div>

    <div class="modal fade text-xs-left" id="editAlbum" tabindex="-1" role="dialog" aria-labelledby="myModalLabel5" aria-hidden="true" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form class="form" method="POST" id="editAlbumForm" action="">
                    <input type="hidden" name="_method" value="PATCH" >
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <h4 class="form-section">Edit Album</h4>
                            <div class="form-group">
                                <label for="editname">Album name</label>
                                <input type="hidden" value="" name="id" id="editid"/>
                                <input class="form-control " id="editname" type="text" placeholder="Album Name"  name="editname" required>
                            </div>

                          
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-outline-primary">Save changes</button>
                    </div>
                </form>                                    
            </div>
        </div>
    </div>
  @endsection


  @section('js')
  <script type="text/javascript">
    function editAlbum(album){
      $('#editAlbumForm').attr('action',"{{ URL::to('/admin/gallery/')}}"+"/"+album.id);
      $("#editname").val(album.name);
      $("#editid").val(album.id);
    }

    $(document).ready(function() {
      var hash=window.location.hash.toLowerCase().slice(1);
      if(hash=='')
      {
        $('#photos-tab').attr('class','nav-link active');
        $('#photos').attr('class','tab-pane fade active in');
      }
      else
      {
        $('#'+hash+'-tab').attr('class','nav-link active');
        $('#'+hash).attr('class','tab-pane fade active in');
      
      }

      $("[id*='deleteAlbum']").click(function(e){
        var ele = this;
        e.preventDefault();

        swal({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
          if (result.value) {
            ele.form.submit();
          }
        });

      });
    });

    
  </script>
  @endsection
          
          
          