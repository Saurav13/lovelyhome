@extends('admin.main')
@section('title','| Teachers' )
@section('body')
<div class="app-content content container-fluid">
        <div class="content-wrapper">
          <div class="content-body">
<section id="addnotice">  
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-9">
                        <h4 class="card-title" >Update Teacher Information of {{$teachers->name}}</h4>
                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                        </div>
                        <div class="col-md-3">
                            <a href="{{route('admin.teachers')}}" class="btn btn-labeled btn-default">
                                <span class="btn-label"><i class="fa fa-arrow-left" aria-hidden="true"></i></span> Back</a>
                        </div>
                    </div>
                    @if (Session::has('message'))
                    <div class="alert alert-info alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          {{ Session::get('message') }}
                        </div>
                    @endif
         
                      <div class="card-body">
                        
                        <div class="card-block ">
                            <form class="form" method="POST" action="{{route('teachers.update')}}" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="form-body">
                                        <div class="form-group col-sm-12">
                                            <label for="name">Name*</label>
                                            <input type="text" id="name" class="form-control" placeholder="example - John Doe" name="name" value="{{$teachers->name}}" required>
                                        </div>
                                        
                                        <div class="form-group col-sm-12">
                                            <label for="designation">Designation*</label>
                                            <input type="text" id="designation" class="form-control" placeholder="example - English Teacher" name="designation" value="{{$teachers->designation}}" required>
                                        </div>                                      
                                        <div class="form-group col-sm-12">
                                        <label for="description">Testimonials Description*</label>
                                        <textarea rows="10" class="form-control" name="description">{{$teachers->description}}</textarea>
                                    </div>

                                    <div class="form-group col-sm-12">
                                            <label for="facebook_link">Facebook Link</label>
                                              <input type="text" id="facebook_link" class="form-control" placeholder="example - https://www.facebook.com/LHCCK/" name="facebook_link" value="{{$teachers->facebook_link}}">
                                          </div> 
                                    
                                    <div  class="form-group col-sm-12">
                                        <label for="image">Select Image(*If you're updating the image)</label>
                                        <input type="file" name="image" class="form-control-file">
                                    </div>
                                    
                                    <div class="form-group col-sm-12">
                                        <button type="submit" class="btn btn-md btn-success" id="add">
                                            <i class="icon-check2">Update Teacher Information</i> 
                                        </button>
                                    </div>  
                                </div>
                            </div>
                            <input type="hidden" name="id" value="{{$teachers->id}}">
                        </form>
                                </div>
                                
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>

@endsection