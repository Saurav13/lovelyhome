<?php

namespace App\Http\Controllers\Frontend\Alumni\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Auth;
use App\Alumni;
use Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/alumni';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:alumni')->except('logout');
    }

    public function showLoginForm()
    {
        return view('frontend.alumni.auth.login');
    }

    protected function validateLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect()->route('alumni.blog')->withErrors($validator,'loginErrors')->withInput();
        }
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        $user = Alumni::where('email','=',$request->email)->first();
        
        if($user){
            $validator = Validator::make($user->toArray(), [
                'verified' => 'accepted'
            ]);
                
            if ($validator->fails()) {
                $validator->errors()->add('notverified', 'Your account has not been verified by the admin yet.');
                return redirect()->route('alumni.blog')->withErrors($validator,'loginErrors')->withInput();
            }
        }
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        $validator = ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ])->validator;
        
        return redirect()->route('alumni.blog')->withErrors($validator,'loginErrors')->withInput();
    }

    protected function guard()
    {
        return Auth::guard('alumni');
    }

    public function logout(Request $request)
    {
        
        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect()->route('alumni_login');
    }
}
