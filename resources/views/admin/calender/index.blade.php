@extends('admin.main')
@section('title','| Calendar')
@section('body')

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
    <link rel="stylesheet" href="/backend/tooltip/jquery.qtip.css">
    <style>
        .fc-unthemed .fc-today {
            background: rgb(227, 234, 252);
        }
        
    </style>
    <div class="app-content content container-fluid">
        <div class="content-wrapper">        
            <div class="content-body">
                <div class="row">
                    <div class="col-xs-12 mt-1">
                        <h4>Calendar</h4>
                        <hr>
                    </div>
                </div>

                <div class="row match-height">
                    <div class="col-xs-12">
                        <div class="card">
                            <div class="card-header">
                                <a href="{{ route('calender.create') }}" class="btn btn-primary pull-right">
                                    Add Event
                                </a>
                            </div>
                            <div class="card-body">
                                <div class="card-block">
                                    {!! $calendar->calendar() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="/backend/tooltip/jquery-ui.min.js"></script>
    <script src="{{ asset('backend/tooltip/jquery.qtip.js') }}"></script>    
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
    {!! $calendar->script() !!}
@endsection