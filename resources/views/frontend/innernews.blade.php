@extends('layouts.app')
@section('title','| News')
@section('body')
<section id="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="{{route('home')}}">Home</a></li>
                    <li><a href="{{route('news')}}">News</a></li>
                    <li class="active">{{$news->title}}</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section id="blog_page" class="blog_page">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="blog_block">
                    <h3 class="blog_title">{{$news->title}}</h3>
                    <div class="row blog_info">
                        <div class="col-md-3 col-sm-3 col-xs-12 pr_0">
                            <div class="date" style="background:#b70404a8;">
                                <ul>
                                    <li class="number">{{date('j',strtotime($news->created_at))}}</li>
                                    <li class="date_month" style="padding-left:20px;"> {{date('M',strtotime($news->created_at))}}</br>{{date('Y',strtotime($news->created_at))}}</li>
                                </ul>
                            </div>
                        </div>
                       
                    </div>
                    <div class="blog_block_img wow fadeIn">
                        <img src="{{asset('news-images'.'/'.$news->image)}}" alt="{{$news->title}}">
                    </div>
                    <div class="blog_brief content">
                        <p>{!! $news->description !!}</p>
                    </div>
                    <div class="blog_link">
                        <div class="blog_share social_icon t_white social">
                            <ul>
                                <li><a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(Request::fullUrl()) }}" target="_blank"><i class="b_blue_dark_social fa fa-facebook"></i></a></li>
                                <li><a href="https://twitter.com/intent/tweet?url={{ urlencode(Request::fullUrl()) }}" target="_blank"><i class="b_blue_social fa fa-twitter"></i></a></li>
                                <li><a href="https://plus.google.com/share?url={{ urlencode(Request::fullUrl()) }}" target="_blank"><i class="b_red_social fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="sidebar sidebar_blog_padding">
                 
                    <div class="last_news" >
                        <h4 class="title">Recent News</h4>
                        @foreach($latestnews as $n)
                        
                        <div class="col-md-12 col-sm-4 block_news">
                            <img src="{{asset('news-images'.'/'.$n->image)}}" class="wow fadeIn" alt="{{$n->title}}">
                            <h3><a href="{{URL::to('news'.'/'.$n->slug)}}" style="color:#b70404a8" >{{$n->title}}</a></h3>
                            <p class="date">{{date('M j, Y',strtotime($n->created_at))}}</p>
                            <p class="brief">{{strip_tags(str_limit($n->description, $limit = 150, $end = '...'))}}</p>
                            <a class="button" href="{{URL::to('news'.'/'.$n->slug)}}" style="background:#b70404a8">Read More</a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('js')
<script>
    var popupMeta = {
      width: 400,
      height: 400
  }
  $('.social').on('click', '.social-share', function(event){
      event.preventDefault();
  
      var vPosition = Math.floor(($(window).width() - popupMeta.width) / 2),
          hPosition = Math.floor(($(window).height() - popupMeta.height) / 2);
      
      var url = $(this).attr('href');
      var popup = window.open(url, 'Social Share',
          'width='+popupMeta.width+',height='+popupMeta.height+
          ',left='+vPosition+',top='+hPosition+
          ',location=0,menubar=0,toolbar=0,status=0,scrollbars=1,resizable=1');
  
      if (popup) {
          popup.focus();
          return false;
      }
  });
  </script>
@endsection