@section('title','| Materials')
@section('body')

<!--  -->
<div class="app-content content container-fluid">
  <div class="content-wrapper">
    <div class="content-body">
          <!-- Basic example section start -->
      <section id="addmaterials">  
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <div class="card-title" ><a data-action="collapse"><h3>Materials</h3><h6>Add, Edit or Remove Material</h6>  </a>
                </div>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="icon-plus4"></i></a></li>
                      <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="card-body">  <!-- //collapse -->
                <div class="card-block">
                    <div class="row">
                      <div class="col-md-12">

                        <a href="/admin/materials/create" class="btn btn-success  btn-fill  pull-right" id="MatAdd" style="float: right;">
                            Add New Material 
                        </a>
                  
                        <div class="table-responsive">
                          <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th>Material Name</th>
                                <th>Detail</th>
                                <th>Resource Name</th>
                                <th>Status</th>
                                <th>Actions</th>

                              </tr>
                            </thead>
                            <tbody>
                              @foreach($materials as $material)
                              <tr>
                                <td class="text-middle">
                                <h4  style="color: inherit; font-family: Atma,cursive">{{$material->title}} </h4>

                                </td>
                          
                                <td class="text-middle" id="edit{{$material->id}}">
                                  {!! $material->detail !!}
                                  </td>

                                  <td>
                                  {!! $material->Rtitle !!}
                                  </td>

                                  <td>
                                    <div class="{{$material->status=='active'?'tag tag-success btn':'tag tag-warning btn'}}" id="status{{$material->id}}"> 
                                    {!! $material->status !!}
                                    </div>
                                  </td>

                                  <td class="text-middle">
                                  <a href="#" id="edit{{$material->id}}" class="btn btn-outline-red"> <i class="icon-edit2"></i></a>
                                  <a href="#" id="delete{{$material->id}}" class="btn btn-outline-red"> <i class="icon-remove"></i></a>
                                  </td>
                              </tr>

                              @endforeach

                            </tbody>
                          </table>
                        </div>

                    <div>
                    
                    </div>
                    
                  </div>

                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>

  </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
  $(document).ready(function() {
    $("[id*='edit']").click(function(){
           //alert('baam');
            var id = $(this).attr("id").slice(4);
            console.log(id);
            //$("#updateCancel"+id).attr("hidden","true");
            $("#updateCancel"+id).removeAttr("hidden");
          });

          $("[id*='cancel']").click(function(){
           //alert('baam');
            var id = $(this).attr("id").slice(6);
            console.log(id);
            //$("#updateCancel"+id).attr("hidden","true");
            $("#updateCancel"+id).attr("hidden","true");
          });
        

       
        $("[id*='delete']").click(function(){
          var id = $(this).attr("id").slice(6);
          swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
            if (result.value) {
              $.post("{{URL::to('admin/materials/delete')}}",{id:id,_token:"{{csrf_token()}}"},function(data){  
                  swal({
                    stitle:"Deleted Successfully",
                    type:"success"
                  }).then(function(){
                    window.location.reload();
                  })
        })
            }
            })

        });
    });
</script>

@endsection
      
<!--  -->





