@extends('layouts.app')

@section('body')
@section('title','| Alumni Password Reset')
    <style>
        
        #alumni_resetpassword_form .has-error{
            border: 3px solid #b70404c7;
        }
    </style>
    <section class="authorization_page">
                            
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10 dot">
                <h2 class="section-title">Alumni Account Recovery</h2>
                <p class="section-description pb_20"></p>
            </div>
            <div class="col-md-1"></div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <form id="alumni_resetpassword_form" method="POST" action="{{ route('alumni_password.request') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="token" value="{{ $token }}">
                    <div class="reg_box">
                        <input class="{{ $errors->has('email') ? 'has-error' : '' }}" id="new_email" name="email" type="email" placeholder="Enter Your email" required="required">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong style="color:  #b70404c7;">{{ $errors->first('email') }}</strong>
                            </span>
                        @endif

                        <input class="reg_pass{{ $errors->has('password') ? ' has-error' : '' }}" id="new_pass" name="password" type="password" placeholder="********" required="required">
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong style="color:  #b70404c7;">{{ $errors->first('password') }}</strong>
                            </span>
                        @endif

                        <input class="reg_pass" id="new_conf_pass" name="password_confirmation" type="password" placeholder="Repeat Password" required="required">

                    </div>
                    <button class="submit" type="submit">Reset Password</button>
                </form>
            </div>
        </div>
    </section>
@endsection



