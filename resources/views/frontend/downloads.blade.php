@extends('layouts.app')

@section('body')
@section('title','| Downloads')


    <section id="about_page" class="about_page">
        <div class="container">
            <div class="row">
                <div class="col-md-12 dot">
                    <h2 class="section-title">Downloads</h2>
                </div>
            </div>
            <div class="row price_table_block">
                @foreach($categories->chunk(3) as $chunk)
                    @foreach($chunk as $c)
                        @if($loop->index==0)
                            <div class="col-md-4">
                                <div class="price_table basic_price">
                                    <div class="header_price">
                                        <div class="price_class">{{$c->category_name}}</div>
                                    </div>
                                    <div class="price_include">
                                        <ul style="max-height:300px; min-height:300px; overflow: auto;">
                                                @foreach($downloads->where('category_id', $c->id) as $d)
                                                    <li><span><i class="fa fa-check-circle"></i></span><a href="{{asset('downloads'.'/'.$d->file)}}" download>{{$d->title}}</a></li>
                                                @endforeach
                                        </ul>
                                    </div>
                                    <div class="price_describe">
                                    </div>
                                
                                </div>
                            </div>
                        @endif
                        @if($loop->index==1)
                            <div class="col-md-4">
                                <div class="price_table stadart_price">
                                    <div class="header_price">
                                        <div class="price_class">{{$c->category_name}}</div>
                                    </div>
                                    <div class="price_include">
                                        <ul style="max-height:300px; min-height:300px; overflow: auto;">
                                                @foreach($downloads->where('category_id', $c->id) as $d)
                                                    <li><span><i class="fa fa-check-circle"></i></span><a href="{{asset('downloads'.'/'.$d->file)}}" download>{{$d->title}}</a></li>
                                                @endforeach
                                        </ul>
                                    </div>
                                    <div class="price_describe">
                                    </div>
                                
                                </div>
                            </div>
                        @endif
                        @if($loop->index==2)
                            <div class="col-md-4">
                                <div class="price_table price_table premium_price">
                                    <div class="header_price">
                                        <div class="price_class">{{$c->category_name}}</div>
                                    </div>
                                    <div class="price_include">
                                        <ul style="max-height:300px; min-height:300px; overflow: auto;">
                                                @foreach($downloads->where('category_id', $c->id) as $d)
                                                    <li><span><i class="fa fa-check-circle"></i></span><a href="{{asset('downloads'.'/'.$d->file)}}" download>{{$d->title}}</a></li>
                                                @endforeach
                                        </ul>
                                    </div>
                                    <div class="price_describe">
                                    </div>
                                
                                </div>
                            </div>
                        @endif
                    @endforeach
                @endforeach
            </div>
        </div>
    </section>



@stop