@extends('admin.main')
@section('title','| Alumni Request')
@section('body')
<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-xs-12 mb-1">
                <h2 class="content-header-title"></h2>
            </div>
            <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
                <div class="breadcrumb-wrapper col-xs-12">
                    <ol class="breadcrumb">
                        <a class="btn btn-primary btn-min-width mr-1 mb-1 " href="{{ route('alumni_requests.index') }}">Back</a>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section class="card">
                <div id="invoice-template" class="card-block">
                        
                    <div style="margin:10px;text-align: center;">
                        <img src="{{ asset('alumni_pp/'.$alreq->photo) }}" style="width: 200px;height:  200px;"/>
                    </div>
                    <p><strong>Name:</strong> {{ $alreq->name }}</p>
                    <p><strong>Email:</strong> {{ $alreq->email }}</p>
                    <p><strong>Batch:</strong> {{ $alreq->batch }}</p>
                    <br>
                    <form action="{{ route('alumni_requests.verify',$alreq->id) }}" method="POST" style="display:inline">
                        {{ csrf_field() }}
                        <button title="Verify" type="submit" class="btn btn-outline-warning btn-md">Verify</button>
                    </form>
                    <form action="{{ route('alumni_requests.destroy',$alreq->id) }}" method="POST" style="display:inline">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE" >
    
                        <button id='deleteReq{{$alreq->id}}' title="delete" type="button" class="btn btn-outline-danger btn-md">Delete</button>
                    </form>
                            

                </div>
            </section>
        </div>
    </div>
</div>
@endsection

@section('js')
    
    <script type="text/javascript">
        $(document).ready( function () {


            $("[id*='deleteReq']").click(function(e){
                var ele = this;
                e.preventDefault();

                swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                if (result.value) {
                    ele.form.submit();
                }
                });

            });
        });
    </script>

@endsection