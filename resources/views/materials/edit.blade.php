@extends('admin.main')
@section('title','| Materials')
@section('body')

<div class="app-content content container-fluid">
      <div class="content-wrapper">
          <div class="content-body">
              <h2>Update Material | {{$material->title}}  <a href="{{ url()->previous() }}" class="btn btn-labeled btn-default">
                  <span class="btn-label"><i class="fa fa-arrow-left" aria-hidden="true"></i></span> Back</a></h2>
    <form class="form" method="POST" action="{{URL::to('/admin/materials/update')}}" enctype="multipart/form-data">
      {{csrf_field()}}
      <input type="text" hidden name="id" value="{{$material->id}}">
      <div class="row col-sm-12">
          <div class="form-body">

            <div class="form-group col-sm-12">
              <label for="title">Title*</label>
              
              <input type="text" id="title" class="form-control" value="{{$material->title}}" name="title" required>

            </div>

            <div class="form-group col-sm-12">
                <div class="row col-sm-12">
                  <label for="image">Select an Image to be featured with Material*</label><br>
                  <input type="file" name="image" onchange="readURL(this);">
                </div>
              <img id="blah" src="{{asset('materials-images'.'/'.$material->image)}}" style="padding-top:5px;" alt="your image" />
              </div>


            <div class="form-group col-sm-12">
              <label for="detail">Details*</label>
               <textarea id="detail" rows="7" class="form-control" id="detail" name="detail"> {{$material->detail}}</textarea>
            </div>

            <div class="form-group col-sm-12">
              <label for="resources">Resource Category*</label>
              <select name="resources" required class="form-control">
                @foreach($resources as $r)
                <option value="{{$r->id}}" {{ $material->resource_id == $r->id ? 'selected':'' }}>
                  {{$r->title}}
                </option>
                @endforeach
              </select>
            </div>

            <div class="form-group col-sm-12">
                <label for="file">Add Material files*</label>
              <input type="file" name="file" id="file">        
            </div>

            <div class="form-group col-sm-12">
               <button type="submit" class="btn btn-success" id="add">
                <i class="icon-check2">Update Material</i> 
            </button>
            </div>  
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
</div>
<script>
    function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              $('#blah')
                  .attr('src', e.target.result)
                  .width(150)
                  .height(200);
          };

          reader.readAsDataURL(input.files[0]);
      }
  }
</script>
@endsection