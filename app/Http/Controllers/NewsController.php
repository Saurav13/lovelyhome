<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;
use Image;
use Session;
use Auth;

class NewsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            
            if (Auth::user()->roles()->where('title', '=', 'News')->exists()){
                return $next($request);
            }
            else
                abort(403);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news=News::all();
        return view('news.index')->with('news',$news);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
                    $this->validate($request, array(
                       'title'=>'required|max:255',
                       'description'=>'required',
                       'image'=>'mimes:jpeg,jpg,png,gif,svg|max:10000', 
                       ));
        
            $news=new News;
            $news->title=$request->title;
            $news->description=$request->description;
            if($request->hasFile('image')){
               $file = $request->file('image');
                $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
                $path = public_path('news-images/'.$filename);
                Image::make($file)->resize(847, 500)->save($path);
                $img = Image::make(public_path('news-images/'.$filename));
                $img->insert(public_path('backend/systemimages/logo.png'), 'bottom-right', 15, 15);
                $img->save(public_path('news-images/'.$filename));
                $news->image = $filename;
            }
            $news->save();

            Session::flash('success','News was added successfully!');

            return redirect()->route('admin.news'); 

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
  

    /**
     * Display the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news=News::findorFail($id);
        return view('news.view')->with('news',$news);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news=News::findorFail($id);
        return view('news.edit')->with('news',$news);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
       $this->validate($request, array(
                       'title'=>'required|max:255',
                       'description'=>'required',
                       'image'=>'mimes:jpeg,jpg,png,gif,svg|max:10000', 
                       ));

        $news=News::find($request->id);
        $news->title=$request->title;
        $news->description=$request->description;
            if($request->hasFile('image')){
                 if($news->image!=null){
                if($news->image!='newspost.jpg'){
                    unlink('news-images/'.$news->image);
                }
                    }       
               $file = $request->file('image');
                $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
                $path = public_path('news-images/'.$filename);
                Image::make($file)->resize(847, 500)->save($path);
                $img = Image::make(public_path('news-images/'.$filename));
                $img->insert(public_path('backend/systemimages/logo.png'), 'bottom-right', 15, 15);
                $img->save(public_path('news-images/'.$filename));
                $news->image = $filename;
            }
            $news->save();

            Session::flash('success','News was Updated successfully!');

            return redirect()->route('admin.news'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $news = News::findorFail($request->id);
         if($news->image!=null){
            if($news->image!='newspost.jpg'){
         unlink('news-images/'.$news->image);
         }
     }
         $news->delete();
         return redirect()->route('admin.news');
    }


}
