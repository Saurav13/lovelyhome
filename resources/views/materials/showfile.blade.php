@extends('admin.main')
@section('title','| Materials')

  @section('body')
<div class="app-content content container-fluid">
  <div class="content-wrapper">
    <div class="content-body">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
              <a href="{{ url()->previous() }}" style="float:right;" class="btn btn-labeled btn-default">
                  <span class="btn-label"><i class="fa fa-arrow-left" aria-hidden="true"></i></span> Back</a>
          	<h4><b>Material Name:</b> </h4> {{$material->title}}<br>  <br>
            <h4><b>Resource Name:</b></h4> {{$resource->title}} <br> <br>
            <h4><b>Material Details:</b></h4> {!!$material->detail!!} <br> <br>
            <h4><b>File:</b></h4><a href="{{asset('materials-files'.'/'.$material->file)}}"> 
               <li>{{$material->file}}</li></i>
            </a>
            <h4><b>Featured Image:</b><br><img src="{{asset('materials-images'.'/'.$material->image)}}" style="height:200px; width:200px;"></img></h4>
          
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
  
    <!-- Modal content-->
    
   


