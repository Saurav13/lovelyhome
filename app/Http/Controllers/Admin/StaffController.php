<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use Session;
use Auth;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $staffs;

    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            
            if (Auth::user()->roles()->where('title', '=', 'Staffs')->exists()){
                return $next($request);
            }
            else
                abort(403);
        });  
    }

    public function index()
    {
        
        $this->staffs = User::where('id','!=',1)->orderBy('name')->paginate(10);
        $roles = Role::where('title','!=','Dashboard')->where('title','!=','Staffs')->get();
        
        return view('admin.staffs.index')->with('staffs',$this->staffs)->with('roles',$roles);
    }

    public function verifyPassword(Request $request){
        
        if (Auth::attempt(['email' => Auth::user()->email, 'password' => $request->password]))
        {
            return 'correct';
        }
        else
            return 'incorrect';
    }

    public function update(Request $request)
    {
        $staff = User::find($request->staffId);
        $roles = Role::where('title','!=','Dashboard')->where('title','!=','Staffs')->get();
        $new_roles = [];
        
        foreach($request->request as $key => $value){
            if(in_array($value, $roles->pluck('id')->toArray())){
                $new_roles []= $value; 
            }
        }

        $staff->roles()->sync($new_roles);
        $staff->save();
        
        Session::flash('success', 'Staff updated.');
        return redirect()->route('staffs.index',array('page'=>$request->page));
    }

    public function destroy($id,Request $request)
    {
        $staff = User::find($id);
        $staff->delete();
        
        Session::flash('success', 'Staff deleted.');
        return redirect()->route('staffs.index',array('page'=>$request->page));
    }
}
