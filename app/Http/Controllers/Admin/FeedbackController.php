<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Feedback;
use Auth;

class FeedbackController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            
            if (Auth::user()->roles()->where('title', '=', 'Feedback Messages')->exists()){
                return $next($request);
            }
            else
                abort(403);
        });
    }

    public function index(){
        $feedbacks = Feedback::orderBy('created_at','desc')->paginate(10);
        return view('admin.feedbacks.index')->with('feedbacks',$feedbacks)->with('state','all');
    }

    public function unseenMsg(){
        $feedbacks = Feedback::where('seen','0')->orderBy('created_at','desc')->paginate(10);
        return view('admin.feedbacks.index')->with('feedbacks',$feedbacks)->with('state','unseen');
    }

    public function show($id)
    {
        $feedback = Feedback::find($id);

        if(!$feedback)
            abort(404);

        if(!$feedback->seen){
            $feedback->seen = 1;
            $feedback->save();
        }
        
        return view('admin.feedbacks.show')->with('feedback',$feedback);
    }

    public function destroy($id,Request $request)
    {
        $feedback = Feedback::find($id);
        if(!$feedback)
            abort(404);
        $feedback->delete();

        $request->session()->flash('success', 'Feedback deleted');
        return redirect()->route('feedbacks.index');
    }

    public function getUnseenFeedbackCount(Request $request){
        
        if ($request->ajax()){
            $fno = Feedback::where('seen','0')->count();
            return response()->json(['fno' => $fno]);
        }
        abort(404);  
    }

    public function getUnseenFeedbackMsg(Request $request){
        if ($request->ajax()){
            $feedbacks = Feedback::where('seen','0')->latest()->limit(5)->get();
            $fno = Feedback::where('seen','0')->count();
            $view = view('admin.partials.fnotis')->with('feedbacks',$feedbacks)->with('fno',$fno)->render();
            
            return response()->json(['html' => $view, 'fno' => $fno]);
        }
        abort(404);
    }
}
