<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;

class NewStaffAdded extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user;
    public $password;
    public $roles;

    public function __construct(User $user, $password, $roles)
    {
        $this->user = $user;
        $this->password = $password;
        $this->roles = $roles;        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.newstaffadded')->with('user',$this->user)->with('password',$this->password)->with('roles',$this->roles);
    }
}
