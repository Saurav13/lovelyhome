@extends('admin.main')
@section('title','| Materials')


@section('body')


<div class="app-content content container-fluid">
      <div class="content-wrapper">
          <div class="content-body">
            <h2>Add Material  <a href="{{ URL::to('admin/resources') }}" style="floar:right;" class="btn btn-labeled btn-default">
              <span class="btn-label"><i class="fa fa-arrow-left" aria-hidden="true"></i></span> Back</a></h2>
    <form class="form" method="POST" enctype="multipart/form-data"  action="{{URL::to('/admin/materials/create')}}">
      {{csrf_field()}}
      <div class="row">
          <div class="form-body">
            <div class="form-group col-sm-12">
              <label for="title">Title*</label>
              <input type="text" id="title" class="form-control" value="{{old('title')}}" placeholder="title" name="title" required>
            </div>
            <div class="form-group col-sm-12">
                <div class="row col-sm-12">
                  <label for="image">Select an Image to be featured with Material*</label>
                  <br>
                  <input type="file" name="image" onchange="readURL(this);">
                </div>
              <img id="blah" style="padding-top:5px;" src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" alt="your image" />
              </div>
            <div class="form-group col-sm-12">
              <label for="detail">Details*</label>
               <textarea id="detail" rows="7" class="form-control" id="detail" name="detail">{{old('detail')}}</textarea>
            </div>
            <div class="form-group col-sm-12">
              <label for="resources">Resource Category*</label>
              <select name="resources" required class="form-control">
                @foreach($resources as $r)
                <option value="{{$r->id}}">
                  {{$r->title}}
                </option>
                @endforeach
              </select>
            </div>
            
            <div class="form-group col-sm-12">
              <label for="file">Add Material files*</label>
              <input type="file" name="file" id="file">        
            </div>

            <div class="form-group col-sm-12">
               <button type="submit" class="btn btn-success" id="add">
                <i class="icon-check2">Add Material</i> 
            </button>
            </div>  
          </div>
        </div>
    </form>
  </div>
</div>
</div>
<script>
    function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              $('#blah')
                  .attr('src', e.target.result)
                  .width(200)
                  .height(200);
          };

          reader.readAsDataURL(input.files[0]);
      }
  }
</script>
@endsection