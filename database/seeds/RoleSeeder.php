<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'title' => 'Dashboard'
        ]);
        DB::table('roles')->insert([
            'title' => 'Staffs'
        ]);
        DB::table('roles')->insert([
            'title' => 'Notices'
        ]);
        DB::table('roles')->insert([
            'title' => 'Downloads'
        ]);
        DB::table('roles')->insert([
            'title' => 'Resources'
        ]);
        DB::table('roles')->insert([
            'title' => 'Gallery'
        ]);
        DB::table('roles')->insert([
            'title' => 'Testimonials'
        ]);
        DB::table('roles')->insert([
            'title' => 'Teachers'
        ]);
        
        DB::table('roles')->insert([
            'title' => 'Event Settings'
        ]);
        DB::table('roles')->insert([
            'title' => 'Calendar'
        ]);
        DB::table('roles')->insert([
            'title' => 'FAQs'
        ]);
        DB::table('roles')->insert([
            'title' => 'Activities'
        ]);
        DB::table('roles')->insert([
            'title' => 'Alumni Requests'
        ]);
        DB::table('roles')->insert([
            'title' => 'Alumni Photos'
        ]);
        
        DB::table('roles')->insert([
            'title' => 'Advertisements'
        ]);
        DB::table('roles')->insert([
            'title' => 'Contact Us Messages'
        ]);
        DB::table('roles')->insert([
            'title' => 'Feedback Messages'
        ]);
        DB::table('roles')->insert([
            'title' => 'Newsletter'
        ]);
        
    }
}
