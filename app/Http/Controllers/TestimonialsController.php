<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Testimonial;
use Auth;

class TestimonialsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            
            if (Auth::user()->roles()->where('title', '=', 'Testimonials')->exists()){
                return $next($request);
            }
            else
                abort(403);
        });
    }

    public function index()
    {
        $testi=Testimonial::all();
        return view('testimonials.index')->with('testi',$testi);
    }

    public function store(Request $request)
    {
        $this->validate($request, array(
            'name'=>'required|max:255',
            'designation'=>'required|max:255',
            'description'=>'required',
            'file'=>'mimes:jpeg,jpg,png|max:10000', 
            ));
        $testi=new Testimonial;
        $testi->designation=$request->designation;
        $testi->name=$request->name;
        $testi->description=$request->description;
        if($request->hasFile('image')){
            $file = $request->file('image');
           $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
           $path = public_path('testimonials-image/');
           $file->move($path,$filename);
           $testi->image = $filename;
        }
        $testi->save();
        session()->flash('message','Testimonial was added!');
        return redirect()->route('admin.testimonials'); 

    }

    public function destroy(Request $request)
    {
        $testi = Testimonial::findorFail($request->id);
        if($testi->image!=null){
            unlink('testimonials-image/'.$testi->image);
        }
        $testi->delete();
        session()->flash('message','Testimonial was deleted!');
        return redirect()->route('admin.testimonials');
    }

    public function edit($id)
    {
        $testi=Testimonial::findorFail($id);
        return view('testimonials.edit')->with('testi',$testi);
    }

    public function update(Request $request)
    {
        $this->validate($request, array(
            'name'=>'required|max:255',
            'designation'=>'required|max:255',
            'description'=>'required',
            'file'=>'mimes:jpeg,jpg,png|max:10000', 
            ));
        $testi=Testimonial::find($request->id);
        $testi->name=$request->name;
        $testi->designation=$request->designation;
        $testi->description=$request->description;
        if($request->hasFile('image')){
            if($testi->image!=null){
               unlink('testimonials-image/'.$testi->image);
               }       
          $file = $request->file('image');
           $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
           $path = public_path('testimonials-image/');
           $file->move($path,$filename);
           $testi->image = $filename;
        }
        $testi->save();
        session()->flash('message','Testimonial was Updated!');
        return redirect()->route('admin.testimonials'); 
    }
    
}
