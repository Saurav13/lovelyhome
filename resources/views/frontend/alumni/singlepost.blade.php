@extends('layouts.app')

@section('body')
@section('title','| '.$post->title)
<style>
    
    .post_img{
        height: 3.5rem;
    }

    .post_img img{
        height: inherit;
        border-radius: 50px;
    }

    #comment_form .error{
        border: 3px solid #b70404c7;
    }

    #comment_form label {
        color: #b70404c7;
        border: 0!important;
    }
</style>
<section id="blog_page" class="blog_page">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="blog_block">
                    <h3 class="blog_title">{{ $post->title }}</h3>
                    <div class="row blog_info">
                        <div class="col-md-2 col-sm-2 col-xs-12 pr_0">
                            <div class="date" style="background:#49b450">
                                <ul>
                                    <li class="number">{{ $post->created_at->day }}</li>
                                    <li class="date_month">{{ date('M',strtotime($post->created_at)) }}</br>{{ $post->created_at->year }}</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12 prpl_0">
                            <div class="author" style="padding: 8px 10px">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="post_img">
                                            <img src="{{ asset('alumni_pp/'.$post->poster->photo) }}" alt="{{ $post->poster->name }}">
                                        </div>
                                    </div>
                                    <div class="cold-md-9">
                                        <ul>
                                            <li class="title">Posted by</li>
                                            <li class="description"><a href="#">{{ $post->poster->name }}</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7 col-sm-7 col-xs-12 pl_0">
                          
                        </div>
                    </div>
                    <div class="blog_block_img wow fadeIn">
                    <img src="{{asset('alumni_blogs'.'/'.$post->image)}}" alt="">
                    </div>
                    <div class="blog_brief content">
                        <p>{{ $post->content }}</p>
                    </div>

                   

                    <div class="comments">
                        <h4 class="title"><span id="num">{{ $post->comments->count() }}</span> comments</h4>
                        
                        <div id="comments">
                            @include('frontend.alumni.partials.comment')
                        </div>

                        @if(count($comments) == 5)
                            <div style="text-align:center">
                                <button class="button" style="background:#49b450;color: #fff;font-size:  15px;display: unset;" id="loadmore">load more</button>
                            </div>
                        @endif
                    </div>
                    @if(Auth::guard('alumni')->check())
                    <div class="leave_reply b_light">
                        <h3>Comment as {{ Auth::guard('alumni')->user()->name }}</h3>
                        <div class="contact_form_white">
                            <form method="post" id="comment_form" action="{{ route('post.comment',$post->id) }}">
                                <div id="comment_error" style="color: #CD3653;font-size: 1.5rem;font-weight: 600;"></div>
                                <textarea id="content" name="content" rows="4" placeholder="Add a comment.." required="required"></textarea>
                                <div id="commentResult" class="messegeResult">
                                    <p>The comment was successfully added</p>
                                    <button class="button" type="submit">Add</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    @endif
                    
                   
                </div>
            </div>
            <div class="col-md-3">
                <div class="sidebar sidebar_blog_padding">
                    <div class="last_news">
                        <h4 class="title">Latest Posts</h4>
                        @foreach($latest_posts as $lp)
                            <div class="col-md-12 col-sm-4 block_news">
                                @if($lp->image)
                                    <img src="{{ asset('alumni_blogs/'.$lp->image) }}" class="wow fadeIn" alt="{{ $lp->title }}">
                                @endif
                                <h3><a href="#" style="color:#be3033">{{ $lp->title }}</a></h3>
                                <p class="date">{{ date('M j, Y',strtotime($lp->created_at)) }}</p>
                                <p class="brief">{{ strip_tags(str_limit($lp->content, $limit = 150, $end = '...')) }}</p>
                                <a class="button" style="background:#49b450" href="{{ route('alumni.singlepost',$lp->slug) }}">read more</a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@stop

@section('js')
    <script>
        $(document).ready(function(){
            var lastId = '{{ $comments->last()? $comments->last()->id:'' }}';
            
            $('#loadmore').click(function(){
                if(lastId == '')
                    return;
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({ 
                    type: 'POST', 
                    url: "{{ route('post.getmorecomments',$post->id) }}",
                    dataType: 'json', 
                    data: {lastId:lastId}, 
                    beforeSend: function(data) {
                        $('#loadmore').html('LOADING').attr('disabled', 'disabled');
                    },
                    success: function(data){
                        if(data.html == '')
                            $('#loadmore').html('NO MORE COMMENTS');
                        else
                            $('#loadmore').html('LOAD MORE')
                        $('#comments').append(data.html);
                        lastId = data.last;
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        if(xhr.status == 422)
                            $('#comment_error').html(xhr.responseJSON.errors.content[0]);
                        else
                            $('#comment_error').html('Something went wrong. Please try again.');
                        $('#loadmore').html('LOAD MORE');
                    },
                    complete: function(data) {
                        $('#loadmore').prop('disabled', false); 
                    }
                            
                });
            });
        });
    </script>

@endsection