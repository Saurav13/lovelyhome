
Dear {{ $user->name }},<br><br>

You have recently been added as one of the admins for 'Lovely Home'. To use the priviledge, please click on the link below and then sign in using this email and password.
<br><br><br>
<div style="text-align:center">
    <a class="btn btn-primary" href ="{{ route('login') }}">
    Login
    </a>
</div>
<br><br><br>
<strong>Your credentials:</strong><br><br>
<strong>Email        :</strong> {{ $user->email }} <br>
<strong>Password     :</strong> {{ $password }} <br>
<strong>Your Modules :</strong> @foreach($roles as $role) {{ $role->title }} | @endforeach <br><br>

Thanks,<br>
Lovely Home