@if($album->category != 'Videos')
    @foreach($images as $image)
        <div class="album portfolio_grid_item col-portfolio col-xs-12 col-sm-6 col-md-4 col-lg-3 wow fadeInUp">
            <figure class="hover-line">
                <div class="image-holder"><img alt="{{ $album->name }}" src="{{ route('asset', ['gallery_images',$image->name,726,408]) }}"></div>
                <figcaption>
                    <div class="portfolio-lead">
                        <div class="gallery_full_screen">
                            <a title="{{ $album->name }}" class="link" href="/gallery_images/{{ $image->name }}"><img src="/frontend-assets/img/icons/search_gallery.png" alt=""></a>
                        </div>
                    </div>
                </figcaption>
            </figure>
        </div>
        
    @endforeach
@else
    @foreach($images as $image)
        <div class="album col-portfolio col-xs-12 col-sm-6 col-md-4 col-lg-3 portfolio_grid_item wow fadeInUp">
            <figure class="hover-line">
                <div class="image-holder">
                    <video style="height:217px;width:325px">
                        <source src="/gallery_images/{{ $image->name }}" >
                    </video>
                </div>
                <figcaption>
                    <div class="portfolio-lead">
                        <div class="gallery_full_screen">
                            <a class="popup-youtube" href="{{ asset('/gallery_images/'.$image->name) }}"><img src="/frontend-assets/img/icons/video_gallery.png" alt=""></a>
                        </div>
                    </div>
                </figcaption>
            </figure>
        </div>
    @endforeach
@endif