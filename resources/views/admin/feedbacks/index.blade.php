@extends('admin.main')
@section('title','| Feedbacks')
@section('body')

    <div class="app-content content container-fluid">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="text-xs-right">
                    @if($state == 'all')
                        <a class="btn btn-primary btn-min-width mr-1 mb-1 " href="{{ route('feedbacks.unseen') }}">See all unseen feedbacks</a>
                    @else
                        <a class="btn btn-primary btn-min-width mr-1 mb-1 " href="{{ route('feedbacks.index') }}">See all feedbacks</a>
                    @endif
                </div>
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Feedbacks</h4>
                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body collapse in">
                        <div class="card-block card-dashboard">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Subject</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($feedbacks as $f)
                                        @if(!$f->seen && $state == 'all')
                                            <tr style="background:#d6d3d3">
                                        @else
                                            <tr>
                                        @endif
                                            <th scope="row">{{ $loop->iteration }}</th>
                                            <td>{{ $f->name }}</td>
                                            <td>{{ $f->subject }}</td>
                                            <td>
                                                <a class="btn btn-outline-info btn-sm" title="View" href="{{ route('feedbacks.show',$f->id) }}"><i class="icon-eye"></i></a>
                        
                                                <form action="{{ route('feedbacks.destroy',$f->id) }}" method="POST" style="display:inline">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="_method" value="DELETE" >

                                                    <button id='deleteFeedback{{ $f->id }}' title="delete" type="button" class="btn btn-outline-danger btn-sm"><i class="icon-trash-o"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                @if(count($feedbacks)==0)
                                    <p class="font-medium-3 text-muted text-xs-center" style="margin:100px">No {{ $state == 'all'?'':'New'}} Feedbacks</p>
                                @endif
                                <div class="text-xs-center mb-3">
									<nav aria-label="Page navigation">
										{{ $feedbacks->links() }}
									</nav>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection

@section('js')
        
    <script type="text/javascript">
        

        $("[id*='deleteFeedback']").click(function(e){
            var ele = this;
            e.preventDefault();

            swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
            if (result.value) {
                ele.form.submit();
            }
            });

        });
    </script>

@endsection
