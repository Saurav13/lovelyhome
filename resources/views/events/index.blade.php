{{-- @extends('admin.main') --}}
@section('title','| Events')
@section('body')
@include('tinymcebox')
@section('css')

@endsection
<style>
        .btn-file {
            position: relative;
            overflow: hidden;
        }
        .btn-file input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            min-width: 100%;
            min-height: 100%;
            font-size: 100px;
            text-align: right;
            filter: alpha(opacity=0);
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
        }
        
        #img-upload{
            height:30%;
            width: 30%;
            text-align: center;
            padding-top:10px;
        }
</style>

<div class="app-content content container-fluid">
        <div class="content-wrapper">
          <div class="content-body">
                <section>  
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title" ><a data-action="collapse"><button  class="btn btn-md btn-primary"><i class="icon-plus4" aria-hidden="true"></i> Add Event</button></a></h4>
                                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="icon-plus4"></i></a></li>
                                                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    
                                    @if (Session::has('message'))
                                    <div class="alert alert-info alert-dismissable">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            {{ Session::get('message') }}
                                        </div>
                                        @endif
                                        
                                        <div class="card-body collapse">                    
                                            <div class="card-block ">
                                                <form class="form" method="POST" action="{{route('events.store')}}" enctype="multipart/form-data">
                                                    {{csrf_field()}}
                                                    <div class="row">
                                                        <div class="form-body">
                                                            <div class="form-group col-sm-12">
                                                                <label for="title">Title for the event*</label>
                                                                <input type="text" id="title" class="form-control" placeholder="example - Zoo Visiting" name="title" value="{{old('title')}}" required>
                                                            </div>
                                                            
                                                            <div class="form-group col-sm-12">
                                                                <label for="description">Events Description*</label>
                                                                <textarea class="form-control" id="detail" name="description" placeholder="Description of the event">{{old('description')}}
                                                                </textarea>
                                                            </div>

                                                            <div class="form-group col-sm-12">
                                                                    <label for="venue">Venue for the event*</label>
                                                                    <input type="text" id="venue" class="form-control" placeholder="example - Lovely Home premises" name="venue" value="{{old('venue')}}" required>
                                                                </div>
                                                                <div class="row col-sm-12">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="timesheetinput5">Start Time</label>
                                                                                <div class="position-relative has-icon-left">
                                                                                    <input type="time" id="timesheetinput5" class="form-control" name="start_time" value="{{old('start_time')}}">
                                                                                    <div class="form-control-position">
                                                                                        <i class="icon-clock5"></i>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="timesheetinput6">End Time</label>
                                                                                <div class="position-relative has-icon-left">
                                                                                    <input type="time" id="timesheetinput6" class="form-control" value="{{old('end_time')}}" name="end_time">
                                                                                    <div class="form-control-position">
                                                                                        <i class="icon-clock5"></i>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                            <div class="form-group col-sm-12">
                                                                <label for="timesheetinput3">Date</label>
                                                                    <div class="position-relative has-icon-left">
                                                                            <input type="date" id="timesheetinput3" class="form-control" value="{{old('event_date')}}" name="event_date">
                                                                            <div class="form-control-position">
                                                                                <i class="icon-calendar5"></i>
                                                                            </div>
                                                                        </div>
                                                            </div>  
                                                            <div class="form-group col-sm-12">
                                                                    <label for="donationinput3">Contact Number of Event Co-ordinator</label>
                                                                    <input type="tel" id="donationinput3" class="form-control square" value="{{old('coordinator_number')}}" name="coordinator_number">
                                                                </div>                                  
                                                        </div>
                                                        <div class="row col-sm-12">
                                                        <div class="form-group col-sm-6" style="margin-bottom:0px;">
                                                                <label>Upload Featured Image</label>
                                                                <div class="input-group">
                                                                    <span class="input-group-btn">
                                                                        <span class="btn btn-primary btn-file">
                                                                            Browse images… <input type="file" name="featured_image" id="imgInp">
                                                                        </span>
                                                                    </span>
                                                                    <input type="text" class="form-control" name="featured_image" readonly>
                                                                </div>
                                                                <img style="padding-bottom: 15px;" id='img-upload'/>
                                                            </div>
                                                        </div>
                                            
                                                    <div class="row col-sm-12">
                                                      <div class="form-group col-sm-6">
                                                          <label for="image">Select Documents for event</label>
                                                          <div class="input-group">
                                                          <span class="input-group-btn">
                                                                <span class="btn btn-primary btn-file">
                                                                    Browse document files… <input type="file" name="document" id="imgInp">
                                                                </span>
                                                            </span>
                                                            <input type="text" class="form-control" name="document" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                      
                                                      <div class="form-group col-sm-12">
                                                          <button type="submit" class="btn btn-md btn-success" id="add">
                                                          <i class="icon-check2">Add Event</i> 
                                                        </button>
                                                      </div>  
                                                    </div>
                                                  </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <div class="row">
                                <div class="col-xs-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title"><a data-action="collapse"><i class="fa fa-calendar" aria-hidden="true"></i> Upcoming Events</a></h4>
                                            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                                            <div class="heading-elements">
                                                <ul class="list-inline mb-0">
                                                    <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card-body collapse in">
                                            <div class="card-block card-dashboard" style="padding-bottom: 0px; padding-top: 10px;">
                                                <p>Upcoming Events are displayed here. You can edit, view or delete the description of the event here.</p>
                                            </div>
                                            <div class="table-responsive">
                                                <table class="table mb-0">
                                                    <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>Event Title</th>
                                                            <th>Event Date</th>
                                                            <th>Event Venue</th>
                                                            <th>Event Time</th>
                                                            <th>Actions</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($events as $event)
                                                        <?php
                                                            $date_facturation = \Carbon\Carbon::parse($event->event_date);
                                                        ?>
                                                        @if($date_facturation >= Carbon\Carbon::today())
                                                        <tr>
                                                            <td>{{$event->id}}</td>
                                                            <td>{{str_limit($event->title, $limit = 30, $end = '..')}}</td>
                                                            <td>{{date('M j,Y',strtotime($event->event_date))}}</td>
                                                            <td>{{str_limit($event->venue, $limit = 30, $end = '..')}}</td>
                                                            <td> {{date('h:i A', strtotime($event->start_time))}} to {{date('h:i A', strtotime($event->end_time))}}</td>
                                                            <td>
                                                                <a href="{{route('events.view',$event->id)}}" class="btn btn-sm btn-info">View</a>
                                                                <a href="{{route('events.edit',$event->id)}}" class="btn btn-sm btn-primary">Edit</a>
                                                                <a href="#" id="delete{{$event->id}}" class="btn btn-sm btn-danger">Delete</a>
                                                            </td>
                                                        </tr>
                                                        @endif
                                                        @endforeach   
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                    <div class="col-xs-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title"><a data-action="collapse"><i class="fa fa-calendar" aria-hidden="true"></i> Past Events</a></h4>
                                                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                                                <div class="heading-elements">
                                                    <ul class="list-inline mb-0">
                                                        <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="card-body collapse in">
                                                <div class="card-block card-dashboard" style="padding-bottom: 0px; padding-top: 10px;">
                                                    <p>Past Events are displayed here. You can add pictures of the events from here.</p>
                                                </div>
                                                <div class="table-responsive">
                                                    <table class="table mb-0">
                                                        <thead>
                                                            <tr>
                                                                <th>ID</th>
                                                                <th>Event Title</th>
                                                                <th>Actions</th>
                                                                <th>Upload pictures</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($events as $event)
                                                            <?php
                                                                $date_facturation = \Carbon\Carbon::parse($event->event_date);
                                                            ?>
                                                            @if($date_facturation < Carbon\Carbon::today())
                                                            <tr>
                                                                <td>{{$event->id}}</td>
                                                                <td>{{str_limit($event->title, $limit = 30, $end = '..')}}</td>
                                                                <td>
                                                                    <a href="{{route('events.view',$event->id)}}" class="btn btn-sm btn-info">View</a>
                                                                    <a href="{{route('events.edit',$event->id)}}" class="btn btn-sm btn-primary">Edit</a>
                                                                    <a href="#" id="delete{{$event->id}}" class="btn btn-sm btn-danger">Delete</a>
                                                                </td>
                                                                <td>
                                                                                <!-- Button trigger modal -->
                                                                    <a class="btn btn-success" href="{{ route('events.images.index',$event->id) }}">
                                                                        <i class="fa fa-picture-o" aria-hidden="true"></i> Add Images
                                                                    </a>    
                                                                </td>
                                                            </tr>
                                                            @endif

                                                            @endforeach   
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade text-xs-left" id="uploadImagesModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel5" aria-hidden="true" style="min-height:500px;min-width:500px;">
        <div class="modal-dialog" role="document">
            <div style="text-align:right">             
                <button id="cross"  type="button" class="btn btn-outline-secondary" data-dismiss="modal" style="background-color: #185284;color: #fff;">&times;</button>
            </div>            
            <div class=" uploader__box js-uploader__box l-center-box">
                <form class="form" enctype="multipart/form-data" method="POST" >
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="uploader__contents">
                            <label class="button button--secondary" for="fileinput">Select Files</label>
                            <input id="fileinput" class="uploader__file-input" name="image" type="file" multiple value="Select Files">
                            <input type="hidden" value="" id="eid" name="id"/>
                        </div>
                        <button class="button button--big-bottom" type="submit">Upload Selected Files</button>
                    </div>
                </form> 
            </div>
        </div>
    </div>
    @section('js')
    <script type="text/javascript">
        $(document).ready(function() {       
              $("[id*='delete']").click(function(){
                var id = $(this).attr("id").slice(6);
                swal({
                  title: 'Are you sure?',
                  text: "You won't be able to revert this!",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                  if (result.value) {
                    $.post("{{route('events.destroy')}}",{id:id,_token:"{{csrf_token()}}"},function(data){  
                        swal({
                          title:"Deleted Successfully",
                          type:"success"
                        }).then(function(){
                          window.location.reload();
                        })
              })
                  }
                  })
      
              });  
      
          });
      </script>
    <script>
            $(document).ready( function() {
                $(document).on('change', '.btn-file :file', function() {
                var input = $(this),
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [label]);
                });
        
                $('.btn-file :file').on('fileselect', function(event, label) {
                    
                    var input = $(this).parents('.input-group').find(':text'),
                        log = label;
                    
                    if( input.length ) {
                        input.val(log);
                    } else {
                        if( log ) alert(log);
                    }
                
                });
                function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        
                        reader.onload = function (e) {
                            $('#img-upload').attr('src', e.target.result);
                        }
                        
                        reader.readAsDataURL(input.files[0]);
                    }
                }
        
                $("#imgInp").change(function(){
                    readURL(this);
                }); 	
            });
    </script>


@endsection