@extends('layouts.app')

@section('body')
@section('title','| Gallery')


<section id="gallery_page" class="gallery_page">
    <div class="container">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10 dot">
                <h2 class="section-title">Albums</h2>
              </div>
            <div class="col-md-1"></div>
        </div>
    </div>
    <div class="container_fluent gallery_portfolio">
        <ul class="filter">
            @foreach($galleries as $category=>$albums)
                <li class="{{ $loop->iteration == 1? 'active' : '' }}"><a href="#" data-filter=".{{ $category }}" id="{{ $category }}">{{ $category }}</a></li>
            @endforeach
            <li><a href="#" id="facebook" data-filter=".facebook">Facebook</a></li>
        </ul>
        <div class="row_fluent">
            <div class="portfolio_grid">
                @foreach($galleries as $category=>$albums)
                    @include('frontend.partials.gallery')
                    <input type="hidden" value='{{ $albums->last()->created_at }}' id="last{{$category}}"/>
                @endforeach
                <div id="albums" class="facebook portfolio_grid_item wow fadeInUp">

                </div>
            </div>
            <div class="load_more">
                <a style="cursor:pointer" id="more">load more</a>
            </div>
        </div>
    </div>
</section>

@stop
@section('js')
<script>
    $(document).ready(function() {
        var _selector = $('.filter .active').children('a').attr('id');
        
        var itemId;

        $.ajaxSetup({ cache: true });
        var next = '';
        $.getScript('https://connect.facebook.net/en_US/sdk.js', function(){
            FB.init({
                appId: '1736045103105696',
                version: 'v2.12' // or v2.1, v2.2, v2.3, ...
            });     
            /* make the API call */
            FB.api(
                '/LHCCK',
                'GET',
                {"fields":"albums.limit(12){name,photos.limit(1){images}}",'access_token': '1736045103105696|eMXM_137WEw3s4NMEeUf8_-F0DE'},
                function(response) {
                    var html = '';
                    for(var i=0;i<response.albums.data.length;i++){
                        html += '<div class="col-portfolio col-xs-12 col-sm-6 col-md-4 col-lg-3 portfolio_grid_item wow fadeInUp"><figure class="hover-line"><div class="image-holder"><img alt="" style="height:217px;width:325px" src="'+response.albums.data[i].photos.data[0].images[0].source+'"></div><figcaption><div class="portfolio-lead"><div class="gallery_description"><p><span><a href="/gallery/facebook/'+response.albums.data[i].id+'">'+response.albums.data[i].name+'</a></span></p></div></div></figcaption></figure></div>';
                    
                    }
                    if(response.albums.paging.next)
                        next = response.albums.paging.next;
   
                    $('#albums').append(html);
                    
                    if(!_selector)
                        $('#facebook').trigger('click');
                }
            );

            $('#more').click(function(){
                _selector = $('.filter .active').children('a').attr('id');
                $('#more').text('LOADING...');
                if(_selector == 'facebook'){
                    if(!next){
                        $('#more').text('No more Fb albums');
                        return;
                    };
                    FB.api(
                        next,
                        'GET',
                        
                        function(response) {
                            var html = '';
                            for(var i=0;i<response.data.length;i++){
                                if(!response.data[i].photos) continue;
                                html += '<div class="col-portfolio col-xs-12 col-sm-6 col-md-4 col-lg-3 portfolio_grid_item wow fadeInUp"><figure class="hover-line"><div class="image-holder"><img style="height:217px;width:325px" alt="" src="'+response.data[i].photos.data[0].images[0].source+'"></div><figcaption><div class="portfolio-lead"><div class="gallery_description"><p><span><a href="/gallery/facebook/'+response.data[i].id+'">'+response.data[i].name+'</a></span></p></div></div></figcaption></figure></div>';
                            }
                            $('#albums').append(html);                            
                            $('#facebook').trigger('click');
                            if(response.paging.next)
                                next = response.paging.next;
                            else{
                                $('#more').text('No more fb albums');
                                next = null;
                            }
                        }
                    );
                }
                else{
                    itemId = $('#last'+_selector).val();

                    $.ajax({
                        url: "{{ route('albums.getmore') }}" ,
                        type: "get",
                        data : { category : _selector ,itemId: itemId},
                    })
                    .done(function(response)
                    {
                        if(response.data == ''){
                            $('#more').text('No more '+_selector+' albums');
                        }
                        else{
                            $('#last'+_selector).val(response.last.date);
                            
                            var $container = $('.portfolio_grid');
                            var $content = $(response.data);
                            $container.append( $content ).isotope( 'appended', $content )
                            $('#'+_selector).trigger('click');
                        }

                    })
                    .fail(function(){
                        location.reload();
                    });
                }
            });
        });

        
    });
</script>
@stop
