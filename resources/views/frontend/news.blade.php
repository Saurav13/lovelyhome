@extends('layouts.app')
@section('body')
@section('title','| News')
<section id="news" class="news" style="padding-top:20px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="section-title">Latest news</h2>
                </div>
            </div>
            @foreach($news->chunk(4) as $chunk)
            @foreach($chunk as $n)
            @if($loop->index==0)
            <div class="row mb_50">
                <div class="col-md-6 col-sm-12 mb_40 wow fadeInUp" >
                    <div class="news_date b_red t_white" style="padding-bottom:2px;">
                        <div class="news_date_number">
                            <p><span>{{date('j',strtotime($n->created_at))}}</span></p>
                            <p>{{date('M,Y',strtotime($n->created_at))}}</p>
                        </div>
                        <div class="news_date_category">
                            <a href="{{route('single.news',$n->slug)}}">Lovely Home</a>
                        </div>
                        <div class="news_date_comments">
                                <img src="{{asset('backend/systemimages/small-logo.png')}}" style="padding-left:10px;"/>
                            </div>
                    </div>
                    <div class="news_describe">
                        <div class="news_img">
                            <a href="{{route('single.news',$n->slug)}}"><img src="{{asset('news-images'.'/'.$n->image)}}" alt="{{$n->title}}"></a>
                        </div>
                        <div class="news_text b_blue t_white">
                                <h4><a href="{{route('single.news',$n->slug)}}">{{(str_limit($n->title,$limit = 30,$end = '...'))}}</a></h4>
                            <p style="min-height:100px;">{{strip_tags(str_limit($n->description, $limit = 300, $end = '...'))}}</p>
                        </div>
                    </div>
                </div>
               
                @elseif($loop->index==1)
                <div class="col-md-6 col-sm-12 mb_40 wow fadeInUp">
                    <div class="news_date b_red t_white" style="padding-bottom:2px;">
                            <div class="news_date_number">
                                    <p><span>{{date('j',strtotime($n->created_at))}}</span></p>
                                    <p>{{date('M,Y',strtotime($n->created_at))}}</p>
                                </div>
                                <div class="news_date_category">
                                    <a href="{{route('single.news',$n->slug)}}">Lovely Home</a>
                                </div>
                                <div class="news_date_comments">
                                        <img src="{{asset('backend/systemimages/small-logo.png')}}" style="padding-left:10px;"/>
                                    </div>
                            </div>
                            <div class="news_describe">
                                <div class="news_img">
                                    <a href="{{route('single.news',$n->slug)}}"><img src="{{asset('news-images'.'/'.$n->image)}}" alt="{{$n->title}}"></a>
                                </div>
                        <div class="news_text b_red t_white">
                                <h4><a href="{{route('single.news',$n->slug)}}">{{(str_limit($n->title,$limit = 30,$end = '...'))}}</a></h4>
                                <p style="min-height:100px;">{{strip_tags(str_limit($n->description, $limit = 300, $end = '...'))}}</p>
                        </div>
                    </div>
                </div>
            </div>
            
            @elseif($loop->index==2)        
            <div class="row">
                     <div class="col-md-6 col-sm-12 mb_40 wow fadeInUp" >
                    <div class="news_date b_red t_white" style="padding-bottom:2px;">
                            <div class="news_date_number">
                                    <p><span>{{date('j',strtotime($n->created_at))}}</span></p>
                                    <p>{{date('M,Y',strtotime($n->created_at))}}</p>
                                </div>
                                <div class="news_date_category">
                                    <a href="{{route('single.news',$n->slug)}}">Lovely Home</a>
                                </div>
                                <div class="news_date_comments">
                                        <img src="{{asset('backend/systemimages/small-logo.png')}}" style="padding-left:10px;"/>
                                    </div>
                            </div>
                            <div class="news_describe">
                                <div class="news_img">
                                    <a href="{{route('single.news',$n->slug)}}"><img src="{{asset('news-images'.'/'.$n->image)}}" alt="{{$n->title}}"></a>
                                </div>
                        <div class="news_text b_green_light t_grey">
                                <h4><a href="{{route('single.news',$n->slug)}}">{{(str_limit($n->title,$limit = 30,$end = '...'))}}</a></h4>
                                <p style="min-height:100px;">{{strip_tags(str_limit($n->description, $limit = 300, $end = '...'))}}</p>
                        </div>
                    </div>
                </div>
                @elseif($loop->index==3)
                <div class="col-md-6 col-sm-12 mb_40 wow fadeInUp">
                        <div class="news_date b_red t_white"  style="padding-bottom:2px;">
                                <div class="news_date_number">
                                        <p><span>{{date('j',strtotime($n->created_at))}}</span></p>
                                        <p>{{date('M,Y',strtotime($n->created_at))}}</p>
                                    </div>
                                    <div class="news_date_category">
                                        <a href="{{route('single.news',$n->slug)}}">Lovely Home</a>
                                    </div>
                                    <div class="news_date_comments">
                                            <img src="{{asset('backend/systemimages/small-logo.png')}}" style="padding-left:10px;"/>
                                        </div>
                                </div>
                                <div class="news_describe">
                                    <div class="news_img">
                                        <a href="{{route('single.news',$n->slug)}}"><img src="{{asset('news-images'.'/'.$n->image)}}" alt="{{$n->title}}"></a>
                                    </div>
                            <div class="news_text b_green_dark t_grey">
                                    <h4><a href="{{route('single.news',$n->slug)}}">{{(str_limit($n->title,$limit = 30,$end = '...'))}}</a></h4>
                                    <p style="min-height:100px;">{{strip_tags(str_limit($n->description, $limit = 300, $end = '...'))}}</p>
                            </div>
                        </div>
                    </div>
                </div>

                @endif
                @endforeach
                @endforeach
        </div>
        <br>
        <br>
        <div class="text-center">
            {!! $news->links()!!}
        </div>
        <br>
        <br>
        <br>	
    </section>
@stop