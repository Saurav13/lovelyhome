@extends('admin.main')
@section('title','| Activities')
@section('body')
<style type="text/css">
    
  
    h1 {
      color: black;
      padding-left: 10px;
    }
  
     h2 {
      color: black;
      padding-left: 10px;
    }
  
    #hover {
      color: rgba(188, 175, 204, 0.9);
    }
  
    h2#testimonials {
      color: #fffae3;
    }
  
    div#all {
      width: 100%;
      height: 100%;
    }
  
  
    /* generic css */
  
    .view {
      margin: 10px;
      float: left;
      border: 10px solid #fff;
      overflow: hidden;
      position: relative;
      text-align: center;
      box-shadow: 1px 1px 2px #e6e6e6;
      cursor: default;
      background: #fff url(../images/bgimg.jpg) no-repeat center center
    }
  
    .view .mask,
    .view .content {
      width: 300px;
      height: 200px;
      position: absolute;
      overflow: hidden;
      top: 0;
      left: 0
    }
  
    .view img {
      display: block;
      position: relative
    }
  
    .view h2 {
      text-transform: uppercase;
      color: #fff;
      text-align: center;
      position: relative;
      font-size: 17px;
      font-family: Raleway, serif;
      padding: 10px;
      /*background: rgba(0, 0, 0, 0.8);*/
      margin: 20px 0 0 0
    }
  
    .view p {
      font-family: Merriweather, serif;
      font-style: italic;
      font-size: 14px;
      position: relative;
      color: #fff;
      padding: 0px 20px 0px;
      text-align: center
    }
  
    .view a.info {
      display: inline-block;
      text-decoration: none;
      padding: 7px 14px;
      background: #000;
      color: #fff;
      font-family: Raleway, serif;
      text-transform: uppercase;
      box-shadow: 0 0 1px #000
    }
  
    .view a.info:hover {
      box-shadow: 0 0 5px #000
    }
  
  
    /*1*/
  
    .view-first img {
      /*1*/
      transition: all 0.2s linear;
      width: 300px;
      height: 200px;
    }
  
    .view-first .mask {
      opacity: 0;
      background-color: rgba(58, 1, 132, 0.44);
      transition: all 0.4s ease-in-out;
    }
  
    .view-first h2 {
      transform: translateY(-100px);
      opacity: 0;
      font-family: Raleway, serif;
      transition: all 0.2s ease-in-out;
    }
  
    .view-first p {
      transform: translateY(100px);
      opacity: 0;
      transition: all 0.2s linear;
    }
  
    .view-first a.info {
      opacity: 0;
      transition: all 0.2s ease-in-out;
    }
  
  
    /* */
  
    .view-first:hover img {
      transform: scale(1.1);
    }
  
    .view-first:hover .mask {
      opacity: 1;
    }
  
    .view-first:hover h2,
    .view-first:hover p,
    .view-first:hover a.info {
      opacity: 1;
      transform: translateY(0px);
    }
  
    .view-first:hover p {
      transition-delay: 0.1s;
    }
  
    .view-first:hover a.info {
      transition-delay: 0.2s;
    }
  
    </style>
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,700|Merriweather' rel='stylesheet' type='text/css'>

  <div class="app-content content container-fluid">
    <div class="content-wrapper">        
      <div class="content-body">
        <div class="row">
          <div class="col-xs-12 mt-1">
            <h4>Activities</h4>
            <hr>
          </div>
        </div>

        <div class="row match-height">
          <div class="col-xs-12">
            <div class="card">
              <div class="card-header">
                <button class="btn btn-md btn-primary"  data-toggle="modal" data-target="#addImage"><i class="fa fa-plus" aria-hidden="true"></i> Activity</button>
              </div>
              <div class="card-body">
                <div class="card-block">
                  <ul class="nav nav-tabs nav-justified">
                    
                    <li class="nav-item">
                      <a class="nav-link active" id="photos-tab" data-toggle="tab" href="#photos" aria-controls="photos" aria-expanded="false">Indoor</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="arts-tab" data-toggle="tab" href="#arts" aria-controls="arts">Outdoor</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="videos-tab" data-toggle="tab" href="#videos" aria-controls="videos">Sports</a>
                    </li>
                    <li class="nav-item">
                            <a class="nav-link" id="events-tab" data-toggle="tab" href="#events" aria-controls="events">Events</a>
                    </li>
                    <li class="nav-item">
                                <a class="nav-link" id="extras-tab" data-toggle="tab" href="#extras" aria-controls="extras">Extras</a>
                    </li>
                    
                  </ul>
                  <div class="tab-content px-1 pt-1">
                      
                        <div class="tab-pane fade active in" id="photos" role="tabpanel" aria-labelledby="photos-tab" aria-expanded="true">
                         <div id="slider" class="row">
                                @foreach($activities->where('category','indoor') as $s)
                                      <div class="view view-first">
                                        <img src="{{asset('activities-images'.'/'.$s->image)}}" id="img{{$s->id}}" />
                                        <div class="mask">
                                                <input type="hidden" value="{{$s->category}}" id="category{{$s->id}}"/>
                                          <h2 id="title{{$s->id}}">{{$s->title}}</h2>
                                          <a href="#" class="info"  data-toggle="modal" data-target="#ActivityEdit" cid={{$s->id}} id="activitiesedit{{$s->id}}">Edit</a>
                                          <a cid={{$s->id}} id="delete{{$s->id}}" class="info">Delete</a>
                                        </div>
                                      </div>
                                    @endforeach  
                        </div>
                    </div>

                    <div class="tab-pane fade" id="arts" role="tabpanel" aria-labelledby="arts-tab" aria-expanded="false">
                            <div id="slider" class="row">
                                    @foreach($activities->where('category','outdoor') as $s)
                                          <div class="view view-first">
                                            <img src="{{asset('activities-images'.'/'.$s->image)}}" id="img{{$s->id}}" />
                                            <input type="hidden" value="{{$s->category}}" id="category{{$s->id}}"/>
                                            <div class="mask">
                                              <h2 id="title{{$s->id}}">{{$s->title}}</h2>
                                              <a href="#" class="info"  data-toggle="modal" data-target="#ActivityEdit" cid={{$s->id}} id="activitiesedit{{$s->id}}">Edit</a>
                                              <a cid={{$s->id}} id="delete{{$s->id}}" class="info">Delete</a>
                                            </div>
                                          </div>
                                        @endforeach  
                            </div>
                    </div>

                    <div class="tab-pane fade" id="videos" role="tabpanel" aria-labelledby="videos-tab" aria-expanded="false">
                            <div id="slider" class="row">
                                    @foreach($activities->where('category','sports') as $s)
                                          <div class="view view-first">
                                            <img src="{{asset('activities-images'.'/'.$s->image)}}" id="img{{$s->id}}" />
                                            <div class="mask">
                                                    <input type="hidden" value="{{$s->category}}" id="category{{$s->id}}"/>
                                              <h2 id="title{{$s->id}}">{{$s->title}}</h2>
                                              <a href="#" class="info"  data-toggle="modal" data-target="#ActivityEdit" cid={{$s->id}} id="activitiesedit{{$s->id}}">Edit</a>
                                              <a cid={{$s->id}} id="delete{{$s->id}}" class="info">Delete</a>
                                            </div>
                                          </div>
                                        @endforeach  
                            </div>
                        
                    </div>

                    <div class="tab-pane fade" id="events" role="tabpanel" aria-labelledby="events-tab" aria-expanded="false">
                            <div id="slider" class="row">
                                    @foreach($activities->where('category','events') as $s)
                                          <div class="view view-first">
                                            <img src="{{asset('activities-images'.'/'.$s->image)}}" id="img{{$s->id}}" />
                                          <input type="hidden" value="{{$s->category}}" id="category{{$s->id}}"/>
                                            <div class="mask">
                                              <h2 id="title{{$s->id}}">{{$s->title}}</h2>
                                              <a href="#" class="info"  data-toggle="modal" data-target="#ActivityEdit" cid={{$s->id}} id="activitiesedit{{$s->id}}">Edit</a>
                                              <a cid={{$s->id}} id="delete{{$s->id}}" class="info">Delete</a>
                                            </div>
                                          </div>
                                        @endforeach  
                            </div>
                        
                    </div>

                    <div class="tab-pane fade" id="extras" role="tabpanel" aria-labelledby="extras-tab" aria-expanded="false">
                            <div id="slider" class="row">
                                    @foreach($activities->where('category','extras') as $s)
                                          <div class="view view-first">
                                            <img src="{{asset('activities-images'.'/'.$s->image)}}" id="img{{$s->id}}" />
                                            <input type="hidden" value="{{$s->category}}" id="category{{$s->id}}"/>
                                            <div class="mask">
                                              <h2 id="title{{$s->id}}">{{$s->title}}</h2>
                                              <a href="#" class="info"  data-toggle="modal" data-target="#ActivityEdit" cid={{$s->id}} id="activitiesedit{{$s->id}}">Edit</a>
                                              <a cid={{$s->id}} id="delete{{$s->id}}" class="info">Delete</a>
                                            </div>
                                          </div>
                                        @endforeach  
                            </div>
                        
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      <!-- add materials section -->
      </div>
    </div>
  </div>
{{--  Add Activity  --}}
{{-- Add Image --}}
<div id="addImage" class="modal fade" role="dialog">
        <div class="modal-dialog">
      
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Add Activity</h4>
            </div>
            <div class="modal-body">
            <form action="{{route('activities.store')}}" enctype="multipart/form-data" method="POST">
              {{csrf_field()}}
              <div class="form-group">
                  <label for="title">Title:</label>
                  <input type="text" name="title" placeholder="Title of the activity" class="form-control" value="{{old('title')}}">
                </div>
                <div class="form-group">
                <label for="category">Category:</label>
                <select class="form-control" name="category">
                        <option value="indoor">Indoor</option>
                        <option value="outdoor">Outdoor</option>
                        <option value="sports">Sports</option>
                        <option value="events">Events</option>
                        <option value="extras">Extras</option>
                      </select>
                </div>
                    <div class="form-group">
                      <label for="image">Image*:</label>
                      <input type="file" name="image" class="form-control-file">
                    </div>
            
              <div class="form-group">
                <input type="submit" name="submit" value="Add" class="btn btn-block btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
      </div>
      {{-- End of add image --}}
      {{--  Edit Activity  --}}
      <div id="ActivityEdit" class="modal fade" role="dialog">
            <div class="modal-dialog">
          
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Edit Activity</h4>
                </div>
                <div class="modal-body">
                <form action="{{route('activities.update')}}" enctype="multipart/form-data" method="POST">
                  {{csrf_field()}}
                  <div class="form-group">
                      <label for="title">Title:</label>
                      <input type="text" name="title" id="titleedit" placeholder="Title of the activity" class="form-control" value="">
                    </div>
                    <div class="form-group">
                    <label for="category">Category:</label>
                    <select class="form-control" name="category" id="catedit">
                            <option value="indoor">Indoor</option>
                            <option value="outdoor">Outdoor</option>
                            <option value="sports">Sports</option>
                            <option value="events">Events</option>
                            <option value="extras">Extras</option>
                          </select>
                    </div>
                        <div class="form-group">
                          <label for="image">Image (If you wish to change the current image):</label>
                          <input type="file" name="image" class="form-control-file">
                        </div>
                        <input type="hidden" id="id" name="id" value=""/>
                
                  <div class="form-group">
                    <input type="submit" name="submit" value="Save" class="btn btn-block btn-success">
                  </div>
                </form>
              </div>
            </div>
          </div>
          </div>
      {{--  End edit activity  --}}
      
{{--  End ADD ACTIVITY  --}}

@section('js')
    
    <script type="text/javascript">
  $(document).ready(function() {       
    $("[id*='delete']").click(function(){
      var id = $(this).attr("id").slice(6);
      swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          // console.log(result.value)
          $.post("{{route('activities.destroy')}}",{id:id,_token:"{{csrf_token()}}"},function(data){ 
            swal({
              title:"Deleted Successfully",
              type:"success"
            }).then(function(){
              window.location.reload();
            })
          })
        }
      })
    });
    });
    $(document).ready(function(){
        $("[id*='activitiesedit']").click(function(){
        var id = $(this).attr("id").slice(14)
        console.log(id);
        var title=$('#title'+id).html();
        var category=$('#category'+id).val();
        console.log(category);
        $('#catedit').val(category);
        $('#titleedit').val(title);
        $('#id').val(id);
    });
    });
</script>
@endsection
        
        