"use strict"; 
$(document).on( 'ready', function() { 
    $("#contact_form").submit(function(){ 
        var form = $(this); 
        var error = false;
        form.find('input').each( function(){
            if ($(this).val() == '') {  
                error = true; 
            }
        });
        form.find('textarea').each( function(){
            if ($(this).val() == '') {  
                error = true; 
            }
        });
        if (!error) { 
            var data = form.serialize();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({ 
                type: 'POST', 
                url: "/contact_us",
                dataType: 'json', 
                data: data, 
                beforeSend: function(data) { 
                    form.find('button').attr('disabled', 'disabled');
                    $('#form-messages').html('');
                },
                success: function(data){ 
                    if (data['error']) { 
                         $('#form-messages').html('Please Provide All Information.');
                    } else { 
                        $("#messegeResult button").fadeOut('fast');
                        setTimeout(function(){
                          $('#messegeResult p').fadeIn('slow')},200);           
                        setTimeout(function(){
                          $('#messegeResult p').fadeOut('slow')},3000); 
                        setTimeout(function(){
                          $('#messegeResult button').fadeIn('slow')},3500); 
                        $('#contact_form')[0].reset();
                    }
                 },
               error: function (xhr, ajaxOptions, thrownError) { 
                    if(xhr.status == 422)
                        $('#form-messages').html('Please Provide All Information.');
                    else
                        $('#form-messages').html('Something went wrong. Please try again.');
                 },
               complete: function(data) { 
                    form.find('button').prop('disabled', false); 
                 }
                          
                 });
        }
        return false; 
    });

    $("#feedback_form").submit(function(){ 
        var form = $(this); 
        var error = false;
        form.find('input').each( function(){
            if ($(this).val() == '') {  
                error = true; 
            }
        });
        form.find('textarea').each( function(){
            if ($(this).val() == '') {  
                error = true; 
            }
        });
        if (!error) { 
            var data = form.serialize();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({ 
                type: 'POST', 
                url: "/feedback",
                dataType: 'json', 
                data: data, 
                beforeSend: function(data) { 
                    form.find('button').attr('disabled', 'disabled');
                    $('#form-messages1').html('');
                },
                success: function(data){ 
                    if (data['error']) { 
                        $('#form-messages1').html('Please Provide All Information.');
                    } else { 
                        $("#messegeResult1 button").fadeOut('fast');
                        setTimeout(function(){
                          $('#messegeResult1 p').fadeIn('slow')},200);           
                        setTimeout(function(){
                          $('#messegeResult1 p').fadeOut('slow')},3000); 
                        setTimeout(function(){
                          $('#messegeResult1 button').fadeIn('slow')},3500); 
                        $('#feedback_form')[0].reset();
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) { 
                    if(xhr.status == 422)
                        $('#form-messages1').html('Please Provide All Information.');
                    else
                        $('#form-messages1').html('Something went wrong. Please try again.');
                },
                complete: function(data) { 
                    form.find('button').prop('disabled', false); 
                }
                          
            });
        }
        return false; 
    });

    $("#contact_form_footer").submit(function(){ 
        var form = $(this); 
        var error = false;
        form.find('input').each( function(){
            if ($(this).val() == '') {  
                error = true; 
            }
        });
        if (!error) { 
            var data = form.serialize(); 
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({ 
                type: 'POST', 
                url: '/subscribe',
                dataType: 'json', 
                data: data, 
                beforeSend: function(data) { 
                    form.find('button').attr('disabled', 'disabled');
                    $('#newsletter_error').html('');
                },
                success: function(data){
                    $("#messegeResultFooter button").fadeOut('fast');
                    setTimeout(function(){
                        $('#messegeResultFooter p').fadeIn('slow')},200);           
                    setTimeout(function(){
                        $('#messegeResultFooter p').fadeOut('slow')},3000); 
                    setTimeout(function(){
                        $('#messegeResultFooter button').fadeIn('slow')},3500); 
                    $('#contact_form_footer')[0].reset();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    if(xhr.status == 422)
                        $('#newsletter_error').html(xhr.responseJSON.errors.email[0]);
                    else
                        $('#newsletter_error').html('Something went wrong. Please try again.');
                },
                complete: function(data) { 
                    form.find('button').prop('disabled', false); 
                }
                          
            });
        }
        return false; 
    });

    $("#comment_form").submit(function(){ 
        var form = $(this); 
        var error = false;
        if (form.find('textarea').val() == '') {  
            error = true; 
        }
        if (!error) { 
            var data = form.serialize(); 
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({ 
                type: 'POST', 
                url: form[0].action,
                dataType: 'json', 
                data: data, 
                beforeSend: function(data) {
                    form.find('button').html('ADDING').attr('disabled', 'disabled');
                    $('#comment_error').html('');
                },
                success: function(data){
                    $('#comments').prepend(data.html);
                    $('#num').text(parseInt($('#num').text())+1);
                    
                    $("#commentResult button").fadeOut('fast');
                    setTimeout(function(){
                        $('#commentResult p').fadeIn('slow')},200);           
                    setTimeout(function(){
                        $('#commentResult p').fadeOut('slow')},3000); 
                    setTimeout(function(){
                        $('#commentResult button').fadeIn('slow')},3500); 
                    $('#comment_form')[0].reset();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    if(xhr.status == 422)
                        $('#comment_error').html(xhr.responseJSON.errors.content[0]);
                    else
                        $('#comment_error').html('Something went wrong. Please try again.');
                },
                complete: function(data) { 
                    form.find('button').html('ADD').prop('disabled', false); 
                }
                          
            });
        }
        return false; 
    });

    $("#add_post_form").submit(function(){ 
        var formData = new FormData(this);
        var form = $(this); 
        var error = false;
        
        if (form.find('input[type=text]').val() == '') {  
                error = true; 
            }
        if (form.find('textarea').val() == '') {  
            error = true; 
        }
        if (!error) { 
            
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({ 
                type: 'POST', 
                url: form[0].action,
                enctype: 'multipart/form-data',
                dataType: 'json', 
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function(data) {
                    form.find('button').html('POSTING').attr('disabled', 'disabled');
                    $('#add_post_error').html('');
                },
                success: function(data){
                    $('#myPostsDiv').prepend(data.html);
                    
                    $("#add_post_result button").fadeOut('fast');
                    setTimeout(function(){
                        $('#add_post_result p').fadeIn('slow')},200);           
                    setTimeout(function(){
                        $('#add_post_result p').fadeOut('slow')},3000); 
                    setTimeout(function(){
                        $('#add_post_result button').fadeIn('slow')},3500); 
                    $('#add_post_form')[0].reset();
                    $('#uploadimg').attr('src','#');
                    $('#uploadimg').attr('hidden','hidden');
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    if(xhr.status == 422){
                        var error = '';
                        
                        $.each( xhr.responseJSON.errors, function( key, value ) {
                            error +=  value[0] ;
                            error += '<br>';
                        });
                        $('#add_post_error').html(error);
                    }
                    else
                        $('#add_post_error').html('Something went wrong. Please try again.');
                },
                complete: function(data) { 
                    form.find('button').html('POST').prop('disabled', false); 
                }            
            });
        }
        return false; 
    });

    $("#edit_post_form").submit(function(){
        var formData = new FormData(this);
        var form = $(this); 
        var error = false;
        if (form.find('input[type=text]').val() == '') {  
            error = true; 
        }
        if (form.find('textarea').val() == '') {  
            error = true; 
        }
        if (!error) { 
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({ 
                type: 'POST', 
                url: form[0].action,
                enctype: 'multipart/form-data',
                dataType: 'json', 
                data: formData,
                cache: false,
                contentType: false,
                processData: false, 
                beforeSend: function(data) {
                    form.find('button').html('UPDATING').attr('disabled', 'disabled');
                    $('#edit_post_error').html('');
                },
                success: function(data){
                    var mypost = data.mypost;
                    $('#edit_post_form').attr('action','');
                    $('#editTitle').val('');
                    $('#editContent').val('');

                    $('#postTitle'+mypost.id).text(mypost.title);
                    if(mypost.image)
                        $('#postImg'+mypost.id).removeAttr('hidden'); 
                    $('#postImg'+mypost.id).attr('src','/alumni_blogs/'+mypost.image).attr('alt',mypost.title);
                    $('#postContent'+mypost.id).html(mypost.content);

                    $("#edit_post_result button").fadeOut('fast');
                    setTimeout(function(){
                        $('#edit_post_result p').fadeIn('slow')},200);           
                    setTimeout(function(){
                        $('#edit_post_result p').fadeOut('slow')},3000); 
                    setTimeout(function(){
                        $('#edit_post_result button').fadeIn('slow')},3500);
                    setTimeout(function(){
                        $('#editpost').modal('hide')},3500);  
                    $('#edit_post_form')[0].reset();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    if(xhr.status == 422){
                        var error = '';
                        
                        $.each( xhr.responseJSON.errors, function( key, value ) {
                            error +=  value[0] ;
                            error += '<br>';
                        });
                        $('#edit_post_error').html(error);
                    }
                    else
                        $('#edit_post_error').html('Something went wrong. Please try again.');
                },
                complete: function(data) { 
                    form.find('button').html('UPDATE').prop('disabled', false); 
                }            
            });
        }
        return false; 
    });

    $("#delete_post_form").submit(function(){ 
        var form = $(this); 
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({ 
            type: 'POST', 
            url: form[0].action,
            dataType: 'json',
            beforeSend: function(data) {
                form.find('button').html('DELETING').attr('disabled', 'disabled');
                $('#delete_post_error').html('');
            },
            success: function(data){

                $('#delete_post_form').attr('action','');
                $('#deletepost').modal('hide');
                $('#mypost'+data.post_id).hide('slow', function(){ $('#mypost'+data.post_id).remove(); });
                
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $('#delete_post_error').html('Something went wrong. Please try again.');
            },
            complete: function(data) { 
                form.find('button').html('DELETE').prop('disabled', false); 
            }            
        });
        
        return false; 
    });
});