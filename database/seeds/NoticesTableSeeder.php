<?php

use Illuminate\Database\Seeder;

class NoticesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //  DB::table('news','20')->insert([
        // 	'title'=>str_random(10),
        // 	'body'=>str_random(100),
        // ]);
        DB::table('notices')->insert([
        	'title'=>str_random(10),
        	'body'=>str_random(100),
        ]);
    }
}
