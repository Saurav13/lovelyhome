@if(count($errors))
    <div style="color: black;background-color: #FEEFB3 background-image: url('warning.png');">
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{$error}}</li>
          @endforeach
      </ul>
  </div>
  @endif