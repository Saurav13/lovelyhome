@extends('layouts.app')

@section('body')
@section('title','| Alumini')

<style>
    alumini_tabs .nav-tabs {
    border: none;
}

@media(min-width: 300px)
{
    .alumini_tabs ul li a {
        padding: 8px 0;
        font-size: 1rem;
        border-radius: 50px;
        font-family: 'OpenSansSemiBold', sans-serif;
    }
}
@media(min-width: 600px)
{
    .alumini_tabs ul li a {
        padding: 8px 0;
        font-size: 1.5rem;
        border-radius: 50px;
        font-family: 'OpenSansSemiBold', sans-serif;
    }
}
@media (min-width: 1200px)
{
    .alumini_tabs ul li a {
        padding: 8px 0;
        font-size: 2rem;
        border-radius: 50px;
        font-family: 'OpenSansSemiBold', sans-serif;
    }
}
.alumini_tabs ul {
    position: relative;
    list-style-type: none;
    margin: 0;
    text-align: center;
    padding: 0;
}
.alumini_tabs ul li {
    display: inline-block;
    float: none;
    text-transform: uppercase;
    width: 23%;
    margin: 30px 7px;
}
.alumini_tabs ul li a {
   
}

.alumini_tabs ul li .tab_red {
    border: 2px solid #b70404;
    color: #b70404;
    -webkit-transition: all .2s linear;
    transition: all .2s linear;
}

.alumini_tabs ul li .tab_yel {
    border: 2px solid #fff400;
    color: #fff400;
    -webkit-transition: all .2s linear;
    transition: all .2s linear;
}

.alumini_tabs ul li .tab_green {
    border: 2px solid #49b450;
    color: #49b450;
    -webkit-transition: all .2s linear;
    transition: all .2s linear;
}


.nav-tabs>li>.tab_yel:hover, .ui_hover .nav-tabs>li>.tab_green {
    background: #fff400;
    color: #fff;
    border: 2px solid #fff400;
    -webkit-background-clip: padding-box;
    background-clip: padding-box;
}
.nav-tabs>li.active>.tab_yel, .nav-tabs>li.active>.tab_yel:focus, .nav-tabs>li.active>.tab_yel:hover {
    background: #fff400;
    color: #fff;
    border: 2px solid #fff400;
}
.post_img{
    height: 3.5rem;
}

.post_img img{
    height: inherit;
    border-radius: 50px;
}
.blog_link .buttonred {
    background-color: #b70404c7;
    border-color: #b70404c7;
}
.blog_link .buttonred a:hover {
    background-color: #fff;
    color: #b70404c7;
}
.blog_link .buttonred a {
    -webkit-transition: all .2s linear;
    transition: all .2s linear;
    color: #fff;
    font-size: 2rem;
    font-family: 'OpenSansSemiBold', sans-serif;
}

.blog_link .buttonyellow {
    background-color: #fff400c7;
    border-color: #fff400c7;
}
.blog_link .buttonyellow a:hover {
    background-color: #fff;
    color: #fff400c7;
}
.blog_link .buttonyellow a {
    -webkit-transition: all .2s linear;
    transition: all .2s linear;
    color: #fff;
    font-size: 2rem;
    font-family: 'OpenSansSemiBold', sans-serif;
}

.year_div{
    float:right;
}
.year_div select{
    font-size: 20px;
    border-radius: 10px;
    padding: 7px;
    margin: 10px;
    float: right;
    margin-right: 5rem;
    margin-left: 0px;
    z-index:9999;
}
.year_div h4{
    font-size: 20px;
    margin-top: 2rem;
}
</style>
<section id="blog_page" class="blog_page">
    <div class="container">
        <div class="row pb_20">
            <div class="col-md-1"></div>
            <div class="col-md-10 dot">
                <h2 class="section-title">Welcome Back!!!</h2>
                <div class="alumini_tabs">
                    <ul class="nav nav-tabs">
                        <li><a class="tab_red acttabfont" href="#gallery" data-toggle="tab">Gallery</a></li>
                        <li class="active"><a class="tab_green acttabfont" href="#blog" data-toggle="tab">Blog</a></li>
                        
                        <li><a class="tab_yel acttabfont" href="#myposts" data-toggle="tab">My posts</a></li>
                       
                        
                    </ul>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
        <div class="tab-content about_tabs_content">
            <div class="tab-pane card-container" id="gallery">
                
                <section id="teachers_portfolio" class="teachers_portfolio text-center section" style="border-top:none; padding:0px;">
                    <div class="container">
                        <div class="year_div">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4>year:</h4>
                                </div>
                                <div class="col-md-6">
                                    <select id="selectyear">
                                        <option>2001</option>
                                        <option>2002</option>
                                        <option>2003</option>
                                        <option>2004</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        
                        <div class="row">
                            <div class="col-md-3 col-sm-4 portfolio_grid_item wow">
                                <div class="events">
                                    <div class="teacher_block grey_block">
                                        <div class="teacher_avatar">
                                            <img src="/frontend-assets/img/teacher/teacher_1.jpg" alt="">
                                        </div>
                                        <div class="teacher_about">
                                            <h3><a href="teacher.html">Helen Douglas</a></h3>
                                            <div class="teacher_link social_icon t_green_dark">
                                                <ul>
                                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-4 portfolio_grid_item wow">
                                <div class="events">
                                    <div class="teacher_block blue_block">
                                        <div class="teacher_avatar">
                                            <img src="/frontend-assets/img/teacher/teacher_2.jpg" alt="">
                                        </div>
                                        <div class="teacher_about">
                                            <h3><a href="teacher.html">John Bishop</a></h3>
                                            <div class="teacher_link social_icon">
                                                <ul>
                                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-4 portfolio_grid_item wow">
                                <div class="events">
                                    <div class="teacher_block yellow_block">
                                        <div class="teacher_avatar">
                                            <img src="/frontend-assets/img/teacher/teacher_3.jpg" alt="">
                                        </div>
                                        <div class="teacher_about">
                                            <h3><a href="teacher.html">Bryan Barker</a></h3>
                                            <div class="teacher_link social_icon">
                                                <ul>
                                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-4 portfolio_grid_item wow">
                                <div class="events">
                                    <div class="teacher_block red_block">
                                        <div class="teacher_avatar">
                                            <img src="/frontend-assets/img/teacher/teacher_4.jpg" alt="">
                                        </div>
                                        <div class="teacher_about">
                                            <h3><a href="teacher.html">Chloe Fowler</a></h3>
                                            <div class="teacher_link social_icon">
                                                <ul>
                                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class=" col-md-3 col-sm-4 portfolio_grid_item wow">
                                <div class="events">
                                    <div class="teacher_block yellow_block">
                                        <div class="teacher_avatar">
                                            <img src="/frontend-assets/img/teacher/teacher_5.jpg" alt="">
                                        </div>
                                        <div class="teacher_about">
                                            <h3><a href="teacher.html">Andra Stevens</a></h3>
                                            <div class="teacher_link social_icon">
                                                <ul>
                                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-4 portfolio_grid_item wow">
                                <div class="events">
                                    <div class="teacher_block grey_block">
                                        <div class="teacher_avatar">
                                            <img src="/frontend-assets/img/teacher/teacher_7.jpg" alt="">
                                        </div>
                                        <div class="teacher_about">
                                            <h3><a href="teacher.html">Mariah Wilson</a></h3>
                                            <div class="teacher_link social_icon t_green_dark">
                                                <ul>
                                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-4 portfolio_grid_item wow">
                                <div class="events">
                                    <div class="teacher_block blue_block">
                                        <div class="teacher_avatar">
                                            <img src="/frontend-assets/img/teacher/teacher_8.jpg" alt="">
                                        </div>
                                        <div class="teacher_about">
                                            <h3><a href="teacher.html">Shonda Bradford</a></h3>
                                            <div class="teacher_link social_icon">
                                                <ul>
                                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
            <div class="tab-pane card-container active" id="blog">
                    <section id="authorization_page" class="authorization_page">
                        
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-10 dot">
                                    <h2 class="section-title">Sign up for alumini</h2>
                                    <p class="section-description">Already have an account? - <a href="login.html">Log In</a></p>
                                </div>
                                <div class="col-md-1"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <form>
                                        <div class="reg_box">
                                            <input id="reg_email" name="reg_email" type="text" placeholder="Your email" required="required">
                                            <input class="reg_pass" id="reg_pass" name="reg_pass" type="password" placeholder="********" required="required">
                                            <input class="rep_pass" id="rep_pass" name="rep_pass" type="text" placeholder="Repeat password" required="required">
                                        </div>
                                        <button class="button" type="submit">Register</button>
                                    </form>
                                </div>
                            </div>
                    </section>
            
                <div class="row">
                    <div class="col-md-10" style="margin-left:8.3%">
                        <div class="blog_block wow fadeIn">
                            <h3 class="blog_title">Preparing for the New Year holiday</h3>
                            <div class="row blog_info">
                                <div class="col-md-2 col-sm-2 col-xs-12 pr_0">
                                    <div class="date" style="background:#49b450">
                                        <ul>
                                            <li class="number">21</li>
                                            <li class="date_month">oct</br>2017</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12 prpl_0">
                                    <div class="author" style="padding: 8px 10px">
                                        <div class="row">
                                            <div class="col-md-3 col-sm-3">
                                                <div class="post_img">
                                                    <img src="{{asset('/frontend-assets/img/blog/default.png')}}" alt="alt">
                                                </div>
                                            </div>
                                            <div class="col-md-9 col-sm-9">
                                                <ul>
                                                    <li class="title">Posted by</li>
                                                    <li class="description"><a href="#">Rachel Brown</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-7 col-sm-7 col-xs-12 pl_0">
                                    
                                </div>
                            </div>
                            <div class="blog_block_img">
                                <img src="img/blog/blog_page_img_1.jpg" alt="">
                            </div>
                            <div class="blog_brief">
                                <p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            </div>
                            <div class="blog_link">
                                <div class="blog_share social_icon t_white">
                                    <ul>
                                        <li class="plr_10" style="margin-top: 2rem;"><a href="#"><i class="plr_10 pr_10 b_comment fa fa-comment">5</i></a></li>
                                    </ul>
                                </div>
                                <div class="button">
                                    <a href="blog_article.html">read more</a>
                                </div>
                            </div>
                        </div>
                        <div class="blog_block wow fadeIn">
                            <h3 class="blog_title">Report - Children's Birthdays!</h3>
                            <div class="row blog_info">
                                <div class="col-md-2 col-sm-2 pr_0">
                                    <div class="date" style="background:#49b450">
                                        <ul>
                                            <li class="number">25</li>
                                            <li class="date_month">oct</br>2017</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 prpl_0">
                                   <div class="author" style="padding: 8px 10px">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="post_img">
                                                    <img src="{{asset('/frontend-assets/img/blog/default.png')}}" alt="alt">
                                                </div>
                                            </div>
                                            <div class="cold-md-9">
                                                <ul>
                                                    <li class="title">Posted by</li>
                                                    <li class="description"><a href="#">Rachel Brown</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-7 col-sm-7 pl_0">
                                    
                                </div>
                            </div>
                            <div class="blog_block_img">
                                <img src="img/blog/blog_page_img_2.jpg" alt="">
                            </div>
                            <div class="blog_brief">
                                <p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            </div>
                            <div class="blog_link">
                                <div class="blog_share social_icon t_white">
                                    <ul>
                                        <li class="plr_10" style="margin-top: 2rem;"><a href="#"><i class="plr_10 pr_10 b_comment fa fa-comment">5</i></a></li>
                                      
                                    </ul>
                                </div>
                                <div class="button">
                                    <a href="#">read more</a>
                                </div>
                            </div>
                        </div>
                        <div class="blog_block wow fadeIn">
                            <h3 class="blog_title">Children take care of nature</h3>
                            <div class="row blog_info">
                                <div class="col-md-2 col-sm-2 pr_0">
                                    <div class="date" style="background:#49b450">
                                        <ul>
                                            <li class="number">03</li>
                                            <li class="date_month">nov</br>2017</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 prpl_0">
                                   
                                    <div class="author" style="padding: 8px 10px">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="post_img">
                                                    <img src="{{asset('/frontend-assets/img/blog/default.png')}}" alt="alt">
                                                </div>
                                            </div>
                                            <div class="cold-md-9">
                                                <ul>
                                                    <li class="title">Posted by</li>
                                                    <li class="description"><a href="#">Rachel Brown</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                        
                                </div>
                                <div class="col-md-7 col-sm-7 pl_0">
                                    
                                </div>
                            </div>
                            <div class="blog_block_img">
                                <img src="img/blog/blog_page_img_3.jpg" alt="">
                            </div>
                            <div class="blog_brief">
                                <p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            </div>
                            <div class="blog_link">
                                <div class="blog_share social_icon t_white">
                                    <ul>
                                        <li class="plr_10" style="margin-top: 2rem;"><a href="#"><i class="plr_10 pr_10 b_comment fa fa-comment">5</i></a></li>
                                    </ul>
                                </div>
                                <div class="button">
                                    <a href="#">read more</a>
                                </div>
                            </div>
                        </div>
                        <div class="blog_nav">
                            <ul class="pagination">
                                <li class="disabled"><a href="#"><i class="fa fa-angle-left"></i></a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane card-container" id="myposts">
                <div class="row">
                    <div class="col-md-10" style="margin-left:8.3%">
                        <div class="leave_reply b_light">
                            <h3>Add a Post</h3>
                            <div class="contact_form_white">
                                <form method="post" id="contact_form" style="padding: 2rem;">
                                    <div id="form-messages"></div>
                                    <h4>Upload an Image(optional)</h4>
                                    <br>
                                    <input type="file" onchange="readURL(this)" class="form-control" accept="image/gif, image/jpeg, image/png" placeholder="Choose an image"/>
                                    <br>
                                    <img src="#" style="width:20rem" id="uploadimg" hidden alt="img" />
                                    <br>
                                    <br>
                                    <h4 >Write your Content</h4>
                                    <textarea id="message" name="message" rows="4" placeholder="Write Something..." required="required"></textarea>
                                    <div id="messegeResult" class="messegeResult">
                                        <p>The message was successfully sent</p>
                                        <button class="button" type="submit">Post</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <h4 class="title">My recent posts: </h4>
                        <br>
                        <br>
                        <div class="blog_block wow fadeIn">
                            <h3 class="blog_title">Preparing for the New Year holiday</h3>
                            <div class="row blog_info">
                                <div class="col-md-2 col-sm-2 col-xs-12 pr_0">
                                    <div class="date" style="background:#49b450">
                                        <ul>
                                            <li class="number">21</li>
                                            <li class="date_month">oct</br>2017</li>
                                        </ul>
                                    </div>
                                </div>
                               
                                <div class="col-md-7 col-sm-7 col-xs-12 pl_0">
                                    
                                </div>
                            </div>
                            <div class="blog_block_img">
                                <img src="img/blog/blog_page_img_1.jpg" alt="">
                            </div>
                            <div class="blog_brief">
                                <p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            </div>
                            <div class="blog_link">
                                <div class="blog_share social_icon t_white">
                                    <ul>
                                        <li class="plr_10" style="margin-top: 2rem;"><a href="#"><i class="plr_10 pr_10 b_comment fa fa-comment">5</i></a></li>
                                    </ul>
                                </div>
                                <div class="button buttonyellow">
                                    <a href="#" data-toggle="modal" data-target="#editpost">Edit</a>
                                </div>
                                <div class="button buttonred">
                                    <a href="#" data-toggle="modal" data-target="#deletepost">Delete</a>
                                </div>
                            </div>
                        </div>
                        <div class="blog_block wow fadeIn">
                            <h3 class="blog_title">Report - Children's Birthdays!</h3>
                            <div class="row blog_info">
                                <div class="col-md-2 col-sm-2 pr_0">
                                    <div class="date" style="background:#49b450">
                                        <ul>
                                            <li class="number">25</li>
                                            <li class="date_month">oct</br>2017</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="blog_block_img">
                                <img src="img/blog/blog_page_img_2.jpg" alt="">
                            </div>
                            <div class="blog_brief">
                                <p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            </div>
                            <div class="blog_link">
                                <div class="blog_share social_icon t_white">
                                    <ul>
                                        <li class="plr_10" style="margin-top: 2rem;"><a href="#"><i class="plr_10 pr_10 b_comment fa fa-comment">5</i></a></li>
                                      
                                    </ul>
                                </div>
                                <div class="button buttonyellow">
                                    <a href="#" data-toggle="modal" data-target="#editpost">Edit</a>
                                </div>
                                <div class="button buttonred">
                                    <a href="#" data-toggle="modal" data-target="#deletepost">Delete</a>
                                </div>
                            </div>
                        </div>
                        <div class="blog_block wow fadeIn">
                            <h3 class="blog_title">Children take care of nature</h3>
                            <div class="row blog_info">
                                <div class="col-md-2 col-sm-2 pr_0">
                                    <div class="date" style="background:#49b450">
                                        <ul>
                                            <li class="number">03</li>
                                            <li class="date_month">nov</br>2017</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="blog_block_img">
                                <img src="img/blog/blog_page_img_3.jpg" alt="">
                            </div>
                            <div class="blog_brief">
                                <p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            </div>
                            <div class="blog_link">
                                <div class="blog_share social_icon t_white">
                                    <ul>
                                        <li class="plr_10" style="margin-top: 2rem;"><a href="#"><i class="plr_10 pr_10 b_comment fa fa-comment">5</i></a></li>
                                    </ul>
                                </div>
                                <div class="button buttonyellow">
                                    <a href="#" data-toggle="modal" data-target="#editpost">Edit</a>
                                </div>
                                <div class="button buttonred">
                                    <a href="#" data-toggle="modal" data-target="#deletepost">Delete</a>
                                </div>
                            </div>
                        </div>
                        <div class="blog_nav">
                            <ul class="pagination">
                                <li class="disabled"><a href="#"><i class="fa fa-angle-left"></i></a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div id="editpost" class="modal fade in"  aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
           
            <div id="modalBody" class="modal-body" style="font-size:  15px;">
                <div class="leave_reply b_light" style="background:white">
                    <h3>Edit post</h3>
                    <div class="contact_form_white">
                        <form method="post" id="contact_form">
                            <div id="form-messages"></div>
                            <textarea id="message" name="message" rows="4" required="required"></textarea>
                            <div id="messegeResult" class="messegeResult">
                                <p>The message was successfully sent</p>
                                <button class="button" type="submit">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="deletepost" class="modal fade in"  aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
           
            <div id="modalBody" class="modal-body" style="font-size:  15px;">
                <div class="leave_reply b_light" style="background:white; margin-bottom:0px; padding-bottom:40px">
                    <h3>Delete post?</h3>
                    <h4>Remove this post permanently? </h4>
                    <br>
                    <br>
                    <button class="btn btn-danger">Delete</button>
                    <button class="btn btn-warning" data-dismiss="modal">cancel</button>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('js')

<script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
    
                reader.onload = function (e) {
                   
                    $('#uploadimg')
                    .removeAttr('hidden')
                        .attr('src', e.target.result);
                };
    
                reader.readAsDataURL(input.files[0]);
            }
        }
        
</script>
@endsection