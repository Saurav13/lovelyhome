<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Alumni;
use App\Mail\AlumniRequestAccepted;
use Mail;
use Auth;

class AlumniRequestController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            
            if (Auth::user()->roles()->where('title', '=', 'Alumni Requests')->exists()){
                return $next($request);
            }
            else
                abort(403);
        });
    }

    public function index()
    {
        $alreqs = Alumni::where('verified',0)->orderBy('created_at','desc')->paginate(10, ['*'], 'requests');
        $alumnis = Alumni::where('verified',1)->orderBy('name','asc')->paginate(10, ['*'], 'alumni');
        $count = Alumni::where('verified',1)->count();
        return view('admin.alumni_requests.index')->with('alreqs',$alreqs)->with('alumnis',$alumnis)->with('count',$count);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $alreq = Alumni::findOrFail($id);
        return view('admin.alumni_requests.show')->with('alreq',$alreq);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        $alreq = Alumni::findOrFail($id);
        
        unlink(public_path('alumni_pp'.'/'.$alreq->photo));
        $alreq->delete();

        $request->session()->flash('success', 'Request deleted');
        return redirect()->route('alumni_requests.index');
    }

    public function getUnseenAlReqCount(Request $request){
        if ($request->ajax()){
            $ano = Alumni::where('verified','0')->count();
            return response()->json(['ano' => $ano]);
        }
        abort(404);  
    }

    public function getUnseenAlReq(Request $request){
        if ($request->ajax()){
            $alreqs = Alumni::where('verified','0')->latest()->limit(5)->get();
            $ano = Alumni::where('verified','0')->count();
            $view = view('admin.partials.anotis')->with('alreqs',$alreqs)->with('ano',$ano)->render();
            
            return response()->json(['html' => $view, 'ano' => $ano]);
        }
        abort(404);
    }

    public function verify($id, Request $request){
        $alreq = Alumni::findOrFail($id);

        $alreq->verified = 1;
        $alreq->save();

        Mail::to($alreq->email)->send(new AlumniRequestAccepted($alreq));
        
        $request->session()->flash('success', 'Request accepted');

        return redirect()->route('alumni_requests.index');
    }
}
