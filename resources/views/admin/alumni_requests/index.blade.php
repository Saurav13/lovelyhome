@extends('admin.main')
@section('title','| Alumni')
@section('body')
    <div class="app-content content container-fluid">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Alumni Requests</h4>
                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body collapse in">
                        <div class="card-block card-dashboard">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Batch</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($alreqs as $r)
                                            <tr>
                                                <th scope="row">{{ $loop->iteration }}</th>
                                                <td>{{ $r->name }}</td>
                                                <td>{{ $r->batch }}</td>
                                                <td>
                                                    <a class="btn btn-outline-info btn-sm" title="View" href="#" data-toggle="modal" data-target="#myModal{{$r->id}}"><i class="icon-eye"></i></a>
                                                    <div id="myModal{{$r->id}}" class="modal fade" role="dialog">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-body">
                                                                    <div style="margin:10px;text-align: center;">
                                                                        <img src="{{ asset('alumni_pp/'.$r->photo) }}" style="width: 200px;height:  200px;"/>
                                                                    </div>
                                                                    <p><strong>Name:</strong> {{ $r->name }}</p>
                                                                    <p><strong>Email:</strong> {{ $r->email }}</p>
                                                                    <p><strong>Batch:</strong> {{ $r->batch }}</p>
                                                                    <br>
                                                                    <form action="{{ route('alumni_requests.verify',$r->id) }}" method="POST" style="display:inline">
                                                                        {{ csrf_field() }}
                                                                        <button title="Verify" type="submit" class="btn btn-outline-warning btn-md">Verify</button>
                                                                    </form>
                                                                    <form action="{{ route('alumni_requests.destroy',$r->id) }}" method="POST" style="display:inline">
                                                                        {{ csrf_field() }}
                                                                        <input type="hidden" name="_method" value="DELETE" >
                    
                                                                        <button id='deleteReq{{$r->id}}' title="delete" type="button" class="btn btn-outline-danger btn-md">Delete</button>
                                                                    </form>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @if(count($alreqs)==0)
                                    <p class="font-medium-3 text-muted text-xs-center" style="margin:50px">No New Requests</p>
                                @endif
                                <div class="text-xs-center mb-3">
									<nav aria-label="Page navigation">
                                        {{ $alreqs->appends(['alumni' => $alumnis->currentPage()])->links() }}   
									</nav>
								</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Alumni Students (Total {{ $count }} students)</h4>
                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body collapse in">
                        <div class="card-block card-dashboard">
                            <div class="table-responsive">
                                <table class="table" id="alumniTable">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Batch</th>
                                            <th>Photo</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($alumnis as $r)
                                            <tr>
                                                <th scope="row">{{ $loop->iteration }}</th>
                                                <td>{{ $r->name }}</td>
                                                <td>{{ $r->email }}</td>
                                                <td>{{ $r->batch }}</td>
                                                <td><img src="{{ asset('alumni_pp/'.$r->photo) }}" width="50px" height="50px"/></td>
                                                <td>
                                                    <form action="{{ route('alumni_requests.destroy',$r->id) }}" method="POST" style="display:inline">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="_method" value="DELETE" >
    
                                                        <button id='deleteReq{{$r->id}}' title="delete" type="button" class="btn btn-outline-danger btn-sm">Delete</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @if(count($alumnis)==0)
                                    <p class="font-medium-3 text-muted text-xs-center" style="margin:50px">No Alumni Yet</p>
                                @endif
                                <div class="text-xs-center mb-3">
                                    <nav aria-label="Page navigation">
                                        {{ $alumnis->appends(['requests' => $alreqs->currentPage()])->links() }}    
                            
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection

@section('js')
    
    <script type="text/javascript">
        $(document).ready( function () {


            $("[id*='deleteReq']").click(function(e){
                var ele = this;
                e.preventDefault();

                swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                if (result.value) {
                    ele.form.submit();
                }
                });

            });
        });
    </script>

@endsection
