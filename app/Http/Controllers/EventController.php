<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\EventsImage;
use Session;
use Image;
use Auth;

class EventController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            
            if (Auth::user()->roles()->where('title', '=', 'Event Settings')->exists()){
                return $next($request);
            }
            else
                abort(403);
        });
    }
    
    public function index()
    {
        $events=Event::all();
        $eimage=EventsImage::all();
        return view('events.index')->with('events',$events)->with('eimage',$eimage);
    }

    public function store(Request $request)
    {
        $this->validate($request, array(
            'title'=>'required|max:255',
            'description'=>'required',
            'start_time'=>'date_format:"H:i"',
            'end_time'=>'date_format:"H:i"',
            'event_date'=>'required|date',
            'venue'=>'required|max:200',
            'coordinator_number'=>'min:7|numeric',
            'featured_image'=>'required|mimes:jpeg,jpg,png|max:10000', 
            'document'=>'sometimes',
            ));
        $events=new Event;
        $events->title=$request->title;
        $events->description=$request->description;
        $events->start_time=$request->start_time;
        $events->end_time=$request->end_time;
        $events->event_date=$request->event_date;
        $events->venue=$request->venue;
        $events->coordinator_number=$request->coordinator_number;
        if($request->hasFile('featured_image')){
                $file = $request->file('featured_image');
                $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
                $path = public_path('events-images/'.$filename);
                Image::make($file)->resize(1600,1140)->save($path);
                $image= Image::make(public_path('events-images/'.$filename));
                $image->insert(public_path('backend/systemimages/logo.png'), 'top-right', 70, 80);
                $image->save(public_path('events-images/'.$filename));
                $events->featured_image = $filename;
        }

        if($request->hasFile('document'))
        {
            $file = $request->file('document');
           $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
           $path = public_path('events-documents/');
           $file->move($path,$filename);
           $events->document = $filename;
        
        }
        $events->save();
        Session::flash('success','Event was added Successfully');
        return redirect()->route('admin.events');
    }

    public function destroy(Request $request)
    {
        $events=Event::findorFail($request->id);
        if($events->featured_image!=null)
        {
            unlink('events-images/'.$events->featured_image);
        }
        if($events->document!=null)
        {
            unlink('events-documents/'.$events->document);
        }
        $events->delete();
        Session::flash('success',"The Event was Deleted Successfully!");
        return redirect()->route('admin.events');
    }

    public function edit($id)
    {
        $events=Event::findorFail($id);
        return view('events.edit')->with('events',$events);
    } 

    public function update(Request $request)
    {
        $this->validate($request, array(
            'title'=>'required|max:255',
            'id'=>'required',
            'description'=>'required',
            'start_time'=>'date_format:"H:i"',
            'end_time'=>'date_format:"H:i"',
            'event_date'=>'required|date',
            'venue'=>'required|max:200',
            'coordinator_number'=>'min:7|numeric',
            'featured_image'=>'mimes:jpeg,jpg,png|max:10000'
        ));
        $events=Event::findorFail($request->id);
        $events->title=$request->title;
        $events->description=$request->description;
        $events->start_time=$request->start_time;
        $events->end_time=$request->end_time;
        $events->event_date=$request->event_date;
        $events->venue=$request->venue;
        $events->coordinator_number=$request->coordinator_number;
        if($request->hasFile('featured_image')){
            if($events->featured_image!=null)
            {
                unlink('events-images/'.$events->featured_image);
            }
            $file = $request->file('featured_image');
            $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
            $path = public_path('events-images/'.$filename);
            Image::make($file)->resize(1600,1140)->save($path);
            $image= Image::make(public_path('events-images/'.$filename));
            $image->insert(public_path('backend/systemimages/logo.png'), 'top-right', 70, 80);
            $image->save(public_path('events-images/'.$filename));
            $events->featured_image = $filename;
        }

        if($request->hasFile('document'))
        {
            if($events->document!=null)
            {
                unlink('events-documents/'.$events->document);
            }
            $file = $request->file('document');
           $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
           $path = public_path('events-documents/');
           $file->move($path,$filename);
           $events->document = $filename;
        
        }
        $events->save();
        Session::flash('success','Event was Updated!');
        return redirect()->route('admin.events');

    }

    public function view($id)
    {
        $events=Event::findorFail($id);
        return view('events.view')->with('events',$events);
    }

    public function images($id)
    {
        $event = Event::findorFail($id);
        return view('events.images')->with('event',$event);
    }
   
    public function UploadImages(Request $request,$id)
    {
        $event = Event::findorFail($id);
        
        if($request->hasFile('files')){
            $request->validate([
                'files.*'=>'image',
            ]);
            
            $images = $request->file('files'); 

            $count = 0;
            foreach($images as $image){
                $eimage= new EventsImage;
                $filename = time().'E'.$count++ . '.' . $image->getClientOriginalExtension();
                $location = public_path('events-images/');
                $image->move($location,$filename);
                $eimage->image = $filename;
                $eimage->event_id = $id;
                $eimage->save();
            }
        }
        $request->session()->flash('success', 'Images Uploaded');
        return;
    }

    public function imageDelete($event_id, $image_id, Request $request){
        $image = EventsImage::find($image_id);

        unlink(public_path('events-images'.'/'.$image->image));
        $image->delete();

        $request->session()->flash('success','Image Succesfully Deleted');
        return back(); 
    }
}
