@extends('admin.main')
@section('body')
@section('title','| FAQ')
<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" ><a data-action="collapse"><button  class="btn btn-md btn-primary"><i class="icon-plus4" aria-hidden="true"></i> Add Faq </button></a></h4>
                            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    
                                    <li><a data-action="collapse"><i class="icon-plus4"></i></a></li>
                                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
            
                                </ul>
                            </div>
                        </div>
                
                        <div class="card-body collapse">
                            
                            <div class="card-block ">
                                <form class="form" method="POST" action="{{route('faqs.store')}}" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="row">
                                        <div class="form-body">
                                            
                                            <div class="form-group col-sm-12">
                                                <label for="question">Question*</label>
                                                <textarea class="form-control" name="question">{{old('question')}}</textarea>
                                            </div>
            
                                            <div class="form-group col-sm-12">
                                                <label for="answer">Answer*</label>
                                                <textarea class="form-control" name="answer">{{old('answer')}}</textarea>
                                            </div>
            
                                            <div class="form-group col-sm-12">
                                                <button type="submit" class="btn btn-primary" id="add">
                                                    <i class="icon-check2">Add FAQ</i> 
                                                </button>
                                            </div>  
                                        </div>
                                    </div>
                                </form>
                            </div>
                                
                        </div>
                    </div>
                </div>
            </div>

            <section id="collapsible">
                <div class="row">
                    <div class="col-lg-12 col-xl-12">
                        <div class="card">
                            @foreach($faqs as $f)
                                <div class="col-md-10">
                                    <div id="headingCollapse2{{$f->id}}"  class="card-header ">
                                        <a data-toggle="collapse" href="#collapse{{$f->id}}" aria-expanded="false" aria-controls="collapse2" class="card-title lead collapsed" id="faqquestions{{$f->id}}">{{$f->question}}</a>
                                    </div>
                                    <div id="collapse{{$f->id}}" role="tabpanel" aria-labelledby="headingCollapse2{{$f->id}}" class="card-collapse collapse" aria-expanded="false">
                                        <div class="card-body">
                                            <div class="card-block" id="faqanswers{{$f->id}}">{{$f->answer}}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2" style="padding-top:10px;">
                                    <button class="btn btn-md btn-primary"  data-toggle="modal" data-target="#EditModal" id="faqedit{{$f->id}}">Edit</button>
                                    <button class="btn btn-md btn-danger"  id="delete{{$f->id}}">Delete</button>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div id="EditModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Edit Faq</h4>
                            </div>
                            <div class="modal-body">
                                <form method="POST" action="{{route('faqs.update')}}">
                                    <div class="row">
                                        {{csrf_field()}}
                                        <div class="form-body">
                                            <div class="form-group col-sm-12">
                                                <label for="question">Question*</label>
                                                <textarea class="form-control" id="qedit" name="question"></textarea>
                                            </div>
                                            <div class="form-group col-sm-12">
                                                <label for="answer">Answer*</label>
                                                <textarea class="form-control" id="aedit" name="answer"></textarea>
                                            </div>
                                            <input type="hidden" value="" name="id" id="id">
                                            <div class="form-group col-sm-12">
                                                <button type="submit" class="btn btn-primary" id="add">
                                                    <i class="icon-check2">Update</i> 
                                                </button>
                                            </div>  
                                        </div>
                                    </div>  
                                </form>    
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

@section('js')
    
    <script type="text/javascript">
  $(document).ready(function() {       
    $("[id*='delete']").click(function(){
      var id = $(this).attr("id").slice(6);
      swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          // console.log(result.value)
          $.post("{{route('faqs.delete')}}",{id:id,_token:"{{csrf_token()}}"},function(data){ 
            swal({
              title:"Deleted Successfully",
              type:"success"
            }).then(function(){
              window.location.reload();
            })
          })
        }
      })
    });
    });
    $(document).ready(function(){
        $("[id*='faqedit']").click(function(){
        var id = $(this).attr("id").slice(7);
        var question=$('#faqquestions'+id).html();
        var answers=$('#faqanswers'+id).html();
        $('#qedit').html(question);
        $('#aedit').html(answers);
        $('#id').val(id);
    });
    });
</script>

@endsection
