@extends('layouts.app')
@section('title','| Pastevents')
@section('body')
<style>
    #index_timer .time span{
        bottom: 0px;
    }
    #index_timer .time{
        margin: 20px 10px;
    }
</style>
<section id="events_header" class="events_header" data-parallax="scroll" data-bleed="200" data-z-index="-100" data-speed="0.1" data-image-src="img/events/events_bg.jpg" data-over-scroll-fix="true">
    <div class="overlay_opacity" @foreach($mainevent as $event)
    style="background:url({{asset('events-images'.'/'.$event->featured_image)}}) no-repeat center"
    @endforeach @if(count($mainevent)==null)  style="background:url({{asset('frontend-assets/img/footer/rainbowbg.png')}})" @endif></div>
    <div class="events_wave"></div>
    <div class="container front_index">
        <div class="row">
            <div class="col-md-12 t_white">
                <ol class="breadcrumb">
                    <li><a href="{{route('home')}}">Home</a></li>
                    <li class="active"><a href="{{route('pastevents')}}">Past Events</a></li>
                </ol>
            </div>
        </div>
        @if(count($mainevent)==null)
        
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <h2 class="section-title">Past Events</h2>
                    <br>
                    <br>
                </div>
                <div class="col-md-1"></div>
            </div>
        @else
        @foreach($mainevent as $event)
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <h2 class="section-title">{{$event->title}}</h2>
                <input type="hidden" id="timerdate" value="{{ $event->event_date. ' '. $event->start_time }}">
                <div id="index_timer" class="index_timer section-title"></div>
                <br>
                <br>
                <div class="events_button">
                    <ul>
                        <li id="upcoming"><a href="{{route('events')}}">Upcoming Events</a></li>
                        <li class="active"><a href="{{route('pastevents')}}">Past Events</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
        @endforeach
        @endif
    </div>
</section>
<section id="events_grids" class="events_grids">
    @foreach($events->chunk(3) as $event)
    <div class="container">
                @foreach($event as $e)
                @if($loop->index==0)
            <div class="col-md-12 wow fadeInUp">
                <div class="full_grid mb_50">
                    <div class="events_grid_img">
                        <img src="{{asset('events-images'.'/'.$e->featured_image)}}" alt="{{$e->title}}">
                    </div>
                    <div class="events_one_block">
                        <div class="events_one_block_left tac">
                            <div class="events_one_top b_green">
                                <p class="pb_10"><span>{{date('j',strtotime($e->event_date))}}</span></p>
                                <p>{{date('M, Y',strtotime($e->event_date))}}</p>
                            </div>
                            <div class="events_one_center b_green">
                                <p class="block_time"><i class="fa fa-clock-o"><span></span></i> {{date('h:i A', strtotime($e->start_time))}} - {{date('h:i A', strtotime($e->end_time))}}</p>
                            </div>
                            <div class="events_one_bottom b_grey_light">
                                <p><i class="fa fa-map-marker"><span></span></i> {{$e->venue}}</p>
                            </div>
                        </div>
                        <div class="grids_brief ml_250">
                            <h4 class="grids_title t_red"><a href="{{URL::to('events'.'/'.$e->slug)}}">{{$e->title}}</a></h4>
                            <p class="grids_description t_grey">{{strip_tags(str_limit($e->description, $limit = 300, $end = '...'))}}</p>
                        </div>
                    </div>
                </div>
            </div>
        @elseif($loop->index==1)
        <div class="col-md-12 wow fadeInUp">
            <div class="full_grid mb_50">
                <div class="events_grid_img">
                        <img src="{{asset('events-images'.'/'.$e->featured_image)}}" alt="{{$e->title}}">
                </div>
                <div class="events_one_block">
                        <div class="events_one_block_left tac">
                            <div class="events_one_top b_green">
                                <p class="pb_10"><span>{{date('j',strtotime($e->event_date))}}</span></p>
                                <p>{{date('M, Y',strtotime($e->event_date))}}</p>
                            </div>
                            <div class="events_one_center b_green">
                                <p class="block_time"><i class="fa fa-clock-o"><span></span></i> {{date('h:i A', strtotime($e->start_time))}} - {{date('h:i A', strtotime($e->end_time))}}</p>
                            </div>
                            <div class="events_one_bottom b_grey_light">
                                <p><i class="fa fa-map-marker"><span></span></i> {{$e->venue}}</p>
                            </div>
                        </div>
                        <div class="grids_brief ml_250">
                            <h4 class="grids_title t_red"><a href="{{URL::to('events'.'/'.$e->slug)}}">{{$e->title}}</a></h4>
                            <p class="grids_description t_grey">{{strip_tags(str_limit($e->description, $limit = 300, $end = '...'))}}</p>
                        </div>
                    </div>
            </div>
        </div>
        @elseif($loop->index==2)
        <div class="col-md-12 wow fadeInUp">
            <div class="full_grid mb_50">
                <div class="events_grid_img">
                        <img src="{{asset('events-images'.'/'.$e->featured_image)}}" alt="{{$e->title}}">
                </div>
                <div class="events_one_block">
                        <div class="events_one_block_left tac">
                            <div class="events_one_top b_green">
                                <p class="pb_10"><span>{{date('j',strtotime($e->event_date))}}</span></p>
                                <p>{{date('M, Y',strtotime($e->event_date))}}</p>
                            </div>
                            <div class="events_one_center b_green">
                                <p class="block_time"><i class="fa fa-clock-o"><span></span></i> {{date('h:i A', strtotime($e->start_time))}} - {{date('h:i A', strtotime($e->end_time))}}</p>
                            </div>
                            <div class="events_one_bottom b_grey_light">
                                <p><i class="fa fa-map-marker"><span></span></i> {{$e->venue}}</p>
                            </div>
                        </div>
                        <div class="grids_brief ml_250">
                            <h4 class="grids_title t_red"><a href="{{URL::to('events'.'/'.$e->slug)}}">{{$e->title}}</a></h4>
                            <p class="grids_description t_grey">{{strip_tags(str_limit($e->description, $limit = 300, $end = '...'))}}</p>
                        </div>
                    </div>
            </div>
            @endif
            @endforeach
        </div>
    </div>
    @endforeach
    <div class="text-center">
        {!!$events->links()!!}
    </div>
</section>
<br>
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<script>
    $(document).ready(function(){
        $('#upcoming').mouseenter(function(){
            $('#upcoming').addClass('active');
        })
        $('#upcoming').mouseleave(function(){
            $('#upcoming').removeClass('active');
        });
    });
</script>
<script>
    $(document).ready(function(){
        var timerdate=$('#timerdate').val(); 
        $("#index_timer").countdown(timerdate, function(event) { /* change date */
          $(this).html(
              event.strftime('<div class="card" style="color: #fffefe;background: #bbbbbbad;" ><div class="time">%D<span class="label">days</span></div>' 
              + '<div class="time">%H<span class="label">hours</span></div>'
              + '<div class="time dots">%M<span class="label">minutes</span></div>'
              + '<div class="time dots">%S<span class="label">seconds</span></div></div>')
              );
          });
      });
      </script>


@stop