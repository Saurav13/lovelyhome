@extends('layouts.app')
@section('title','| Faqs')

@section('body')
<section id="faq_page" class="faq_page">
    <div class="container">
        <div class="row pb_30">
            <div class="col-md-1"></div>
            <div class="col-md-10 dot">
                <h2 class="section-title">FAQs</h2>
                
            </div>
            <div class="col-md-1"></div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-contact">
                        <div class="panel-heading actives">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$faqs->first()->id}}">
                                    {{$faqs->first()->question}}</a>
                                </h4>
                        </div>
                        <div id="collapse{{$faqs->first()->id}}" class="panel-collapse collapse in">
                            <div class="panel-body">{!! $faqs->first()->answer !!}</div>
                        </div>
                    </div>
                    @foreach($faqs->slice(1) as $f)
                    <div class="panel panel-contact">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$f->id}}">{{$f->question}}</a>
                            </h4>
                        </div>
                        <div id="collapse{{$f->id}}" class="panel-collapse collapse">
                            <div class="panel-body">{!!$f->answer!!}
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
<br>
<br> <br>
<br> <br>
<br> <br>
<br>    
@endsection
