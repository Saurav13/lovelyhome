<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AlumniPhoto;
use Auth;

class AlumniPhotoController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            
            if (Auth::user()->roles()->where('title', '=', 'Alumni Photos')->exists()){
                return $next($request);
            }
            else
                abort(403);
        });
    }

    public function index()
    {
        $alumni_photos = AlumniPhoto::orderBy('updated_at','desc')->paginate(12);
        return view('admin.alumni_photos.index')->with('alumni_photos',$alumni_photos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'photo'=>'required|image',
            'name' => 'required',
            'batch' => 'required|numeric'
        ]);

        $alumni_photo = new AlumniPhoto;
        $alumni_photo->name = $request->name;
        $alumni_photo->batch = $request->batch;
                
        if($request->hasFile('photo')){
            $photo = $request->file('photo');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            $location = public_path('alumni_photos/');
            $photo->move($location,$filename);
            $alumni_photo->photo = $filename;
        }
        $alumni_photo->save();

        $request->session()->flash('success', 'Photo added.');        
        
        return redirect()->route('alumni_photos.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $alumni_photo = AlumniPhoto::findOrFail($id);
        return view('admin.alumni_photos.edit')->with('alumni_photo',$alumni_photo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'photo'=>'image',
            'name' => 'required',
            'batch' => 'required|numeric'
        ]);

        $alumni_photo = AlumniPhoto::findOrFail($id);
        $alumni_photo->name = $request->name;
        $alumni_photo->batch = $request->batch;
                
        if($request->hasFile('photo')){
            unlink(public_path('alumni_photos/'.$alumni_photo->photo));
            
            $photo = $request->file('photo');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            $location = public_path('alumni_photos/');
            $photo->move($location,$filename);
            $alumni_photo->photo = $filename;
        }
        $alumni_photo->save();

        $request->session()->flash('success', 'Photo updated.');        
        
        return redirect()->route('alumni_photos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $alumni_photo = AlumniPhoto::findOrFail($id);
        
        unlink(public_path('alumni_photos/'.$alumni_photo->photo));
        $alumni_photo->delete();
        
        $request->session()->flash('success', 'Photo deleted.');        
        
        return redirect()->route('alumni_photos.index');
    }
}
