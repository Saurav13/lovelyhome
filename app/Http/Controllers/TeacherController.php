<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Teacher;
use Image;
use Auth;

class TeacherController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            
            if (Auth::user()->roles()->where('title', '=', 'Teachers')->exists()){
                return $next($request);
            }
            else
                abort(403);
        });
    }

    public function index()
    {
        $teachers=Teacher::all();
        return view('teachers.index')->with('teachers',$teachers);
    }
    public function store(Request $request)
    {
        $this->validate($request, array(
            'name'=>'required|max:255',
            'designation'=>'required|max:255',
            'description'=>'required',
            'facebook_link'=>'max:255',
            'image'=>'required|mimes:jpeg,jpg,png|max:10000', 
            ));
        $teachers=new Teacher;
        $teachers->name=$request->name;
        $teachers->designation=$request->designation;
        $teachers->description=$request->description;
        $teachers->facebook_link=$request->facebook_link;
        if($request->hasFile('image')){
            $file = $request->file('image');
           $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
           $path = public_path('teachers-image/'.$filename);
           Image::make($file)->resize(423, 640)->save($path);
           $img = Image::make(public_path('teachers-image/'.$filename));
           $img->insert(public_path('backend/systemimages/logo.png'), 'bottom-right', 15, 15);
           $img->save(public_path('teachers-image/'.$filename));
           $teachers->image = $filename;
        }
        $teachers->save();
        session()->flash('message','Teacher information added!');
        return redirect()->route('admin.teachers'); 
    }

    public function edit($id)
    {
        $teachers=Teacher::findorFail($id);
        return view('teachers.edit')->with('teachers',$teachers);
    }

    public function update(Request $request)
    {
        $this->validate($request, array(
            'name'=>'required|max:255',
            'designation'=>'required|max:255',
            'description'=>'required',
            'facebook_link'=>'max:255',
            'image'=>'mimes:jpeg,jpg,png|max:10000', 
            ));
        $teachers=Teacher::find($request->id);
        $teachers->name=$request->name;
        $teachers->designation=$request->designation;
        $teachers->description=$request->description;
        $teachers->facebook_link=$request->facebook_link;
        if($request->hasFile('image')){
            if($teachers->image!=null){
               unlink('teachers-image/'.$teachers->image);
               }       
          $file = $request->file('image');
           $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
           $path = public_path('teachers-image/');
           Image::make($file)->resize(423, 640)->save($path);
           $img = Image::make(public_path('teachers-image/'.$filename));
           $img->insert(public_path('backend/systemimages/logo.png'), 'bottom-right', 15, 15);
           $img->save(public_path('teachers-image/'.$filename));
           $teachers->image = $filename;
       }
       $teachers->save();
       session()->flash('success','Teachers Information was Updated Successfully!');
       return redirect()->route('admin.teachers'); 
    }

    public function destroy(Request $request)
    {
        $teachers = Teacher::findorFail($request->id);
         if($teachers->image!=null){
         unlink('teachers-image/'.$teachers->image);
     }
         $teachers->delete();
         return redirect()->route('admin.teachers');
}
}
