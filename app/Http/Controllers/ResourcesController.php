<?php

namespace App\Http\Controllers;

use App\Resources;
use App\Materials;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Storage;
use Image;
use Session;
use Auth;

class ResourcesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            
            if (Auth::user()->roles()->where('title', '=', 'Resources')->exists()){
                return $next($request);
            }
            else
                abort(403);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resources = Resources::latest()->get();
         $materials = Materials::join('resources', 'resources.id', '=','materials.resource_id')
        ->select ('materials.*','resources.title as Rtitle')
        ->get();
        return view('resources.index',compact('resources','materials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       // return 1;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(
            request(),
            [
                'title'=>'required|unique:resources',
            ]);
            Resources::create([
                'title'=>$request->title,
                'detail'=>'default detail',
            ]);
            return redirect('/admin/resources');
    }

    



    /**
     * Display the specified resource.
     *
     * @param  \App\Resources  $resources
     * @return \Illuminate\Http\Response
     */
    public function show(Resources $resources)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Resources  $resources
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Resources  $resources
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate(
            request(),
            [
                'title'=>'required',
            ]);
        
        $resource = Resources::find($request->get('id'));
        $resource->title = $request->get('title');
        $resource->save();
        session()->flash('message','Notice Succesfully Edited');
        return redirect('/admin/resources');
    }





    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Resources  $resources
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Resources::where('id',$request->id)->delete();
         //$notice->delete();
         //session()->flash('message','Notice Succesfully Delted');
        //$products->delete();
        return redirect('/admin/resources');
    }



    public function matcreate()
    {
        $resources = Resources::all();
        return view('materials.create',compact('resources'));
    }

    public function matstore(Request $request)
    {
        $this->validate(
        request(),
        [
            'title'=>'required|unique:materials',
            'detail'=>'required',
            'file'=>'required',
            'image'=>'required|mimes:jpeg,png,jpg',
            'resources'=>'required'
        ]);
        $materials=new Materials;
        $materials->title=$request->title;
        $materials->detail=$request->detail;
        $materials->resource_id=$request->resources;
        $materials->title=$request->title;
        if($request->hasFile('image')){
            $file = $request->file('image');
            $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
            $path = public_path('materials-images/'.$filename);
            Image::make($file)->resize(220,220)->save($path);
            $image= Image::make(public_path('materials-images/'.$filename));
            $image->insert(public_path('backend/systemimages/logo.png'), 'top-right', 20, 30);
            $image->save(public_path('materials-images/'.$filename));
            $materials->image = $filename;
    	}
         if($request->hasFile('file'))
        {
            $f = $request->file('file');
            $fname = time().rand(111,999). '.' . $f->getClientOriginalExtension();
            $p = public_path('materials-files/');
            $f->move($p,$fname);
            $materials->file = $fname;
        }
        $materials->save();
    	Session::flash('success','Material was added successfully!');    
        return redirect('/admin/resources');
    }

    public function matupdate(Request $request)
    {
        $this->validate(
            request(),
            [
                'title'=>'required',
                'detail'=>'required',
                'image'=>'mimes:jpeg,png,jpg',
                'resources'=>'required'
            ]);

        $material= Materials::find($request->id);       // return $material ;
        $material->title = $request->get('title');
        $material->detail = $request->get('detail');
        $material->resource_id=$request->resources;
        // dd($material->file,$material->image);
        if($request->hasFile('file')){
            $f = $request->file('file');
            $fname = time().rand(111,999). '.' . $f->getClientOriginalExtension();
            $p = public_path('materials-files');
            unlink($p.'/'.$material->file);
            
            $f->move($p,$fname);
            $material->file = $fname;
        }
        if($request->hasFile('image')){
            // unlink(public_path('materials-image').'/'.$material->image);
            $file = $request->file('image');
            $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
            $path = public_path('materials-images/'.$filename);
            $p = public_path('materials-images');
            unlink($p.'/'.$material->image);
            Image::make($file)->resize(220,220)->save($path);
            $image= Image::make(public_path('materials-images/'.$filename));
            $image->insert(public_path('backend/systemimages/logo.png'), 'top-right', 20, 30);
            $image->save(public_path('materials-images/'.$filename));
            $material->image = $filename;
        }
        
        $material->save();
        Session::flash('success','Material Succesfully Edited');
        
        return redirect('/admin/resources');
    }

    public function matedit(Materials $material)
    {
        $resources = Resources::all();
        return view('materials.edit',compact('material','resources'));
    }

    public function matchangeStatus(Request $request)
    {
        $material = Materials::find($request->id);
        //($var > 2 ? true : false)

        $material->status=($material->status == 'active' ? 'inactive' : 'active');
            # code...
        
        $material->save();

        return redirect('/admin/resources');
    }


    public function matshow(Materials $material)
    {
        // $url = Storage::url('materials/'.$material->file);
        $resource = Resources::find($material->resource_id);
        return view('materials.showfile',compact('url','material','resource'));
        //return redirect('/admin/resources');
    }


    public function matdestroy(Request $request)
    {    
        $material= Materials::find($request->id);
        unlink('materials-files'.'/'.$material->file);
        unlink('materials-images'.'/'.$material->image);
        $material->delete();
        session()->flash('message','Material Succesfully Delted');
        //$products->delete();
        return redirect('/admin/resources');
    }
}
