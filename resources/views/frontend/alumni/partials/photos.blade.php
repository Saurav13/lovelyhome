@forelse($batches as $b=>$alumni)
    <p style="font-size:  2rem;font-weight:  600;padding-top: 15px;">{{ $b }}</p>
    <div class="row">
        @forelse($alumni as $a)
            <div class="col-md-3 col-sm-4 portfolio_grid_item wow">
                <div class="events">
                    <div class="teacher_block grey_block">
                        <div class="teacher_avatar">
                            <img style="max-height:200px;height:200px" src="{{ asset('alumni_photos/'.$a->photo) }}" alt="{{ $a->name }}">
                        </div>
                        <div class="teacher_about">
                            <h3><a href="#">{{ $a->name }}</a></h3>
                            <div class="teacher_link social_icon t_green_dark">
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @empty
            <p style="font-size:2rem;margin-top:50px">No photos found</p>
        @endforelse
    </div>
@empty
    <p style="font-size:2rem;">No photos yet</p>
@endforelse