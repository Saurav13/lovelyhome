<?php

namespace App\Http\Controllers\Frontend\Alumni\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Session;
use App\Alumni;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:alumni');
    }

    /**
     * Get a validator for an incoming registration request.
    *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {   
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'remail' => 'required|string|email|max:255|unique:alumnis,email',
            'rpassword' => 'required|string|min:6|confirmed',
            'photo' => 'required|image',
            'batch' => 'required|numeric',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        $user = new Alumni;
        $user->name = $request->name;
        $user->email = $request->remail;
        $user->password = bcrypt($request->rpassword);

        $image = $request->file('photo');
        $filename = time().rand(111,999).'.'.$image->getClientOriginalExtension();
        $location = public_path('alumni_pp/');
        $image->move($location,$filename);
        $user->photo = $filename;

        $user->batch = $request->batch;
        $user->save();

        Session::flash('notverified','Your information has been sent for verification. We will respond to you soon.');

        return redirect()->route('alumni.blog');
    }

    protected function guard()
    {
        return Auth::guard('alumni');
    }
}
