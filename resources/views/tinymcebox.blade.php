<script src="{{asset('js/tinymce/js/tinymce/tinymce.min.js')}}"></script>
  <script>
    tinymce.init({
            selector: "textarea#detail",
            

            plugins: [
                "advlist autolink lists link charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons paste textcolor colorpicker textpattern","image",
            ],
            toolbar1: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | image | link",
            toolbar2: "print preview | forecolor backcolor emoticons |",

            height: "300",
        });
        
</script>
   