<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Alumni;

class AlumniResetPasswordEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $token, $alumni;

    public function __construct(Alumni $alumni, $token)
    {
        $this->token = $token;
        $this->alumni = $alumni;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to($this->alumni->email)->view('emails.alumniresetpassword')->with('name',$this->alumni->name)->with('token',$this->token);
    }
}
