<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notices;
use Carbon\Carbon;
use Flash;
use Auth;

class NoticeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            
            if (Auth::user()->roles()->where('title', '=', 'Notices')->exists()){
                return $next($request);
            }
            else
                abort(403);
        });
    }

    public function index()
    {
       // flash('Welcome Aboard!');
    	// $posts = blog::latest()
     //        -> filter(request(['month','year'])) 
     //        -> get();
    	$notices = Notices::latest()->get();
    	return view('notice.index',compact('notices'));
    }
    public function create()
    {
    	// $posts = blog::latest()
     //        -> filter(request(['month','year'])) 
     //        -> get();
    	// $notices = Notices::latest()->get();
    	return view('notice.create');
    }

    public function store(Request $request)
    {
        //validate 
        $this->validate(
            request(),
            [
            'title' => 'required|unique:notices',
            'detail' => 'required'
            ]);
        // create notice
        Notices::create([
         'title' => $request->title,
         'detail'=>$request->detail,
        ]);
        //Redirect to home pagess
        // Alert::message('Robots are working!');
        session()->flash('message','Notice Succesfully Created');
        return redirect('/admin/notice');
    }
    public function edit(Notices $notice)
    {
        return view('notice.edit', compact('notice'));
    }
    public function update(Request $request,$id)
    {
        $this->validate(
            request(),
            [
            'title' => 'required',
            'detail' => 'required',
            ]);
        $notice = Notices::find($id);
        $notice->title = $request->get('title');
        $notice->detail = $request->get('detail');
        $notice->save();
        session()->flash('message','Notice Succesfully Edited');
        return redirect('/admin/notice');
    }
    public function changeStatus(Request $request)
    {
        $notice = Notices::find($request->id);
        //($var > 2 ? true : false)

        $notice->status=($notice->status == 'published' ? 'unpublished' : 'published');
            # code...
        
        $notice->save();

        return redirect('/admin/notice');
    }
    public function search()
    {

        //$notices = blog::find()->where('title');
        $search = \Request::get('searchNotice'); 
        // return $search;
        $notices = Notices::where('title','like','%'.$search.'%')->get();
       // dd($notices);
        return view('notice.index',compact('notices'));
           //return $post->user->email;
        //return view('notice.search', compact('r'));
    }
    public function destroy(Request $request)
    {
         Notices::where('id',$request->id)->delete();
         //$notice->delete();
         session()->flash('message','Notice Succesfully Delted');
        //$products->delete();
        return redirect('/admin/notice');
    }
}
