<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Materials extends Model
{
    use Sluggable;
    protected $table= 'materials';
    protected $fillable = ['title','detail','status','resource_id','file'];
    public function resources()
    {
    	return $this->belongsTo(Resources::class);
    }
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
