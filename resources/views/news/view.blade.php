@extends('admin.main')

@section('title','| News')
@section('body')
@include('tinymcebox')


<div class="card">

<div class="content">
    <div class="container-fluid">
      <div class="row">
          <div class="col-md-12">
              <div class="card" style="margin-top: 15px;">
                  <div class="card-header">
                      <h4 class="card-title" ><a data-action="collapse"><button  class="btn btn-md btn-primary"><i class="icon-plus4" aria-hidden="true"></i> Add news </button></a></h4>
                      <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                      <div class="heading-elements">
                          <ul class="list-inline mb-0">
                              <a href="{{ url()->previous() }}" class="btn btn-labeled btn-default">
                                  <span class="btn-label"><i class="fa fa-arrow-left" aria-hidden="true"></i></span> Back</a>
                              <li><a data-action="collapse"><i class="icon-plus4"></i></a></li>
                              <li><a data-action="expand"><i class="icon-expand2"></i></a></li>

                          </ul>
                      </div>
                  </div>
       
                    <div class="card-body collapse">
                      
                      <div class="card-block ">
                          <form class="form" method="POST" action="{{route('news.store')}}" enctype="multipart/form-data">
                              {{csrf_field()}}
                              <div class="row">
                                  <div class="form-body">
                                    <div class="form-group col-sm-12">
                                      <label for="title">Title</label>
                                      <input type="text" id="title" class="form-control" placeholder="title" name="title" required>
                                    </div>

                                    <div class="form-group col-sm-12">
                                      <label for="detail">News Description</label>
                                       <textarea id="detail" class="form-control" name="description">
                                       </textarea>
                                    </div>

                                    <div  class="form-group col-sm-12">
                                      <input type="file" name="image" class="form-control-file">
                                    </div>

                                    <div class="form-group col-sm-12">
                                       <button type="submit" class="btn btn-primary" id="add">
                                        <i class="icon-check2">Add News</i> 
                                    </button>
                                    </div>  
                                  </div>
                                </div>
                              </form>
                              </div>
                          
                      </div>
                  </div>
              </div>
          </div>
          <section id="description" class="card">
              <div class="card-header">
                  <h2 class="card-title">{{$news->title}}</h2>
                  <p class="category"><i class="pe-7s-timer"></i> Last updated at: {{$news->updated_at->diffForHumans()}}</p>
                  <a href="{{route('news.edit',$news->id)}}" class="btn btn-primary a-btn-slide-text">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        <span><strong>Edit</strong></span>            
                    </a>
                    <a href="#" id="delete" cid={{$news->id}} class="btn btn-danger a-btn-slide-text">
                       <i class="fa fa-trash-o" aria-hidden="true"></i>
                        <span><strong>Delete</strong></span>            
                    </a>
              </div>
              <div class="card-body collapse in">
                  <div class="card-block">
                      <div class="card-text">
                        <img src="{{asset('news-images/'.'/'.$news->image)}}" class="img-thumbnail" style="height: 50%; width: 50%;">
                          <p>{!! $news->description !!}</p>
                          <p class="category"><i class="pe-7s-timer"></i> Posted at: {{$news->created_at->diffForHumans()}}</p>
                          
                      </div>
                  </div>
              </div>
          </section>
    </div>
</div>
</div>
</script>

<script src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>
          <script>
        $(document).ready(function(){

          $('#delete').click(function(event){
            var id= $('#delete').attr('cid');

            swal({
              title: "Are you sure?",
              text: "You will not be able to recover this file!",
              type: "warning",

              showCancelButton: true,

            }).then(function(){
              $.post("{{route('news.delete')}}",{id:id,_token:"{{csrf_token()}}"},function(data){
                swal({
                  title:"News was deleted Successfully",
                  type:"success"

                }).then(function(){
                  window.location.assign("{{route('admin.news')}}");
                })
              })
            });
          });
        });
      </script>
@endsection

