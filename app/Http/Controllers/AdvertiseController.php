<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Advertise;
use Session;
use Image;
use Auth;


class AdvertiseController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        // $this->middleware(function ($request, $next) {
            
        //     if (Auth::user()->roles()->where('title', '=', 'advertises')->exists()){
        //         return $next($request);
        //     }
        //     else
        //         abort(403);
        // });
    }

    public function index()
    {
        $advertises=Advertise::all();
        return view('admin.advertises.index')->with('advertises',$advertises);
    }

    public function store(Request $request)
    {
       $this->validate($request, array(
                           'image'=>'mimes:jpeg,jpg,png,gif|required|max:10000', 
                           'title'=>'required',
                           
    	    	           ));
    	$advertises=new Advertise;
    	$advertises->link=$request->title;
       if($request->hasFile('image')){
            $file = $request->file('image');
            $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
            $path = public_path('advertises-images/'.$filename);
            Image::make($file)->resize(480,360)->save($path);
            $image= Image::make(public_path('advertises-images/'.$filename));
            $image->insert(public_path('backend/systemimages/logo.png'), 'bottom-right', 20, 30);
            $image->save(public_path('advertises-images/'.$filename));
            $advertises->image = $filename;
    	}
    	$advertises->save();
    	Session::flash('success','Advertise was added successfully!');

    	return redirect()->route('admin.advertises'); 
    }
    public function update(Request $request)
    {
        $this->validate($request, array(
            'image'=>'mimes:jpeg,jpg,png,gif|max:10000', 
            'title'=>'required',
        ));
        $advertises=Advertise::find($request->id);
        $advertises->link=$request->title;
        if($request->hasFile('image')){
            unlink('advertises-images/'.$img->image);
             $file = $request->file('image');
            $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
            $path = public_path('advertises-images/'.$filename);
            Image::make($file)->resize(480, 360)->save($path);
            $image = Image::make(public_path('advertises-images/'.$filename));
            $image->insert(public_path('backend/systemimages/logo.png'), 'bottom-right', 20, 30);
            $image->save(public_path('advertises-images/'.$filename));
            $advertises->image = $filename;
        }
        $advertises->save();
        Session::flash('success','Advertise was Updated successfully!');

        return redirect()->route('admin.advertises'); 
    }

    public function destroy(Request $request)
    {
        $advertises=Advertise::findorFail($request->id);
        unlink('advertises-images/'.$advertises->image);
        $advertises->delete();
        return redirect()->route('admin.advertises');
    }
}
