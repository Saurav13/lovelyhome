@extends('layouts.app')

@section('body')
@section('title','| My Alumini Posts')

<style>
    alumini_tabs .nav-tabs {
        border: none;
    }

    @media(min-width: 300px)
    {
        .alumini_tabs ul li a {
            padding: 8px 0;
            font-size: 1rem;
            border-radius: 50px;
            font-family: 'OpenSansSemiBold', sans-serif;
        }
    }
    @media(min-width: 600px)
    {
        .alumini_tabs ul li a {
            padding: 8px 0;
            font-size: 1.5rem;
            border-radius: 50px;
            font-family: 'OpenSansSemiBold', sans-serif;
        }
    }
    @media (min-width: 1200px)
    {
        .alumini_tabs ul li a {
            padding: 8px 0;
            font-size: 2rem;
            border-radius: 50px;
            font-family: 'OpenSansSemiBold', sans-serif;
        }
    }
    .alumini_tabs ul {
        position: relative;
        list-style-type: none;
        margin: 0;
        text-align: center;
        padding: 0;
    }
    .alumini_tabs ul li {
        display: inline-block;
        float: none;
        text-transform: uppercase;
        width: 23%;
        margin: 30px 7px;
    }
    .alumini_tabs ul li a {
    
    }

    .alumini_tabs ul li .tab_red {
        border: 2px solid #b70404;
        color: #b70404;
        -webkit-transition: all .2s linear;
        transition: all .2s linear;
    }

    .alumini_tabs ul li .tab_yel {
        border: 2px solid #fff400;
        color: #fff400;
        -webkit-transition: all .2s linear;
        transition: all .2s linear;
    }

    .alumini_tabs ul li .tab_green {
        border: 2px solid #49b450;
        color: #49b450;
        -webkit-transition: all .2s linear;
        transition: all .2s linear;
    }


    .nav-tabs>li>.tab_yel:hover, .ui_hover .nav-tabs>li>.tab_green {
        background: #fff400;
        color: #fff;
        border: 2px solid #fff400;
        -webkit-background-clip: padding-box;
        background-clip: padding-box;
    }
    .nav-tabs>li.active>.tab_yel, .nav-tabs>li.active>.tab_yel:focus, .nav-tabs>li.active>.tab_yel:hover {
        background: #fff400;
        color: #fff;
        border: 2px solid #fff400;
    }
    .post_img{
        height: 3.5rem;
    }

    .post_img img{
        height: inherit;
        border-radius: 50px;
    }
    .blog_link .buttonred {
        background-color: #b70404c7;
        border-color: #b70404c7;
    }
    .blog_link .buttonred a:hover {
        background-color: #fff;
        color: #b70404c7;
    }
    .blog_link .buttonred a {
        -webkit-transition: all .2s linear;
        transition: all .2s linear;
        color: #fff;
        font-size: 2rem;
        font-family: 'OpenSansSemiBold', sans-serif;
    }

    .blog_link .buttonyellow {
        background-color: #fff400c7;
        border-color: #fff400c7;
    }
    .blog_link .buttonyellow a:hover {
        background-color: #fff;
        color: #fff400c7;
    }
    .blog_link .buttonyellow a {
        -webkit-transition: all .2s linear;
        transition: all .2s linear;
        color: #fff;
        font-size: 2rem;
        font-family: 'OpenSansSemiBold', sans-serif;
    }

    .blog_link .buttongreen {
        background-color: #49b450;
        border-color: #49b450;
    }
    .blog_link .buttongreen a:hover {
        background-color: #fff;
        color: #49b450;
    }
    .blog_link .buttongreen a {
        -webkit-transition: all .2s linear;
        transition: all .2s linear;
        color: #fff;
        font-size: 2rem;
        font-family: 'OpenSansSemiBold', sans-serif;
    }

    #add_post_form .error, #edit_post_form .error{
        border: 3px solid #b70404c7;
    }

    #add_post_form label, #edit_post_form label {
        color: #b70404c7;
        border: 0!important;
    }
</style>
<section id="blog_page" class="blog_page">
    <div class="container">
        <div class="row pb_20">
            <div class="col-md-1"></div>
            <div class="col-md-10 dot">
                <h2 class="section-title">Welcome Back!!!</h2>
                <div class="alumini_tabs">
                    <ul class="nav nav-tabs">
                        <li><a class="tab_red acttabfont" href="{{ route('alumni.gallery') }}">Gallery</a></li>
                        <li><a class="tab_green acttabfont" href="{{ route('alumni.blog') }}">Blog</a></li>
                        <li class="active"><a class="tab_yel acttabfont" href="#myposts">My posts</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
        <div class="tab-content about_tabs_content" style="width:100%;">
            <div class="tab-pane card-container" id="gallery">
                
            </div>

            <div class="tab-pane card-container" id="blog">
            
            </div>

            <div class="tab-pane card-container active" id="myposts">
                <div class="row">
                    <div class="col-md-10" style="margin-left:8.3%">
                        <div class="leave_reply b_light">
                            <h3>Add a Post</h3>
                            <div class="contact_form_white">
                                <form method="post" id="add_post_form" action="{{ route('alumni.myposts.add') }}" enctype="multipart/form-data" style="padding: 2rem;">
                                    <div id="add_post_error" style="color: #CD3653;font-size: 1.5rem;font-weight: 600;margin-bottom: 10px;"></div>
                                    <h4 >Enter Post Title</h4>
                                    <br>
                                    <input type="text" name="title" placeholder="Enter Post Title" class="form-control" required="required"/>
                                    <br>
                                    <br>
                                    <h4>Upload an Image(optional)</h4>
                                    <br>
                                    <input type="file" accept="image/*" name="image" onchange="readURL(this)" class="form-control" placeholder="Choose an image"/>
                                    <br>
                                    <img src="#" style="width:20rem" id="uploadimg" hidden alt="img" />
                                    <br>
                                    <br>
                                    <h4 >Write your Content</h4>
                                    <textarea id="content" name="content" rows="4" placeholder="Write Something..." required="required"></textarea>
                                    <div id="add_post_result" class="messegeResult">
                                        <p>The post was successfully posted</p>
                                        <button class="button" type="submit">Post</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <h4 class="title">My recent posts: </h4>
                        <br>
                        <br>
                        <div id="myPostsDiv">
                            @include('frontend.alumni.partials.mypost')
                        </div>
                        
                        <div class="blog_nav" style="text-align: center;">
                            <img id="loader" hidden src="{{ asset('ajax-loader.gif') }}"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div id="editpost" class="modal fade in"  aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
           
            <div id="modalBody" class="modal-body" style="font-size:  15px;">
                <div class="leave_reply b_light" style="background:white">
                    <h3 style="padding-bottom:0px">Edit post</h3>
                    <div class="contact_form_white">
                        <form method="post" id="edit_post_form" action="" enctype="multipart/form-data" style="padding: 2rem;">
                            <div id="edit_post_error" style="color: #CD3653;font-size: 1.5rem;font-weight: 600;margin-bottom: 10px;"></div>
                            <h4 >Enter Post Title</h4>
                            <input type="text" name="title" id="editTitle" placeholder="Enter Post Title" class="form-control" value="" required="required"/>
                            <br>
                            <h4>Upload an Image(optional)</h4>
                            <input type="file" accept="image/*" name="image" class="form-control" placeholder="Choose an image"/>
                            <br>
                            <h4 >Write your Content</h4>
                            <textarea style="margin-top: 0px;" name="content" id="editContent" rows="4" placeholder="Write Something..." required="required"></textarea>
                            <div id="edit_post_result" class="messegeResult">
                                <p>The post was successfully updated</p>
                                <button class="button" type="submit">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="deletepost" class="modal fade in"  aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
           
            <div id="modalBody" class="modal-body" style="font-size:  15px;">
                <div class="leave_reply b_light" style="background:white; margin-bottom:0px; padding-bottom:40px">
                    <h3>Delete post?</h3>
                    <h4>Remove this post permanently? </h4>
                    <br>
                    <div id="delete_post_error" style="color: #CD3653;font-size: 1.5rem;font-weight: 600;margin-bottom: 10px;"></div>                    
                    <br>
                    <form method="post" id="delete_post_form" action="" enctype="multipart/form-data" style="padding: 2rem;display:  inline;">                            
                        <div id="edit_post_error" style="color: #CD3653;font-size: 1.5rem;font-weight: 600;margin-bottom: 10px;"></div>    
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                    <button class="btn btn-warning" type="button" data-dismiss="modal">cancel</button>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('js')

    <script>
        
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
    
                reader.onload = function (e) {
                   
                    $('#uploadimg')
                    .removeAttr('hidden')
                        .attr('src', e.target.result);
                };
    
                reader.readAsDataURL(input.files[0]);
            }
        }
        $(document).ready(function(){

            var ajaxCall = false;
            var lastId = '{{ $myposts->last() ? $myposts->last()->id: '' }}';

            $(window).scroll(function() {
                if($(window).scrollTop() > $(window).height()+$('#myPostsDiv').height() && !ajaxCall && lastId != '') {
                    ajaxCall = true;
                    loadMoreData();
                }
            });

            function loadMoreData(){

                $.ajax({ 
                    type: 'GET', 
                    url: "{{ route('alumni.myposts.getmoreposts') }}",
                    dataType: 'json', 
                    data: {lastId : lastId}, 
                    beforeSend: function(data) {
                        $('#loader').removeAttr('hidden');
                    },
                    success: function(data){
                        lastId = data.last;
                        $('#myPostsDiv').append(data.html);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        // location.reload();
                    },
                    complete: function(data) {
                        ajaxCall = false;
                        $('#loader').attr('hidden','hidden');
                    }         
                });
            }

            $('#myPostsDiv').on('click','a.editMyPost',function(){
                var post_id = $(this).attr('post-id');
                $('#edit_post_error').html('');
                $('#edit_post_form').validate().resetForm();
                $('#editTitle').removeClass('error');
                $('#editContent').removeClass('error');

                $('#edit_post_form').attr('action','/alumni/myposts/'+post_id+'/edit');
                $('#editTitle').val($('#postTitle'+post_id).text());
                $('#editContent').val($('#postContent'+post_id).text());
                $('#editpost').modal('show');
            });

            $('#myPostsDiv').on('click','a.deleteMyPost',function(){
                $('#delete_post_error').html('');
                var post_id = $(this).attr('post-id');
                $('#delete_post_form').attr('action','/alumni/myposts/'+post_id+'/delete');
                $('#deletepost').modal('show');
            });
        });
    </script>
@endsection