@extends('admin.main')
@section('title','| Add Calendar Event')
@section('body')
<link href="{{ asset('backend/datetimepicker/bootstrap-datetimepicker.css') }}" rel="stylesheet" type="text/css">
    <div class="app-content content container-fluid">
        <div class="content-wrapper">        
            <div class="content-body">
                <div class="row">
                    <div class="content-header-left col-md-6 col-xs-12 mb-1">
                        <h4>Edit Event on Calendar</h4>
                    </div>
                    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
                        <div class="breadcrumb-wrapper col-xs-12">
                            <ol class="breadcrumb">
                                <a class="btn btn-primary" href="{{ route('calender.index') }}">Back</a>
                            </ol>
                        </div>
                    </div>
                </div>

                <div class="row match-height">
                    <div class="col-xs-12">
                        <div class="card">
                            <div class="card-header">
                                <form action="{{ route('calender.destroy',$event->id) }}" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE" >
                                    <button id='deleteEvent{{$event->id}}' type="button" class="btn btn-danger pull-right">Delete Event</button>
                                </form>
                            </div>
                                
                            <div class="card-body">
                                <div class="card-block">
                                    <form id="addevent" class="form" method="POST" action="{{ route('calender.update',$event->id) }}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="PATCH" >
                                        <div class="row">
                                            <div class="form-body" style="overflow:hidden;">
                                                <div class="form-group col-sm-12">
                                                    <label for="name">Event Date</label>
                                                    <br><br>
                                                    <div class="row">
                                                        <div class="col-md-2"></div>
                                                        <div class="col-md-8">
                                                            <div id="datetimepicker"></div>
                                                            <input type="hidden" name="date" id="event_date" value="{{ $event->event_date }}">
                                                        </div>
                                                        <div class="col-md-2"></div>
                                                    </div>
                                                </div>

                                                <div class="form-group col-sm-12">
                                                    <label for="name">Event Name</label>
                                                    <input type="text" id="name" class="form-control" placeholder="Event Name" name="name" value="{{ $event->event_name }}" required>
                                                </div>

                                                <div class="form-group col-sm-12">
                                                    <label for="name">Event Type</label>
                                                    <select class="form-control" name="type" required>
                                                        <option value="Holiday" {{ $event->type=='Holiday' ? 'selected' : '' }}>Holiday</option>
                                                        <option value="Half-Holiday" {{ $event->type=='Half-Holiday' ? 'selected' : '' }}>Half Holiday</option>
                                                        <option value="Celebration Day" {{ $event->type=='Celebration Day' ? 'selected' : '' }}>Celebration Day</option>
                                                    </select>
                                                </div>

                                                <div class="form-group col-sm-12">
                                                    <label for="description">Event Description</label>
                                                    <textarea id="description" rows="6" class="form-control" placeholder="Event Description" name="description" required>{{ $event->description }}</textarea>
                                                </div>
                
                                                <div class="form-group col-sm-12">
                                                    <button type="submit" class="btn btn-success">
                                                        <i class="icon-check2">Edit Event</i> 
                                                    </button>
                                                </div>  
                                            </div>
                                        </div>
                                    </form>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="{{ asset('backend/datetimepicker/bootstrap-datetimepicker.js') }}"></script>    
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker').datetimepicker({
                inline: true,
                defaultDate:'{{ $event->event_date }}',
                format: 'MM/DD/YYYY'
            });
        });
        
        $('#datetimepicker').on('dp.change', function(event) {
            var formatted_date = event.date.format('MM/DD/YYYY');
            $('#event_date').val(formatted_date);
        });

        $("[id*='deleteEvent']").click(function(e){
            var ele = this;
            e.preventDefault();

            swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
            if (result.value) {
                ele.form.submit();
            }
            });

        });
    </script>

@endsection