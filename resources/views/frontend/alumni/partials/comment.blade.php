@foreach($comments as $comment)
    <div class="level_one">
        <div class="comment_content">
            <div class="avatar">
                <img src="{{ asset('alumni_pp/'.$comment->commenter->photo) }}" alt="{{ $comment->commenter->name }}">
            </div>
            <div class="block_comment">
                <div class="head_comment">
                    <div class="comment_title">
                        <h5>{{ $comment->commenter->name }}</h5>		
                        <p class="date"><i class="fa fa-calendar"></i> {{ date('M j, Y',strtotime($comment->created_at)) }}</p>
                    </div>					
                </div>
                <div class="comment_describe">
                    <p>{!! $comment->content !!}</p>
                </div>
            </div>
        </div>
    </div>
@endforeach