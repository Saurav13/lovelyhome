
@if(!$errors->isEmpty())
<div class="app-content content alert alert-danger alert-dismissible" style="color:black;" id="errormessage" role="alert">    
	<button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick="document.getElementById('errormessage').style.display='none'" style="right: 0;"><span aria-hidden="true">&times;</span></button>	
	<strong>Errors:</strong>
	<div style="color:black;">
		@include('errors')
	</div>
</div>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
   <script type="text/javascript"> 
     $(document).ready( function() {
       $('#errormessage').delay(1000000).fadeOut();
     });
   </script>

@endif

@if(Session::has('success'))

		<div class=" app-content content container-fluid" id="successmessage">
			<div class="alert alert-success alert-dismissible" role="alert" style="background-color: green;">
			    <button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick="document.getElementById('successmessage').style.display='none'"><span aria-hidden="true">&times;</span></button>
			    {{ Session::get('success') }}
			 
		</div>
	</div>
	<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
	   <script type="text/javascript"> 
	     $(document).ready( function() {
	       $('#successmessage').delay(6000).fadeOut();
	     });
	   </script>
	
	

@endif
@if(Session::has('error'))

		<div class=" app-content content container-fluid" id="errormessage">
			<div class="alert alert-danger alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick="document.getElementById('errormessage').style.display='none'"><span aria-hidden="true">&times;</span></button>
				{{ Session::get('error') }}			

		</div>
	</div>
@endif
