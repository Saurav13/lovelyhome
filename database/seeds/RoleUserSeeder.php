<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(Role::all() as $role){
            DB::table('role_user')->insert([
                'role_id' => $role->id,
                'user_id' => 1
            ]);
            
        }
    }
}
