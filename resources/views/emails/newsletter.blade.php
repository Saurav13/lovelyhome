
<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <style type="text/css">
        /* CLIENT-SPECIFIC STYLES */
        body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
        table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
        img { -ms-interpolation-mode: bicubic; }
        
        /* RESET STYLES */
        img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
        table { border-collapse: collapse !important; }
        body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }
        
        /* iOS BLUE LINKS */
        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }
        
        /* MEDIA QUERIES */
            @media  screen and (max-width: 480px) {
            .mobile-hide {
                display: none !important;
            }
            .mobile-center {
                text-align: center !important;
            }
        }
        
        /* ANDROID CENTER FIX */
        div[style*="margin: 16px 0;"] { margin: 0 !important; }
            

            .tag ul li {
                display: inline-block;  
                margin-bottom: 5px;
            }
            
            .tag ul li{
                color: black;
                padding: 4px 7px;
                border: 1px solid #7bc634;
                border-radius: 25px;
                font-size: .6rem;
                font-weight:600;
                background-color: #f6c42c;
                font-family:'Boogaloo', cursive;
            }
        </style>
    </head>
    <body style="margin: 0 !important; padding: 0 !important;" bgcolor="#eeeeee">
        
        
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td align="center" style="background-color: #eeeeee;" bgcolor="#eeeeee">
                <!--[if (gte mso 9)|(IE)]>
                <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                <tr>
                <td align="center" valign="top" width="600">
                <![endif]-->
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                    <tr>
                        <td align="left" valign="top" style="padding: 8px 35px" bgcolor="#0aa910">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                            
                                <tr>
                                    <td align="left" style="font-family:brandon-grotesque; font-size: 14px; font-weight: 400; line-height: 24px;">
                                        <a target="_blank" href="{{URL::to('/')}}"><img width="200" height="47" src="{{asset('/frontend-assets/img/header/logo.png')}}" alt="Lovely Home"></a>
                                    </td>
                                    <td align="center">
                                        <div class="tag" style="float:right">
                                            <span style="color:#fff00f;">Head Office: Jawalakhel</span><br>
                                            <span style="color:#fff00f;">Phone No.: 01-5520225, 01-5554368</span>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                                
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="padding: 35px; background-color: #f9f9f9;" bgcolor="#f9f9f9">
        
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                            <tr>
                                <td align="left" style="font-family:brandon-grotesque; font-size: 16px; font-weight: 400; line-height: 24px;">
                                    {!! $content !!}
                                </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                        <tr>
                            <td style="max-width:600px;height: 117px;background: url({{ asset('frontend-assets/img/footer/mail_bg1.png') }}) center center repeat-x;z-index: 1;background-color: #f9f9f9;"></td>
                        </tr>
                    <tr>
                        <td align="center" style="padding: 35px; background-color: #3a251b;" bgcolor="#3a251b">
                            
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                <tr>
                                    <td align="left" style="font-family:brandon-grotesque; font-size: 14px; font-weight: 400; line-height: 24px;">
                                        <p style="font-size: 14px; font-weight: 400; line-height: 20px; color: #777777;">
                                            <a href="{{ URL::to('/') }}" style="color:#777777;" target="_blank">Home</a> | 
                                            <a href="{{ URL::to('events') }}" style="color:#777777;" target="_blank">Events</a> | 
                                            <a href="{{ URL::to('download') }}" style="color:#777777;" target="_blank">Downloads</a> | 
                                            <a href="{{ URL::to('calendar') }}" style="color:#777777;" target="_blank">Calendar</a> | 
                                            <a href="{{ URL::to('gallery') }}" style="color:#777777;" target="_blank">Gallery</a> | 
                                            <a href="{{ URL::to('notices') }}" style="color:#777777;" target="_blank">Notices</a>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="padding: 10px 0px;font-family:brandon-grotesque; font-size: 14px; font-weight: 400; line-height: 24px;">
                                        <p style="font-size: 14px; font-weight: 400; line-height: 20px; color: #777777;">
                                            If you didn't create an account using this email address, please ignore this email or <a href="{{route('unsubscribe',$subscriber->token)}}" target="_blank" style="color: #777777;">unsusbscribe</a>.
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" style="font-family:brandon-grotesque; font-size: 14px; font-weight: 400; line-height: 24px;">
                                        <p style="font-size: 14px; font-weight: 400; line-height: 20px; color: #777777;">
                                            Copyright © 2018 Lovely Home. Powered by <a href="#"><strong>inCube</strong></a>
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
        </table>
        
    </body>
</html>
                    
                          