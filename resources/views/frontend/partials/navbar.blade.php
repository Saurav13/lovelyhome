<body >

	<div class="loader">
		<div class="sk-folding-cube">
			<div class="sk-cube1 sk-cube"></div>
			<div class="sk-cube2 sk-cube"></div>
			<div class="sk-cube4 sk-cube"></div>
			<div class="sk-cube3 sk-cube"></div>
		</div>
	</div>
	<style>
		.tag{
			padding:25px;
							}
							.tag ul li {
								display: inline-block;
								
								margin-bottom: 14px;
								margin-right: 8px;
							}
							
							 .tag ul li a {
									color: #be3033;
								padding: 4px 14px;
								border-radius: 25px;
								font-size: 1.7rem;
								background-color: #f6c42c;
								font-family:'Boogaloo', cursive;
								-webkit-transition: all .2s linear;
								transition: all .2s linear;
							}
							.tagactive{
								color: black;
								padding: 4px 14px;
								border: 2px groove #337ab7 !important;
								border-radius: 25px;
								font-size: 1.5rem;
								background-color: #ffb700 !important;
								font-family:'Boogaloo', cursive;
								-webkit-transition: all .2s linear;
								transition: all .2s linear;
							}
							.tag ul li a:hover {
									color: #337ab7;
									background-color: #f6c42c;
							}
	</style>
	<header id="home" style="background-color: #0aa910;">
		<div class="section_logo" style="background: #0aa910;">
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-sm-6 col-xs-6">
						<div class="logo_block">
                           
							<a href="index.html"><img style="height:7.9rem;"  src="/frontend-assets/img/header/logo.png" alt="">
						
							</a>
						</div>
					</div>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                    	{{-- <div class="mnu" style="">
                                
							<style>
									.mnu ul .drop4 .asd:hover,
									.show_mnu .drop4 .active{
										background: none;
										
									}
									

								
							</style>
                                <br>
                                <ul>
										
									<li class="drop4" ><h4 style="color:white;">Block: </h4><a class="mnu asd" href="#" style="color:white"><h4 style="color:white;"><mark id="mainBlock" style="background: #f6c42c; border-radius:10px;">Jawalakhel</mark></h4></a>
                                                <ul class="mnu_child"  >
                                                        <li><a href="#Jawalakhel" id="blockJawalakhel" style="color:black;">Jawalakhel</a></li>
                                                        <li><a href="#Bhanimandal" id="blockBhanimandal" style="color:black;">Bhanimandal</a></li>
                                                        <li><a href="#Sanogaun" id="blockSanogaun" style="color:black;">Sanogaun</a></li>
                                                        
                                                    </ul>
                                            </li>
                                            
											
								</ul>
										
                                 

						</div> --}}
						<div class="tag pull-right">
							<h4 style="color:white;">Our Blocks:</h4>
							<br>
							<ul>
								<li><a href="#Blocks" id="blockJawalakhel" class="tagactive scrollpage _mPS2id-h mPS2id-clicked">Jawalakhel</a></li>
								<li><a href="#Blocks" id="blockSanogaun" class="scrollpage _mPS2id-h mPS2id-clicked">Sanogaun</a></li>
								<li><a href="#Blocks" id="blockBhanimandal" class="scrollpage _mPS2id-h mPS2id-clicked">Bhanimandal</a></li>
								<li><a href="#Blocks" id="blockDhobighat" class="scrollpage _mPS2id-h mPS2id-clicked">Dhobighat</a></li>
                            </ul>
					
						</div>
						
					</div>
					
				</div>	
			</div>		
		</div>
		<div class="section_menu">
			<div class="mnu_fixed" style="background-color: #ffb700;">
				<div class="mnu_wave"></div>
				<div class="container">
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="mnu mnu_mobile hidden-lg hidden-md">
								
								<div class="input_search hidden-lg hidden-md">
										<a href="{{route('notices')}}" ><h3>Notices</h3></a>
								</div>
								<div class="toggle_block hidden-lg hidden-md">
									<a href="#" class="toggle-mnu"><span></span></a>
								</div>
							</div>
							<div class="logo_block">
								<img src="/frontend-assets/img/header/logo.png" alt="">
							</div>
							<div class="search hidden-sm hidden-xs" style="padding-top:15px;">
									<a href="{{route('notices')}}" ><h3>Notices</h3></a>
							</div>
							<div class="mnu hidden-sm hidden-xs">
								<ul>
									<li class="drop5 active"><a href="{{route('home')}}" @if(Request::is('/')) 
										style="background: #58b600;"
										@endif>Home</a>
									</li>
									<li class="drop5"><a href="{{route('events')}}" 
										@if(Request::is('events*') || Request::is('pastevents')) 
										style="background: #58b600;"
										@endif
										>Events</a>
										<ul class="mnu_child">
										<li><a href="{{route('events')}}">Upcoming Events</a></li>
											<li><a href="{{route('pastevents')}}">Past Events</a></li>
										</ul>
									</li>
									{{--  <li class="drop5"><a href="{{route('news')}}"
										@if(Request::is('news*')) 
										style="background: #58b600;"
										@endif
										>News</a>
								
									</li>  --}}
									<li class="drop5"><a href="{{route('download')}}"
										@if(Request::is('download*') || Request::is('resources*') ) 
										style="background: #58b600;"
										@endif
										>Downloads</a>
										<ul class="mnu_child">
											<li><a href="{{route('resources')}}">Kid's Resource</a></li>
											<li><a href="{{route('download')}}">Downloads</a></li>
										</ul>
									</li>
									<li class="drop5"><a href="{{URL::to('calendar')}}" @if(Request::is('calendar*')) 
										style="background: #58b600;"
										@endif>Calendar</a>
									</li>
									
									<li class="drop5"><a href="{{URL::to('gallery')}}"
										@if(Request::is('gallery*')) 
										style="background: #58b600;"
										@endif
										>Gallery</a>
									</li>
									<li class="drop5"><a href="{{URL::to('/alumni')}}" 
										@if(Request::is('alumni*')) 
										style="background: #58b600;"
										@endif
										>Alumini</a>
									</li>
									<li class="drop5"><a href="{{URL::to('/#feature-section')}}" class="scrollpage _mPS2id-h mPS2id-clicked"
										@if(Request::is('about*')) 
										style="background: #58b600;"
										@endif
										>Facilities</a>
									</li>
										
								</ul>
							</div>
						</div>
						<div class="col-sm-12 col-xs-12 top_mnu">
							<div class="mobile_mnu">
								<ul>
									<li class="drop5"><a href="{{route('home')}}">Home</a>
									</li>
									<li class="drop5"><a href="{{route('events')}}">Events</a>
									</li>
									
									<li class="drop5"><a href="{{route('download')}}">Downloads</a>
										
									</li>	
									
									<li class="drop5"><a  href="{{route('calender')}}">Calendar</a>
										
									</li>
									<li class="drop5"><a  href="{{route('resources')}}">Kid's Resources</a>
										
									</li>
									<li class="drop5"><a href="{{URL::to('gallery')}}">Gallery</a>
										
									</li>
										<li class="drop5"><a href="{{URL::to('/#about-us-section')}}" class="scrollpage _mPS2id-h mPS2id-clicked"

											>About Us</a>
									</li>
									<li class="drop5"><a href="{{URL::to('/#feature-section')}}" class="scrollpage _mPS2id-h mPS2id-clicked"
											>Facilities</a>
									</li>
										
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
