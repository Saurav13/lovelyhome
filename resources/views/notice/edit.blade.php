@extends('admin.main')
@section('title','| Notice')
@section('body')



<div class="app-content content container-fluid">
      <div class="content-wrapper">
          <div class="content-body">

	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					
					<h3 class="card-title content-header-center" id="updatenotice">Update Notice | {{$notice->title
					}}</h3>
					
					
					<div class="heading-elements">
						<ul class="list-inline mb-0">
								
										<a href="{{ url()->previous() }}" class="btn btn-labeled btn-default">
											<span class="btn-label"><i class="fa fa-arrow-left" aria-hidden="true"></i></span> Back</a>
									

							<li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
							<li><a data-action="expand"><i class="icon-expand2"></i></a></li>
						</ul>
					</div>

				</div>

				<div class="card-body collapse in">
					<div class="card-block">

						<form class="form" method="POST" action="update">
							{{csrf_field()}}
							
							<div class="row">
								<div class="col-md-12">
									<div class="form-body">
										<div class="form-group col-sm-12">
											<label for="title">Title</label>
											<input type="text" id="title" class="form-control" value="{{$notice->title}}" name="title" cols="40" rows="20">
										</div>
										
										<div class="form-group col-sm-12">
										
											<label for="detail">Details</label>
											<textarea id="detail" class="form-control" id="detail" name="detail">{{$notice->detail}}</textarea>
										</div>
									</div>
								</div>
							</div>

							<div class="form-group col-sm-12">
								<button type="submit" class="btn btn-primary">
									<i class="icon-check2"></i> Update
								</button>
							</div>
							
						</form>	

					</div>
				</div>
			</div>
			
		</div>
	</div>
	
</div>
</div>
</div>


@endsection

@section('js')
@endsection