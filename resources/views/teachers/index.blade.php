@extends('admin.main')
@section('title','| Teachers' )
@section('body')

<div class="app-content content container-fluid">
        <div class="content-wrapper">
          <div class="content-body">
                <section>  
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title" ><a data-action="collapse"><button  class="btn btn-md btn-primary"><i class="icon-plus4" aria-hidden="true"></i> Add Teachers Information </button></a></h4>
                                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="icon-plus4"></i></a></li>
                                                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    
                                    @if (Session::has('message'))
                                    <div class="alert alert-info alert-dismissable">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            {{ Session::get('message') }}
                                        </div>
                                        @endif
                                        
                                        <div class="card-body collapse">                    
                                            <div class="card-block ">
                                                <form class="form" method="POST" action="{{route('teachers.store')}}" enctype="multipart/form-data">
                                                    {{csrf_field()}}
                                                    <div class="row">
                                                        <div class="form-body">
                                                            <div class="form-group col-sm-12">
                                                                <label for="name">Name*</label>
                                                                <input type="text" id="name" class="form-control" placeholder="example - John Doe" name="name" value="{{old('name')}}" required>
                                                            </div>
                                                            
                                                            <div class="form-group col-sm-12">
                                                                <label for="designation">Designation*</label>
                                                                <input type="text" id="designation" class="form-control" placeholder="example - Nepali Teacher" name="designation" value="{{old('designation')}}" required>
                                                        </div>                                      
                                                        <div class="form-group col-sm-12">
                                                            <label for="description">Teachers Description*</label>
                                                            <textarea class="form-control" rows="10" name="description" placeholder="Experience and background of teachers">{{old('description')}}</textarea>
                                                        </div>
                                                      <div class="form-group col-sm-12">
                                                          <label for="facebook_link">Facebook Link</label>
                                                            <input type="text" id="facebook_link" class="form-control" placeholder="example - https://www.facebook.com/LHCCK/" name="facebook_link" value="{{old('facebook_link')}}">
                                                        </div> 
                      
                                                      <div  class="form-group col-sm-12">
                                                          <label for="image">Select Image*</label>
                                                        <input type="file" name="image" class="form-control-file">
                                                    </div>
                      
                                                      <div class="form-group col-sm-12">
                                                          <button type="submit" class="btn btn-md btn-success" id="add">
                                                          <i class="icon-check2">Add</i> 
                                                        </button>
                                                      </div>  
                                                    </div>
                                                  </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section id="naming-a-source-default" class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title">Teachers Information Manager</h4>
                                            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                                            <div class="heading-elements">
                                                <ul class="list-inline mb-0">
                                                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card-body collapse in">
                                            <div class="card-block">
                                                <div class="card-text">
                                                    @if(count($teachers)==null)
                                                    <p>There are no teachers information added yet. Proceed by pressing <button  class="btn btn-md btn-primary"><i class="icon-plus4" aria-hidden="true"></i> Add Teachers Information </button> button above</p>
                                                    @endif
                                                    @if(count($teachers)!=null)
                                                    <p>Teachers information currently being displayed on Lovely Home website</p>
                                                    @endif
                                                    @foreach($teachers as $t)
                                                    <div class="col-md-1" style="padding-top:10px;right:10px;">
                                                            <img src="{{asset('teachers-image'.'/'.$t->image)}}" style="height:80px; width:70px; border-radius:50%;" />
                                                        </div>
                                                    <blockquote class="blockquote col-md-11" style="min-height:90px;">
                                                        <p class="mb-0">{{$t->description}}</p>
                                                        <footer class="blockquote-footer col-md-6">{{$t->name   }} |
                                                                <cite title="Source Title"> {{$t->designation}} |</cite>
                                                                <a href="{{$t->facebook_link}}" target=_blank><img src="{{asset('backend/systemimages/facebook-logo.svg')}}" style="width:20px; padding-bottom:2px;" ></a>
                                                            </footer>
                                                            <div class="col-md-6">
                                                                <div class="row">
                                                                    <a href="{{route('teachers.edit',$t->id)}}" class="btn btn-sm btn-primary" >Edit</a>
                                                                    <a href="#"  id="delete{{$t->id}}" class="btn btn-sm btn-danger">Delete</a>
                                                                </div>
                                                            </div>
                                                    </blockquote>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
 @endsection
 @section('js')
<script type="text/javascript">
    $(document).ready(function() {       
        $("[id*='delete']").click(function(){
          var id = $(this).attr("id").slice(6);
          swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
            if (result.value) {
              $.post("{{route('teachers.destroy')}}",{id:id,_token:"{{csrf_token()}}"},function(data){ 
          swal({
            title:"Deleted Successfully",
            type:"success"
          }).then(function(){
            window.location.reload();
          })
        })
            }
        })
    });
});
</script>
@endsection