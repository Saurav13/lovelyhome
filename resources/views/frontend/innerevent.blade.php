@extends('layouts.app')
@section('body')
@section('title','| '.$event->title)

<section id="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="{{route('home')}}">Home</a></li>
                    @if(app('router')->getRoutes()->match(app('request')->create(URL::previous()))->getName() == 'pastevents')
                    <li><a href="{{route('pastevents')}}">Past Events</a></li>
                    @else
                    <li><a href="{{route('events')}}">Events</a></li>
                    @endif
                    <li class="active">{{$event->title}}</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<input type="hidden" id="timerdate" value="{{ $event->event_date. ' '. $event->start_time }}">
<section id="single_event" class="single_event">
    <div class="container">
        <div class="row pb_20">
            <div class="col-md-1"></div>
            <div class="col-md-10 dot">
                <h2 class="section-title">{{$event->title}}</h2>
            </div>
            <div class="col-md-1"></div>
        </div>
        <div class="row sideber_height">
            <div class="col-md-9">
                <div class="main_block">
                    <div class="main_block_img">
                        <img src="{{asset('events-images'.'/'.$event->featured_image)}}" alt="{{$event->title}}">
                    </div>
                    @if($event->event_date.' '.$event->end_time >= \Carbon\Carbon::now()->format('Y-m-d H:i'))
                        <div class="main_block_timer">
                            <div id="register_timer"></div>
                           		
                        </div>
                    @endif
                    <div class="main_block_description pb_10">
                        <h4>Description</h4>
                        {!! $event->description !!}
                    </div>

                    @if(count($event->images) > 0 )
                        <div class="main_block_gallery">
                            <h4>Gallery</h4>
                            <div id="single_event_gallery" class="single_event_gallery owl-carousel owl-theme">
                                @foreach($event->images as $image)
                                    <div class="item">
                                        <div class="col-md-12 col-portfolio">
                                            <div class="event_gallery_block">
                                                <figure>
                                                    <div class="event_gallery_img">
                                                        <img src="{{ asset('events-images/'.$image->image) }}" alt="{{ $event->title }}">
                                                    </div>
                                                    <figcaption>
                                                        <div class="portfolio-lead">
                                                            <div class="event_gallery_full_screen">
                                                                <a class="teacher_link_img" href="{{ asset('events-images/'.$image->image) }}">
                                                                    <img src="{{ asset('frontend-assets/img/icons/search_gallery.png') }}" title="View">
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </figcaption>
                                                </figure>
                                            </div>
                                        </div>
                                    </div>	
                                @endforeach		
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div id="sidebar" class="sidebar">
                    <div class="info_block t_white">
                        <div class="date b_green_light">
                            <ul>
                                <li class="date_number">{{date('j',strtotime($event->event_date))}}</li>
                                <li class="date_month">{{date('M, Y',strtotime($event->event_date))}}</li>
                            </ul>
                        </div>
                        <div class="contact b_green">
                            <ul>
                                <li class="fz_18"><i class="fa fa-clock-o"></i>{{date('h:i A', strtotime($event->start_time))}} - {{date('h:i A', strtotime($event->end_time))}}</li>
                                <li class="fz_18"><i class="fa fa-phone"></i>{{$event->coordinator_number}}</li>
                                <li class="fz_15 normal"><i class="fa fa-envelope"></i>event@lovelyhome.com</li>
                                <li class="fz_14 lh_16 normal"><i class="fa fa-map-marker"></i> {{$event->venue}}</li>
                            </ul>
                        </div>
                        @if($event->document!=null)
                        <div class="document b_green_dark">
                            <h4><i class="fa fa-folder-open"></i>Event Document</h4>
                            <ul>
                                <li><a href="{{asset('events-documents'.'/'.$event->document)}}" download>{{$event->document}}</a></li>
                            </ul>
                        </div>
                        @endif
                    </div>  
                </div>
            </div>
        </div>
    </div>
</section>
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<script>
    
    $(document).ready(function(){
        var timerdate=$('#timerdate').val(); 
        $("#register_timer").countdown(timerdate, function(event) {
        $(this).html(
            event.strftime('<div class="time">%D<span class="label">days</span></div>' 
            + '<div class="time">%H<span class="label">hours</span></div>'
            + '<div class="time dots">%M<span class="label">minutes</span></div>'
            + '<div class="time dots">%S<span class="label">seconds</span></div>')
            );
        });
    });

</script>
@stop