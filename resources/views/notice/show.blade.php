

<div id="myModal{{$notice->id}}" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <h1 align="center"> <b>{{$notice->title}}</b>  </h1>
      <div class="container">
        <h5 class="pull-left"> <i class="pe-7s-timer"></i> Published on: {{$notice->updated_at->toFormattedDateString()}}</h5>
         <h5 class="pull-right"> Status: {{$notice->status}} </i>  </h5>
        <hr>
      </div>
		  <div class="modal-body">
			<p><br>{!!$notice->detail!!}</p>
		  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
