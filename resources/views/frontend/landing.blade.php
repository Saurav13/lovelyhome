@extends('layouts.app')
@section('title','| Home')
@section('body')

<style>
.about_tabs ul li{
    width:16%;
}
.contact_form_transparent textarea.error {
    border: 3px solid #b70404c7;
}
.acttabfont{
    font-size:20px;
}
</style>

<link rel="stylesheet" type="text/css" href="/frontend-assets/css/frontpage/frontpage.css">
<section id='main-container'>
  <div id='container-svg'>
    <div id='front-grass'>
    </div>
    <img id='sun-img' >
    <img id='mountain-img' >
    <div id="plane-container">
      <img id='plane-img' src='/frontend-assets/img/frontpage/plane.gif'>
    </div>
    <div id="clock-container" class='parallax-layer-3'>
      <img id='minute-hand' src='/frontend-assets/img/frontpage/minute-hand.svg'>
      <img id='hour-hand' src='/frontend-assets/img/frontpage/hour-hand.svg'>
    </div>
    <img id='flag-svg' class='parallax-layer-3 blink-click-animation'>
    <img id='school-flag' class='parallax-layer-3 blink-click-animation'>
    <img id='stupa-img' class='parallax-layer-4'>
    <img id='temple-img' class='parallax-layer-2'>
    <div id="notice-board-container" class='parallax-layer-2'>
      <img id='board-img'>
      <div class='notice-board-text notice-text'>
        <p>notice</p>
      </div>
      <div class='notice-board-text actual-text'>
        <p>{{$notice ? $notice->title : 'No Notice!'}}</p>
      </div>
    </div>
    <div id="bus-container" class='parallax-layer-2'>
      <img id='bus-svg' >
      <img id='wheel-front'>
      <img id='wheel-back'>
      </div>
    </div>
  <div id='logo-div' class='heartbeat-anim'>
    <img id='logo-img-kid' class='logo-img-class bounceInUp' src='/frontend-assets/img/frontpage/logo/logo-kid.svg'>
      <img id='logo-img-h' class='logo-img-class bounceInRight' src='/frontend-assets/img/frontpage/logo/logo-h.svg'>
      <img id='logo-img-l' class='logo-img-class bounceInLeft' src='/frontend-assets/img/frontpage/logo/logo-l.svg'>
      <img id='logo-img-roof' class='logo-img-class bounceInDown' src='/frontend-assets/img/frontpage/logo/logo-roof.svg'>
  </div>
  <div id='video-drop-down'>
    <img id="balloon-image" src="">
    <div id='home-video-container'>
      <iframe src="https://www.youtube.com/embed/bu9qVmwFL68" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen>
      </iframe>
      <img id='video-close-button' src='/frontend-assets/img/frontpage/flip-arrow.svg'>
    </div>
  </div>
</section><!-- horizontal parallax end -->
<div class="transition-parallax-slider" id="Blocks"></div>
<section id='my-slider-section'>
  <div class="parallax-in-slider">
    <img class="slider-parallax-layer slider-parallax-layer-3" src="/frontend-assets/img/frontpage/slider/top.png"/>
    <img class="slider-parallax-layer slider-parallax-layer-2" src="/frontend-assets/img/frontpage/slider/mid.png"/>
    <div class="tranition-div-slider slider-parallax-layer slider-parallax-layer-1">
      <div class='parallax-bottom-padding'></div>
      <img class="" src="/frontend-assets/img/frontpage/slider/bottom.png"/>
    </div>
  </div>
  <div id='parent-carausel-container' class="lovelyhome-blocks">
        <div class="parent-slider-container">
          <div id="myCarousel" class="" data-ride="carousel">
            <div class="myCarousel-inner" id="block-changes" >
              <div id="sliderJawalakhel" class="carousel-item image-frame-container active">
                <p class="myCarousel-caption">Jawalakhel Block</p>
                <img class="customs-slider-image slider-big-image" src="/landingslider-images/jawalakhel-1.jpg" alt="Los Angeles">
                <img class="customs-slider-image slider-small-image slider-small-image-left" src="/landingslider-images/jawalakhel-2.jpg" alt="Los Angeles">
                <img class="customs-slider-image slider-small-image slider-small-image-right" src="/landingslider-images/jawalakhel-3.jpg" alt="Los Angeles">
              </div>
              <div id="sliderBhanimandal" class="carousel-item image-frame-container">
                <p class="myCarousel-caption">Bhanimandal Block</p>
                <img class="customs-slider-image slider-big-image" src="/landingslider-images/bhanimandal-1.jpg" alt="Los Angeles">
                <img class="customs-slider-image slider-small-image slider-small-image-left" src="/landingslider-images/bhanimandal-2.jpg" alt="Los Angeles">
                <img class="customs-slider-image slider-small-image slider-small-image-right" src="/landingslider-images/bhanimandal-3.jpg" alt="Los Angeles">
              </div>
              <div id="sliderSanogaun" class="carousel-item image-frame-container">
                <p class="myCarousel-caption">Sanogaun Block</p>
                <img class="customs-slider-image slider-big-image" src="/landingslider-images/sanogaun-1.jpg" alt="Los Angeles">
                <img class="customs-slider-image slider-small-image slider-small-image-left" src="/landingslider-images/sanogaun-2.jpg" alt="Los Angeles">
                <img class="customs-slider-image slider-small-image slider-small-image-right" src="/landingslider-images/sanogaun-3.jpg" alt="Los Angeles">
                </div>
                <div id="sliderDhobighat" class="carousel-item image-frame-container">
                <p class="myCarousel-caption">Dhobighat Block</p>
                <img class="customs-slider-image slider-big-image" src="/landingslider-images/dhobighat-1.jpg" alt="Los Angeles">
                <img class="customs-slider-image slider-small-image slider-small-image-left" src="/landingslider-images/dhobighat-2.jpg" alt="Los Angeles">
                <img class="customs-slider-image slider-small-image slider-small-image-right" src="/landingslider-images/dhobighat-3.jpg" alt="Los Angeles">
                </div>
            </div>
            <img class="myCarousel-nav myCarousel-nav-left" src="/frontend-assets/img/frontpage/slider/next.png" alt="next"/>
            <img class="myCarousel-nav myCarousel-nav-prev" src="/frontend-assets/img/frontpage/slider/next.png" alt="previous"/>
          </div>
        </div>
      </div>
</section><!-- slider section end -->
<div class="transition-layer"></div>
<section id='about-us-section'>
  <div class="container">
    <div class='about-us-board'>
      <p class='hidden-text-for-size'>
        It is a matter of great pleasure to introduce Lovely Home Child Care & Kindergarten, established in 2002. Currently, Lovely Home is providing its services in 4 locations viz. Jawalakhel, Bhanimandal, Sanogaun and Dhobighat. The main office is located in Jawalakhel, Lalitpur, (Behind Alka Hospital). We provide daycare and kindergarten facilities to the children aged from 3 months to 6 years.
      </p>
      <p class='about-us-text'></p>
    </div>
  </div><!-- about us section end -->
  <div id="table-chair-layer">
    <img class="parallax-chair" src="/frontend-assets/img/frontpage/slider/chair.png"/>
    <img class="parallax-table" src="/frontend-assets/img/frontpage/slider/table.png"/>
  </div>
</section><!-- about us section end -->
<section id='feature-section' class='container lovelyhome-facilties'>
  <div id="feature-section-top-padding"></div>
  <div id='feature-container' class='container'>
    <div class="row">
          <div class="col-xs-offset-3 col-xs-6 col-sm-offset-0 col-sm-6 col-md-offset-1 col-md-4 col-lg-offset-0 col-lg-3  drop-animation-1">
            <div class="cloud-container-parent">
              <div class="cloud-container cloud-container-1">
                <div class="cloud-wrapper">
                  <img class="cloud-hanger-img cloud-hanger-1" src="/frontend-assets/img/frontpage/features/Asset 1.svg" alt="">
                </div>
              </div>
              <div class="cloud-container cloud-container-2">
                <div class="cloud-wrapper">
                  <img class="cloud-hanger-img cloud-hanger-2" src="/frontend-assets/img/frontpage/features/Asset 2.svg" alt="">
                </div>
              </div>
            </div>
          </div>
          <!-- col end -->
          <div class="col-xs-offset-3 col-xs-6 col-sm-offset-0 col-sm-6 col-md-offset-2 col-md-4 col-lg-offset-0 col-lg-3  drop-animation-2">
            <div class="cloud-container-parent">
              <div class="cloud-container cloud-container-3">
                <div class="cloud-wrapper">
                  <img class="cloud-hanger-img cloud-hanger-3" src="/frontend-assets/img/frontpage/features/Asset 3.svg" alt="">
                </div>
              </div>
              <div class="cloud-container cloud-container-4">
                <div class="cloud-wrapper">
                  <img class="cloud-hanger-img cloud-hanger-4" src="/frontend-assets/img/frontpage/features/Asset 4.svg" alt="">
                </div>
              </div>
            </div>
          </div><!-- col end -->
          <div class="col-xs-offset-3 col-xs-6 col-sm-offset-0 col-sm-6 col-md-offset-1 col-md-4 col-lg-offset-0 col-lg-3  drop-animation-3">
            <div class="cloud-container-parent">
              <div class="cloud-container cloud-container-5">
                <div class="cloud-wrapper">
                  <img class="cloud-hanger-img cloud-hanger-5" src="/frontend-assets/img/frontpage/features/Asset 6.svg" alt="">
                </div>
              </div>
              <div class="cloud-container cloud-container-6">
                <div class="cloud-wrapper">
                  <img class="cloud-hanger-img cloud-hanger-6" src="/frontend-assets/img/frontpage/features/Asset 5.svg" alt="">
                </div>
              </div>
            </div>
          </div><!-- col end -->
          <div class="col-xs-offset-3 col-xs-6 col-sm-offset-0 col-sm-6 col-md-offset-2 col-md-4 col-lg-offset-0 col-lg-3  drop-animation-4">
            <div class="cloud-container-parent">
              <div class="cloud-container cloud-container-7">
                <div class="cloud-wrapper">
                  <img class="cloud-hanger-img cloud-hanger-7" src="/frontend-assets/img/frontpage/features/Asset 7.svg" alt="">
                </div>
              </div>
              <div class="cloud-container cloud-container-8">
                <div class="cloud-wrapper">
                  <img class="cloud-hanger-img cloud-hanger-8" src="/frontend-assets/img/frontpage/features/Asset 8.svg" alt="">
                </div>
              </div>
            </div>
          </div><!-- col end -->
        </div>
  </div>
</section><!-- feature section end -->
<div class="transition-to-facilities-1"></div>
<div class="transition-to-facilities-2"></div>

<section id="facilities-section" style="">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12" style="padding-left:0px;padding-right:0px;">
                <div class="about_tabs">
                    <div class="about_bear_img hidden-sm hidden-xs">
                        <img src="/frontend-assets/img/home/about_bear.png" alt="">
                    </div>
                    <ul class="nav nav-tabs">
                        <li class="active"><a class="tab_red acttabfont" href="#activity_1" data-toggle="tab">Indoor</a></li>
                        <li><a class="tab_green acttabfont" href="#activity_2" data-toggle="tab">Outdoor</a></li>
                        <li><a class="tab_blue acttabfont" href="#activity_3" data-toggle="tab">Sports</a></li>
                        <li><a class="tab_yellow acttabfont" href="#activity_4" data-toggle="tab">Events</a></li>
                        <li><a class="tab_red acttabfont" href="#activity_5" data-toggle="tab">Extras</a></li>
                    </ul>
                    <div class="tab-content about_tabs_content">
                        <div class="tab-pane tab_red active card-container" id="activity_1">
                            @foreach($activities->where('category','indoor') as $a)
                            <div class="card-list">
                                <div class="card-body">
                                <img src="{{asset('/activities-images'.'/'.$a->image)}}" alt="puppy image">
                                <p>{{$a->title}}</p>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="tab-pane tab_red card-container" id="activity_2">
                            @foreach($activities->where('category','outdoor') as $a)
                                <div class="card-list">
                                    <div class="card-body">
                                    <img src="{{asset('/activities-images'.'/'.$a->image)}}" alt="puppy image">
                                    <p>{{$a->title}}</p>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="tab-pane tab_red card-container" id="activity_3">
                            @foreach($activities->where('category','sports') as $a)
                                <div class="card-list">
                                    <div class="card-body">
                                    <img src="{{asset('/activities-images'.'/'.$a->image)}}" alt="puppy image">
                                    <p>{{$a->title}}</p>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="tab-pane tab_red card-container" id="activity_4">
                                @foreach($activities->where('category','events') as $a)
                                <div class="card-list">
                                    <div class="card-body">
                                    <img src="{{asset('/activities-images'.'/'.$a->image)}}" alt="puppy image">
                                    <p>{{$a->title}}</p>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="tab-pane tab_red card-container" id="activity_5">
                                @foreach($activities->where('category','extras') as $a)
                                <div class="card-list">
                                    <div class="card-body">
                                    <img src="{{asset('/activities-images'.'/'.$a->image)}}" alt="puppy image">
                                    <p>{{$a->title}}</p>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
    </div>
</section>
{{--  <section id="offer" class="offer">
    <div class="container">
        <div class="row">
            <h2>What we offer</h2>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="offer_block wow fadeInUp">
                    <i class="fa fa-diamond" style="color: #b70404;"></i>
                    <h4>Event</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="offer_block wow fadeInUp" >
                    <i class="fa fa-music" style="color:#fff400"></i>
                    <h4>Music lesson</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="offer_block wow fadeInUp">
                    <i class="fa fa-magic" style="color:#58b600"></i>
                    <h4>Healthy food</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                </div>
            </div>
        </div>
    </div>
</section>  --}}
<div class="transition-layer-2"></div>

<section id="teacher" class="teacher text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title">Management Team</h2>
            </div>
        </div>
        <div class="row col-md-12 arrows_red">
            <div id="teacher_owl" class="owl-carousel owl-theme">
                @foreach($teachers->chunk(4) as $tchunk)

                <div class="item">
                    @if(count($tchunk)>0)
                    <div class="row">
                        <div class="col-md-12">
                            <div class="teacher_block grey_block wow fadeInUp">
                                <div class="teacher_avatar">
                                    <img src="{{asset('/teachers-image'.'/'.$tchunk[0+$loop->index*4]->image)}}" alt="">
                                </div>
                                <div class="teacher_about">
                                    <h3>{{$tchunk[0+$loop->index*4]->name}}</h3>
                                    <span>{{$tchunk[0+$loop->index*4]->designation}}</span>
                                    <p>{{$tchunk[0+$loop->index*4]->description}}</p>
                                    <div class="teacher_link social_icon t_green_dark">
                                        <ul>
                                        <li><a target="_blank" href="{{$tchunk[0+$loop->index*4]->facebook_link}}"><i class="fa fa-facebook"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @if(count($tchunk)>1)
                    <div class="row teach_row">
                        <div class="col-md-12">
                            <div class="teacher_block blue_block wow fadeInUp">
                                <div class="teacher_avatar">
                                    <img src="{{asset('/teachers-image'.'/'.$tchunk[1+$loop->index*4]->image)}}" alt="">
                                </div>
                                <div class="teacher_about">
                                    <h3><a href="teacher.html">{{$tchunk[1+$loop->index*4]->name}}</a></h3>
                                    <span>{{$tchunk[1+$loop->index*4]->designation}}</span>
                                    <p>{{$tchunk[1+$loop->index*4]->description}}</p>
                                    <div class="teacher_link social_icon">
                                        <ul>
                                            <li><a href="{{$tchunk[1+$loop->index*4]->facebook_link}}"><i class="fa fa-facebook"></i></a></li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                <div class="item">
                    @if(count($tchunk)>2)
                    <div class="row">
                        <div class="col-md-12">
                            <div class="teacher_block red_block wow fadeInUp">
                                <div class="teacher_avatar">
                                    <img src="{{asset('/teachers-image'.'/'.$tchunk[2+$loop->index*4]->image)}}" alt="">
                                </div>
                                <div class="teacher_about social_icon">
                                    <h3><a href="teacher.html">{{$tchunk[2+$loop->index*4]->name}}</a></h3>
                                    <span>{{$tchunk[2+$loop->index*4]->designation}}</span>
                                    <p>{{$tchunk[2+$loop->index*4]->description}}</p>
                                    <div class="teacher_link">
                                        <ul>
                                            <li><a href="{{$tchunk[2+$loop->index*4]->facebook_link}}"><i class="fa fa-facebook"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @if(count($tchunk)>3)
                    <div class="row teach_row">
                        <div class="col-md-12">
                            <div class="teacher_block yellow_block wow fadeInUp">
                                <div class="teacher_avatar">
                                    <img src="{{asset('/teachers-image'.'/'.$tchunk[3+$loop->index*4]->image)}}" alt="">
                                </div>
                                <div class="teacher_about">
                                    <h3><a href="teacher.html">{{$tchunk[3+$loop->index*4]->name}}</a></h3>
                                    <span>{{$tchunk[3+$loop->index*4]->designation}}</span>
                                    <p>{{$tchunk[3+$loop->index*4]->description}}</p>
                                    <div class="teacher_link social_icon">
                                        <ul>
                                            <li><a href="{{$tchunk[3+$loop->index*4]->facebook_link}}"><i class="fa fa-facebook"></i></a></li>
                                          </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>


@if(count($testimonials)>=3)
<section id="stories" class="stories">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title">Happy Parents</h2>
            </div>
        </div>
        <div class="row col-md-12 arrows_red">
            <div id="stories_owl" class="owl-carousel owl-theme">
                <div class="item col-md-12 wow fadeInUp">
                @foreach($testimonials->chunk(3) as $chunk)
                    @foreach($chunk as $t)
                    @if($loop->index == 0)
                    <div class="s_green_block all_color">
                            <div class="top_stories">
                                <img src="{{asset('frontend-assets/img/home/top_stories_green.png')}}">
                            </div>
                            <div class="bottom_stories">
                                <img src="{{asset('frontend-assets/img/home/bottom_stories_green.png')}}"/>
                            </div>
                            <p><i class="fa fa-quote-left"></i></p>
                            <p>{{$t->description}}</p>
                        </div>
                        <div class="stories_autor">
                            <div class="stories_autor_avatar">
                                <img src="{{asset('testimonials-image'.'/'.$t->image)}}" alt="{{$t->name}}">
                            </div>
                            <div class="stories_autor_text c_green">
                                <p><span>{{$t->name}}</span></p>
                                <p>{{$t->designation}}</p>
                            </div>
                        </div>
                    </div>
                @elseif($loop->index==1)
                <div class="item col-md-12 wow fadeInUp" data-wow-delay="0.1s">
                        <div class="s_red_block all_color">
                            <div class="top_stories">
                                <img src="/frontend-assets/img/home/top_stories_red.png">
                            </div>
                            <div class="bottom_stories">
                                <img src="/frontend-assets/img/home/bottom_stories_red.png">
                            </div>
                            <p><i class="fa fa-quote-left"></i></p>
                            <p>{{$t->description}}</p>
                        </div>
                        <div class="stories_autor">
                            <div class="stories_autor_avatar">
                                <img src="{{asset('testimonials-image'.'/'.$t->image)}}" alt="{{$t->name}}">
                            </div>
                            <div class="stories_autor_text c_red">
                                <p><span>{{$t->name}}</span></p>
                                <p>{{$t->designation}}</p>
                            </div>
                        </div>
                    </div>
                @elseif($loop->index==2)
                <div class="item col-md-12 wow fadeInUp" data-wow-delay="0.2s">
                        <div class="s_margin_block all_color">
                            <div class="top_stories">
                                <img src="/frontend-assets/img/home/top_stories_blue.png" alt="">
                            </div>
                            <div class="bottom_stories">
                                <img src="/frontend-assets/img/home/bottom_stories_blue.png" alt="">
                            </div>
                            <p><i class="fa fa-quote-left"></i></p>
                            <p>{{$t->description}}</p>

                        </div>
                        <div class="stories_autor">
                            <div class="stories_autor_avatar">
                                <img src="/frontend-assets/img/blog/author4.jpg" alt="">
                            </div>
                            <div class="stories_autor_text c_blue">
                                <p><span>{{$t->name}}</span></p>
                                <p>{{$t->designation}}</p>
                            </div>
                        </div>
                    </div>
                @endif
                @endforeach
                @endforeach
            </div>
        </div>
    </div>
</section>
@endif


<!-- Replace start-->
<section id="contact" class="contact_v1">
    <div class="contact_overlay_ball hidden-sm hidden-xs"></div>
    <div class="contact_overlay_plain hidden-sm hidden-xs"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <h2 class="section-title">Send us your feedbacks</h2>
                <span class="section-title" style="font-size:14px;">* for existing parents or any queries</span>
                <div class="contact_form contact_form_transparent">
                    <form method="post" id="feedback_form">
                        <div id="form-messages1" style="color: #CD3653;font-size: 1.5rem;font-weight: 600;"></div>
                        <div class="form-group">
                            <input id="name1" name="name" type="text" placeholder="Enter Your Name">
                        </div>
                        <div class="form-group">
                            <input id="email1" name="email" type="text" placeholder="Enter Your E-mail">
                        </div>
                        <div class="form-group">
                            <input id="phone1" name="phone" type="text" placeholder="Enter Your Phone Number">
                        </div>
                        <div class="form-group">
                            <input id="subject1" name="subject" type="text" placeholder="Enter Your Subject">
                        </div>
                        <textarea id="message1" name="message" rows="3" placeholder="Enter Your Feedback"></textarea>
                        <div id="messegeResult1" class="messegeResult">
                            <p>Feedback was successfully sent</p>
                            <button type="submit">Send</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <h2 class="section-title">Contact Us <a href="/faqs" class="btn btn-primary pull-right" style="cursor:pointer;">Faqs</a></h2>

                <span class="section-title" style="font-size:14px;">* for new and prospective parents.</span>
                <div class="contact_form contact_form_transparent">
                    <form method="post" id="contact_form">
                        <div id="form-messages" style="color: #CD3653;font-size: 1.5rem;font-weight: 600;"></div>
                        <div class="form-group">
                            <input id="name" name="name" type="text" placeholder="Enter Your Name">
                        </div>
                        <div class="form-group">
                            <input id="email" name="email" type="text" placeholder="Enter Your E-mail">
                        </div>
                        <div class="form-group">
                            <input id="phone" name="phone" type="text" placeholder="Enter Your Phone Number">
                        </div>
                        <div class="form-group">
                            <input id="subject" name="subject" type="text" placeholder="Enter Your Subject">
                        </div>
                        <textarea id="message" name="message" rows="3" placeholder="Enter Your Message"></textarea>
                        <div id="messegeResult" class="messegeResult">
                            <p>The message was successfully sent</p>
                            <button type="submit">Send</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        </div>
</section>
<section id="google_maps" class="google_maps">
    <div id="map"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-5 location_info">
                <div class="location_block">
                    <div class="overlay_footer_copter hidden-sm hidden-xs"></div>
                    <p id="location" class="location">Jawalakhel</p>
                    <p id="phone" class="phone">01-5520225, 01-5554368</p>
                    <p id="address" class="adress">Jawalakhel-20, Lalitpur</p>

                </div>
            </div>
        </div>
    </div>
</section>
<div style="height: 72px;"></div>

<input type="hidden" id="mapcoord" lat="27.674321" lng="85.316561"/>
@stop

@section('js')
    <script type='text/javascript' src="https://code.createjs.com/1.0.0/preloadjs.min.js"></script>
    <script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/snap.svg/0.5.1/snap.svg-min.js"></script>
        <script type='text/javascript'src="https://code.createjs.com/1.0.0/soundjs.min.js"></script>
    <script type='text/javascript' src="/frontend-assets/js/frontpage/frontpage.js"></script>

    <script>
    $(document).ready(function(){
        var timerdate=$('#timerdate').val();
        $("#index_timer").countdown(timerdate, function(event) { /* change date */
        $(this).html(
            event.strftime(' <div class="time">%D<span class="label">days</span></div>'
            + '<div class="time">%H<span class="label">hours</span></div>'
            + '<div class="time dots">%M<span class="label">minutes</span></div>'
            + '<div class="time dots">%S<span class="label">seconds</span></div>')
            );
        });
    });

    $(document).ready(function(){
        $('#blockJawalakhel').click(function(){
            $('#location').html('Jawalakhel Block');
            $('#phone').html('01-5520225, 01-5554368');
            $('#address').html('Jawalakhel-20, Lalitpur');
            $('#mapcoord').attr('lat','27.674321');
            $('#mapcoord').attr('lng','85.316561');

            initMap_footer();
            $('#sliderJawalakhel').attr('class','carousel-item image-frame-container active');
            $('#sliderBhanimandal').attr('class','carousel-item image-frame-container');
            $('#sliderSanogaun').attr('class','carousel-item image-frame-container');
            $('#sliderDhobighat').attr('class','carousel-item image-frame-container');

            $('#blockJawalakhel').addClass('tagactive');
            $('#blockBhanimandal').removeClass('tagactive');
            $('#blockSanogaun').removeClass('tagactive');
            $('#blockDhobighat').removeClass('tagactive');



        })
        $('#blockBhanimandal').click(function(){
            $('#location').html('Bhanimandal Block');
            $('#phone').html('01-5536382');
            $('#address').html('Ekantakuna Road, Patan');
            $('#mapcoord').attr('lat','27.669603');
            $('#mapcoord').attr('lng','85.306492');

            initMap_footer();
            $('#sliderJawalakhel').attr('class','carousel-item image-frame-container');
            $('#sliderBhanimandal').attr('class','carousel-item image-frame-container active');
            $('#sliderSanogaun').attr('class','carousel-item image-frame-container');
            $('#sliderDhobighat').attr('class','carousel-item image-frame-container');

            $('#blockJawalakhel').removeClass('tagactive');
            $('#blockBhanimandal').addClass('tagactive');
            $('#blockSanogaun').removeClass('tagactive');
            $('#blockDhobighat').removeClass('tagactive');

        })
        $('#blockSanogaun').click(function(){
            $('#location').html('Sanogaun Block');
            $('#phone').html('01-5582550');
            $('#address').html('Sanogaun, Lalitpur');
            $('#mapcoord').attr('lat','27.674321');
            $('#mapcoord').attr('lng','85.316561');
            initMap_footer();
            $('#sliderJawalakhel').attr('class','carousel-item image-frame-container');
            $('#sliderBhanimandal').attr('class','carousel-item image-frame-container');
            $('#sliderSanogaun').attr('class','carousel-item image-frame-container active');
            $('#sliderDhobighat').attr('class','carousel-item image-frame-container');

            $('#blockJawalakhel').removeClass('tagactive');
            $('#blockBhanimandal').removeClass('tagactive');
            $('#blockSanogaun').addClass('tagactive');
            $('#blockDhobighat').removeClass('tagactive');
        });
        $('#blockDhobighat').click(function(){
            $('#location').html('Dhobighat Block');
            $('#phone').html('01-5582550');
            $('#address').html('Dhobighat, Lalitpur');
            $('#mapcoord').attr('lat','27.674321');
            $('#mapcoord').attr('lng','85.316561');
            initMap_footer();
            $('#sliderJawalakhel').attr('class','carousel-item image-frame-container');
            $('#sliderBhanimandal').attr('class','carousel-item image-frame-container');
            $('#sliderSanogaun').attr('class','carousel-item image-frame-container');
            $('#sliderDhobighat').attr('class','carousel-item image-frame-container active');

            $('#blockJawalakhel').removeClass('tagactive');
            $('#blockBhanimandal').removeClass('tagactive');
            $('#blockSanogaun').removeClass('tagactive');
            $('#blockDhobighat').addClass('tagactive');
        })
    });
    </script>
@stop
