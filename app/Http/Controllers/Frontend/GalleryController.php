<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Gallery;

class GalleryController extends Controller
{
    public function gallery()
    {
        $cats = Gallery::has('images')->groupBy('category')->pluck('category');
        
        $galleries = [];
        foreach($cats as $cat){
            $galleries[$cat] = Gallery::has('images')->where('category',$cat)->orderBy('created_at','desc')->limit(12)->get();
        }
        
        return view('frontend.gallery',compact('galleries'));
    }

    public function getmorealbums(Request $request){
        if ($request->ajax()) {
            $last = '';
            try{
                $albums = Gallery::has('images')->where('category',$request->category)->where('created_at','<',$request->itemId)->orderBy('created_at','desc')->limit(12)->get();
                
                $view = view('frontend.partials.gallery')->with('category',$request->category)->with('albums',$albums)->render();
                $last = count($albums) > 0 ? $albums->last()->created_at : '';
                
                return response()->json(['data'=>$view,'last'=>$last]);
            }
            catch (\Exception $e) {
                return response()->json('error',422);
            }
        }
    }

    public function images($category, $slug){
        if($category == 'facebook')
            return view('frontend.innerfbgallery')->with('id',$slug);

        $album = Gallery::where('slug',$slug)->first();
        
        if(!$album) abort(404);

        $images = $album->images()->orderBy('id','desc')->limit(12)->get();

        return view('frontend.innergallery')->with('album',$album)->with('images',$images);
    }

    public function getmoreimages(Request $request,$slug){
        if ($request->ajax()) {
            $last = 0;
            try{
                $album = Gallery::where('slug',$slug)->first();
                $images = $album->images()->where('id','<',$request->itemId)->orderBy('id','desc')->limit(12)->get();
                $view = view('frontend.partials.innergallery')->with('album',$album)->with('images',$images)->render();
                $last = count($images) > 0 ? $images->last()->id : 0;
                
                return response()->json(['data'=>$view,'last'=>$last]);
            }
            catch (\Exception $e) {
                return response()->json('error',422);
            }
        }
    }
}

