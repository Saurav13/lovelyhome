$(function() {
  var $imgPath = '/frontend-assets/img/frontpage/';
  var $soundPath = '/frontend-assets/sound/';
  //for preload
	var preload;
	var timeoutvar = null;
	var current_sound;
  initParallax();
	function initParallax() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "tree-png", src: $imgPath + "tree.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tree-gif", src: $imgPath + "tree.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "plane-svg", src: $imgPath + "plane-final.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud-1", src: $imgPath + "cloud-1.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud-2", src: $imgPath + "cloud-2.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud-3", src: $imgPath + "cloud-3.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud-4", src: $imgPath + "cloud-4.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud-5", src: $imgPath + "cloud-5.svg", type: createjs.AbstractLoader.IMAGE},

			{id: "sun", src: $imgPath + "sun.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "bus", src: $imgPath + "van.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "wheel", src: $imgPath + "wheel-2.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "balloon", src: $imgPath + "balloon.png", type: createjs.AbstractLoader.IMAGE},
			{id: "board", src: $imgPath + "board.png", type: createjs.AbstractLoader.IMAGE},
			{id: "flag", src: $imgPath + "flag.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "school-flag", src: $imgPath + "flag-lh.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "school-flag-gif", src: $imgPath + "flag-lh.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "flag-gif", src: $imgPath + "flag.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "mountain", src: $imgPath + "mountain.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "hill", src: $imgPath + "hill.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "kids", src: $imgPath + "kids-animals.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "road", src: $imgPath + "road-new.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "school", src: $imgPath + "school-2.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "sky", src: $imgPath + "sky-pattern.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "stupa", src: $imgPath + "stupa.png", type: createjs.AbstractLoader.IMAGE},
			{id: "temple", src: $imgPath + "temple.png", type: createjs.AbstractLoader.IMAGE},
			{id: "teachers", src: $imgPath + "teachers.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "wrong", src: $imgPath + "wrongicon.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tree-png", src: $imgPath + "tree.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tree-gif", src: $imgPath + "tree.gif", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound-anthem", src: $soundPath+"/anthem.mp3"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		// $('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		// $('#loading-wrapper').hide(0);
		// call main function
    $('#logo-div').css('animation','none');
    $('#logo-div').fadeOut(500, 'linear', function(){
			$('#logo-div').remove();
      mainWindow();
    });
	}
  // adding trees
  function mainWindow(){
    $('#notice-board-container').show(0);
    $('#clock-container').show(0);
		$('#stupa-img').attr('src', preload.getResult("stupa").src);
		$('#temple-img').attr('src', preload.getResult("temple").src);
		$('#bus-svg').attr('src', preload.getResult("bus").src);
		$('#wheel-front').attr('src', preload.getResult("wheel").src);
		$('#wheel-back').attr('src', preload.getResult("wheel").src);
		$('#mountain-img').attr('src', preload.getResult("mountain").src);
		$('#board-img').attr('src', preload.getResult("board").src);
		$('#school-flag').attr('src', preload.getResult("school-flag").src);
		$('#sun-img').attr('src', preload.getResult("sun").src);
    $('#balloon-image').attr('src', preload.getResult("balloon").src);
    for (var i = 1; i < 23; i++) {
			if(i==8 ||i ==21){
			} else if (i < 15) {
        $('#container-svg').append("<img class='tree-img parallax-layer-4 tree-" + i + "' src='"+preload.getResult("tree-png").src+"'>");
      } else if (i < 19) {
        $('#container-svg').append("<img class='tree-img parallax-layer-3 tree-" + i + "' src='"+preload.getResult("tree-png").src+"'>");
      } else if (i < 23) {
        $('#container-svg').append("<img class='tree-img parallax-layer-2 tree-" + i + "' src='"+preload.getResult("tree-png").src+"'>");
      }
    }
    var treePng = document.createElement("img"),
    treePngUrl = preload.getResult("tree-png").src;
    treePng.src = treePngUrl;
    var treeGif = document.createElement("img"),
    treeGifUrl = preload.getResult("tree-gif").src;
    treeGif.src = treeGifUrl;
    $('.tree-img').hover(function() {
      $(this).attr('src', treeGif.src);
    }, function() {
      $(this).attr('src', treePng.src);
    })
    var container = Snap('#container-svg');
    var skyPattern = Snap.load(preload.getResult("sky").src, function(loadedFragment) {
      container.append(loadedFragment);
    });
    var cloud1 = Snap.load(preload.getResult("cloud-1").src, function(loadedFragment) {
      container.append(loadedFragment);
    });
    var cloud2 = Snap.load(preload.getResult("cloud-2").src, function(loadedFragment) {
      container.append(loadedFragment);
    });
    var cloud3 = Snap.load(preload.getResult("cloud-3").src, function(loadedFragment) {
      container.append(loadedFragment);
    });
    var cloud4 = Snap.load(preload.getResult("cloud-4").src, function(loadedFragment) {
      container.append(loadedFragment);
    });
    var cloud5 = Snap.load(preload.getResult("cloud-5").src, function(loadedFragment) {
      container.append(loadedFragment);
    });
    var hill = Snap.load(preload.getResult("hill").src, function(loadedFragment) {
      container.append(loadedFragment);
    });
    var school = Snap.load(preload.getResult("school").src, function(loadedFragment) {
      container.append(loadedFragment);
      startClock();
    });

		var startClock = function(){
			var startTime = new Date(),
	    // get the starting positions of each hand (in seconds)
	    startS = startTime.getSeconds(),
	    startM = startTime.getMinutes() * 60 + startS,
	    startH = startTime.getHours() % 12 * 3600 + startM;
			refreshClock();
			// compute the rotation amount relative to the starting time
			function refreshClock() {
			  var now = new Date(),
			      diff = timeDifference(startTime, now),
			      degM = (startM + diff) / 3600 * 360,
			      degH = (startH + diff) / 43200 * 360;
				$("#minute-hand").css("transform", "rotate("+ degM +"deg)");
				$("#hour-hand").css("transform", "rotate("+ degH +"deg)");
			  setTimeout(refreshClock, 1000);
			}
			// compute the time difference between two date objects (in seconds)
			function timeDifference(date1, date2) {
			  return Math.round(Math.abs(date2.getTime() - date1.getTime()) / 1000);
			}
		};

		var flagToggle = true;
		$('#flag-svg').attr('src', preload.getResult("flag").src);
		$('#flag-svg').click(function(){
			if(flagToggle){
				$('#flag-svg').attr('src', preload.getResult("flag-gif").src);
        $('#flag-svg').removeClass('blink-click-animation');
				var current_sound = createjs.Sound.play('sound-anthem');
				current_sound.play();
				current_sound.on("complete", function(){
						$('#flag-svg').attr('src', preload.getResult("flag").src);
						flagToggle = !flagToggle;
				});
			} else{
					$('#flag-svg').attr('src', preload.getResult("flag").src);
					createjs.Sound.stop();
			}
			flagToggle = !flagToggle;
		});
    var roadPoly;
    var roadSVG = Snap.load(preload.getResult("road").src, function(loadedFragment) {
      container.append(loadedFragment);
      roadPoly = Snap.select('#road-poly');
    });
    var kidsSVG = Snap.load(preload.getResult("kids").src, function(loadedFragment) {
      container.append(loadedFragment);
    });

    var mainContainerWidth = $('#main-container').width(),
      mainContainerHeight = $('#main-container').height(),
      mainContainerWidthHalf = mainContainerWidth / 2;
    $('#container-svg').width(mainContainerWidth);
    $('#container-svg').height(mainContainerWidth * 900 / 2048);
    $('#quote-text').css('font-size', 30 * mainContainerWidth / 2048 + 'px');

    var containerWidth = $('#container-svg').width(),
      containerWidthHalf = containerWidth / 2,
      maxParallax4 = containerWidth * 0.1,
      maxParallax3 = containerWidth * 0.25,
      maxParallax2 = containerWidth * 0.375,
      maxParallax1 = containerWidth * 0.4,
      scrollConstant = -1,
      scrollSpeed = 5;
    var offsetArr = [0, 0, 0, 0];
    var scrollChangeFlag = true;
    // for resize event
    window.onresize = function(event) {
      mainContainerWidth = $('#main-container').width();
      mainContainerHeight = $('#main-container').height();
      mainContainerWidthHalf = mainContainerWidth / 2;
      $('#container-svg').width(mainContainerWidth);
      $('#container-svg').height(mainContainerWidth * 900 / 2048);
      $('#quote-text').css('font-size', 30 * mainContainerWidth / 2048 + 'px');
      containerWidth = $('#container-svg').width();
      containerWidthHalf = containerWidth / 2;
      maxParallax4 = containerWidth * 0.1;
      maxParallax3 = containerWidth * 0.25;
      maxParallax2 = containerWidth * 0.375;
      maxParallax1 = containerWidth * 0.5;
      scrollConstant = -1;
      scrollSpeed = 5;
      if (maxParallax5 < 0) maxParallax5 = 0;
    };
    $('#container-svg').mousemove(function(event) {
      var offset = $("#container-svg").offset();
      var posX = event.pageX - offset.left;
      var scrollTemp = (posX / mainContainerWidthHalf - 1);
      if(Math.abs(scrollTemp)>0.3){
        scrollSpeed = Math.round((posX / mainContainerWidthHalf - 1) * 35) / 10;
      } else{
        scrollSpeed = 0;
      }
    });
    var interval1 = null;
    var pointsInRoad0 = "975,-10 ";
    var ptsAdjustable1 = 904;
    var pointsInRoad1 = ",71 -13,71 -13,195 2098,195 2098,71 "
    var ptsAdjustable2 = 1188;
    var pointsInRoad3 = ",71 1109,-12";
    $('#container-svg').hover(function() {
      interval1 = setInterval(function() {
        scroller('.parallax-layer-4', 3, maxParallax4, 1);
        scroller('.parallax-layer-3', 2, maxParallax3, 2);
        scroller('.parallax-layer-2', 1, maxParallax2, 3);
        scroller('.parallax-layer-1', 0, maxParallax1, 4);
        var roadPoints = '';
        if (offsetArr[0] > 0) {
          ptsAdjustable1 = 904 + (offsetArr[0] / 20) * 2048 / containerWidth;
          ptsAdjustable2 = 1188 + (offsetArr[0] / 20) * 2048 / containerWidth;
        } else {
          ptsAdjustable1 = 904 + (offsetArr[0] / 20) * 2048 / containerWidth;
          ptsAdjustable2 = 1188 + (offsetArr[0] / 20) * 2048 / containerWidth;
        }
        roadPoints = pointsInRoad0 + ptsAdjustable1 + pointsInRoad1 + ptsAdjustable2 + pointsInRoad3;
        roadPoly.attr('points', roadPoints);
      }, 17);
    }, function() {
      clearInterval(interval1);
    });

    function scroller(objectName, arrIndex, maxOffset, scrollFactor) {
      offsetArr[arrIndex] += scrollConstant * scrollSpeed * scrollFactor;
      if (offsetArr[arrIndex] > maxOffset) {
        offsetArr[arrIndex] = maxOffset;
      } else if (offsetArr[arrIndex] < -maxOffset) {
        offsetArr[arrIndex] = -maxOffset;
      } else {
        $(objectName).css('transform', 'translateX(' + (offsetArr[arrIndex]) + 'px)');
      }
    }
    var timeout_variable, timeout_variable2, timeout_variable3 = null;
		var youtubeVideoURL = "https://www.youtube.com/embed/bu9qVmwFL68";


		$('#bus-container').click(function(){
			$('#wheel-back, #wheel-front').addClass('wheel-anim');
			$('#bus-container').addClass('bus-anim');
      timeout_variable2 = setTimeout(function(){
        $('#wheel-back, #wheel-front').removeClass('wheel-anim');
        $('#bus-container').removeClass('bus-anim');
        clearTimeout(timeout_variable2);
      }, 10500);
		});

    var videoPlayingFlag = false;
		$('#main-container').on('click', '#school-flag', function(){
      // to stop another flag and sound
      createjs.Sound.stop();
      $('#flag-svg').attr('src', preload.getResult("flag").src);
      videoPlayingFlag = true;
			$('#video-drop-down').css({
				'top': '0%',
				'transition': 'top 1.5s cubic-bezier(.46,.82,.68,1.18)'
			});
      $('#main-container>*:not(#video-drop-down)').addClass('disable-mouse');
      timeout_variable = setTimeout(function(){
        $('#home-video-container>iframe').attr('src', youtubeVideoURL+'?autoplay=1');
      }, 1000);
      $('#flag-svg').removeClass('blink-click-animation');
      $('#school-flag').removeClass('blink-click-animation');
		});
    function closeVideo(){
      videoPlayingFlag = false;
      $('#home-video-container>iframe').attr('src', youtubeVideoURL);
      $('#video-drop-down').css({
        'top': '-100%',
        'transition': 'top 1.5s cubic-bezier(.39,-0.14,.78,.42)'
      });
      //to close flag as well
      $('#flag-svg').addClass('blink-click-animation');
      $('#school-flag').addClass('blink-click-animation');
      timeout_variable = setTimeout(function(){
        $('#main-container>*:not(#video-drop-down)').removeClass('disable-mouse');
      }, 1000);
    }
		$('#video-close-button, #container-svg:not(#notice-board-container)').click(function(){
      closeVideo();
		});
    //to trigger click
    setTimeout(function(){
      $('#school-flag').trigger('click');
    }, 1500);
    /**  slider portion starts  */
    $('.cloud-wrapper').hover(function(){
      $(this).addClass('jello animated');
    }, function(){
        $(this).removeClass('jello animated');
    })

    $('#school-flag').hover(function(){
      $('#school-flag').attr('src', preload.getResult("school-flag-gif").src);
    }, function(){
      $('#school-flag').attr('src', preload.getResult("school-flag").src);
    });
    //carousel controls
    $('.myCarousel-nav-left').click(function(){
      var navIndex = 0;
      var nextIndex = 1;
      var totalNum = $('.carousel-item').length;
      for(var i=0; i<totalNum; i++){
        if($('.carousel-item').eq(i).hasClass('active')){
          navIndex = i;
        }
      }
      nextIndex = navIndex+1;
      if(nextIndex == totalNum){
        nextIndex = 0;
      }
      $('.carousel-item').eq(navIndex).removeClass('active');
      $('.carousel-item').eq(nextIndex).addClass('active');
    });
    $('.myCarousel-nav-prev').click(function(){
      var navIndex = 0;
      var prevIndex = 1;
      var totalNum = $('.carousel-item').length;
      for(var i=0; i<totalNum; i++){
        if($('.carousel-item').eq(i).hasClass('active')){
          navIndex = i;
        }
      }
      prevIndex = navIndex-1;
      if(prevIndex < 0){
        prevIndex = totalNum-1;
      }
      $('.carousel-item').eq(navIndex).removeClass('active');
      $('.carousel-item').eq(prevIndex).addClass('active');
    });

    resizeSliderWindow();
    $(window).on('resize', function() {
      resizeSliderWindow();
    });

    function resizeSliderWindow() {
      $('#my-slider-section').height($('#my-slider-section').width()*9/16);
      $('#table-chair-layer').height($('#table-chair-layer').width() * 0.15);
      $('.transition-layer').height($('.transition-layer').width() * 0.025);
      $('.transition-parallax-slider').height($('.transition-parallax-slider').width() * 0.075);
      $('#feature-section-top-padding').height($('#feature-section-top-padding').width() * 0.1);
      //about us text
      $('.about-us-text').css({
        top: $('.hidden-text-for-size').position().top,
        left: $('.hidden-text-for-size').position().left,
        width: $('.hidden-text-for-size').width(),
        height: $('.hidden-text-for-size').height(),
      });
    }
    // $('.parallax-layer-1')
    $(window).scroll(function() {
      var parallaxSectionTop = $('#main-container').offset().top,
        parallaxSectionHeight = $('#main-container').height(),
        sliderTop = $('#my-slider-section').offset().top,
        sliderHeight = $('#my-slider-section').height(),
        tableChairTop = $('#about-us-section').offset().top,
        tableChairHeight = $('#about-us-section').height(),
        featureTop = $('#feature-section').offset().top,
        aboutTop = $('#about-us-section').offset().top,
        windowTop = $(this).scrollTop(),
        windowHeight = window.innerHeight;
      if (windowTop > (parallaxSectionTop+parallaxSectionHeight)) {
        closeVideo();
      }
      if (windowTop < (sliderTop+sliderHeight) && windowTop > sliderTop) {
        var scrollPosition = ((windowTop-sliderTop)/sliderHeight)*100;
        $('.slider-parallax-layer-3').css('transform', 'translateY(-'+ 0.1*scrollPosition +'%) translateX(-50%)');
        $('.slider-parallax-layer-2').css('transform', 'translateY(-'+ 0.2*scrollPosition +'%) translateX(-50%)');
      }
      if (windowTop < (tableChairTop+tableChairHeight) && windowTop > tableChairTop) {
        var scrollPosition = ((windowTop-tableChairTop)/tableChairHeight)*100;
        $('.parallax-chair').css('transform', 'translateY(-'+ 0.3*scrollPosition +'%)');
        $('.parallax-table').css('transform', 'translateY(-'+ 0.5*scrollPosition +'%)');
      }
      if(!hasAboutAnimated){
        if(windowTop+windowHeight > aboutTop){
          typeWriter();
          hasAboutAnimated = true;
        }
      }
      if(!hasFeatureAnimated){
        if(windowTop+(0.8*windowHeight) > featureTop){
          $('.drop-animation-1, .drop-animation-2, .drop-animation-3').show(0);
          hasAboutAnimated = true;
        }
      }
    });

    //for typing effect
    var typingEffectCounter = 0;
    var currentText = '';
    var fullText =  $('.hidden-text-for-size').html();
    function typeWriter() {
      if (typingEffectCounter < fullText.length) {
        currentText += fullText.charAt(typingEffectCounter);
        $('.about-us-text').html(currentText);
        typingEffectCounter++;
        setTimeout(typeWriter, 50);
      }
    }
    //for animation on scroll
    var hasFeatureAnimated = false;
    var hasAboutAnimated = false;
    $('.drop-animation-1, .drop-animation-2, .drop-animation-3').hide(0);

    if(window.location.hash == '#about-us-section' || window.location.hash == '#feature-section')
      return;
    $(document).scrollTop(0);
  }
});
