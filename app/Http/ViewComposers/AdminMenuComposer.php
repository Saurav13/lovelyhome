<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Feedback;
use App\ContactUsMessage;
use App\Alumni;
use Auth;

class AdminMenuComposer
{
    /**
     * Create a movie composer.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $mno = 0;
        $fno = 0;
        $ano = 0;

        if (Auth::user()->roles()->where('title', '=', 'Contact Us Messages')->exists())
            $mno = ContactUsMessage::where('seen','0')->count();
        if (Auth::user()->roles()->where('title', '=', 'Feedback Messages')->exists())
            $fno = Feedback::where('seen','0')->count();
        if (Auth::user()->roles()->where('title', '=', 'Alumni Requests')->exists())
            $ano = Alumni::where('verified','0')->count();
        $view->with('fno',$fno)->with('mno',$mno)->with('ano',$ano);
    }
}