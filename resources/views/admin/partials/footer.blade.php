
 <footer class="footer footer-static footer-light navbar-border">
   
      <p class="clearfix text-muted text-sm-center mb-0 px-2"><span class="float-md-left d-xs-block d-md-inline-block">Copyright  &copy; 2017 <a href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent" target="_blank" class="text-bold-800 grey darken-2">PIXINVENT </a>, All rights reserved. </span><span class="float-md-right d-xs-block d-md-inline-block">Hand-crafted & Made with <i class="icon-heart5 pink"></i></span></p>
    </footer>

    <!-- BEGIN VENDOR JS-->
    <script src="{{asset('backend/app-assets/js/core/libraries/jquery.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend/app-assets/vendors/js/ui/tether.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend/app-assets/js/core/libraries/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend/app-assets/vendors/js/ui/perfect-scrollbar.jquery.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend/app-assets/vendors/js/ui/unison.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend/app-assets/vendors/js/ui/blockUI.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend/app-assets/vendors/js/ui/jquery.matchHeight-min.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend/app-assets/vendors/js/ui/screenfull.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend/app-assets/vendors/js/extensions/pace.min.js')}}" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="{{asset('backend/app-assets/vendors/js/charts/chart.min.js')}}" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN ROBUST JS-->
    <script src="{{asset('backend/app-assets/js/core/app-menu.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend/app-assets/js/core/app.js')}}" type="text/javascript"></script>
    <!-- END ROBUST JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="{{asset('backend/app-assets/js/scripts/pages/dashboard-lite.js')}}" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->
  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.1.0/sweetalert2.all.js"></script>
    <script type="text/javascript">
  document.onreadystatechange = function () {
    var state = document.readyState
    if (state == 'interactive') {
        document.getElementById('contents').style.visibility="hidden";
    } else if (state == 'complete') {
      Pace.stop();
      
      setTimeout(function(){
          document.getElementById('interactive');
          document.getElementById('load').style.visibility="hidden";
          document.getElementById('contents').style.visibility="visible";
      },1000);
    }
  }
</script>
  <script>

    function getUnseenMsgCount(){
      Pace.ignore(function(){ 
        $.get("{{ route('getUnseenMsgCount') }}",
          function(data,status){
              $("#mno1" ).text(data.mno);  
          }
        );
      });
    }

    function getUnseenAlReqCount(){
      Pace.ignore(function(){ 
        $.get("{{ route('getUnseenAlReqCount') }}",
          function(data,status){
              $("#ano1" ).text(data.ano);  
          }
        );
      });
    }

    function getUnseenFeedbackCount(){
      Pace.ignore(function(){ 
        $.get("{{ route('getUnseenFeedbackCount') }}",
          function(data,status){
              $("#fno1" ).text(data.fno);  
          }
        );
      });
    }

    setInterval(function(){
        if("{{Auth::user()->roles()->where('title', '=', 'Contact Us Messages')->exists()}}"=='1')
          getUnseenMsgCount();
        if("{{Auth::user()->roles()->where('title', '=', 'Feedback Messages')->exists()}}"=='1')
          getUnseenFeedbackCount();
        if("{{Auth::user()->roles()->where('title', '=', 'Alumni Requests')->exists()}}"=='1')
          getUnseenAlReqCount();
        
    },60000);

    function getUnseenMsg(){
      $('#Mnotis').append('<div class="text-xs-center" style="margin:20px"><img src="{{ asset("loading1.gif") }}"/></div>');        
      $.get("{{ route('getUnseenMsg') }}",
        function(data,status){
            $("#mno1" ).text(data.mno);
            $("#mno2" ).text(data.mno+" New");

            $('#Mnotis').empty();
            $('#Mnotis').append(data.html);
            $('#Mnotis').perfectScrollbar('update');
        }
      ).fail(function() {
        location.reload();
      });
    }

    function getUnseenFeedback(){
      $('#Fnotis').append('<div class="text-xs-center" style="margin:20px"><img src="{{ asset("loading1.gif") }}"/></div>');        
      $.get("{{ route('getUnseenFeedbackMsg') }}",
        function(data,status){
            $("#fno1" ).text(data.fno);
            $("#fno2" ).text(data.fno+" New");

            $('#Fnotis').empty();
            $('#Fnotis').append(data.html);
            $('#Fnotis').perfectScrollbar('update');
        }
      ).fail(function() {
        location.reload();
      });
    }

    function getUnseenAlReq(){
      $('#Anotis').append('<div class="text-xs-center" style="margin:20px"><img src="{{ asset("loading1.gif") }}"/></div>');        
      $.get("{{ route('getUnseenAlReq') }}",
        function(data,status){
            $("#ano1" ).text(data.ano);
            $("#ano2" ).text(data.ano+" New");

            $('#Anotis').empty();
            $('#Anotis').append(data.html);
            $('#Anotis').perfectScrollbar('update');
        }
      ).fail(function() {
        location.reload();
      });
    }

    
  </script>

     @yield('js');
     
      
  </body>
</html>