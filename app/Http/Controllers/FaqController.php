<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Faq;
use Session;
use Auth;

class FaqController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            
            if (Auth::user()->roles()->where('title', '=', 'FAQs')->exists()){
                return $next($request);
            }
            else
                abort(403);
        });
    }

    public function index()
    {
        $faqs=Faq::all();
        return view('admin.faqs.index')->with('faqs',$faqs);
    }
    public function store(Request $request)
    {
        $this->validate($request, array(
            'question'=>'required',
            'answer'=>'required', 
            ));
        $faqs=New Faq;
        $faqs->question=$request->question;
        $faqs->answer=$request->answer;
        $faqs->save();
        Session::flash('success','FAQ was added!');
        return redirect()->route('admin.faqs');
        
    }
    public function update(Request $request)
    {
        $this->validate($request,array(
            'question'=>'required',
            'answer'=>'required', 
        ));
        $faqs=Faq::find($request->id);
        $faqs->question=$request->question;
        $faqs->answer=$request->answer;
        $faqs->save();
        Session::flash('success','FAQ was updated');
        return redirect()->route('admin.faqs');
    }
    public function destroy(Request $request)
    {
        $faqs=Faq::findorFail($request->id);
        $faqs->delete();
        Session::flash('success','FAQ was deleted successfully!');
        return redirect()->route('admin.faqs');
    }
}
