<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $fillable=['name','category'];
    
    public function images()
    {
    	return $this->hasMany(Image::class);
    }
}
