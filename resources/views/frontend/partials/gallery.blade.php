@foreach($albums as $album)
    <div class="{{ $category }} portfolio_grid_item col-portfolio col-xs-12 col-sm-6 col-md-4 col-lg-3 wow fadeInUp">
        @if($category != 'Videos')
            <figure class="hover-line">
                <div class="image-holder" ><img alt="{{ $album->name }}" src="{{ route('asset', ['gallery_images',$album->images->first()->name,726,408]) }}" ></div>
                <figcaption>
                    <div class="portfolio-lead">
                        <div class="gallery_description">
                            <p><span><a href="{{ route('frontend.album',[$album->category, $album->slug])}}">{{ $album->name }}</a></span></p>
                        </div>
                    </div>
                </figcaption>
            </figure>
        @else
            <figure class="hover-line">
                <div class="image-holder">
                    <video style="height:217px;width:325px">
                        <source src="/gallery_images/{{ $album->images->first()->name }}" >
                    </video>
                </div>
                <figcaption>
                    <div class="portfolio-lead">
                        <div class="gallery_full_screen">
                            <a href="{{ route('frontend.album',[$album->category, $album->slug])}}"><img src="/frontend-assets/img/icons/video_gallery.png" alt=""></a>
                        </div>
                        <div class="gallery_description">
                            <p><span><span><a href="{{ route('frontend.album',[$album->category, $album->slug])}}">{{ $album->name }}</a></span></p>
                        </div>
                    </div>
                </figcaption>
            </figure>
        @endif
    </div>
@endforeach