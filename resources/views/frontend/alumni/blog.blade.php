@extends('layouts.app')

@section('body')
@section('title','| Alumini Blog')

<style>
    alumini_tabs .nav-tabs {
        border: none;
    }

    @media(min-width: 300px)
    {
        .alumini_tabs ul li a {
            padding: 8px 0;
            font-size: 1rem;
            border-radius: 50px;
            font-family: 'OpenSansSemiBold', sans-serif;
        }
    }
    @media(min-width: 600px)
    {
        .alumini_tabs ul li a {
            padding: 8px 0;
            font-size: 1.5rem;
            border-radius: 50px;
            font-family: 'OpenSansSemiBold', sans-serif;
        }
    }
    @media (min-width: 1200px)
    {
        .alumini_tabs ul li a {
            padding: 8px 0;
            font-size: 2rem;
            border-radius: 50px;
            font-family: 'OpenSansSemiBold', sans-serif;
        }
    }
    .alumini_tabs ul {
        position: relative;
        list-style-type: none;
        margin: 0;
        text-align: center;
        padding: 0;
    }
    .alumini_tabs ul li {
        display: inline-block;
        float: none;
        text-transform: uppercase;
        width: 23%;
        margin: 30px 7px;
    }
    .alumini_tabs ul li a {
    
    }

    .alumini_tabs ul li .tab_red {
        border: 2px solid #b70404;
        color: #b70404;
        -webkit-transition: all .2s linear;
        transition: all .2s linear;
    }

    .alumini_tabs ul li .tab_yel {
        border: 2px solid #fff400;
        color: #fff400;
        -webkit-transition: all .2s linear;
        transition: all .2s linear;
    }

    .alumini_tabs ul li .tab_green {
        border: 2px solid #49b450;
        color: #49b450;
        -webkit-transition: all .2s linear;
        transition: all .2s linear;
    }


    .nav-tabs>li>.tab_yel:hover, .ui_hover .nav-tabs>li>.tab_green {
        background: #fff400;
        color: #fff;
        border: 2px solid #fff400;
        -webkit-background-clip: padding-box;
        background-clip: padding-box;
    }
    .nav-tabs>li.active>.tab_yel, .nav-tabs>li.active>.tab_yel:focus, .nav-tabs>li.active>.tab_yel:hover {
        background: #fff400;
        color: #fff;
        border: 2px solid #fff400;
    }
    .post_img{
        height: 3.5rem;
    }

    .post_img img{
        height: inherit;
        border-radius: 50px;
    }
    .blog_link .buttonred {
        background-color: #b70404c7;
        border-color: #b70404c7;
    }
    .blog_link .buttonred a:hover {
        background-color: #fff;
        color: #b70404c7;
    }
    .blog_link .buttonred a {
        -webkit-transition: all .2s linear;
        transition: all .2s linear;
        color: #fff;
        font-size: 2rem;
        font-family: 'OpenSansSemiBold', sans-serif;
    }

    .blog_link .buttonyellow {
        background-color: #fff400c7;
        border-color: #fff400c7;
    }
    .blog_link .buttonyellow a:hover {
        background-color: #fff;
        color: #fff400c7;
    }
    .blog_link .buttonyellow a {
        -webkit-transition: all .2s linear;
        transition: all .2s linear;
        color: #fff;
        font-size: 2rem;
        font-family: 'OpenSansSemiBold', sans-serif;
    }
    #alumni_register_form .has-error, #alumni_login_form .has-error{
        border: 3px solid #b70404c7;
    }
</style>
<section id="blog_page" class="blog_page">
    <div class="container">
        <div class="row pb_20">
            <div class="col-md-1"></div>
            <div class="col-md-10 dot">
                <h2 class="section-title">Welcome Back!!!</h2>
                <div class="alumini_tabs">
                    <ul class="nav nav-tabs">
                        <li><a class="tab_red acttabfont" href="{{ route('alumni.gallery') }}">Gallery</a></li>
                        <li class="active"><a class="tab_green acttabfont" href="#blog">Blog</a></li>
                        @if(Auth::guard('alumni')->check())
                            <li><a class="tab_yel acttabfont" href="{{ route('alumni.myposts') }}">My posts</a></li>
                        @endif
                    </ul>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
        
            <div class="tab-pane card-container active" style="width:100%;" id="blog">
                @if(Auth::guard('alumni')->guest())
                    <section id="authorization_page" style="padding: 50px 0 100px 0;" class="authorization_page">
                        
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10 dot">
                                <h2 class="section-title">Sign up for alumini</h2>
                                <p class="section-description">Already have an account? - <a href="#" data-toggle="modal" data-target="#loginModal">Log In</a></p>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                        <div class="text-center">
                            @if (Session::has('notverified'))
                                <span class="help-block" style="text-align:  center;font-size: 15px;">
                                    <strong>{{ Session::get('notverified') }}</strong>
                                </span>
                            @endif

                            <form id="alumni_register_form" method="POST" action="{{ route('alumni_register') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="reg_box">
                                    <input class="{{ $errors->has('name') ? 'has-error' : '' }}" id="reg_name" name="name" type="text" placeholder="Enter Your Name"  value="{{ old('name') }}" required="required">
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong style="color:  #b70404c7;">{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif

                                    <input class="{{ $errors->has('remail') ? 'has-error' : '' }}" id="reg_email" name="remail" type="email" placeholder="Enter Your email"  value="{{ old('remail') }}" required="required">
                                    @if ($errors->has('remail'))
                                        <span class="help-block">
                                            <strong style="color:  #b70404c7;">{{ $errors->first('remail') }}</strong>
                                        </span>
                                    @endif

                                    <input class="{{ $errors->has('batch') ? 'has-error' : '' }}" id="reg_batch" name="batch" type="text" placeholder="Enter Your Batch"  value="{{ old('batch') }}" required="required">
                                    @if ($errors->has('batch'))
                                        <span class="help-block">
                                            <strong style="color:  #b70404c7;">{{ $errors->first('batch') }}</strong>
                                        </span>
                                    @endif

                                    <input class="reg_pass {{ $errors->has('rpassword') ? 'has-error' : '' }}" id="reg_pass" name="rpassword" type="password" placeholder="********"  value="{{ old('rpassword') }}" required="required">
                                    @if ($errors->has('rpassword'))
                                        <span class="help-block">
                                            <strong style="color:  #b70404c7;">{{ $errors->first('rpassword') }}</strong>
                                        </span>
                                    @endif

                                    <input class="rep_pass" id="rep_pass" name="rpassword_confirmation" type="password" placeholder="Repeat password" required="required">
                                    
                                    <p style="padding-bottom:0px;margin-top: 15px;margin-bottom: -15px;font-family: 'Boogaloo', cursive;">Upload Your Image</p>
                                    <input class="{{ $errors->has('photo') ? 'has-error' : '' }}" style="padding-top: 10px;" id="reg_photo" name="photo" type="file" placeholder="Upload Your Photo" accept="image/*" required="required">
                                    @if ($errors->has('photo'))
                                        <span class="help-block">
                                            <strong style="color:  #b70404c7;">{{ $errors->first('photo') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <button class="submit" type="submit">Register</button>
                            </form>
                        </div>
                    </section>
                @else
            
                <div class="row">
                    <div class="col-md-10" style="margin-left:8.3%">
                        <div id="PostsDiv">
                            @include('frontend.alumni.partials.post')
                        </div>
                        
                        <div class="blog_nav" style="text-align:center">
                            <img id="loader" hidden src="{{ asset('ajax-loader.gif') }}"/>
                        </div>
                    </div>
                </div>
                @endif
            </div>
       
    </div>
    @if(Auth::guard('alumni')->guest())
        <div id="loginModal" class="modal fade in"  aria-hidden="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    
                    <div id="modalBody" class="modal-body" style="font-size:  15px;">
                        <section style="padding:50px 0 100px 0" class="authorization_page">
                            
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-10 dot">
                                    <h2 class="section-title">Log In</h2>
                                </div>
                                <div class="col-md-1"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                        
                                    @if ($errors->loginErrors->has('notverified'))
                                        <span class="help-block" style="text-align:  center;">
                                            <strong style="color:  #b70404c7;">{{ $errors->loginErrors->first('notverified') }}</strong>
                                        </span>
                                    @endif
                                    <form id="alumni_login_form" method="POST" action="{{ route('alumni_login') }}">
                                        {{ csrf_field() }}
                                        <div class="reg_box">
                                            <input class="{{ $errors->loginErrors->has('email') ? 'has-error' : '' }}" id="login_email" name="email" type="email" placeholder="Enter Your email" required="required">
                                            @if ($errors->loginErrors->has('email'))
                                                <span class="help-block">
                                                    <strong style="color:  #b70404c7;">{{ $errors->loginErrors->first('email') }}</strong>
                                                </span>
                                            @endif

                                            <input class="reg_pass{{ $errors->loginErrors->has('password') ? ' has-error' : '' }}" id="login_pass" name="password" type="password" placeholder="********" required="required">
                                            @if ($errors->loginErrors->has('password'))
                                                <span class="help-block">
                                                    <strong style="color:  #b70404c7;">{{ $errors->loginErrors->first('password') }}</strong>
                                                </span>
                                            @endif

                                        </div>
                                        <div class="reg_checkbox">
                                            <input type="checkbox" class="checkbox" id="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}/>
                                            <label for="checkbox"></label>
                                            <span>Remember Me</span>
                                        </div>
                                        <button class="submit" type="submit">Log In</button>
                                        <div class="forgot_pass">
                                            <a href="{{ route('alumni_password.request') }}">Forgot password?</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    @endif
</section>

@stop

@section('js')
    <script>
        
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
    
                reader.onload = function (e) {
                   
                    $('#uploadimg')
                    .removeAttr('hidden')
                        .attr('src', e.target.result);
                };
    
                reader.readAsDataURL(input.files[0]);
            }
        }
    
        $(document).ready(function(){

            var ajaxCall = false;
            var lastId = '{{ $posts->last() ? $posts->last()->id : '' }}';

            $(window).scroll(function() {
                
                if($(window).scrollTop() > $(window).height()+$('#PostsDiv').height() && !ajaxCall && lastId != '') {
                    ajaxCall = true;
                    loadMoreData();
                }
            });

            function loadMoreData(){

                $.ajax({ 
                    type: 'GET', 
                    url: "{{ route('alumni.blog.getmoreposts') }}",
                    dataType: 'json', 
                    data: {lastId : lastId}, 
                    beforeSend: function(data) {
                        $('#loader').removeAttr('hidden');
                    },
                    success: function(data){
                        lastId = data.last;
                        $('#PostsDiv').append(data.html);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        // location.reload();
                    },
                    complete: function(data) {
                        ajaxCall = false;
                        $('#loader').attr('hidden','hidden');
                    }         
                });
            }
            
            if({{ count($errors->loginErrors)}} > 0){
                $('#loginModal').modal('show');
            }
        });
    </script>
@endsection