<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Resources extends Model
{
    protected $fillable=['title','detail','status'];
    public function materials()
    {
    	return $this->hasMany(Materials::class);
    }
    use Sluggable;
    protected $table= 'resources';
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    // public function products()
    // {
    // 	return $this->hasMany(Product::class);
    // }
    // public function publish(Product $product)
    // {
    //     $this -> products() ->save($product);
    // }
}
       