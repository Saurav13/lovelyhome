@extends('admin.main')
@section('title','| Resources')
@section('body')
<div class="app-content content container-fluid">
    <div class="content-wrapper">        
      <div class="content-body">
          <!-- Basic example section start -->
          <section id="addresources">  
            <div class="row">
              <div class="col-md-12">
                  <div class="card">
                    <div class="card-header">
                        <div class="card-title" ><a data-action="collapse"><h3>RESOURCES</h3><h6>Add, Edit or Remove Resources</h6>  </a></div>
                        <a class="heading-elements-toggle">
                          <i class="icon-ellipsis font-medium-3"></i>
                        </a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body"> <!-- // collapse -->
                      <div class="card-block">
                        <div class="row">
                          <div class="col-md-12">
                              @if(count($resources)==null)
                              <h6>No Resource category added yet procced by adding Resource category first and then you will be able to add new Materials</h6>
                              @endif
                            @if(count($resources)!=null)
                            <div class="table-responsive " style= "
                            height: 200px;
                            overflow: auto; " >
                                
                              <table class="table table-bordered">
                                <thead>
                                <tr>
                                  <th style="width: 40%">Resource</th>
                                  <th style="width: 20%">Edit</th>
                                  <th style="width: 20%">Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($resources as $resource)
                                  <tr>
                                    <td class="text-middle">
                                    <h4  style="color: inherit; font-family: Atma,cursive">{{$resource->title}} </h4>
                                    <div class="row">
                                    <div id="ResupdateCancel{{$resource->id}}" hidden>
                                      <form class="form" method="POST" action="{{URL::to('/admin/resources/update')}}" >
                                          <div class="form-body">
                                            <div class="form-group col-sm-6">
                                              {{csrf_field()}}
                                              <input type="text" hidden name="id" value="{{$resource->id}}">

                                              <input type="text" id="Restitle" class="form-control" placeholder="Add New Name" name="title" required> 

                                            <button type="submit" class="btn btn-blue" id="Resupdate{{$resource->id}}"> 
                                              <i class="icon-check2">Update</i> </button>

                                              <button class="btn btn-red" type="button" id="Rescancel{{$resource->id}}">
                                              <i class="icon-check2">Cancel</i>
                                              </button>          
                                          </div>
                                          </div> 
                                      </form>
                                    </div>
                                  </div>
                                    </td>
                                  
                                  <td class="text-middle" id="Resedit{{$resource->id}}">
                                    <a href="javascript:void(0)"  class="btn btn-outline-blue " style=""> <i class="icon-edit"></i></a>
                                    </td>
                                    <td class="text-middle">
                                    <a href="javascript:void(0)" id="Resdelete{{$resource->id}}" class="btn btn-outline-red"> <i class="icon-remove"></i></a>
                                    </td>
                                  </tr>

                                @endforeach

                                </tbody>
                              </table>
                            </div>
                            @endif
                            <div>
                              <div style="padding-top:20px;">
                                <form class="form" method="post" action="resources">
                                    {{csrf_field()}}
                                    <div class="row">
                                      <div class="form-body">
                                        <div class="form-group col-sm-3">
                                          <input type="text" id="Restitle" class="form-control" placeholder="Add New Resource" name="title" required>
                                        </div>

                                        <div class="form-group  col-sm-3">
                                          <button type="submit" class="btn btn-primary" id="Resadd">
                                            <i class="icon-check2">Add Resource</i> 
                                        </button>
                                        </div>  
                                      </div>
                                    </div>
                                </form>
                              </div>
                            </div>
                          </div>

                        </div>

                      </div>
                    </div>
                  </div>
              </div>
            </div>
          </section>



<!-- add materials section -->
@if(count($resources)!=null)
  <section id="addmaterials">  
  <div class="row">
    <div class="col-md-12">
      <div class="card">
      <div class="card-header">
        <div class="card-title" >
          <a href="/admin/materials/create" class="btn btn-success" id="MatAdd">
            <i class="icon-plus4"></i> Add New Material 
          </a>
          
        </div>
        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
        <div class="heading-elements">
          <ul class="list-inline mb-0">
              <li><a data-action="expand"><i class="icon-expand2"></i></a></li>

          </ul>
        </div>
      </div>
      @if(count($materials)==null)
        <div class="card-body">  <!-- //collapse -->
          <div class="card-block">
            <p>No materials added yet. Proceed by pressing the <strong>Add New Material</strong> button.</p>
          </div>
        </div>
      @endif
      @if(count($materials)!=null)
          <div class="card-body">  <!-- //collapse -->
            <div class="card-block">
              <div class="row">
                
                  <div class="table-responsive fixed">
                        <table class="table table-bordered  table-striped
                        table-layout: fixed; width: 100px;">
                      
                          <thead>
                            <tr>
                              <th style="width: 15%">Material Name</th>
                              <th style="width: 40%">Detail</th>
                              <th style="width: 15%">Resource Category</th>
                              <th style="width: 20%">Actions</th>
                            </tr>
                          </thead>
                          <tbody>

                            <div class="row col-sm-12">

                              @foreach($materials as $material)
                                    <tr>
                                      

                                      <td>
                                          <h4  style="color: inherit; font-family: Atma,cursive">
                                            <div id="matlesstitle{{$material->id}}">
                                            
                                          
                                              <p class="card-text">

                                                {!!$material->title!!}
                                              </p>
                                            </div>
                                          </h4>

                                      
                                      </td>

                                      <td id="Matdetail{{$material->id}}"> 
                                        <div id="matless{{$material->id}}">
                                          <p class="card-text"> {{str_limit(strip_tags($material->detail),150)}}<!-- <a href="javascript:void(0)" id="viewMoreMatDetail{{$material->id}}">
                                            ....View More    
                                            </a></p> -->
                                            </div>
                                          <div id="matmore{{$material->id}}" hidden>
                                          <p class="card-text"> {!! $material->detail !!}
                                            </p>
                                          </div>

                                      </td>

                                      <td>
                                      {!! $material->Rtitle !!}
                                      </td>


                                      <td>
                                        <div class="form-group row">
                                        
                                          <a href="/admin/materials/{{$material->id}}/edit" id="Matedit{{$material->id}}" class="btn mr-1 mb-1 btn-sm btn-primary">  <i class="fa fa-edit">
                                          </i>
                                          </a>

                                          <a href="javascript:void(0)" id="Matdelete{{$material->id}} btn-red" class="btn mr-1 mb-1 btn-sm btn-danger">
                                          <i class="fa fa-trash" >  </i></a>
                                          
                                          
                                          <a href="/admin/materials/{{$material->id}}/matshow" class="btn btn-info mr-1 mb-1 btn-sm "  id="MatView{{$material->id}}">View</a>
                                          
                                        </div> 
                                      </td>
                                    
                                    </tr>

                              @endforeach
                            </div>

                      </tbody>
                    </table>
                  </div>

              
            
              
          

          </div>

        </div>
      </div>
      @endif

        </div>
    </div>

</div>
</section>
@endif
</div>
</div>
</div>


@section('js')

<script type="text/javascript">
  $(document).ready(function() {
        $("[id*='viewMoreMatTitle']").click(function(){
          var id = $(this).attr("id").slice(16);
          $("#matlesstitle"+id).attr("hidden","true");
          $("#matmoretitle"+id).removeAttr("hidden");
        });

        $("[id*='viewMoreMatDetail']").click(function(){
          //console.log($(this).attr("id"));
           //alert('baam');
            var id = $(this).attr("id").slice(17);
            
            $("#matmore"+id).removeAttr("hidden");
            console.log(id);
            $("#matless"+id).attr("hidden","true");
          });
  

    $("[id*='Resedit']").click(function(){
           //alert('baam');
            var id = $(this).attr("id").slice(7);
            console.log(id);
            //$("#updateCancel"+id).attr("hidden","true");
            $("#ResupdateCancel"+id).removeAttr("hidden");
          });

          $("[id*='Rescancel']").click(function(){
           //alert('baam');
            var id = $(this).attr("id").slice(9);
            console.log(id);
            //$("#updateCancel"+id).attr("hidden","true");
            $("#ResupdateCancel"+id).attr("hidden","true");
          });
        
          $("[id*='Matstatus'").click(function(){
          var id = $(this).attr("id").slice(9);
          //jquery bata ajax 
          $.post("{{URL::to('admin/materials/matchangeStatus')}}",{id:id,_token:"{{csrf_token()}}"},function(data){ 
            console.log(id);
          swal({
            title:"Material Status Changed Successfully",
            type:"success"
          }).then(function(){
            window.location.reload();
          })
        })
      
          });




       
        $("[id*='Resdelete']").click(function(){
          var id = $(this).attr("id").slice(9);
          swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
            if (result.value) {
              $.post("{{URL::to('admin/resources/delete')}}",{id:id,_token:"{{csrf_token()}}"},function(data){  
                  swal({
                    title:"Deleted Successfully",
                    type:"success"
                  }).then(function(){
                    window.location.reload();
                  })
        })
            }
            })

        });


       

         
        

        $("[id*='Matdelete']").click(function(){
          var id = $(this).attr("id").slice(9);
          swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
            if (result.value) {
              $.post("{{URL::to('admin/materials/delete')}}",{id:id,_token:"{{csrf_token()}}"},function(data){  
                  swal({
                    title:"Deleted Successfully",
                    type:"success"
                  }).then(function(){
                    window.location.reload();
                  })
        })
            }
            })

        });


    });
</script>

@endsection
@endsection