@extends('admin.main')
@section('title','| Alumni Photos')
@section('body')
 <div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-xs-12 mb-1">
                <h2 class="content-header-title">Alumni Photos</h2>
            </div>
        </div>
        <section>  
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" ><a data-action="collapse"><button  class="btn btn-md btn-primary"> Add Alumni Photo </button></a></h4>
                            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="{{ count($errors)>0 ? 'icon-minus4':'icon-plus4' }}"></i></a></li>
                                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        
                        @if (Session::has('message'))
                            <div class="alert alert-info alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {{ Session::get('message') }}
                            </div>
                        @endif
             
                        <div class="card-body collapse {{ count($errors)>0 ? 'in':'' }}">                    
                            <div class="card-block ">
                                <form class="form" method="POST" action="{{route('alumni_photos.store')}}" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="row">
                                        <div class="form-body">
                                            <div class="form-group col-sm-12">
                                                <label for="name">Name</label>
                                                <input type="text" id="name" class="form-control" placeholder="example - John Doe" name="name" value="{{old('name')}}" required>
                                            </div>
            
                                            <div class="form-group col-sm-12">
                                                <label for="designation">Batch</label>
                                                <input type="text" id="designation" class="form-control" placeholder="2018" name="batch" value="{{old('batch')}}" required>
                                            </div> 
            
                                            <div  class="form-group col-sm-12">
                                                <label for="image">Select Image</label>
                                                <input type="file" name="photo" class="form-control-file" required>
                                            </div>
        
                                            <div class="form-group col-sm-12">
                                                <button type="submit" class="btn btn-md btn-success" id="add">
                                                    <i class="icon-check2">Add Photo</i> 
                                                </button>
                                            </div>  
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="row match-height">
            
            @forelse($alumni_photos as $p)
                
                <div class="col-xl-3 col-md-6 col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-block">
                                <img class="card-img img-fluid mb-1" src="{{ route('asset', ['alumni_photos',$p->photo,290,217]) }}" alt="Card image cap">
                                <h4 class="card-title">{{ $p->name }}</h4>
                                <h6 class="card-subtitle text-muted">{{ $p->batch }}</h6>
                                <br>
                                <a href="{{ route('alumni_photos.edit',$p->id) }}" class="btn btn-outline btn-warning">Edit</a>
                                
                                <form action="{{ route('alumni_photos.destroy',$p->id) }}" style="display:inline" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE" >
                                    <button id='deletePhoto{{$p->id}}' type="button" class="btn btn-outline btn-danger">Delete</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <p class="font-medium-3 text-muted text-xs-center" style="margin:150px">No Photos Yet</p>
            @endforelse
            
        </div>
        <div class="text-xs-center mb-3">
            <nav aria-label="Page navigation">
                {{ $alumni_photos->links() }}   
            </nav>
        </div>
    </div>
</div>
@endsection

@section('js')
        
    <script type="text/javascript">
        

        $("[id*='deletePhoto']").click(function(e){
            var ele = this;
            e.preventDefault();

            swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
            if (result.value) {
                ele.form.submit();
            }
            });

        });
    </script>

@endsection