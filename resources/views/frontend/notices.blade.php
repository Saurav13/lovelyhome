@extends('layouts.app')
@section('body')
@section('title','| Notices')
<section id="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li><a href="/">Home</a></li>
                        <li class="active">Notices</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    
            <form id="contact_form" class="white-popup-block mfp-hide">
                <div class="register_block">
                    <button title="Close (Esc)" type="button" class="mfp-close">×</button>
                    <div class="register_form_top">
                        <div class="register_wave"></div>
                        <h2 class="section-title" id="noticetitle">Great Event Title!</h2>
                        <p class="section-description" id="noticedescription">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        <h4 style="float:left;">Lovely Home</h4>
                        <h4 style="float:right;" id="noticedate"></h4>
                    </div>
                </div>			
            </form>


    <section id="reviews_content" class="reviews_content" style="padding-top: 10px;">
        @foreach($notices->chunk(3) as $chunk)
        @foreach($chunk as $n)
        <div class="container">
            @if($loop->index==0)
                <div class="row review_comment comment_green">
                    <div class="comment_grid">
                        <div class="col-md-12 last_week_c reviews_block comment_grid_item">
                            <div class="author_block">
                                <img class="author_avatar" style="width: unset;height: unset;" src="{{asset('backend/systemimages/small-logo.png')}}" alt="Lovely Home">
                                <p class="author_name">Lovely Home</p>
                                <p class="author_description">Bring a smile to your child.</p>
                            </div>
                            <div class="comment_block">
                                <div class="comment_title">
                                    <p id="ntitle{{$n->id}}">{{$n->title}}</p>
                                </div>
                                <input type="hidden" value="{{date('j, M Y',strtotime($n->updated_at))}}" id="ndate{{$n->id}}"/> 
                                <input type="hidden" value="{{$n->detail}}" id="noticedetails{{$n->id}}"/>
                                <p style="min-height:72px;" id="noticedetails{{$n->id}}">{{str_limit(strip_tags($n->detail),300)}}</p>
                                @if(strlen($n->detail) >=300)
                                    <p><a class="b_white register-form" href="#contact_form" id="shownotice{{$n->id}}"><b>Show more</b></a></p>
                                   
                                @endif
                                <div class="comment_date">
                                    <p><i class="fa fa-calendar"></i> {{date('j, M Y',strtotime($n->updated_at))}}</p>
                                   
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
                @elseif($loop->index==1)
                <div class="row review_comment comment_red">
                    <div class="comment_grid">
                        <div class="col-md-12 month_c reviews_block comment_grid_item">
                            <div class="author_block">
                                        <img class="author_avatar" src="{{asset('backend/systemimages/small-logo.png')}}" alt="Lovely Home">
                                        <p class="author_name">Lovely Home</p>
                                        <p class="author_description">Bring a smile to your child.</p>
                            </div>
                            <div class="comment_block">
                                    <div class="comment_title">
                                        <p id="ntitle{{$n->id}}">{{$n->title}}</p>
                                    </div>
                                    <input type="hidden" value="{{date('j, M Y',strtotime($n->updated_at))}}" id="ndate{{$n->id}}"/>
                                    <input type="hidden"  value="{{$n->detail}}" id="noticedetails{{$n->id}}"/>
                                    <p style="min-height:72px;" >{{str_limit(strip_tags($n->detail),300)}}</p>
                                    @if(strlen($n->detail) >=300)
                                    
                                    <p><a class="b_white register-form" href="#contact_form" id="shownotice{{$n->id}}"><b>Show more</b></a></p>
                                    @endif
                                    <div class="comment_date">
                                            <p><i class="fa fa-calendar"></i> {{date('j, M Y',strtotime($n->updated_at))}}</p>
                                       
                                    </div> 
                                </div>
                        </div>
                    </div>
                </div>
                @elseif($loop->index==2)
                <div class="row review_comment comment_green">
                    <div class="comment_grid">
                        <div class="col-md-12 last_week_c reviews_block comment_grid_item">
                            <div class="author_block">
                                        <img class="author_avatar" src="{{asset('backend/systemimages/small-logo.png')}}" alt="Lovely Home">
                                        <p class="author_name">Lovely Home</p>
                                        <p class="author_description">Bring a smile to your child.</p>
                            </div>
                            <div class="comment_block">
                                    <div class="comment_title">
                                        <p id="ntitle{{$n->id}}">{{$n->title}}</p>
                                    </div>
                                    <input type="hidden" value="{{date('j, M Y',strtotime($n->updated_at))}}" id="ndate{{$n->id}}"/>
                                    <input type="hidden"  value="{{$n->detail}}" id="noticedetails{{$n->id}}"/>
                                    <p style="min-height:72px;" id="noticedetails{{$n->id}}">{{str_limit(strip_tags($n->detail),300)}}</p>
                                    @if(strlen($n->detail) >=300)
                                    <p><a class="b_white register-form" href="#contact_form" id="shownotice{{$n->id}}"><b>Show more</b></a></p>
                                    
                                    @endif
                                    <div class="comment_date">
                                            <p><i class="fa fa-calendar"></i> {{date('j, M Y',strtotime($n->updated_at))}}</p>
                                       
                                    </div> 
                                </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
            @endforeach
            @endforeach
                    <div class="text-center">
                          {{$notices->links()}}
                        </div>	
        </section>

        <section id="contact_us" class="contact_us">
            <div class="container">
            </div>
        </section>
      
@section('js')
<script>
    $(document).ready(function(){
        $(document).on('click', "[id*='shownotice']", function() {
            $('#noticetitle').empty();
            $('#noticedescription').empty();
            var id = $(this).attr("id").slice(10);
            var title=$('#ntitle'+id).html();
            var desc=$('#noticedetails'+id).val();
            var date=$('#ndate'+id).val();
            $('#noticetitle').html(title);
            $('#noticedescription').html(desc);
            $('#noticedate').html("Issued Date: "+date);
        });
    });
</script>
@endsection

@stop