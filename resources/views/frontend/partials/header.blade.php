<!DOCTYPE html>
<html lang="zxx">
<head>
	<meta charset="utf-8">
	<title>Lovely Home @yield('title') </title>
	<meta name="description" content="">

	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- Favicon and Apple Touch Icons -->
	<link rel="shortcut icon" href="{{asset('backend/systemimages/small-logo.ico')}}" type="image/x-icon">
	<link rel="apple-touch-icon" href="{{asset('backend/systemimages/small-logo.ico')}}">
	<link rel="apple-touch-icon" sizes="72x72" href="{{asset('backend/systemimages/small-logo.ico')}}">
	<link rel="apple-touch-icon" sizes="114x114" href="{{asset('backend/systemimages/small-logo.ico')}}">

	<!-- Stylesheets -->

	<!-- Bootstrap -->
	<link rel="stylesheet" href="/frontend-assets/css/bootstrap/bootstrap.min.css">

	<link rel="stylesheet" href="/frontend-assets/css/style.css?114">

	<link rel="stylesheet" href="/frontend-assets/css/media.css?037">
	<style>
			.pagination>li>a, .pagination>li>span {
				/* position: relative; */
				/* float: left; */
				/* padding: 6px 12px; */
				/* margin-left: -1px; */
				/* line-height: 1.42857143; */
				/* color: #337ab7; */
				/* text-decoration: none; */
				/* background-color: #fff; */
				/* border: 1px solid #ddd; */
				/* position: relative; */
				float: left;
				margin-right: 8px;
				color: #4b434d;
				text-decoration: none;
				background-color: #fff;
				border: 2px solid #e2e2e2;
				width: 40px;
				padding: 0 12px;
				line-height: 2;
				text-align: center;
				font-size: 1.8rem;
				border-radius: 50%;
				font-family: 'OpenSansSemiBold', sans-serif;
				-webkit-transition: all .2s linear;
				transition: all .2s linear;
			}
			.pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
				z-index: 3;
				color: #fff;
				cursor: default;
				background-color: #b70404c7;
				border-color: #b70404c7;
			}
	</style>


	<!--[if lt IE 9 ]>
<script src="/js/html5shiv-master/html5shiv.js" type="text/javascript"></script><meta content="no" http-equiv="imagetoolbar">
<![endif]-->

</head>
