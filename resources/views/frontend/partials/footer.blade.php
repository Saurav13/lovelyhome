<style>
    .quick-links{
        font-size: 11px;
        color: #ffffac;
    }
    .quick-links:hover{
        color: #7f8926;
    }
</style>

<footer>
    <div class="overlay_footer_land"></div>
    <div class="container">
        <div class="row pb_40">
            <div class="col-md-3 col-sm-12 col-xs-12 mb_15">
                <div class="footer_about">
                    <h3>Alumini</h3>
                    <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit <a href="{{ route('alumni') }}">here</a></p>
                    <br>
                    <div class="footer_social social_icon">
                        <h3>Social Links</h3>
                  
                        <ul>
                            <li><a href="https://www.facebook.com/lhcck" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://twitter.com/lhcck" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://instagram.com/lovelyhome_ofcl" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="https://www.linkedin.com/in/lhcck/" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12 mb_15">
                <h3>Quick Links</h3>
                <div class="footer_tweets">
                    <p><a href="/" class="quick-links">Home</a></p>
                    <p><a href="/events" class="quick-links">Events</a></p>
                    <p><a href="/download" class="quick-links">Downloads</a></p>
                    <p><a href="/calendar" class="quick-links">Calendar</a></p>
                    <p><a href="/gallery" class="quick-links">Gallery</a></p>
                    <p><a href="/notices" class="quick-links">Notices</a></p>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12 mb_15">
                <h3>Advertisements</h3>
                <div class="footer_instagram">
                    <ul>
                        @foreach($advertises as $a)
                            <li><a href="{{$a->link}}" target="_blank"><img src="{{asset('advertises-images'.'/'.$a->image)}}" alt=""></a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12 mb_15">
                <h3>Newsletter</h3>
                <div class="footer_newsletters">
                    <p id="newsletter_error" style="color: #CD3653;font-size: 1.5rem;font-weight: 600;"></p>
                    <div class="footer_mail">
                        <form method="post" id="contact_form_footer">
                            <div class="form-group">
                                <input id="email_footer" name="email" type="text" placeholder="Your email">
                            </div>	
                            <div id="messegeResultFooter" class="messegeResult">
                                <p>You have successfully subcribed to us!</p>
                                <button type="submit">Send</button>
                            </div>					
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container_fluent copywriting">
        <div class="row">
            <div class="col-md-12">
                <p>Copyright © 2018 Lovely Home. Powerd by  <a href="#" targer="_blank"><strong>inCube</strong></a></p>
            </div>
        </div>
    </div>
</footer>
<!-- Replace end-->
<!-- go to top -->
<a href="#home" class="scrollup"><span><i class="fa fa-angle-up"></i></span></a>
<!-- end go to top -->

<!-- JS -->
<script type="text/javascript" src="/frontend-assets/js/jquery/jquery.min.js"></script>
<script type="text/javascript" src="/frontend-assets/js/owl.carousel/owl.carousel.min.js"></script>
<script type="text/javascript" src="/frontend-assets/js/bootstrap/bootstrap.js"></script>
<script type="text/javascript" src="/frontend-assets/js/isotope/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="/frontend-assets/js/imagesloaded/imagesloaded.pkgd.js"></script>
<script type="text/javascript" src="/frontend-assets/js/magnific-popup/magnific-popup.min.js"></script>
<script type="text/javascript" src="/frontend-assets/js/parallax/parallax.min.js"></script>
<script type="text/javascript" src="/frontend-assets/js/page-scroll-to-id/jquery.malihu.PageScroll2id.js"></script>
<script type="text/javascript" src="/frontend-assets/js/wow/wow.min.js"></script>
<script type="text/javascript" src="/frontend-assets/js/object-fit-images/ofi.browser.js"></script>
<script type="text/javascript" src="/frontend-assets/js/count/jquery.countdown.min.js"></script>
<script type="text/javascript" src="/frontend-assets/js/count/jquery.countup.min.js"></script>
<script type="text/javascript" src="/frontend-assets/js/waypoints/jquery.waypoints.min.js"></script>
<script type="text/javascript" src="/frontend-assets/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/frontend-assets/js/slick/slick.min.js"></script>
<script type="text/javascript" src="/frontend-assets/js/validate/validate.js"></script>
<script type="text/javascript" src="/frontend-assets/js/sticky-kit-master/sticky-kit.min.js"></script>
<script type="text/javascript" src="/frontend-assets/js/slider-pro-master/jquery.sliderPro.min.js"></script>
<script type="text/javascript" src="/frontend-assets/js/functions.js?100"></script>
<script type="text/javascript" src="/frontend-assets/js/send.js?031"></script>
<script type="text/javascript" src="/frontend-assets/js/parallax-object/parallax.min.js"></script>
<script>
    var scene = document.getElementById('scene');
    var parallax = new Parallax(scene);
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCbXi2uWlIICzBfpjQ_xBVI6ae1M-cmBzg&callback=window.initMap"
async defer></script>
<!--End JS -->
<script>
    @if(Request::is('/'))
    function ScrollBlock(){
        $('html, body').animate({
        scrollTop: $(".lovelyhome-blocks").offset().top - 80
    }, 1000);
    }
    @else
    function ScrollBlock(){
        window.location.replace("{{URL::to('/')}}");
    }
    @endif
     $("#blockJawalakhel").click(function() {
        ScrollBlock();     
    });

$("#blockSanogaun").click(function() {
  ScrollBlock();
});
$("#blockBhanimandal").click(function() {
   ScrollBlock();
});
$("#blockDhobighat").click(function() {
   ScrollBlock();
});
$('#facilties-lovelyhome').click(function(){
    $('html, body').animate({
        scrollTop: $(".lovelyhome-facilties").offset().top + 150
    }, 1000);
});
$('#facilties-lovelyhome-mobile').click(function(){
    $('html, body').animate({
        scrollTop: $(".lovelyhome-facilties").offset().top + 150
    }, 1000);
});

</script>


@yield('js')


</body>
</html>
